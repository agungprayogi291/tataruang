  <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">DAFTAR KONSUMEN</h3>

                <div class="card-tools">
                   <!--<button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Tambah</button>-->
                     <button type="button" class="btn btn-success" id="tambah">
                        Tambah
                        </button>
                </div>
              </div>
                <div class="card-body">                  
                <div class="row">   
                    <div class="col-md-2">
                        <label for="nama">Nama Pelanggan</label>
                    </div> 
                    <div class="col-md-10">
                        <input type="text" class="form-control" name ="kunci" id="kunci" placeholder="masukkan nama untuk melakukan pencarian">
                    </div> 
                </div>
                </div>
                    
                    
                    
                  
                 <table id="t_tabel" class="table table-bordered table-hover">
                <thead>
                <tr>
                   
                    <th style="width:30px">No</th>
                    <th>Nama Pelanggan</th>
                    <th>Alamat</th>
                    <th>Telp</th>
                    <th>Tipe Member</th>
                    <th>Katerangan</th> 
                    <th style="width:50px">Aksi</th>  
                </tr>
                </thead>
                <tbody id="Tisi">   </tbody>
                 
              </table> 
                
                
                
              </div>
<!--               /.card-body 
              <div class="card-footer">
                Footer
              </div>
               /.card-footer-->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  
   
    
     
     <div class="modal fade" id="modal-tambah">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">TAMBAH DATA KONSUMEN</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

            <form class="form-row-seperated" action="#" name ="frm_konsumen" id="frm_konsumen" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="form-horizontal form-row-seperated">                    
            
                <input type="hidden" id="kode" name="kode">
            <div class="row">   
                <div class="col-md-12">
                
                <div class="form-group">
                  <label for="nama">Nama</label>
                  <input type="text" class="form-control" name ="nama" id="nama" placeholder="masukkan nama">
                </div>
                    
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan alamat">
                </div>
                    
                <div class="form-group">
                  <label for="telp">Telp</label>
                  <input type="text" class="form-control" name="telp" id="telp" placeholder="No Telp">
                </div>
                
                <div class="form-group">
                  <label for="tipe">Tipe Member</label>
<!--                  ?<input type="email" class="form-control" name ="tipe" id="tipe" placeholder="pILIH tIPE">-->
                  
                   <select class="form-control" name ="tipe" id="tipe" >
                    
                     <?php 
                     foreach ($tipe as $tip ){
                        echo ("<option>". $tip->nama ."</option>");
                     }
                     ?>
                    
                     
                    
                  </select>
                  
                  
                </div>
                    
                 
                    
                    
                    
                
                <div class="form-group">
                  <label for="ket">Keterangan</label>
                  <input type="email" class="form-control" name="ket"  id="ket" placeholder="keterangan">
                </div>
                
                
                </div>
                
<!--            <div class="col-md-6">
                kanan
            </div>-->
           
              
             
            </div>
    </form>     
                    
            <div class="modal-footer justify-content-between">
              <button type="button"   class="btn btn-default" data-dismiss="modal">Tutup</button>
              <button type="button"  id="simpan" class="btn btn-primary">Simpan</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
     </div>
      
      
      
      
      
      
      
 <script type="text/javascript">    
    
jQuery(document).ready(function() {
    TampilkanData(); 
    
    
    $("#simpan").click(function(event) {  
     SimpanKonsumen();
    });  
    $("#tambah").click(function(event) {  
     BukaTambah();
    });  
    $("#hapus").click(function(event) {  
     BukaHapus();
    });  
    $("#btn_hapus").click(function(event) {  
     HapusData();
    });  
});   

function BukaTambah(){
      $('#frm_konsumen').trigger("reset");
      $('#modal-tambah').modal('show');  
}
function BukaHapus(Kode){     
    $('#kdHapus').val(Kode);    
    $('#link').val('Tarif/hapus');    
    $('#modal-hapus').modal('show');  
}
function BukaEdit(Kode){    
               
               $.ajax({                   
                type: "POST", 
                url: 'Konsumen/buka',
                data:{kode:Kode}, 
                dataType:'json', 
                success: function(data) {         
// alert(data.hasildata.namapelanggan);
                          
                             $('#kode').val(data.hasildata.urutan);
                             $('#nama').val(data.hasildata.namapelanggan);
                             $('#alamat').val(data.hasildata.alamatpelanggan);
                             $('#telp').val(data.hasildata.notelp);
                             $('#tipe').val(data.hasildata.kategory);
                             $('#ket').val(data.hasildata.keterangan); 
                             $('#modal-tambah').modal('show');  
                             
//                        }else{    
//                            alert("Data tidak ditemukan");
//                        }
                    }
                    }); 
                    
      
}

function SimpanKonsumen(event){
            //event.preventDefault();   
           // document.getElementById("hasil_data").style.display = "none";
           // document.getElementById("isipesanX").style.display = "none"; ; 
            var formX = document.getElementById('frm_konsumen');
            formData = new FormData(formX);   
               $.ajax({                    
                    type: "POST", 
                    url: '<?php  echo base_url(); ?>Konsumen/simpan',
                    data: formData,  
                    contentType: false,
                    cache: false,             
                    processData:false,  
                    dataType:'json', 
                    success: function(data) {  
                        var row=data.hasildata; 
                        if(data.hasil!="1"){  
                            $('#modal-tambah').modal('hide');
                            TampilkanData();                                 
                        }else{    
                            PesanError(data.pesan); 
                        }
                    }
                    }); 
} 

function TampilkanData(){   

$('#t_tabel').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": '<?php  echo base_url(); ?>Konsumen/tampil_data'
        "columns": [
            { "data": "name" },
            { "data": "position" },
            { "data": "office" },
            { "data": "extn" },
            { "data": "start_date" },
            { "data": "salary" }
        ]
    } );
    
    
//     $.ajax({                   
//                type: "POST", 
//                url: '<?php  echo base_url(); ?>Konsumen/tampil',                 
//                dataType:'json', 
//                success: function(data) { 
//                
//                 var HasilPencarian;
//                     for (var i = 0; i < data.hasildata.length; i++){  
////                            var Biaya = FormatUang(data.hasildata[i].biaya);
//                           HasilPencarian+="<tr>";
//                           HasilPencarian+="<td>"+ (i+1) +"</td>";                            
//                           HasilPencarian+="<td>"+ data.hasildata[i].kode +' -> '+ data.hasildata[i].namapelanggan +"</td>";                            
//                           HasilPencarian+="<td>"+ data.hasildata[i].alamatpelanggan +"</td>";                            
//                           HasilPencarian+="<td>"+ data.hasildata[i].notelp +"</td>";                            
//                           HasilPencarian+="<td>"+ data.hasildata[i].kategory +"</td>";                            
//                         
//                           HasilPencarian+="<td>"+data.hasildata[i].keterangan +"</td>"; 
//                           
//                           
//                          
//                           
//                           
//                            HasilPencarian+="<td>";                              
//                                HasilPencarian+="<span class='tag tag-danger'>";
//                                    HasilPencarian+="<button type='button' class='btn btn-block btn-success btn-xs' onclick='BukaEdit("+ data.hasildata[i].urutan+");' >Edit</button>";
//                                    HasilPencarian+="<button type='button' class='btn btn-block btn-danger btn-xs' onclick='BukaHapus("+ data.hasildata[i].urutan+");' >Hapus</button>";
//                                HasilPencarian+="</span>";                            
//                                                                
//                           HasilPencarian+="</td>";     
//                           
//                           
//                           HasilPencarian+="</tr>";  
//                     } 
//                     
//                     if ( data.hasildata.length==0){                         
//                          HasilPencarian="<tr class='warning'> <td colspan=5>Data tidak ditemukan</td> </tr>";
//                     } 
//                       $('#Tisi').html(HasilPencarian); 
//                       
//                 }    
//    });  
}

function HapusData(){
 $('#modal-hapus').modal('hide'); 
var Kode = $("#kdHapus").val();
 
 $.ajax({                   
                type: "POST", 
                url: 'Konsumen/hapus',
                data:{kode:Kode}, 
                dataType:'json', 
                success: function(data) {         
                     
                        if (data['hasil']=="1"){
                            TampilkanData();
                        }else{
                            alert(data.pesan);
                        }
                          
 
                    }
                    }); 
    

}




 </script>   