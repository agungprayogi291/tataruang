<style>
        .dropbtn {
                background-color: #4CAF50;
                color: white;
                padding: 16px;
                font-size: 16px;
                border: none;
        }

        /* The container <div> - needed to position the dropdown content */
        .dropdown {
                position: relative;
                display: inline-block;
        }

        /* Dropdown Content (Hidden by Default) */
        .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f1f1f1;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
                z-index: 1;
        }

        /* Links inside the dropdown */
        .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
        }

        /* Change color of dropdown links on hover */
        .dropdown-content a:hover {
                background-color: #ddd;
        }

        /* Show the dropdown menu on hover */
        .dropdown:hover .dropdown-content {
                display: block;
        }

        /* Change the background color of the dropdown button when the dropdown content is shown */
        .dropdown:hover .dropbtn {
                background-color: #3e8e41;
        }
</style>
<nav id="nav">
        <ul>
                <li class="<?php echo($beranda);?>"><a href="<?= base_url();?>">Home<br>Beranda</a></li>
                <li class="<?php echo($TR);?>"><a href="<?= base_url() ;?>tataruang">Tata Ruang<br>Kota Magelang</a></li>
                <li class="<?php echo($IMB);?>"><a href="<?= base_url() ;?>imb">IMB <br>SKRK</a></li>
                <li class="<?php echo($SS);?>"><a href="<?= base_url() ;?>imb-statistic">Statistik<br>IMB</a></li>

                <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Form <br>Permohonan</a>
                        <ul>
                                <li ><a href="<?= base_url();?>form-surat-irk" class="hvr-underline-reveal">IRK</a> </li>
                                <li ><a href="<?= base_url();?>form-surat-pkkpr" class="hvr-underline-reveal">KRP</a> </li>
                                <li ><a href="<?= base_url();?>form-surat-pbg" class="hvr-underline-reveal">Rekomtek</a> </li>
                                <li ><a href="<?= base_url();?>cari-surat" class="hvr-underline-reveal"> Penelusuran Permohonan</a> </li>
                                <li><a href="<?= base_url() ;?>jadi-gampang/utama.php">Jadi Gampang</a></li>
                        </ul>
                </li>



                <?php  if (isset($_SESSION["logged"])){  ?>

                <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">User <br> Administrator</a>
                        <ul>
                                <li ><a href="<?= base_url();?>setting/simbologi" class="hvr-underline-reveal">Master Peta</a> </li>
                                <li ><a href="<?= base_url();?>slider" class="hvr-underline-reveal">Slider Beranda</a> </li>
                                
                                <?php if($this->session->userdata('user_level') > 0) :?>
                                <li><a href="<?= base_url() ;?>tambah-pengguna" class="hvr-underline-reveal">Tambah User</a> </li>
                                <?php endif ;?>
                                <li><a href="<?= base_url();?>setting-account" class="hvr-underline-reveal">Edit Password</a> </li>
                                 
                        </ul>
                </li>   
                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Daftar <br>Permohonan</a>
                                <ul>
                                        <li ><a href="<?= base_url();?>daftar-surat-irk" class="hvr-underline-reveal">IRK</a> </li>
                                        <li ><a href="<?= base_url();?>daftar-permohonan-pkkpr" class="hvr-underline-reveal"> KPR</a> </li>
                                        <li ><a href="<?= base_url();?>daftar-permohonan-pbg" class="hvr-underline-reveal"> Rekomtek</a> </li>
                                        
                                </ul>
                </li>
                        
<!--                <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pengaturan</a>
                        <ul>
                                <li ><a href="<?= base_url();?>setting/simbologi" class="hvr-underline-reveal">Simbologi</a> </li>
                                <li ><a href="<?= base_url();?>slider" class="hvr-underline-reveal">Slider Beranda</a> </li>
                                
                        </ul>
                </li>-->
                <li class=""><a href="<?= base_url() ;?>logout">Logout<br> </a></li>

                <?php } $SudahLogin=true; ?>

        </ul>


</nav>