

 <section id="bannerheader2">  
                   Data & Informasi Geospasial Pemerintah Kota Magelang 
 </section>
			<!-- Highlights -->
				<section class="wrapper style1">
					<div class="container">
						<div class="row 200%">
							<section class="4u 12u(narrower)">
								<div class="box highlight"> 
<!--                                                                    <i class="icon major fa-map-marker"></i>-->
                                                                    <i class="icon major fa-square-o"></i>
                                                                        
                                                                        
									<h4>Tata Ruang Kota Magelang</h4>
									<p>Rencana Tata Ruang Wilayah Kota atau RTRW Kota adalah arahan kebijakan dan strategi pemanfaatan ruang wilayah kota.

Peraturan Menteri Pekerjaan Umum (PerMen PU) No.17/PRT/M/2009 tentang Pedoman Penyusunan Rencana Tata Ruang Wilayah Kota merupakan tindak lanjut dari pelaksanaan ketentuan Pasal 18 ayat (3) Undang-Undang Nomor 26 tahun 2007 tentang Penataan Ruang.</p>
								</div>
							</section>
							<section class="4u 12u(narrower)">
								<div class="box highlight">
									<i class="icon major fa-map-marker"></i>
									<h4>IMB Dan SKRK</h4>
									<p>Produk hukum yang berisi persetujuan atau perizinan yang dikeluarkan oleh Kepala Daerah Setempat (Pemerintah kabupaten / kota) dan wajib dimiliki / diurus pemilik bangunan yang ingin membangun, merobohkan, menambah / mengurangi luas, ataupun merenovasi suatu bangunan</p>
								</div>
							</section>
							<section class="4u 12u(narrower)">
								<div class="box highlight">
									<i class="icon major fa-user"></i>
                                                                        <h4>Jadi Gampang</h4>
									<p> [Jaringan Penyedia Layanan Perizinan dan Penyampaian Gagasan Masyarakat tentang Penataan Ruang]
Memudahkan masyarakat untuk mengakses informasi, mendapatkan pelayanan perizinan dan melaporkan pelanggaran tata ruang di mana saja (anywhere), kapan saja (anytime) melalui desktop maupun smartphone</p>
								</div>
							</section>
						</div>
					</div>
				</section> 
			<!-- Footer -->
				<div id="footer">
					<div class="container">

					</div>

					 

					<!-- Copyright -->
						<div class="copyright">
<!--							<ul class="menu">-->
								&copy; COPYRIGHT 2018 || Data & Informasi Geospasial Pemerintah Kota Magelang  
                                                                <?php  $linkLogin=base_url().'login'; ?>
																<?php if(!isset($_SESSION['logged'])) :?>
                                                                  <a href="<?php echo $linkLogin; ?>">&reg;</a>
																  <?php endif ;?>
<!--							</ul>-->
						</div>

				</div>

		</div>



	</body>
</html>

