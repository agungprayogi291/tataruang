<!DOCTYPE HTML> 
<html>
<head>
    <title>SIG Kota Magelang </title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" /> 
    <?php $BaseMapPeta=$baseMap; ?>
    <link rel="icon" type="image/png" href="<?= base_url() ;?>assets/css/logo.png"> 
    <link rel="stylesheet" href="<?= base_url() ;?>assets/css/main.css" /> 
    <link href="<?= base_url() ;?>assets/slider/4/js-image-slider.css" rel="stylesheet" type="text/css" />    
    <link href="<?= base_url() ;?>assets/slider/generic.css" rel="stylesheet" type="text/css" />  
    <link rel="stylesheet" href="<?= base_url() ;?>assets/plugin/leaflet/lib/jquery/jquery-ui.css">       
    <link rel="stylesheet" href="<?= base_url() ;?>assets/plugin/leaflet/lib/leaflet/leaflet.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugin/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link href="<?php echo base_url(); ?>assets/plugin/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ;?>assets/css/bootstrap.min.css" /> 
    <?php   if ($home) {  $BaseMapPeta=false;  ?>
         
        <link rel='stylesheet' id='css1-css'  href='<?= base_url() ;?>assets/slider/css/skeleton.css?ver=3.5.2' type='text/css' media='all' />  
        <link rel='stylesheet' id='css5-css'  href='<?= base_url() ;?>assets/slider/css/camera.css?ver=3.5.2' type='text/css' media='all' />  
        <link rel='stylesheet' id='css12-css'  href='<?= base_url() ;?>assets/slider/css/shortcodes.css?ver=3.5.2' type='text/css' media='all' />
    <?php } ?> 
    <?php   if($this->uri->segment(1) == "tataruang" || $this->uri->segment(2) == 'simbologi'){  ?>  
        <link href="<?php echo base_url(); ?>assets/plugin/dynatree/src/skin/ui.dynatree.css" rel="stylesheet" type="text/css">
    <?php } ?>
    <script src="<?php echo base_url(); ?>assets/plugin/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugin/bootstrap/js/src/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugin/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugin/select2/dist/js/select2.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ;?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url() ;?>assets/js/skel.min.js"></script>
    <script src="<?= base_url() ;?>assets/js/util.js"></script> 
    <script src="<?= base_url() ;?>assets/js/main.js"></script> 
    <script src="<?= base_url() ;?>assets/js/jquery.dropotron.min.js"></script> 
    <script src="<?= base_url() ;?>assets/js/bootstrap.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.3.0/sweetalert2.all.js" integrity="sha512-+gmGvjxNh87FkVPTosMbuFDIz8BGmabm7/IM9hp2rMdoy7A8dOEuaEugTVN1EnB9UC/iky10CfhRB3UqWQaN2w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <?php   if ($BaseMapPeta) {  ?>
        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
        <script type="text/javascript" src="<?= base_url() ;?>assets/plugin/leaflet/lib/jquery/jquery.js"></script>
        <script type="text/javascript" src="<?= base_url() ;?>assets/plugin/leaflet/lib/jquery/jquery-ui.js"></script>
        <script type="text/javascript" src="<?= base_url() ;?>ajax/peta.js"></script>
        <script type="text/javascript" src="<?= base_url() ;?>ajax/base_maps.js"></script>         
    <?php    } ?>  
   <?php if($this->uri->segment(1) == "tataruang" || $this->uri->segment(2) == 'simbologi'){ ;?>
        <script src="<?php echo base_url(); ?>assets/plugin/dynatree/jquery/jquery.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/plugin/dynatree/jquery/jquery-ui.custom.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/plugin/dynatree/jquery/jquery.cookie.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/plugin/dynatree/src/jquery.dynatree.js" type="text/javascript"></script>       
    <?php  } ;?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>   
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
  
  
   <script type="text/javascript">
   var TidakLoading =false;
   <?php echo "var URLE='". base_url()."';"; ?>                             
    <?php    echo "const MainLangLat = [".$this->session->userdata('app_lang').",".$this->session->userdata('app_lat')."];"; ?>
   </script>     
        
        <style>
    .legenda{
        position: absolute;
        top:125px;
        margin-left: 10px;
        margin-right: 10px;
        padding: 5px;
        /*//bottom: 0;*/
        right: 0;
        width: 300px;     
        background-color: white;
        font-size: 9pt;
        opacity: 80%;
        z-index: 999;
    }
    .legendatree{
        opacity: 85%;
        position: absolute;
        top:205px;
        margin-left: 10px;
        margin-right: 10px;
        padding: 10px;
        /*//bottom: 0;*/
        right: 0;
        width: 300px;
        min-height: 90%px;
        max-height: 470px;
        background-color: white;
        font-size: 9pt;    
        overflow-x:auto;
        z-index: 999;
        /*overflow-y: auto;*/
    }
    .legendaKiriAtas{ 
        position: absolute;
        top:198px;
        left: 2px;
        width: 220px; 
        margin-left: 10px; 
        padding: 5px;  
        background-color: #FAE198;
        font-size: 9pt;
        overflow-x:auto;
        min-height: 90%px;
        max-height: 420px;
        z-index: 999;
        /*display: none;*/ 
    }
    .legendaKiri{
        position: absolute;
        bottom:16px;
        left: 5px;
        width: 220px; 
        margin-left: 10px;
        /*//margin-right: 10px;*/
        padding: 5px; 
        background-color: #FAE198;
        font-size: 8pt;
        /*display: none;*/ 
    }
    .loading{
        position: absolute;
        top:200px;
        margin-left: 50%;
        margin-right:50%;
        padding: 10px; 
        right: 0;
        width: 230px;
        background-color: yellow;
        font-size: 15pt;
        color: blue;
        height: 50px;
        text-align: center;
        -moz-border-radius: 15px;
        border-radius: 15px;
        z-index: 999;  
    }
</style> 
<!-- <style>
        .legenda{
            position: absolute;
        top:20%;
            margin-left: 10px;
            margin-right: 10px;
            padding: 10px;
            /* //bottom: 0; */
            right: 0;
            width: 220px;
            background-color: white;
            font-size: 9pt;
        }

        .legendatree{
        opacity: 85%;
        position: absolute;
        top:205px;
        margin-left: 10px;
        margin-right: 10px;
        padding: 10px;
        /*//bottom: 0;*/
        right: 0;
        width: 300px;
        min-height: 90%px;
        max-height: 470px;
        background-color: white;
        font-size: 9pt;    
    overflow-x:auto;
        z-index: 999;
        /*overflow-y: auto;*/
    }
    
    .legendaKiri{
        
    position: absolute;
    bottom:-8%;
    left: 3%;
    width: 220px;
        
        margin-left: 10px;
        margin-right: 10px;
        padding: 5px; 
        
        background-color: #FAE198;
        font-size: 8pt;
        display: none; 
    }
    .legendaKiriAtas{ 
        position: absolute;
        top:190px;
        left: 2px;
        width: 220px; 
        margin-left: 10px; 
        padding: 5px;  
        background-color: #FAE198;
        font-size: 9pt;
        display: none; 
    }
    .loading{
        position: absolute;
        top:200px;
        margin-left: 40%;
        margin-right:50%;
        padding: 10px; 
        right: 0;
        width: 230px;
        background-color: yellow;
        font-size: 20pt;
        color: blue;
        height: 60px;
        text-align: center;
        -moz-border-radius: 15px;
    border-radius: 15px;
        
    }


    /* breakpoint dekstop */
    @media (min-width: 768px) {
    .legenda {
        top: 35%;
    }
    .legendakiri{
        top:35%;
    }
    }
</style> -->
<?php 
  $Terselect="current";  
        $TR='';  
        $IMB='';  
        $JJ='';  
        $SS='';
        $DF='';
        $JG='';
        $beranda='';            
                    
  if (!isset($_GET['page'])) {    
                    $beranda=$Terselect;
		} else {
                    if ($_GET['page']=="tr") { $TR=$Terselect; }
                    if ($_GET['page']=="imb"){$IMB=$Terselect; }  
                    if ($_GET['page']=="jj") {$JJ=$Terselect; } 
                    if ($_GET['page']=="ss") {$SS=$Terselect; }
                    if ($_GET['page']=="df") {$DF=$Terselect; }
                    if ($_GET['page']=="u_d") {$UD=$Terselect; }
                    if ($_GET['page']=="skrk_a"){$IMB=$Terselect; }  
                }  
                
                
                
                
function getBaseUrl() 
{
    // output: /myproject/index.php
    $currentPath = $_SERVER['PHP_SELF']; 
    
    // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index ) 
    $pathInfo = pathinfo($currentPath); 
    
    // output: localhost
    $hostName = $_SERVER['HTTP_HOST']; 
    
    // output: http://
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
    
    // return: http://localhost/myproject/
    return $protocol.$hostName.$pathInfo['dirname']."/";
}


?>