

<!-- Modal -->
<div class="modal fade" id="modal-ajukan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pengajuan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url() ;?>Pengajuan" class="text-center" method="post" target="_blank">
      <div class="modal-body">
      
          <div class="form-check">
                    <input type="text" name="ajukan-lang">
                    <input type="text" name="ajukan-lat">
                    <label for="">Jenis : </label>
                    <br>
                    <input class="form-check-input" type="radio" name="jenis"id="irk-cek" value="1" checked>
                    <label class="form-check-label" for="irk-cek">
                        <span class="mx-4">Permohonan IRK</span>
                    </label>
                    <input class="form-check-input" type="radio" name="jenis" id="kkrp-cek" value="2" >
                    <label class="form-check-label" for="kkrp-cek">
                        <span class="mx-4">Permohonan KKRP</span>
                    </label>
                    <input class="form-check-input" type="radio" name="jenis"id="rekomtek-cek" value="3" >
                    <label class="form-check-label" for="rekomtek-cek">
                        <span class="mx-4">Permohonan Rekomtek</span>
                    </label>
                </div>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Ajukan</button>
      </div>
      </form>
    </div>
  </div>
</div>

 
<!-- Modal -->
<div class="modal fade" id="statusPermohonan" tabindex="-1" role="dialog" aria-labelledby="statusTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Status Permohonan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-4">
        <form class="form" action="" id="form-status">
            <input type="hidden" name="id_permohonan" id="id_permohonan">
            <div class="form-group">
                <label for="tanggal_status">tanggal</label>
                <input id="tanggal_status" name="tanggal_status"type="date" class="form-control" >
            </div>
            <div class="form-group">
              <label for="level_status">Status</label>
                <select name="level_status" id="level_status" class="form-control form-control-lg">
                  <option selected>-- Pilih Status ----</option>
                  <option value="1">proses</option>
                  <option value="2">selesai</option>
                </select>
            </div>
            <div class="form-group">
              <label for="keterangan_status">Keterangan</label>
              <input type="text" name="keterangan_status" id="keterangan_status" class="form-control form-control-lg" placeholder="masukan keterangan ">
            </div>
        </form> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary"  id="close_status">Close</button>
        <button type="submit" id="btn_status" class="btn btn-primary">Update</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="loading" role="dialog">
    <div class="modal-dialog modal-small"> 
            <div class='modal-content loading'>
                     Loading . . .
                     
            </div>
    </div>
</div>


<!-- <div id="loading" class="loading modal">Loading . . . </div> -->

<div class="loading collapse" id='msg'>Loading</div>
 <section id="bannerheader2" style="margin-bottom:-10%;">  
                    
                  &copy; COPYRIGHT 2018 || Data & Informasi Geospasial Pemerintah Kota Magelang  
                    <?php  $linkLogin='login'; ?>
                    <?php if(!isset($_SESSION['logged'])) :?>
                      <a href="<?php echo $linkLogin; ?>">&reg;</a>
                    <?php endif;?>
 </section> 
</div>
	</body>
</html>

 
<script type="text/javascript"> 
    $( document ).ajaxStart(function() {
        $("#msg").removeClass('collapse');
    });
    $( document ).ajaxStop(function() {
      $("#msg").addClass('collapse');
    });

    $(document).ajaxStart(function(){  
             // $('#loading').modal({backdrop: false});
             if(!TidakLoading){ 
               
              // $('#loading').modal('show');
          }
        
    });     
    $(document).ajaxStop(function(){
       if(!TidakLoading){ 
          //  $('#loading').modal("hide");
            $("body").css("margin-right","0")
          }
    });
    $(document).ready(function(){
     
      $("#statusPermohonan").on('hide.bs.modal', function(){
        setTimeout(() => {
          $("body").css("padding-right","0")
        }, 1000);
      
      
        // alert("A")
      });
      $("#close_status").click(function(){
        $("#statusPermohonan").modal("hide")
        $("body").css("padding-right","0")
      })
      
    })

    function TampilkanpesanX(element, pesan, Err = 0) {
    element.removeClass('alert-warning');
    element.removeClass('alert-danger');
    element.removeClass('alert-info');
    if (pesan == '') {
      element.fadeIn();
    } else {
      if (pesan.toLowerCase() == 'loading') {
        element.toggleClass('alert-warning');
        var IPesan = '<span class="spinner-border spinner-border-sm ml-1" role="status" aria-hidden="true"></span> &nbsp;&nbsp;&nbsp;';
        IPesan += 'Loading...';
        element.html(IPesan);
        element.fadeIn(4500);
      } else {
        if (Err == 0) {
          element.toggleClass('alert-danger');
          element.html(pesan);          
          //          element.removeClass('collapse');          
//          element.fadeIn(800).delay(4500).fadeOut();
          element.fadeIn(800);
        } else {
          element.toggleClass('alert-info');
          element.html(pesan);
          element.fadeIn(1500).delay(4500).fadeOut();
        }
      }
    }
  }
   

    //$(body).css("padding-right","0")


    function inisialTabel(tableT) {
        tableT.dataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": true,
            "scrollX" : true
        });
    }

    function RefreshTable(tableT, IsiT, dataT, baris = 2) {
        if (baris > 1) {
        tableT.DataTable().destroy();
        } else {
        tableT.dataTable().fnClearTable();
        }
        IsiT.html(dataT);
        if (baris > 1) {
        inisialTabel(tableT);
        }
    }
    function setAlert(clas,message,elm){
        elm.removeClass().addClass(clas).text(message)
    }

    function IsiDataTabelKosong(colspan){
      var data=  "<tr class='text-center'><td colspan='"+colspan+"'>Tidak ada data</td></tr>"
      return data;
    }
</script> 
 
  