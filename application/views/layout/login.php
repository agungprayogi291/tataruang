<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title><?php echo $this->config->item('app_name');?></title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/<?php // echo $this->config->item('color');?>.min.css">
    <!--[if lt IE 9]>
        <script src="<?php echo base_url();?>assets/html5shiv.min.js"></script>
        <script src="<?php echo base_url();?>assets/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">

	<div class="login-box">
		<div class="login-logo">
			<a href="#"><b><?php echo $this->config->item('singkatan');?></b> LOGIN</a>
		</div>
		<div class="login-box-body">
			<p class="login-box-msg"><?php echo $this->config->item('app_name');?></p>
			<?php echo validation_errors(); ?>
			<?php echo $this->session->flashdata('msg');?>
			<form action="<?php echo base_url();?>login/masuk" method="post">
	          <div class="form-group has-feedback">
	            <input type="text" class="form-control" placeholder="Username" required name="username">
	            <span class="glyphicon glyphicon-user form-control-feedback"></span>
	          </div>
	          <div class="form-group has-feedback">
	            <input type="password" class="form-control" placeholder="Password" required name="password">
	            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	          </div>
	          <div class="row">
				<div class="col-xs-4">
					<button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
				</div>
	          </div>	          
			</form>
		</div>
		<p class="text-center" style="margin-top:20px;"><small>&copy; <?php echo date('Y');?> <?php echo $this->config->item('author');?></small></p>
	</div>

    <script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/dist/js/app.min.js"></script>
</body>
</html>