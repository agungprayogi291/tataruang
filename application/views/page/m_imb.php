<div id="map" style="height:30%;"></div>
<div id="panel"class="legenda"> 
     <input type="radio" name="basemaps" value="0" checked="true" onchange="clickbasemaps();">Citra Satelit<br>
    <input type="radio" name="basemaps" value="1" onchange="clickbasemaps();">Google Satellite<br>
    <input type="radio" name="basemaps" value="2" onchange="clickbasemaps();">Google Hybrid<br>
    <input type="radio" name="basemaps" value="3" onchange="clickbasemaps();">Google Streets<br>
    <input type="radio" name="basemaps" value="4" onchange="clickbasemaps();">Google Terrain<br>
    <input type="radio" name="basemaps" value="5" onchange="clickbasemaps();">OpenStreetMap<br>
    
    <hr>
    <strong>Referensi Peta Dasar</strong> <br>
    <input type="checkbox" name="adm" id="adm" onchange="clickadministrasi('1');">Batas Adm. Kabupaten/Kota<br> 
    <input type="checkbox" name="kec" id="kec" onchange="clickadministrasi('2');">Batas Adm. Kecamatan<br> 
    <input type="checkbox" name="kel" id="kel" onchange="clickadministrasi('3');">Batas Adm. Desa/Kelurahan<br>  
    <hr>
    <strong>IMB dan SKRK</strong>  <br>
    <input type="checkbox" name="imb1" id="imb1" onclick="ClickIMB('1');">KEC. MAGELANG UTARA<br> 
    <input type="checkbox" name="imb2" id="imb2" onclick="ClickIMB('2');">KEC. MAGELANG TENGAH<br> 
    <input type="checkbox" name="imb3" id="imb3" onclick="ClickIMB('3');">KEC. MAGELANG SELATAN<br>  
    <hr> 
    <!--   <input type="button" class="form-control" value="Pencarian">
   <button type="button"  class="btn btn-warning">Pencarian</button>
     <hr>-->
     
    
    <input type="checkbox" name="skrk" id="skrk" onclick="ClickSKRK();">SKRK 
    <?php 
    if (isset($_SESSION["logged"])){
    ?>
 <!--   <hr>
    <input type="checkbox" name="baru" id="baru">Perizinan Baru<br> -->
    <?php } ?>
<!--    <input type="checkbox" name="baru" id="baru" onclick="ClickBaru();">Perizinan Baru<br> -->
    <hr>
    <input class="form-check-input"type="checkbox"  id="ckPoint">
    <label class="form-check-label"   style="margin-left:8%;"  for="ckPoint">Titik Point Pada Peta</label>
        
   
</div>

<?php  include_once 'm_imb_tambah.php';  ?>
<style>
    legenda{
        width: 5px;
        height:3px; 
        padding:  5px;
        margin:  5px; 
            
    }    
    .merah {  background-color: #f50733;}
    .kuning {  background-color:#fbf306;}
    .hijau {  background-color: #15f363;}
    .biru{  background-color: #0c33f7;}

</style>

  
<div id="panel_cari"class="legendaKiriAtas"> 
 
<!--     <strong>  Dasar Pencarian </strong><br>
     <form action="#" name ="frm_cari_imb" id="frm_cari_imb" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="form-horizontal form-row-seperated">    
            <select id="dasar" name="dasar" onchange="TampilForm();"class="form-control">  
                                    <option value="1">Nama Pemohon</option> 
                                    <option value="2">Alamat</option> 
                                    <option value="3">No SK</option> 
                                    <option value="4">Koordinat</option>  
              </select> <br>
              <div id="f_langlat" style="display:none;"> 
               <input type="text" id="x" name="x" class="form-control" placeholder="Titik X"> 
               <input type="text" id="y" name="y" class="form-control" placeholder="Titik Y"> <br>
              </div>

              <input type="text" id="kunci" name="kunci" class="form-control" placeholder="kunci pencarian"> <br> 
              <button type="button"  onclick="CariIMB();"  class="btn btn-warning">Cari</button>
      </form>-->
</div>



<div id="panel2"class="legendaKiri"> 
    <legenda class='merah'></legenda>Tidak IMB<p>
    <legenda class='hijau'></legenda>IMB<p> 
    <legenda class='biru'></legenda>IMB BARU<p> 
</div>

<script type="text/javascript" src="<?= base_url();?>ajax/imb_js.js"></script>

<script> 
const MainZoom =12;
    const arrPoint = []; 
    var map, autocomplete = [];
    var LayerPeta; 
    var opct;
    var simbologi; 
    var LayerPoint;
    var LayerGeolistrik;
    var newMarker;
    var MarkerG=[];
    var CheckPeta=false;
    var lang,lat;
    $(document).ready(function(e){
        map.on('click', onMapClick)
    })
    
    $('#ckPoint').on('change', function() {
        if ( $('#ckPoint').is(':checked') ) {  
            PointCheck(1);   
        }else{           
            PointCheck(0);                        
        }
    });

    function PointCheck(cek){    
        if (cek==0){
            $('#frprose').addClass('collapse'); 
            $('#panel_btn').addClass('collapse');
            $('#t_lang').val('');
            $('#t_lat').val('');
            if(map.hasLayer(newMarker)){ map.removeLayer(newMarker);}        
            NgeplayOut();
        }
        CheckPeta=cek;
    }

    function NgeplayOut(){               
        map.flyTo([-7.4764243720274335,110.21827958618167],MainZoom, {
        animate: true,
        duration: 2, // in seconds
        essential: true  
        }); 
    }

    function onMapClick(e) {            
        if (CheckPeta==false) { return false;      }
        lang =e.latlng.lng
        lat =e.latlng.lat
        var info = "Lang: " + e.latlng.lat + " \nLat : " + e.latlng.lng +"<br><button type='button' class='btn btn-primary my-2' onclick='ajukan()' data-toggle='modal' data-target='#modal-ajukan'>Ajukan</button>";  
        addMarker(lat, lang, info);
        $('#t_lang').val(lang);
        $('#t_lat').val(lat);      
        $('#frprose').removeClass('collapse');
        $('#panel_btn').addClass('collapse');
    }

    function addMarker(lang,lat,info=''){
        var icone = L.icon({
            iconUrl: '<?= base_url() ;?>assets/image/icons_map/default.png',
            iconSize:     [50, 55],

        });      
        if(map.hasLayer(newMarker)){ map.removeLayer(newMarker);} 
        newMarker = new L.marker([lang,lat],{icon:icone}).addTo(map); 
        
        map.flyTo([lang,lat],17, {
                animate: true,
                duration: 2, // in seconds
                essential: true  
            }); 
        if (info!=''){ newMarker.bindPopup(info);}
    
    }

    function ajukan(){
        
            
            $("[name='ajukan-lang']").val(lang)
            $("[name='ajukan-lat']").val(lat)  
        
       
    }

    function TampilForm(){
        var nilai =$("#dasar").val(); 
        if (nilai=="4"){
            document.getElementById("f_langlat").style.display = "block"; 
            document.getElementById("kunci").style.display = "none"; 
        }else{
            document.getElementById("f_langlat").style.display = "none"; 
            document.getElementById("kunci").style.display = "block"; 
        }
    }
</script>

