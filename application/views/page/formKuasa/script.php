<script type="text/javascript">
    jQuery(document).ready(function() {
     
        $("input[type=radio][name='bertindakUntuk']").change(function() {
            if (this.value == '1') {
                $('#bertindakAtasNama').addClass('d-none')
                $('#bertindakAtasNama').val('')

                if ($('#namaPemohon').val() == '') {
                    alert("Mohon isi nama pemohon")
                    this.checked = false;
                } else {
                    $('#bertindakAtasNama').val($('#namaPemohon').val())
                }
            } else {
                $('#bertindakAtasNama').removeClass('d-none')
                $('#bertindakAtasNama').val('')
            }
        })


        $('#selectRencana').change(function() {
            $('#rencanaLain').val('')
            if (this.value == 'lainnya') {
                $('#rencanaLain').removeClass('d-none')
            } else {
                $('#rencanaLain').addClass('d-none')
            }
        })

        $('#formIRK').submit(function(e) {
            e.preventDefault();

            let data = $('#formIRK').serializeArray();

            $.ajax({
                url: '<?= base_url('FormSuratKuasa/simpan') ?>',
                data: data,
                type: 'POST',
                dataType: 'JSON',
                success: function(response) {
                    let html ="";
                    if(response.success == 1){
                        Swal.fire(
                            'Good Job!',
                            `${response.message}`,
                            'success'
                        ).then(e=>{
                            for (const prop in response.data) {
                                html += `<input type="text" name="${prop}" value="${response.data[prop]}">`
                            }

                            $("#formCetak").html(html);
                            $("#formCetak").trigger('submit');
                            $("#formIRK")[0].reset();
                        })
                    }else{
                        Swal.fire(
                            'warning!',
                            `${response.message}`,
                            'warning'
                        )
                    }
                   
                }
            })
        })

    })
</script>