<section class="content">
    <div class="container">
        <div class="col-md-10 col-sm-12 mx-auto">
            <div class="shadow card my-3">
                <div class="card-header">SURAT KUASA</div>
                <div class="card-body">
                    <form id="formIRK">
                        <!-- Pihak pertama -->
                        <div class="mt-3">
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="namaP1" class="col-form-label">Nama</label>
                                </div>
                            
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="namaP1" class="form-control"  id="namaP1" placeholder="Nama pihak pertama">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="noktpP1" class="col-form-label">No. KTP</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <input type="number" name="noktpP1" class="form-control"  id="noktpP1" placeholder="No. KTP">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="pekerjaanP1" class="col-form-label">Pekerjaan</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="pekerjaanP1" class="form-control"  id="pekerjaanP1" placeholder="Pekerjaan">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="noTelpP1" class="col-form-label">No. Telp / Hp</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="noTelpP1" class="form-control"  id="noTelpP1" placeholder="081234567890">
                                </div>
                            </div>

                            <div class=" form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="alamatP1" class="col-form-label">Alamat</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="jalanP1" placeholder="Jalan" class="form-control"  id="alamatP1">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="number" name="rtP1" placeholder="RT" class="form-control" >
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="number" name="rwP1" placeholder="RW" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kelurahanP1" placeholder="Desa/Kelurahan" class="form-control" >
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kecamatanP1" placeholder="Kecamatan" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="kabupatenP1" placeholder="Kabupaten/Kota" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Pihak pertama -->
                        <div class="card-footer">
                            <p>Selanjutnya disebut PIHAK PERTAMA</p>
                        </div>

                        <hr class="my-5">

                        <!-- Pihak kedua -->
                        <div class="mt-3">
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="namaP2" class="col-form-label">Nama</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="namaP2" class="form-control"  id="namaP2" placeholder="Nama pihak kedua">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="noktpP2" class="col-form-label">No. KTP</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="number" name="noktpP2" class="form-control"  id="noktpP2" placeholder="No. KTP">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="pekerjaanP2" class="col-form-label">Pekerjaan</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="pekerjaanP2" class="form-control"  id="pekerjaanP2" placeholder="Pekerjaan">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="noTelpP2" class="col-form-label">No. Telp / Hp</label>
                                </div>
                              
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="noTelpP2" class="form-control"  id="noTelpP2" placeholder="No. Telp">
                                </div>
                            </div>

                            <div class=" form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="alamatP2" class="col-form-label">Alamat</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="jalanP2" placeholder="Jalan" class="form-control"  id="alamatP2">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="number" name="rtP2" placeholder="RT" class="form-control" >
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="number" name="rwP2" placeholder="RW" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kelurahanP2" placeholder="Desa/Kelurahan" class="form-control" >
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kecamatanP2" placeholder="Kecamatan" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="kabupatenP2" placeholder="Kabupaten/Kota" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Pihak kedua -->
                        <div class="card-footer">
                            <p>Selanjutnya disebut PIHAK KEDUA</p>
                        </div>

                        <hr class="my-5">
                        <div class="card-header">
                            <p>PIHAK KESATU memberikan kuasa kepada PIHAK KEDUA guna mengurus IRK, KKPR dan IMB/PBG di DPUPR Kota Magelang  untuk  Bangunan : </p>
                        </div>
                        <div class="mt-3">
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="fungsiBangunan" class="col-form-label">Fungsi Bangunan</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="fungsiBangunan" class="form-control"  id="fungsiBangunan" placeholder="Fungsi bangunan">
                                </div>
                            </div>

                            <div class=" form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="alamatLahan" class="col-form-label">Letak Bangunan</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="jalanLahan" placeholder="Jalan" class="form-control"  id="alamatLahan">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="number" name="rtLahan" placeholder="RT" class="form-control" >
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="number" name="rwLahan" placeholder="RW" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kelurahanLahan" placeholder="Desa/Kelurahan" class="form-control" >
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kecamatanLahan" placeholder="Kecamatan" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="kabupatenLahan" placeholder="Kabupaten/Kota" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="statusBangunan" class="col-form-label">Status Tanah</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="hakTanah" class="form-control"  id="statusBangunan" placeholder="Hak tanah">
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="noTanah" class="form-control"  placeholder="No. Tanah">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="pemilikTanah" class="form-control"  placeholder="Pemilik tanah">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="luasTanah" class="col-form-label">Luas Tanah</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <input type="number" name="luasTanah" class="form-control"  id="luasTanah" placeholder="Luas tanah">
                                </div>
                            </div>
                        </div>

                        <div class="text-center mt-5">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                    <form id="formCetak" target="_blank" class="d-none" action="<?= base_url('FormSuratKuasa/cetak') ;?>" method="POST">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    body{
        background-color:#dce0e8;
    }
</style>
<?php include 'script.php'; ?>