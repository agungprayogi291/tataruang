 

 <div class="modal fade" id="ModalUser" role="dialog">
    <div class="modal-dialog modal-small"> 
            <div class='modal-content'> 
                
                <div class='modal-header'>
                            <h4 class='modal-title' id='app_modal_label'>Tambah User</h4>
                </div>
                <form action="#" name ="frm_input_user" id="frm_input_user" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="form-horizontal form-row-seperated">    
                    <div id='app_modal_body' class='modal-body'>
                        
                        <input type="hidden" id="kode" name="kode">
                        <input type="hidden" id="aksi" name="aksi" value="tambah">
                            <table class='table table-condensed table-striped table-bordered'>
                            <thead></thead>
                            <tbody> 
                         
                            
                            <tr><td>Nama Lengkap</td><td><input type="text"  id="a_nama" name="a_nama" class="form-control"></td></tr>
                            <tr><td>User Name</td><td><input type="text"  id="a_user" name="a_user" class="form-control"></td></tr>
                            <tr><td>Password</td><td><input type="password"  id="a_passw" name="a_passw" class="form-control"></td></tr>
                            <tr><td>Ulangi Password</td><td><input type="password"  id="a_ulang" name="a_ulang" class="form-control"></td></tr>                             
                            <tr><td>Level</td><td>
                                <select class="form-control" id="a_level" name="a_level">
                                                <option value="" selected=""></option>
                                                   <option value="1">Admin</option>
                                                   <option value="0">Pengguna</option>
                                                   
                                            </select>
                                    
                                   
                                </td></tr>
                            <tr><td colspan="2" align="center"><div  id="pesanerror" style="color:red;" ></div></td></tr> 
                            </tbody>
                            </table>
                    </div>
                </form> 
                    
                    <div id='app_modal_footer' class='modal-footer'>
                            <div class="col-md-12 left"> 
                            <div class="col-md-6 text-left"> 
                                <button type="button" onclick="SimpanDataUser();"  class="btn btn-warning">Proses</button>
                                     
                             </div>
                             <div class="col-md-6">                    
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>                  
                             </div>
                             </div>
                        
                             
                    </div>
            </div>
    </div>
</div> 
<?php if($edit):?>
<script>
    BukaEditUser(<?= $_SESSION['user_id'] ;?>)
    function BukaEditUser(Kode){
      $.ajax({                    
          type: "POST", 
          url: '<?= base_url() ;?>modul/user_modul.php', 
          data: {
          aksi:'edit',
          kode:Kode},  
          //contentType: false,
          //cache: false,             
          //processData:false,  
          dataType:'json', 
          success: function(data) {  
              $("#app_modal_label").text("Edit Password")
            $("#kode").val(data[0].kode); 
            $("#a_nama").val(data[0].nama_lengkap); 
            $("#a_user").val(data[0].nama_user); 
            //$("#a_passw").val(data[0].passw_user); 
            $("#a_passw").val(''); 
            //$("#a_ulang").val(data[0].passw_user);  
            $("#a_ulang").val('');  
            $("#a_level").val(data[0].levele);  
            $("#ModalUser").modal('show');                        
          }
      });
    }
     
</script>
<?php endif; ?>

