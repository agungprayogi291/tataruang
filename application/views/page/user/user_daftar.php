
<?php  if (!isset($_SESSION["logged"])){    header('Location: login');  }?>
<!-- ================================================================================== -->        


<div class="clearfix " style="min-height:450px;" >
  <div class="container">
    <div class="pengaduan">
      <div id="main_j"> 
        <div class="pengaduan-top-grid">
            <h3>DAFTAR PENGGUNA</h3>
        </div>
      </div>
      <br>
      <?php if ($_SESSION['user_level']==1){ ?>
        <button type="button"  onclick="BukaTambahUser()"  class="btn btn-warning" style="margin-bottom:20px;">Tambah User</button>    
      <?php } ?>
      <table class='table table-condensed table-striped table-bordered'>
        <thead>
          <tr>
              <td><strong>No</strong></td> 
              <td><strong>Nama User</strong></td>
              <td><strong>User Name</strong></td> 
              <td><strong>Password</strong></td> 
              <td><strong>Level</strong></td> 
              <td><strong>Aksi</strong></td>
          </tr>
        </thead>
        <tbody id="IsiTabel">   
        </tbody>
      </table>      
    </div>
  </div>    
</div>         
  <?php include_once 'user_tambah.php'; ?>  
  <script>
    <?php 
      echo ('var KodeUser='.$_SESSION["user_id"].';'); 
      echo ('var LevelE='.$_SESSION["user_level"].';');          
    ?>  
    jQuery(document).ready(function() {  
     TampilDataUser();
        console.log('e')
    });       
    
    function HapusUser(Kode){ 
      $.ajax({
          type: "POST", 
          url: '<?= base_url() ;?>modul/user_modul.php', 
          data: {aksi:'hapus',
          kode:Kode},  
          //contentType: false,
          //cache: false,             
          //processData:false,  
          dataType:'json', 
          success: function(data) {   
             TampilDataUser();
          }                                      
      });
    }
    function BukaTambahUser(){     
      $("#kode").val('');
      $("#ModalUser").modal('show');
    }
    function SimpanDataUser(){ 
        var formX = document.getElementById('frm_input_user');
        formData = new FormData(formX);  
        var Kode  = $('#kode').val();        
        $.ajax({                    
          type: "POST", 
          url: '<?= base_url() ;?>modul/user_modul.php', 
          data: formData,   
          contentType: false,
          cache: false,             
          processData:false,  
          dataType:'json', 
          success: function(data) {   
            if (data.hasil=='ok'){
              TampilDataUser();
              $("#pesanerror").html('');
              $('#frm_input_user').trigger('reset');  
              if (Kode!=''){ $("#ModalUser").modal('hide'); }
              
            }else{
                $("#pesanerror").html(data.pesan)
            }
          }
        });
    }
    function BukaEditUser(Kode){
      $.ajax({                    
          type: "POST", 
          url: '<?= base_url() ;?>modul/user_modul.php', 
          data: {
          aksi:'edit',
          kode:Kode},  
          //contentType: false,
          //cache: false,             
          //processData:false,  
          dataType:'json', 
          success: function(data) {  
            $("#kode").val(data[0].kode); 
            $("#a_nama").val(data[0].nama_lengkap); 
            $("#a_user").val(data[0].nama_user); 
            //$("#a_passw").val(data[0].passw_user); 
            $("#a_passw").val(''); 
            //$("#a_ulang").val(data[0].passw_user);  
            $("#a_ulang").val('');  
            $("#a_level").val(data[0].levele);  
            $("#ModalUser").modal('show');                        
          }
      });
    }
    
    
    function TampilDataUser(){   
      $.ajax({                    
        type: "POST", 
        url: "<?= base_url() ;?>modul/user_modul.php", 
        data: {
          aksi:'tampil',
          level:LevelE,
          KodeUsr:KodeUser},  
          //contentType: false,
          //cache: false,             
          //processData:false,  
        dataType:"json", 
        success: function(data) {   
          var isiT;
          var x=1;
          if (data.length>0){                                           
            for (var i = 0; i < data.length; i++){
              isiT+='<tr>';
              isiT+='<td>'+x+'</td>';
              isiT+='<td>'+data[i].nama_lengkap+'</td>';
              isiT+='<td>'+data[i].nama_user+'</td>';
              isiT+='<td>********</td>'; 
              var Lev="Admin";
              if (data[i].levele==0){Lev="Pengguna";}  
              isiT+='<td>'+Lev+'</td>';
                <?php if($_SESSION["user_level"]=='1'){   ?>
                  isiT+='<td><a href="#" onclick="BukaEditUser('+data[i].kode +');">Edit</a> &nbsp; &nbsp;<a  href="#" onclick="HapusUser('+data[i].kode +');">Delete </a></td>';
                <?php }else{ ?>  
                  if (KodeUser==data[i].kode){
                    isiT+='<td><a href="#" onclick="BukaEditUser('+data[i].kode +');">Edit</a></td>';
                  }
                <?php }?> 
                isiT+='</tr>';
                x++;                                     
            }
            $('#IsiTabel').html(isiT);
          } else{
            $('#IsiTabel').html(IsiDataTabelKosong(6));
          }
          
        }
      });               
    }

    function IsiDataTabelKosong(colspan){
      var data=  "<tr><td colspan='"+colspan+"' style='text-center'>Tidak ada data</td></tr>"
      return data;

    }
</script>