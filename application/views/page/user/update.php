
<?php  if (!isset($_SESSION["logged"])){    header('Location:login'); exit;  }?>
<div class="container " style="margin:5%; min-height:450px;">
<form action="#" name ="frm_input_user" id="frm_input_user" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="form-horizontal form-row-seperated">    
    <div id='app_modal_body' class='modal-body'>
        
        <input type="hidden" id="kode" name="kode">
        <input type="hidden" id="aksi" name="aksi" value="tambah">
            <table class='table table-condensed table-striped table-bordered'>
            <thead></thead>
            <tbody> 
            
            
            <tr><td>Nama Lengkap</td><td><input type="text"  id="a_nama" name="a_nama" class="form-control"></td></tr>
            <tr><td>User Name</td><td><input type="text"  id="a_user" name="a_user" class="form-control"></td></tr>
            <tr><td>Password</td><td><input type="password" autocomplete ="off" onfocus="showPassword()" onblur="hidePassword()"   id="a_passw" name="a_passw" class="form-control" value=""></td></tr>
            <tr><td>Ulangi Password</td><td><input type="password"  id="a_ulang" onfocus="showPassword2()" onblur="hidePassword2()" name="a_ulang" class="form-control" value=""></td></tr>                             
            <tr><td>Level</td><td>
                <select class="form-control form-control-lg" id="a_level" name="a_level">
                               
                                <?= $elm ;?>
                            </select>
                    
                    
                </td></tr>
         
            </tbody>
            </table>
    </div>
</form> 
<!-- <input type="hidden" value="<?= $pass ;?>" id="hide"> -->
<div id='app_modal_footer' class='modal-footer'>
        <div class="col-md-12 left"> 
            <div class="col-md-6 text-left"> 
                <button type="button" onclick="SimpanDataUser();"  class="btn btn-warning">Proses</button>
                        
            </div>
            <div class="col-md-3">
                <div class="alert alert-info collapse"  id="pesanerror-edit" ></div>
            </div>
        </div>
</div>
</div>

<?php if($edit):?>
<script>
    window.console.log = function(){
        console.error('Sorry , developers tools are blocked here....');
        window.console.log = function() {
            return false;
        }
    }
    window.document = function(){
        console.error('Sorry , developers tools are blocked here....');
        window.document = function() {
            return false;
        }
    }
 
    // console.log(window)
    document.onkeydown = function(e) {
        if(event.keyCode == 123) {
            return false;
        }
        if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
        if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }
//     $(document).filter(function(property) {
//      return typeof console[property] == 'function';
//   }).forEach(function (verb) {
//      console[verb] =function(){return 'Sorry, for security reasons...';};
//   });
	
    $(document).ready(function(){

        hidePassword();
        hidePassword2();
    })
    password = '';
    function hidePassword(){
        p = document.getElementById('a_passw');
        password = p.value;
        if(password == "" || password == null){
            p.value = "<?= $pass ;?>";
        }else{
            p.value = password;
        }
       
    }
    function showPassword(){
        p = document.getElementById('a_passw');  
        p.type= "password"; 
        p.value = "";
    }

    function hidePassword2(){
        p = document.getElementById('a_ulang');
        password = p.value;
        if(password == "" || password == null){
            p.value = "<?= $pass ;?>";
        }else{
            p.value = password;
        }
        
    }
    function showPassword2(){
        p = document.getElementById('a_ulang');  
        p.type= "password"; 
        p.value = "";
    }

    BukaEditUser(<?= $_SESSION['user_id'] ;?>)
    function BukaEditUser(Kode){
      $.ajax({                    
          type: "POST", 
          url: '<?= base_url() ;?>modul/user_modul.php', 
          data: {
          aksi:'edit',
          kode:Kode},  
          //contentType: false,
          //cache: false,             
          //processData:false,  
          dataType:'json', 
          success: function(data) {  
              $("#app_modal_label").text("Edit Password")
            $("#kode").val(data[0].kode); 
            $("#a_nama").val(data[0].nama_lengkap); 
            $("#a_user").val(data[0].nama_user); 
            //$("#a_passw").val(data[0].passw_user); 
            // $("#a_passw").val(); 
            // //$("#a_ulang").val(data[0].passw_user);  
            // $("#a_ulang").val('');  
            $("#a_level").val(data[0].levele);  
            $("#ModalUser").modal('show');                        
          }
      });
    }
    function SimpanDataUser(){ 
        var formX = document.getElementById('frm_input_user');
        formData = new FormData(formX);  
        var Kode  = $('#kode').val();   
       
        $.ajax({                    
          type: "POST", 
          url: '<?= base_url() ;?>modul/user_modul.php', 
          data: formData,   
          contentType: false,
          cache: false,             
          processData:false,  
          dataType:'json', 
          success: function(data) {   
            if (data.hasil=='ok'){
            //   TampilDataUser();
                pesan("berhasil dirubah")
                setTimeout(() => {
                    BukaEditUser()
                    <?php if($self) :?>
                    reset()
                    <?php endif;?>
                }, 2000);
              
            //   if (Kode!=''){ $("#ModalUser").modal('hide'); }
              
            }else{
                pesan(data.pesan)
            }
          }
        });
    }

    function pesan(pesan){
        $("#pesanerror-edit").removeClass("collapse")
        $("#pesanerror-edit").text(pesan)
        setTimeout(() => {
            $("#pesanerror-edit").addClass("collapse")
        }, 3000);
    }

    function reset(){
        $.ajax({
            type:"POST",
            url :"<?= base_url() ;?>s-p",
            data : {
                sp : $("#a_ulang").val()
            },
            dataType:"JSON",
            success:function(e){

            }

        })
    }

   
</script>
<?php endif; ?>

