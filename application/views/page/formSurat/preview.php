<html>

<body>
    <h3><u>SURAT IZIN PENGGUNAAN TANAH</u></h3>

    <p>Yang bertanda tangan dibawah ini :</p>

    <table style="width: 100%">
        <tr>
            <td>1</td>
            <td>Nama</td>
            <td> : </td>
            <td><?= $cetak['namapp1'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>No. KTP</td>
            <td> : </td>
            <td><?= $cetak['noktppp1'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Pekerjaan</td>
            <td> : </td>
            <td><?= $cetak['pekerjaanpp1'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>No. Telp / Hp</td>
            <td> : </td>
            <td><?= $cetak['notelppp1'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td> : </td>
            <td>Jalan <?= $cetak['jalanpp1'] ?></td>
            <td>RT <?= $cetak['rtpp1'] ?> / RW <?= $cetak['rwpp1'] ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Desa / Kelurahan <?= $cetak['kelurahanpp1'] ?></td>
            <td>Kecamatan <?= $cetak['kecamatanpp1'] ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="2">Kabupaten / Kota <?= $cetak['kabupatenpp1'] ?></td>
        </tr>
    </table>

    <table style="width: 100%">
        <tr>
            <td>2</td>
            <td>Nama</td>
            <td> : </td>
            <td><?= $cetak['namapp2'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>No. KTP</td>
            <td> : </td>
            <td><?= $cetak['noktppp2'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Pekerjaan</td>
            <td> : </td>
            <td><?= $cetak['pekerjaanpp2'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>No. Telp / Hp</td>
            <td> : </td>
            <td><?= $cetak['notelppp2'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td> : </td>
            <td>Jalan <?= $cetak['jalanpp2'] ?></td>
            <td>RT <?= $cetak['rtpp2'] ?> / RW <?= $cetak['rwpp2'] ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Desa / Kelurahan <?= $cetak['kelurahanpp2'] ?></td>
            <td>Kecamatan <?= $cetak['kecamatanpp2'] ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="2">Kabupaten / Kota <?= $cetak['kabupatenpp2'] ?></td>
        </tr>
    </table>

    <p>Selanjutnya disebut <b>PIHAK PERTAMA</b></p>

    <p>Dengan ini memberikan izin kepada :</p>

    <table style="width: 100%">
        <tr>
            <td></td>
            <td>Nama</td>
            <td> : </td>
            <td><?= $cetak['namapk'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>No. KTP</td>
            <td> : </td>
            <td><?= $cetak['noktppk'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Pekerjaan</td>
            <td> : </td>
            <td><?= $cetak['pekerjaanpk'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>No. Telp / Hp</td>
            <td> : </td>
            <td><?= $cetak['notelppk'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td> : </td>
            <td>Jalan <?= $cetak['jalanpk'] ?></td>
            <td>RT <?= $cetak['rtpk'] ?> / RW <?= $cetak['rwpk'] ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Desa / Kelurahan <?= $cetak['kelurahanpk'] ?></td>
            <td>Kecamatan <?= $cetak['kecamatanpk'] ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="2">Kabupaten / Kota <?= $cetak['kabupatenpk'] ?></td>
        </tr>
    </table>

    <p>Selanjutnya disebut <b>PIHAK KEDUA</b></p>

    <p>Untuk mengajukan permohonan <b>IRK, PKKPR, PBG dll</b> atas nama PIHAK KEDUA, dan diatas lahan sebagai berikut :</p>

    <table style="width: 100%">
        <tr>
            <td>Letak Bangunan</td>
            <td> : </td>
            <td>Jalan <?= $cetak['jalanlahan'] ?></td>
            <td>RT <?= $cetak['rtlahan'] ?> / RW <?= $cetak['rwlahan'] ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td>Kelurahan <?= $cetak['kelurahanlahan'] ?></td>
            <td>Kecamatan <?= $cetak['kecamatanlahan'] ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td>Kota Magelang</td>
        </tr>
        <tr>
            <td>Status Tanah</td>
            <td> : </td>
            <td>Hak <?= $cetak['haklahan'] ?></td>
            <td>No. <?= $cetak['nolahan'] ?></td>
        </tr>
        <tr>
            <td>Luas Tanah</td>
            <td> : </td>
            <td><?= $cetak['luaslahan'] ?> m<sup>2</sup></td>
        </tr>
    </table>

    <p>Demikian Surat Izin ini dibuat untuk dijadikan periksa bagi yang berkepentingan serta dapat dipergunakan semestinya.</p>

    <div class="ttd" style="display: inline;">

        <table style="width: 100%;">
            <tr>
                <td width="60%">
                    <table>
                        <tr>
                            <td style="color: white;" colspan="2">Magelang,</td>
                        </tr>
                        <tr>
                            <td class="center"><b>PIHAK KEDUA</b></td>
                        </tr>
                        <tr>
                            <td class="center">Yang Menerima Izin</td>
                        </tr>
                        <tr>
                            <td style="height: 50px;"></td>
                        </tr>
                        <tr>
                            <td class="center"><?= $cetak['namapk'] ?></td>
                        </tr>
                    </table>
                </td>
                <td width="40%">
                    <table>
                        <tr>
                            <td class="center" colspan="2">Magelang,</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="2"><b>PIHAK PERTAMA</b></td>
                        </tr>
                        <tr>
                            <td>Yang Memberi Izin</td>
                            <td>Yang Memberi Izin</td>
                        </tr>
                        <tr>
                            <td style="height: 50px;"></td>
                        </tr>
                        <tr>
                            <td>1. <?= $cetak['namapp1'] ?></td>
                            <td>2. <?= $cetak['namapp2'] ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </div>

</body>

</html>