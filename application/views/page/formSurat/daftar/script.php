<script>
    $(document).ready(function(){
        loadData()
    })

    function loadData(){
        $.ajax({
            type:"POST",
            url:"<?= base_url() ;?>list-penggunaan-tanah",
            dataType:"JSON",
            success:function(data){
                RefreshTable($('#tabel_surat'), $('#show_surat'), data.tabel, data.row);
            }
        })
    }


    function inisialTabel(tableT) {
        tableT.dataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": false,
            "autoWidth": true,
            "scrollX" : true
        });
    }

    function RefreshTable(tableT, IsiT, dataT, baris = 2) {
        if (baris > 1) {
        tableT.DataTable().destroy();
        } else {
        tableT.dataTable().fnClearTable();
        }
        IsiT.html(dataT);
        if (baris > 1) {
        inisialTabel(tableT);
        }
    }

    function cetakUlang(id){
        Swal.fire(
            "info!",
            "test",
            "info"
        )
    }

    function cekStatus(){
        
    }
  
</script>