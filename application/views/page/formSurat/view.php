<section class="content " >
    <div class="container">
        <div class="col-md-10 col-sm-12 mx-auto">
            <div class="card my-3">
                <div class="card-header">SURAT IZIN PENGGUNAAN TANAH</div>
                <div class="card-body">
                    <form id="formSuratIzin">
                        <!-- Pihak Pertama 1 -->
                        <div class="pp1">
                            <h5 class="card-title mb-5">Pihak Pertama 1</h5>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="namaPihakPertama1" class="col-form-label">Nama</label>
                                </div>
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="namaPP1" class="form-control " style="line-height: 1.5; padding: 6px 12px;" id="namaPihakPertama1">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="noktpPihakPertama1" class="col-form-label">No. KTP</label>
                                </div>
                              
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="noktpPP1" class="form-control"  id="noktpPihakPertama1">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="pekerjaanPihakPertama1" class="col-form-label">Pekerjaan</label>
                                </div>
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="pekerjaanPP1" class="form-control"  id="pekerjaanPihakPertama1">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="noTelpPihakPertama1" class="col-form-label">No. Telp / Hp</label>
                                </div>
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="noTelpPP1" class="form-control"  id="noTelpPihakPertama1">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="alamatPihakPertama1" class="col-form-label">Alamat</label>
                                </div>
                              
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="jalanPP1" placeholder="Jl." class="form-control"  id="alamatPihakPertama1">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rtPP1" placeholder="RT" class="form-control"  id="alamatPihakPertama1">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rwPP1" placeholder="RW" class="form-control"  id="alamatPihakPertama1">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kelurahanPP1" placeholder="Desa/Kelurahan" class="form-control"  id="alamatPihakPertama1">
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kecamatanPP1" placeholder="Kecamatan" class="form-control"  id="alamatPihakPertama1">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="kabupatenPP1" placeholder="Kabupaten/Kota" class="form-control"  id="alamatPihakPertama1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Pihak Pertama 2 -->
                        <div class="pp2 mt-3">
                            <h5 class="card-title mb-5">Pihak Pertama 2</h5>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="namaPihakPertama2" class="col-form-label">Nama</label>
                                </div>
                              
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="namaPP2" class="form-control"  id="namaPihakPertama2">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="noktpPihakPertama2" class="col-form-label">No. KTP</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="noktpPP2" class="form-control"  id="noktpPihakPertama2">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="pekerjaanPihakPertama2" class="col-form-label">Pekerjaan</label>
                                </div>
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="pekerjaanPP2" class="form-control"  id="pekerjaanPihakPertama2">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="noTelpPihakPertama2" class="col-form-label">No. Telp / Hp</label>
                                </div>
                          
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="noTelpPP2" class="form-control"  id="noTelpPihakPertama2">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                 <label for="alamatPihakPertama2" class=" col-form-label">Alamat</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="jalanPP2" placeholder="Jl." class="form-control"  id="alamatPihakPertama2">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rtPP2" placeholder="RT" class="form-control" >
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rwPP2" placeholder="RW" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kelurahanPP2" placeholder="Desa/Kelurahan" class="form-control" >
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kecamatanPP2" placeholder="Kecamatan" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="kabupatenPP2" placeholder="Kabupaten/Kota" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr class="my-5">

                        <!-- Pihak Pertama 2 -->
                        <div class="pk mt-3">
                            <h5 class="card-title mb-5">Pihak Kedua</h5>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="namaPihakKedua" class="col-form-label">Nama</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="namaPK" class="form-control"  id="namaPihakKedua">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="noktpPihakKedua" class="col-form-label">No. KTP</label>
                                </div>
                             
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="noktpPK" class="form-control"  id="noktpPihakKedua">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="pekerjaanPihakKedua" class="col-form-label">Pekerjaan</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="pekerjaanPK" class="form-control"  id="pekerjaanPihakKedua">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="noTelpPihakKedua" class="col-form-label">No. Telp / Hp</label>
                                </div>
                              
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="noTelpPK" class="form-control"  id="noTelpPihakKedua">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="alamatPihakKedua" class="col-form-label">Alamat</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="jalanPK" placeholder="Jl." class="form-control"  id="alamatPihakKedua">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rtPK" placeholder="RT" class="form-control" >
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rwPK" placeholder="RW" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kelurahanPK" placeholder="Desa/Kelurahan" class="form-control" >
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kecamatanPK" placeholder="Kecamatan" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="kabupatenPK" placeholder="Kabupaten/Kota" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr class="my-5">

                        <!-- Penggunaan Lahan -->
                        <div class="lahan mt-3">
                            <!-- <h5 class="card-title mb-5">Penggunaan Lahan</h5> -->
                            <div class="card-header my-5">
                                <p>Untuk mengajukan permohonan IRK, PKKPR, PBG dll atas nama PIHAK KEDUA, dan diatas lahan sebagai berikut:</p>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="lahan" class="col-form-label">Letak Bangunan</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="jalanLahan" placeholder="Jl." class="form-control"  id="lahan">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rtLahan" placeholder="RT" class="form-control" >
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rwLahan" placeholder="RW" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kelurahanLahan" placeholder="Desa/Kelurahan" class="form-control" >
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kecamatanLahan" placeholder="Kecamatan" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="kabupatenLahan" placeholder="Kabupaten/Kota" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="hak" class=" col-form-label">Status Tanah</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="hakLahan" id="hak" placeholder="Hak" class="form-control" >
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="noLahan" placeholder="No." class="form-control" >
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="" class="col-form-label">Luas Tanah</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                <!-- <div class="row mt-2">
                                        <div class="col-md-6">
                                            <div class="input-group"> -->
                                                <div class="col-sm-8 col-md-8">
                                                    <input type="text" name="luasLahan" class="form-control " placeholder="Luas Tanah">  
                                                </div>
                                               <div class="col-sm-4 col-md-4">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="basic-addon2">m<sup>2</sup></span>
                                                    </div>
                                               </div>
                                               
                                            <!-- </div>
                                        </div>
                                        
                                    </div> -->
                                </div>
                            </div>
                        </div>

                        <div class="text-center mt-5">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    
                    </form>
                    <form id="formCetak" target="_blank" class="d-none" action="<?= base_url('FormSurat/cetak') ?>" method="POST">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<form action="<?php base_url() ;?>form-surat-cetak" method="post" id="cetakform"></form>
<style>
    body{
        background-color:#dce0e8;
    }
</style>

<?php include_once 'script.php'; ?>