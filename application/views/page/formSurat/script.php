
<script type="text/javascript">
    jQuery(document).ready(function() {
       

        $("#formSuratIzin").submit(function(e) {
            e.preventDefault();
            let data = $("#formSuratIzin").serializeArray();

            $.ajax({
                url: '<?= base_url('FormSurat/simpan') ?>',
                data: data,
                type: 'POST',
                dataType: 'json',
                success: function(response) {
                    // console.log(data)
                    if (response.success == 1) {
                        // alert(data.message);
                     //   console.log(data.data)

                     let html ='';
                        Swal.fire(
                            'Good Job!',
                            `${response.message}`,
                            'success'
                        ).then(e=>{

                            for (const prop in response.data) {
                                html += `<input type="text" name="${prop}" value="${response.data[prop]}">`
                            }

                            $("#formCetak").html(html)
                            $("#formCetak").trigger('submit');
                            $("#formSuratIzin")[0].reset();
                           
                            // $("#cetakform").submit()
                        })

                    }else{
                        Swal.fire(
                            'warning!',
                            `${response.message}`,
                            'warning'
                        );
                    }
                }
            })

            // console.log($("#formSuratIzin").serializeArray())
        })

    })
  
</script>