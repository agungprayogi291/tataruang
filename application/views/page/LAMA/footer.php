

 <section id="bannerheader2">  
                   Data & Informasi Geospasial Pemerintah Kota Magelang 
 </section>
			<!-- Highlights -->
				<section class="wrapper style1">
					<div class="container">
						<div class="row 200%">
							<section class="4u 12u(narrower)">
								<div class="box highlight"> 
<!--                                                                    <i class="icon major fa-map-marker"></i>-->
                                                                    <i class="icon major fa-square-o"></i>
                                                                        
                                                                        
									<h4>SIG Tata Ruang Kota Magelang</h4>
									<p>Sistem Informasi Geografis Tata Ruang Kota Magelang</p>
								</div>
							</section>
							<section class="4u 12u(narrower)">
								<div class="box highlight">
									<i class="icon major fa-map-marker"></i>
									<h4>SIG Data IMB Dan SKRK</h4>
									<p>Sistem Informasi Geografis Izin Mendirikan  Bangunan dan SKRK Kota Magelang</p>
								</div>
							</section>
							<section class="4u 12u(narrower)">
								<div class="box highlight">
									<i class="icon major fa-user"></i>
                                                                        <h4>SIG Jalan Dan Jembatan</h4>
									<p>Sistem Informasi Geografis Basis Data Jalan dan Jembatan</p>
								</div>
							</section>
						</div>
					</div>
				</section> 
			<!-- Footer -->
				<div id="footer">
					<div class="container">

					</div>

					 

					<!-- Copyright -->
						<div class="copyright">
<!--							<ul class="menu">-->
								<li>&copy; COPYRIGHT 2018 || Pemerintah Kota Magelang</li>
                                                                 
<!--							</ul>-->
						</div>

				</div>

		</div>

 

	</body>
</html>

