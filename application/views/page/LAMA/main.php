  
<div id="slider_container">   
               
                    <div data-thumb="../assets/slider/img/1.jpg" data-src="../assets/slider/img/2.jpg" >
                        <div class="camera_caption">
                                <div class="container" style="display:none; opacity: 0; filter: alpha(opacity=0);"> 
                                </div>
                        </div>
                    </div>
                    <div data-thumb="../assets/slider/img/1.jpg" data-src="../assets/slider/img/2.jpg" >
                        <div class="camera_caption">
                                <div class="container" style="display:none; opacity: 0; filter: alpha(opacity=0);"> 
                                </div>
                        </div>
                    </div> 
    </div>  
 
    <script type="text/javascript">
		//var f=jQuery.noConflict();  	
//                f(document).ready(function(){
                    $(document).ready(function(){
			
				var styleColor = "#7ab317"; 
				/* slideshow */
				window.firstSlider = true;
				window.sliderIndex = 0;
				
				window.firstLoad = true;
					
				if ($('#slider_container').length){
				
					$('#slider_container').camera({
						pagination: true,
						loader: 'bar',
						fx: 'random',
						transPeriod: 500,
						barPosition: 'bottom',
						loaderStroke: 3,
						loaderPadding: 0,
						loaderOpacity: .6,
						loaderColor: 'rgba(0,0,0,.3)',
						height: '400px',
						time: 6000,
						hover: true,
						playPause: false,
						pauseOnClick: false,
						autoAdvance: true,
						thumbnails: false,
						imagePath: $('#templatepath').html() + "images/",
						onLoaded: function(){
							if (window.firstSlider){
								//$('#white_content > div').prepend($('#slider_container .camera_pag').html());
								$('#white_content > #wrapper').prepend('<div class="cameracontrols" style="position: absolute; height: 66px; right: 2%; top: -66px; z-index: 99;  "><div class="controls-mover" style="position: relative; float: left; top: 33px;"><div class="camera-controls-toggler closed" style="position: relative; float: right; padding: 5px 0px; display: inline-block; clear: both; background: #95c245; color: white; font-weight: bold; font-size: 14px; cursor: pointer; width: 34px; text-align: center; border-bottom: 3px solid '+$('#styleColor').html()+';" onclick="toggleCameraControls($(this));" >+</div><div class="cameraholder" /></div></div>');
								$('#slider_container .camera_pag_ul').clone(true, true).prependTo($('#white_content .cameraholder'));
								$('#white_content .cameraholder').prepend('<div id="triangle-bottomleft" /><div class="vert-sep" style="display: inline-block; position: absolute; float: right; width: 1px; height: 17px; top: 9px; right: 33px; background: #628f12;" />');
								
								var classPlaying = "playing";
																	classPlaying = "paused";
																	
								$('#white_content .cameraholder #triangle-bottomleft').html('<div id="play_pause" class="'+classPlaying+'" onclick="playpause($(this));"/>');

								$('.camera_caption .container').css({'opacity': 1, 'filter': 'alpha(opacity=100)', 'display':'block'});
																
								var ind = parseInt($('#slider_container .camera_pag_ul .cameracurrent').index('li'),10);
								window.sliderIndex = ind;
								
								window.firstSlider = false;
								
								$('.camera_prev').hover(function(){
									$(this).css('opacity', 1);
								}, function(){
									$(this).css('opacity', .6);
								});
								$('.camera_next').hover(function(){
									$(this).css('opacity', 1);
								}, function(){
									$(this).css('opacity', .6);
								});
								
							}
							
							if ($('#bodyLayoutType').html() == "boxed"){
								$('.camera_caption').each(function(){
									$(this).find('.container > .eight.columns').css('margin-right','0px');
									$(this).find('.container > .eight.columns').eq(0).css('margin-left','20px');
								});
							}
							
						},
						onStartTransition: function(){
							var ind = 0;
								//ind = parseInt($('#slider_container .camera_pag_ul .cameracurrent').index('li'),10);
							
							for (var x=0; x< $('#slider_container .camera_pag_ul li').length; x++){
								if ($('#slider_container .camera_pag_ul li').eq(x).hasClass('cameracurrent')){
									ind = x;
								}
							}
							
							//console.log(ind);
							//var ind = $('#slider_container .camera_pag_ul li').find('.cameracurrent').index();
							$('#white_content .camera_pag_ul').find('.cameracurrent').removeClass('cameracurrent');
							$('#white_content .camera_pag_ul li').eq(ind).addClass('cameracurrent');
							//$('.camera_target_content .cameraContent:eq('+ind+')').unbind('hover');
							
							var source = $('#slider_container .camera_pag_ul li').eq(ind).html();
							
							$('.camera_target_content .cameraContents').children().eq(ind).unbind('mouseenter mouseleave');
										
						}, 
						onEndTransition: function(){
							
							if ($('.cameracurrent .camera_caption').length){
								if ($('.cameracurrent .camera_caption').find('.title').length){
									var animation = $('.cameracurrent .camera_caption').find('.title').attr('class').split(' ');
										type = animation[0];
										animation = animation[animation.length-1];
									animateElement($('.cameracurrent .camera_caption').find('.title'), type, animation);
								}
								if ($('.cameracurrent .camera_caption').find('.text').length){
									var animation = $('.cameracurrent .camera_caption').find('.text').attr('class').split(' ');
										type = animation[0];
										animation = animation[animation.length-1];
									animateElement($('.cameracurrent .camera_caption').find('.text'), type, animation);
								}
								if ($('.cameracurrent .camera_caption').find('.image').length){
									var animation = $('.cameracurrent .camera_caption').find('.image').attr('class').split(' ');
										type = animation[0];
										animation = animation[animation.length-1];
									animateElement($('.cameracurrent .camera_caption').find('.image'), type, animation);
								}
								if ($('.cameracurrent .camera_caption').find('.button').length){
									var animation = $('.cameracurrent .camera_caption').find('.button').attr('class').split(' ');
										type = animation[0];
										animation = animation[animation.length-1];
									animateElement($('.cameracurrent .camera_caption').find('.button'), type, animation);
								}

							}
							
						} 
					});
				
				}
				
			});
			

			
	 </script>  
 