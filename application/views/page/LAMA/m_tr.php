<div id="map" style="height:30%;"></div>
<div id="panel"class="legenda"> 
    <input type="radio" name="basemaps" value="0" checked="true" onchange="clickbasemaps();">Citra Satelit<br>
    <input type="radio" name="basemaps" value="1" onchange="clickbasemaps();">Google Satellite<br>
    <input type="radio" name="basemaps" value="2" onchange="clickbasemaps();">Google Hybrid<br>
    <input type="radio" name="basemaps" value="3" onchange="clickbasemaps();">Google Streets<br>
    <input type="radio" name="basemaps" value="4" onchange="clickbasemaps();">Google Terrain<br>
    <input type="radio" name="basemaps" value="5" onchange="clickbasemaps();">OpenStreetMap<br>
    
    <hr>
    <strong>Referensi Peta Dasar</strong> <br>
    <input type="checkbox" name="adm" id="adm" onchange="clickadministrasi('1');">Batas Adm. Kabupaten/Kota<br> 
    <input type="checkbox" name="kec" id="kec" onchange="clickadministrasi('2');">Batas Adm. Kecamatan<br> 
    <input type="checkbox" name="kel" id="kel" onchange="clickadministrasi('3');">Batas Adm. Desa/Kelurahan<br>  
    <hr>
    <strong>Referensi Tata Ruang</strong>  <br>     
    <input type="checkbox" name="tinggi" id="tinggi" onchange="ClickKetinggianBangunan();">Ketinggian Maksimum Bangunan<br> 
    <input type="checkbox" name="koef" id="koef" onchange="ClickKoofbangunan();">Koefisien Dasar Bangunan<br> 
    
    <input type="checkbox" name="pola" id="pola" onclick="ClickPolaruang();">Pola Ruang<br> 
    
    <hr>
    <input type="checkbox" name="xxx" id="xxx" onclick="ClickInfo();">Info Detail Tataruang<br> 
      
        
    
   
</div>

<?php include_once 'm_tr_tambah.php'; ?>
  <script type="text/javascript" src="../ajax/tr_js.js"></script>
  
  
  <style>
legenda{
   width: 5px;
    height:3px; 
    padding:  5px;
    margin:  2px;     
    }    
.s1 {  background-color: #000068;}
.s2 {  background-color:#000000;}
.s3 {  background-color: #ffff00;}
.s4{  background-color: #18f727;}
.s5{  background-color: #00e3ee;}
.s6{  background-color: #ff7600;}
.s7{  background-color: #ff0700;}
.s8{  background-color: #ec1ee4;}  
                
.e1{  background-color: #000000;}
.e2{  background-color: #ffff00;}
.e3{  background-color: #f1bc1d;}
.e4{  background-color: #c50500;}
.e5{  background-color: #ff3e39;}
.e6{  background-color: #36e033;}
.e7{  background-color: #017277;}
.e8{  background-color: #ff9439;}
.e9{  background-color: #291ec5;}


.r1{  background-color: #800026;}
.r2{  background-color: #7151e8;}
.r3{  background-color: #E31A1C;}
.r4{  background-color: #b7713e;}
.r5{  background-color: #b7713e;}
.r6{  background-color: #FD8D3C;}
.r7{  background-color: #70f169;}
.r8{  background-color: #b4c339;}
.r9{  background-color: #FED976;}
.r10{  background-color: #5d6ce0;}
.r11{  background-color: #ca33bd;}
.r12{  background-color: #2a0c9e;}
.r13{  background-color: #fbe809;}
.r14{  background-color: #9c9e24;}
.r15{  background-color: #3c8cd2;}
.r16{  background-color: #8145dc;}
.r17{  background-color: #4ae6e0;}
.r18{  background-color: #FEB24C;}
.r19{  background-color: #FEB24C;}
.r20{  background-color: #FC4E2A;}
 
 
  

</style>

  
<div id="p_kooef" class="legendaKiri">  
  <legenda class='s1'></legenda>Sungai<p>
  <legenda class='s2'></legenda>Jalan<p>
  <legenda class='e2'></legenda>Kawasan Kewenangan Pemerintah<p>
  <legenda class='s4'></legenda>KDB 0%<p>
  <legenda class='s5'></legenda>KDB 40%<p>
  <legenda class='s6'></legenda>KDB 60%<p>
  <legenda class='s7'></legenda>KDB 80%<p>
  <legenda class='s8'></legenda>Lain-Lain<p> 
</div>


 

 
                 
                
                
                
  
<div id="p_tinggi" class="legendaKiri">  
   <legenda class='e1'></legenda>Kawasan Pemakaman<p>
  <legenda class='e2'></legenda>Kawasan Kewenangan Pemerintah<p>
  <legenda class='e3'></legenda>Maksimum 0 Lantai<p>
  <legenda class='e4'></legenda>Maksimum 3 Lantai<p>
  <legenda class='e5'></legenda>Maksimum 4 Lantai<p>
  <legenda class='e6'></legenda>Maksimum 5 Lantai<p>
  <legenda class='e7'></legenda>Maksimum 8 Lantai<p>
  <legenda class='e8'></legenda>Maksimum 10 Lantai<p>
  <legenda class='e9'></legenda>Lain-Lain<p>
</div>

 

 




<div id="p_pola" class="legendaKiri">  
   
                <legenda class='r1'></legenda>kawasan pariwisata<p>
                <legenda class='r2'></legenda>kawasan kesehatan<p>
                <legenda class='r3'></legenda>kawasan pendidikan<p>
                <legenda class='r4'></legenda>iplt<p>
                <legenda class='r5'></legenda>kawasan militer<p>
                <legenda class='r6'></legenda>kawasan permukiman<p>
                <legenda class='r7'></legenda>ruang terbuka hijau<p>
                <legenda class='r8'></legenda>kawasan pemakaman<p>
                <legenda class='r9'></legenda>kawasan evakuasi bencana<p>
                <legenda class='r10'></legenda>jalan<p>
                <legenda class='r11'></legenda>sungai<p>
                <legenda class='r12'></legenda>kawasan pertanian<p>
                <legenda class='r13'></legenda>kawasan industri (pendukung perdagangan / jasa)<p>
                <legenda class='r14'></legenda>kawasan perdagangan / Jasa<p>
                <legenda class='r15'></legenda>kawasan Olahraga<p>
                <legenda class='r16'></legenda>kawasan perkantoran<p>
                <legenda class='r17'></legenda>kawasan peribadatan<p>
                <legenda class='r18'></legenda>kawasan perlindungan terhadap kawasan Bawahannya<p> 
                <legenda class='r19'></legenda>kawasan terminal<p> 
                <legenda class='r20'></legenda>Lain-Lain<p>
</div>


