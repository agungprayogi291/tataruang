                 
<div id="msg" class="alert alert-danger  justify-content-center collapse" accesskey="" style="margin-top:5px;width: 100%;
    min-height: 35px;
    padding-top: 2px;
    margin-bottom:5px; 
    text-align: center;">
</div>      

<form class="form-row-seperated" action="#" name ="frm" id="frm" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="form-horizontal form-row-seperated">                                
     
    <div class="form-group input-group input-group-sm">
        <div class="form-group input-group input-group-sm <?php //echo $master; ?>">
                    <label for="id" class="col-sm-4 col-form-label">Id</label> 
                    <!--<input type="text" class="form-control col-sm-2" name ="id" id="id" readonly="true">-->
                    <input type="text" class="form-control col-sm-2" name ="id" id="id">
                    <input type="hidden"  name ="kode" id="kode">
                    
        </div>

        <div class="form-group input-group input-group-sm">
                    <label for="nama" class="col-sm-4 col-form-label">Nama</label> 
                    <input type="text" class="form-control col-sm-8" name ="nama" id="nama">
        </div>
        <br>
        <div class="form-group input-group input-group-sm">
            <label for="induk" class="col-sm-4 col-form-label">Induk</label> 
            <select class="form-control select2 col-sm-8 " name ="induk" id="induk" style="height: 31px; width:65%;" >                    
            <?php echo $induk ;?> 
            </select> 
        </div> 

        <div class="form-group input-group input-group-sm <?php //echo $master; ?>">
                    <label for="tabel" class="col-sm-4 col-form-label">Tabel</label> 
                    <input type="text" class="form-control col-sm-8" name ="tabel" id="tabel"> 
        </div>   

        <div class="form-group input-group input-group-sm <?php //echo $master; ?>">
                    <label for="jenis" class="col-form-label col-sm-4 ">Nama Field kondisi - Kodisi</label> 
                    <input type="text" class="form-control col-sm-2" name ="jenis" id="jenis">
                    &nbsp;&nbsp;
                    <input type="text" class="form-control col-sm-4" name ="fieldKondisi" id="fieldKondisi">
        </div>
        <div class="form-group input-group input-group-sm <?php //echo $master; ?>">
            <label for="jenis" class="col-form-label col-sm-4 ">Field tampil</label> 
            <input type="text" class="form-control col-sm-8" name ="fielde" id="fielde">
        </div>
    </div>

    
    <div class="input-group input-group-sm">
        <label for="warna" class="col-sm-4 col-form-label">Symbologi Warna</label> 
        
        <div class="input-group-sm col-sm-4"> 
        <input type="color" class="form-control col-sm-3" name ="warna" id="warna"> &nbsp;&nbsp;
        <input type="checkbox" name="random" id="random" >        
        <label for=""> Random</label>       
        </div>
       
        <div class="input-group-sm col-sm-4"> 
        <label for="opacity">Opacity</label>   
        <input type="text" class="col-sm-3 form-control" name ="opacity" id="opacity">  &nbsp;&nbsp;
        </div>
            
    </div>
    <br>


    <div class="form-group input-group input-group-sm">
        <label for="urutan" class="col-sm-4 col-form-label">Urutan</label> 
        <input type="text" class="form-control col-sm-2" name ="urutan" id="urutan">
        <label for="umum" class="col-sm-2 col-form-label">Akses Publik</label> 
        <select class="form-control select2  col-sm-4" name ="umum" id="umum">
            <option value="1">Publish</option>
            <option value="0">Non Publish</option>                                 
        </select> 
    </div>
                                                                                      
    <div class="input-group input-group input-group-lg mb-3 <?php //echo $master; ?>">
        <label for="aktif" class="col-sm-4 col-form-label">Status</label> 
        <select class="form-control  select2  col-sm-4 form-control-lg"  name ="aktif" id="aktif">
            
            <option value="1">Aktif </option>
            <option value="0">Non Aktif </option>                                 
        </select> 
                    
    </div> 

     
</form>     
                
  
<div class="modal-footer justify-content-between">
    <div class="col-sm-4">

    </div>
    <div class="col-sm-8">
    <?php if($this->session->userdata('user_level')==1){ ?>
    <button type="button" id='btn_hapus' class="btn btn-danger" data-dismiss="modal">Hapus</button>   
    <?php  } ?>
    
    <button type="button" id='btn_reset' class="btn btn-warning" data-dismiss="modal">Reset</button>
    <button type="button" id="btn_simpan" class="btn btn-primary">Simpan</button>  
    </div>
</div>  
