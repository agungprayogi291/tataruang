<html>
<head>
    <style></style>
</head>
<body>
<htmlpagefooter name="myFooter1" style="display:none;">
        <table>
            <tr>
                <td width="33%" style="font-style:italic;font-size:10px;">
                    {DATE j-m-y}
                </td>
                <td width="33%" style="text-align:right; font-size:10px; padding-right:0;">
                {PAGENO}/{nbpg}

                </td>
                
            </tr>
        </table>        
    </htmlpagefooter>
    <div class="kepada">
        <p>Kepada</p>
        <p>Yth. Walikota Magelang</p>
        <p>Cq. Kepala Dinas Pekerjaan Umum dan Penataan Ruang</p>
        <p style="margin-left: 30px;">Jl. Jend. Sudirman No. 54, Kel. Magersari, Kec. Magelang Selatan, Kota Magelang 56126</p>
        <p style="margin-left: 30px;">Telp. (0293) 362542</p>
        <p style="margin-left: 30px;">di <u><b>MAGELANG</b></u></p>
    </div>

    <p>Yang bertanda tangan di bawah ini :</p>

    <table style="width: 100%;">
        <tr>
            <td width="30px">1.</td>
            <td width="35%">Nama Pemohon</td>
            <td width="20px"> : </td>
            <td colspan="2"><?= $cetak->namapemohon ?></td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Alamat</td>
            <td> : </td>
            <td>Jalan <?= $cetak->jalanpemohon ?></td>
            <td>RT <?= $cetak->rtpemohon ?> / RW <?= $cetak->rwpemohon ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Desa / Kelurahan <?= $cetak->kelurahanpemohon ?></td>
            <td>kecamatan <?= $cetak->kecamatanpemohon ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="2">Kabupaten <?= $cetak->kabupatenpemohon ?></td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Telepon</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->notelppemohon ?></td>
        </tr>
        <tr>
            <td>4.</td>
            <td>Nomor KTP</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->noktppemohon ?></td>
        </tr>
        <tr>
            <td>5.</td>
            <td>Pekerjaan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->pekerjaanpemohon ?></td>
        </tr>
    </table>

    <p>Dengan ini mengajukan permohonan pemeriksaan persyaratan teknis PBG untuk pembangunan baru/rehabilitasi/renovasi/pelestarian/balik nama/Pemecahan)* bangunan gedung/prasarana bangunan gedung)*</p>

    <table style="width: 100%;">
        <tr>
            <td width="30px">1.</td>
            <td colspan="5">Bangunan Gedung</td>
        </tr>
        <tr>
            <td></td>
            <td width="30px">a.</td>
            <td width="30%">Fungsi Utama</td>
            <td width="20px"> : </td>
            <td colspan="2"><?= $cetak->fungsigedung ?></td>
        </tr>
        <tr>
            <td></td>
            <td>b.</td>
            <td>Pondasi</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->pondasigedung ?></td>
        </tr>
        <tr>
            <td></td>
            <td>c.</td>
            <td>Dinding</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->dindinggedung ?></td>
        </tr>
        <tr>
            <td></td>
            <td>d.</td>
            <td>Lantai</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->lantaigedung ?></td>
        </tr>
        <tr>
            <td></td>
            <td>e.</td>
            <td>Kusen</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->kusengedung ?></td>
        </tr>
        <tr>
            <td></td>
            <td>f.</td>
            <td>Rangka Atap</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->rangkaatapgedung ?></td>
        </tr>
        <tr>
            <td></td>
            <td>g.</td>
            <td>Atap</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->atapgedung ?></td>
        </tr>
        <!-- //////////////////////////////////////////////////////// -->
        <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td width="30px">2.</td>
            <td colspan="5">Lokasi bangunan gedung</td>
        </tr>
        <tr>
            <td></td>
            <td>a.</td>
            <td>Nama Jalan/Kampung</td>
            <td> : </td>
            <td><?= $cetak->namalokasi ?></td>
            <td>RT <?= $cetak->rtlokasi ?> / RW <?= $cetak->rwlokasi ?></td>
        </tr>
        <tr>
            <td></td>
            <td>b.</td>
            <td>Kelurahan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->kelurahanlokasi ?></td>
        </tr>
        <tr>
            <td></td>
            <td>c.</td>
            <td>Kecataman</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->kecamatanlokasi ?></td>
        </tr>
        <tr>
            <td></td>
            <td>d.</td>
            <td>Kota</td>
            <td> : </td>
            <td colspan="2">Magelang</td>
        </tr>
        <!-- //////////////////////////////////////////////////////// -->
        <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td width="30px">3.</td>
            <td colspan="5">Tanah</td>
        </tr>
        <tr>
            <td></td>
            <td>a.</td>
            <td>Luas Tanah</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->luastanah ?> m<sup>2</sup></td>
        </tr>
        <tr>
            <td></td>
            <td>b.</td>
            <td>Status Hak Tanah</td>
            <td> : </td>
            <td><?= $cetak->haktanah ?></td>
            <td>Nomor : <?= $cetak->notanah ?></td>
        </tr>
        <tr>
            <td></td>
            <td>c.</td>
            <td>Nama pemilik tanah</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->pemiliktanah ?></td>
        </tr>
        <tr>
            <td></td>
            <td>d.</td>
            <td>Batas-batas tanah</td>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>- Sebelah timur</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->batastanahtimur ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>- Sebelah selatan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->batastanahselatan ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>- Sebelah barat</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->batastanahbarat ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>- Sebelah utara</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->batastanahutara ?></td>
        </tr>
        <!-- //////////////////////////////////////////////////////// -->
        <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td width="30px" style="height:30px">4.</td>
            <td colspan="4">Lampiran</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->fcsertifikattanah > 0 ? "√" : ""); ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Fotokopi Sertifikat Tanah</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->fcktp > 0 ? "√" : "" );?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Fotokopi KTP Pemohon / Pemilik Bangunan / Pemilik Tanah</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->teknisbangunan > 0 ? "√" : ""); ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Gambar rencana teknik bangunan gedung dan/atau prasarana bangunan gedung</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->petalokasi > 0 ? "√" : ""); ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Peta lokasi, SITEPLAN, denah bangunan, denah sanitasi-drainase, Tampal 4 sisi, Potongan </td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->detailstruktur > 0 ? "√" : ""  ) ;?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Detail struktur pondasi, kolom dan balok dan atap untuk bangunan bertingkat</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->sistemutilitas > 0 ? "√" : "" );?>
            </td>
            <td colspan="4" style="padding-left: 10px;">Sistem utilitas, sistem penanggulangan kebakaran dan gambar kerja untuk bangunan kepentingan umum</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->pbglama > 0 ? "√" : "" );?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">PBG lama untuk Bangunan Renovasi</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->pkkpr > 0 ? "√" : "" );?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">PKKPR (Persetujuan Kesesuaian Kegiatan Pemanfaatan Ruang)</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->doklinkungan > 0 ? "√" : "" );?>
            </td>
            <td colspan="4" style="padding-left: 10px;">Dokumen Lingkungan dari Dinas Lingkungan Hidup Kota Magelang untuk bangunan berdampak pada lingkungan</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->rekomendasilalulintas > 0 ? "√" : ""); ?>
            </td>
            <td colspan="4" style="padding-left: 10px;">Rekomendasi Lalu Lintas dari Dinas Perhubungan Kota Magelang untuk bangunan berdampak lalu lintas</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->konstruksibetontulang > 0 ? "√" : "" );?>
            </td>
            <td colspan="4" style="padding-left: 10px;">Perhitungan Konstruksi Beton Bertulang ditandatangani oleh penanggung jawab konstruksi untuk gedung bertingkat</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->konstruksibajaringan > 0 ? "√" : "" );?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Perhitungan Konstruksi Baja/Baja Ringan ditandatangani oleh Penanggun jawab konstruksi</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= ($cetak->suratpenyelidikantanah > 0 ? "√" : "" );?>
            </td>
            <td colspan="4" style="padding-left: 10px;">Surat hasil penyelidikan Tanah/sondir ditandatangani oleh penanggung jawab untuk bangunan 4 lantai</td>
        </tr>
    </table>

    <!-- √ -->

    <!-- <div class="cekbox">√</div> -->


    <p>Demikian permohonan ini kami ajukan untuk dapat diproses sebagaimana ketentuan yang berlaku.</p>

    <table style="width: 100%;">
        <tr>
            <td width="60%"></td>
            <td>
                <table class="center">
                    <tr>
                        <td>Magelang,.......................20....</td>
                    </tr>
                    <tr>
                        <td>PEMOHON</td>
                    </tr>
                    <tr>
                        <td style="height: 50px;"></td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px dotted black;"><?= $cetak->namapemohon ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div style="page-break-before: always;"></div>

    <h1 style="text-align:center">SURAT PERNYATAAN</h1>
    <p>Yang bertandatangandibawahini :</p>
    <table>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td><?= $cetak->namapemohon ?></td>
        </tr>
        <tr>
            <td>Tempat/tanggal lahir:</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Alamat </td>
            <td>:</td>
            <td>Jalan <?= $cetak->jalanpemohon ?>
            RT <?= $cetak->rtpemohon ?> / RW <?= $cetak->rwpemohon ?><br>
            Desa/Kelurahan <?= $cetak->kelurahanpemohon ;?>
            Kecamatan <?= $cetak->kecamatanpemohon ;?><br>
            Kabupaten/Kota Magelang
            </td>
        </tr>
        <tr>
            <td>Telepon</td>
            <td>:</td>
            <td><?= $cetak->notelppemohon ?> hp.............</td>
        </tr>
        <tr>
            <td>Nomor KTP</td>
            <td>:</td>
            <td><?= $cetak->noktppemohon ?></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>:</td>
            <td><?= $cetak->pekerjaanpemohon ?></td>
        </tr>
    </table>
    <p>Selaku pemilik tanah / bangunan berkenaan dengan Surat Permohonan Pemeriksaan Persyaratan Teknis PBG Bangunan…………………………………………………………………….
    <br>yang terletak di :    
    </p>
    <table>
    <tr>
        <td>
        Nama Jalan/kampung
        </td>
        <td>:</td>
        <td><?= $cetak->namalokasi ;?></td>
    </tr>
    <tr>
        <td>
            RT / RW
        </td>
        <td>:</td>
        <td>
            RT 
        <?= $cetak->rtlokasi ;?> RW <?= $cetak->rwlokasi ;?>
        </td>
    </tr>
    <tr>
        <td>
            Kelurahan
        </td>
        <td>
            :
        </td>
        <td>
            <?= $cetak->kelurahanlokasi ;?>
        </td>
    </tr>
    <tr>
        <td>
            Kecamatan
        </td>
        <td>
            :
        </td>
        <td>
            <?= $cetak->kecamatanlokasi ;?>
        </td>
    </tr>
    <tr>
        <td>
            Kecamatan
        </td>
        <td>
            :
        </td>
        <td>
        <br>Magelang....................
        </td>
    </tr>
    <tr>
        <td>
            Kota
        </td>
        <td>
            :
        </td>
        <td>
            Magelang
        </td>
    </tr>
    <tr>
        <td>Diatas Tanah Itu</td>
    </tr>
    <tr>
        <td>
            Status hak atas tanah
        </td>
        <td>
            :
        </td>
        <td>
            <?= $cetak->statustanah ;?>
        </td>
        <td>
            nomor
        </td>
        <td>
            :
        </td>
        <td>
            <?= $cetak->notanah ;?>
        </td>
    </tr>
    <tr>
        <td>
            Luas Tanah
        </td>
        <td>
            :
        </td>
        <td>
            <?= $cetak->luastanah ;?>m2
        </td>
    </tr>
    <tr>
        <td>Nama Pemilik tanah</td>
        <td>
            :
        </td>
        <td><?= $cetak->pemiliktanah;?></td>
    </tr>
    </table>
    <p> Menyatakandenganini bahwa saya:</p>
    <ul style="list-style:none;">
        <li>
            1. Menjamin tanah dan bangunan gedung di lokasi tersebut tidak dalam sengketa / perkara. Oleh karena itu bilamana permohonan ini disetujui dan apabila di kemudian hari ternyata terjadi sengketa atas tanah dan bangunan, maka kami setuju terhadap surat rekomendasi yang diberikan untuk dibatalkan tanpa menuntut penggantian atas seluruh biaya atau yang telah dikeluarkan;
        </li>
        <li>
            2. Sanggup dan bersedia membongkar sendiri bangunan / bagian bangunan apabila sewaktu-waktu Pemerintah Kota Magelang menerapkan peraturan dan ketentuan berkenaan dengan tata ruang yang berlaku, antara lain berupa pelebaran jalan, penertiban garis sempadan,  penyediaan jalur hijau / ruang terbuka hijau;
        </li>
        <li>
            3. Sanggup menyediakan kebutuhan area parkir kendaraan maupun ruang terbuka baik halaman paving atau taman / pohon peneduh di dalam lokasi persil, sesuai dengan ketentuan yang berlaku;
        </li>
        <li>
            4. Sanggup menyediakan sistem sanitasi, sistem drainase dan sumur resapan air;
        </li>
        <li>
            5. Bertanggungjawab penuh terhadap pembangunan dan/atau bangunan yang telah berdiri baik menyangkut kekuatan konstruksi, kekokohan dan kualitas struktur bangunan serta keselamatan umum dilingkungan sekitarnya dari kegagalan konstruksi;
        </li>
    </ul>


    <table style="width: 100%;">
        <tr>
            <td width="60%">Pemohon</td>
            <td>
                <table class="center">
                    <tr>
                        <td>...........,...........,............,....</td>
                    </tr>
                    <tr>
                        <td>Disetujui Pemilik Tanah</td>
                    </tr>
                    <tr>
                        <label for="">Materai 10.000</lp>
                        <td style="height: 50px;"></td>
                    </tr>
                    <tr>
                        <td>(.......................)<br>(..........................)</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>