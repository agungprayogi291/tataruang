<script>
    $(document).ready(function(e){
        loadData()
        $("#btn_status").click(function(e){
            updateStatus()
        })
        $("#form-filter").submit(function(e){
            e.preventDefault()
            loadData()
        })
        $("#btn-filter").click(function(e){
            if($("#container-filter").hasClass("collapse")){
                $("#container-filter").removeClass("collapse")
            }else{
                $("#container-filter").addClass("collapse")
            }
        })
    })

    function loadData(){
        $.ajax({
            type:"POST",
            url:"<?= base_url() ;?>list-permohonan-pbg",
            dataType:"JSON",
            data : $("#form-filter").serialize(),
            success:function(data){
                if(data.row > 1){
                    RefreshTable($('#tabel_surat'), $('#show_surat'), data.tabel, data.row);
                }else{
                    RefreshTable($('#tabel_surat'),$('#show_surat'),IsiDataTabelKosong(7),1);
                }
            }
        })
    }
    function cetakUlang(kode){
        $("#kode_form").val(kode)
        $("#formCetak").trigger('submit')
    }

    function openStatus(kode){
        $.ajax({
            type:"POST",
            url:"<?= base_url();?>permohonan-pbg-get",
            data:{
                kode : kode
            },
            dataType:"JSON",
            success : function(response){
         
            let timerInterval
            Swal.fire({
            title: 'Sebentar!',
            html: 'I will close in <b></b> milliseconds.',
            timer: 1000,
            timerProgressBar: true,
            didOpen: () => {
                Swal.showLoading()
                const b = Swal.getHtmlContainer().querySelector('b')
                timerInterval = setInterval(() => {
                b.textContent = Swal.getTimerLeft()
                }, 10)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
            }).then((result) => {
            /* Read more about handling dismissals below */
                if(response.data == null){
                    $("#form-status")[0].reset()
                    $("#id_permohonan").val(kode)
                }else{
                    $("#id_permohonan").val(kode)
                    $("#tanggal_status").val(response.data.tanggal)
                    $("#keterangan_status").val(response.data.keterangan)
                    $("#level_status").val(response.data.status).trigger("change")
                }
                $("#statusPermohonan").modal("show")    
            })
                    
              
            }
        })
        
    }

    function updateStatus(){
        let form = $("#form-status").serialize()
        $.ajax({
            type:"POST",
            url : "<?= base_url() ;?>permohonan-pbg-update-status",
            data : form,
            dataType:"JSON",
            success: function(response){
                if(response.success){
                    // $("#form-status")[0].reset()
                    Swal.fire(
                        "success",
                        `${response.message}`,
                        "success"
                    ).then(e=>{
                        loadData()    
                    })
                }else{
                    Swal.fire(
                        "failed",
                        `${response.message}`,
                        "error"
                    );
                }
            }
        })
    }


    function cetakUlangKuasa(kode){
        $("#kode_form_kuasa").val(kode)
        $("#formCetakKuasa").trigger('submit')
    }

    function openDetail(kode){
        $.ajax({
            type:"POST",
            url :"<?= base_url() ;?>permohonan-pbg-get-surat",
            dataType:"JSON",
            data: { 
                kode: kode
            },
            success: function(response){
                $("#isi").html(response.detail)
                $("#content-image").html(response.img)
                $("#indicator").html(response.indicator)
                // $("#list-lampiran").html(response.lampiran)
                $("#largeModal").modal("show")
            }
        })
    }

</script>