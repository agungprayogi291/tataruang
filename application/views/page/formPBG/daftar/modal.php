

  <!-- modal -->
  <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" width="100%">
    <div class="modal-dialog modal-lg" width="100%">
        
      <div class="modal-content" style="border-top:8px solid #03518A; padding:4px;">
        <div class="modal-body">
           <!-- carousel --> 
           
            <table class="table table-bordered" style="padding:2px;" width="400px">
                <tbody id="isi">
                </tbody>    
            </table>
            <h4 class="mx-2">Lampiran :</h4>
           <div class="row">
                <div class="col-md-6 mx-auto">
                    <div id='carouselExampleIndicators' class='carousel slide'data-ride='carousel' style="height:200px;" class="mx-5">
                        <ol class='carousel-indicators' id="indicator">
                            
                        </ol>
                        <div class='carousel-inner mx-5' id="content-image">
                        
                        </div>
                        <a
                            class='carousel-control-prev'
                            href='#carouselExampleIndicators'
                            role='button'
                            data-slide='prev'
                            >
                            <span class='carousel-control-prev-icon'
                                    aria-hidden='true'
                                    ></span>
                            <span class='sr-only'>Previous</span>
                        </a>
                        <a
                            class='carousel-control-next'
                            href='#carouselExampleIndicators'
                            role='button'
                            data-slide='next'
                            >
                            <span
                                    class='carousel-control-next-icon'
                                    aria-hidden='true'
                                    ></span>
                            <span class='sr-only'>Next</span>
                        </a>
                    </div>
                </div>
           </div>

          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <style>
      .img-size{
    /* 	padding: 0;
        margin: 0; */
        height: 150px;
        width: 300px;
        background-size: cover;
        overflow: hidden;
    }
    .modal-content {
        width:100%;
        border:none;
    }
    .modal-body {
        padding: 0;
    }

    .carousel-control-prev-icon {
        background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23009be1' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E");
        width: 30px;
        height: 48px;
    }
    .carousel-control-next-icon {
        background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23009be1' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
        width: 30px;
        height: 48px;
    }
    
  </style>