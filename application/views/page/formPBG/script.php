<script type="text/javascript">
    jQuery(document).ready(function() {
        // console.log('ok')
        $("#formIRK input").prop("readonly", true)
        $("input[type=radio][name='bertindakUntuk']").change(function() {
            if (this.value == '1') {
                $('#bertindakAtasNama').addClass('d-none')
                $('#bertindakAtasNama').val('')

                if ($('#namaPemohon').val() == '') {
                    alert("Mohon isi nama pemohon")
                    this.checked = false;
                } else {
                    $('#bertindakAtasNama').val($('#namaPemohon').val())
                }
            } else {
                $('#bertindakAtasNama').removeClass('d-none')
                $('#bertindakAtasNama').val('')
            }
        })

        $('input[type=checkbox]').change(function() {
            let elm = $(this).next().next().toggleClass('d-none')
            let requiredFile = $(this).attr("data-file")
            if (elm.hasClass("d-none") == false) {
                $("[name='" + requiredFile + "']").prop('required', true)
            } else {
                $("[name='" + requiredFile + "']").removeAttr('required')
            }
        })


        $('#statusTanah').change(function() {
            $('#statusLain').val('')
            if (this.value == 'Lainnya') {
                $('#statusLain').removeClass('d-none')

            } else {
                $('#statusLain').addClass('d-none')

            }
        })
        $('#selectRencana').change(function() {
            $('#rencanaLain').val('')
            if (this.value == 'lainnya') {
                $('#rencanaLain').removeClass('d-none')
            } else {
                $('#rencanaLain').addClass('d-none')
            }
        })
        $("#form-verif").submit(function(e) {
            e.preventDefault()
            verifikasiTelephone();
        })
        $('#formIRK').submit(function(e) {
            e.preventDefault();

            // console.log(new FormData(this))

            $.ajax({
                url: '<?= base_url('FormSuratPBG/simpan') ?>',
                type: 'POST',
                dataType: 'json',
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                success: function(response) {
                    let html = ''
                    if (response.success == 1) {
                        Swal.fire(
                            'Good Job!',
                            `${response.message}`,
                            'success'
                        ).then(e => {
                            for (const prop in response.data) {
                                html += `<input type="text" name="${prop}" value="${response.data[prop]}">`
                            }

                            $("#formCetak").html(html)
                            $("#formCetak").trigger('submit');
                            $("#formIRK")[0].reset();
                            window.location.reload()
                        })
                    } else {
                        Swal.fire(
                            'warning!',
                            `${response.message}`,
                            'warning'
                        )
                    }
                }
            })
        })

        $('input[type=file]').change(validateInputFile);
    })

    function validateInputFile() {
        const allowedExtensions = ['pdf', 'docx', 'xlsx', 'jpeg', 'png', 'doc', 'mkv'],
            sizeLimit = 2000000;

        const {
            name: fileName,
            size: fileSize
        } = this.files[0];
        const fileType = fileName.split(".").pop();

        if (!allowedExtensions.includes(fileType)) {
            alert("Tipe file tidak diijinkan")
            this.value = null
        } else if (fileSize > sizeLimit) {
            alert("File tidak boleh berukuran lebih dari 2MB")
            this.value = null
        }
    }

    function verifikasiTelephone() {
        $.ajax({
            type: "POST",
            url: "<?= base_url(); ?>verification-pbg",
            dataType: "JSON",
            data: $("#form-verif").serialize(),
            success: function(response) {
                if (response.access) {
                    $("#formIRK input").prop("readonly", false)
                    setAlert("alert alert-success", response.message, $("#msg-verif"))
                    $("#no_telp_verif").prop("readonly", true)
                    $("#btn-verif").prop("disabled", true)
                    $("#verifikasi_telp").val($("#no_telp_verif").val())
                } else {
                    let attr = $("#formIRK input").attr("readonly")
                    if (attr == undefined && attr == false) {
                        $("#formIRK input").prop("readonly", true)
                    }
                    setAlert("alert alert-danger", response.message, $("#msg-verif"))
                }
            }
        })
    }
</script>