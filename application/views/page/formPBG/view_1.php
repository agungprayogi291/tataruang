<section class="content">
    <div class="container">
        <div class="col-md-10 col-sm-12 mx-auto">
            <div class="shadow card my-3">
                <div class="card-header">TEKNIK PERSETUJUAN BANGUNAN GEDUNG</div>
                <div class="card-body">
                    <form id="formIRK" enctype='multipart/form-data'>
                        <!-- Identitas Pemohon -->
                        <div class="mt-3">
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="namaPemohon" class=" col-form-label">Nama Pemohon</label>
                                </div>
                              
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="namaPemohon" class="form-control" style="line-height: 1.5; padding: 6px 12px;" id="namaPemohon" placeholder="Masukan nama pemohon">
                                </div>
                            </div>
                            <div class=" form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="alamatPemohon" class=" col-form-label">Alamat</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="jalanPemohon" placeholder="Jalan" class="form-control" style="line-height: 1.5; padding: 6px 12px;" id="alamatPemohon">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="number" name="rtPemohon" placeholder="RT" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="number" name="rwPemohon" placeholder="RW" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kelurahanPemohon" placeholder="Desa/Kelurahan" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kecamatanPemohon" placeholder="Kecamatan" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="kabupatenPemohon" placeholder="Kabupaten/Kota" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="noTelpPemohon" class=" col-form-label">Telepon</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="noTelpPemohon" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Masukan no telepon" id="noTelpPemohon">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="noktpPemohon" class=" col-form-label">Nomor KTP</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="noktpPemohon" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Masukan no telepon" id="noktpPemohon">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="pekerjaanPemohon" class=" col-form-label">Pekerjaan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="pekerjaanPemohon" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Pekerjaan" id="pekerjaanPemohon">
                                </div>
                            </div>
                        </div>
                        <!-- Identitas Pemohon -->

                        <hr class="my-5">
                        <div class="card-header my-5">
                            <p>dengan ini mengajukan permohonan pemeriksaan persyaratan teknis PBG untuk pembangunan baru/rehabilitasi/renovasi/ pelestarian/balik nama / Pemecahan)* bangunan gedung/prasarana bangunan gedung)* </p>
                        </div>
                        <!-- Bangunan Gedung -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">1.Bangunan Gedung</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="fungsiGedung" class=" col-form-label">Fungsi Utama</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="fungsiGedung" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Fungsi gedung" id="fungsiGedung">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="pondasiGedung" class=" col-form-label">Pondasi</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="pondasiGedung" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Pondasi gedung" id="pondasiGedung">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="dindingGedung" class=" col-form-label">Dinding</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="dindingGedung" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Dinding gedung" id="dindingGedung">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="lantaiGedung" class=" col-form-label">Lantai</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="lantaiGedung" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Lantai gedung" id="lantaiGedung">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="kusenGedung" class=" col-form-label">Kusen</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="kusenGedung" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Kusen gedung" id="kusenGedung">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="rangkaAtapGedung" class=" col-form-label">Rangka Atap</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="rangkaAtapGedung" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Rangka atap gedung" id="rangkaAtapGedung">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="atapGedung" class=" col-form-label">Atap</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="atapGedung" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Atap gedung" id="atapGedung">
                                </div>
                            </div>
                        </div>
                        <!-- Bangunan Gedung -->

                        <hr class="my-5">

                        <!-- Lokasi Bangunan Gedung -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">2.Lokasi Bangunan Gedung</h5>
                            <div class=" form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="alamatPemohon" class=" col-form-label">Nama Lokasi</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="namaLokasi" placeholder="Jalan" class="form-control" style="line-height: 1.5; padding: 6px 12px;" id="alamatPemohon">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="number" name="rtLokasi" placeholder="RT" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="number" name="rwLokasi" placeholder="RW" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="kelurahanLokasi" class=" col-form-label">Kelurahan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="kelurahanLokasi" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Masukan kelurahan bangunan gedung" id="kelurahanLokasi">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="kecamatanLokasi" class=" col-form-label">Kecamatan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="kecamatanLokasi" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Masukan kecamatan bangunan gedung" id="kecamatanLokasi">
                                </div>
                            </div>
                        </div>
                        <!-- Lokasi Bangunan Gedung -->

                        <hr class="my-5">

                        <!-- Lokasi Lahan -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">3.Tanah</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="luasTanah" class=" col-form-label">Luas Tanah</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="input-group">
                                        <div class="col-xs-8 col-md-8">
                                            <input type="text" name="luasTanah" class="form-control" id="luasTanah" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                        <div class="col-xs-4 col-md-4 input-group-append">
                                            <span class="input-group-text" id="basic-addon2">m<sup>2</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="statusTanah" class=" col-form-label">Status Tanah</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <select class="form-control form-control-lg" id="statusTanah" name="statusTanah">
                                        <option selected vlaue="">-- Status Tanah --</option>
                                        <option value="HM">HM</option>
                                        <option value="HGB">HGB</option>
                                        <option value="HP">HP</option>
                                        <option value="HPL">HPL</option>
                                        <option value="Lainnya">Lainnya</option>
                                    </select>
                                    <div class="row mt-2">
                                        <div class="col-md-12 mt-1">
                                            <input type="text" name="statusLain" placeholder="Masukan status" class="form-control d-none" style="line-height: 1.5; padding: 6px 12px;" id="statusLain">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label class=" col-form-label">No Tanah</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="noTanah" class="form-control" placeholder="No Tanah" style="line-height: 1.5; padding: 6px 12px;">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label class=" col-form-label">Nama Pemilik</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="pemilikTanah" class="form-control" placeholder="Pemilik tanah" style="line-height: 1.5; padding: 6px 12px;">
                                </div>
                            </div>
                        </div>
                        <!-- Lokasi Lahan -->

                        <hr class="my-5">

                        <!-- Batas-batas Tanah -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">Batas-batas tanah</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="batasTanahUtara" class=" col-form-label">Utara</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="batasTanahUtara" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Batas utara tanah" id="batasTanahUtara">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="batasTanahSelatan" class=" col-form-label">Selatan</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="batasTanahSelatan" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Batas selatan tanah" id="batasTanahSelatan">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="batasTanahTimur" class=" col-form-label">Timur</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="batasTanahTimur" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Batas timur tanah" id="batasTanahTimur">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="batasTanahBarat" class=" col-form-label">Barat</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="batasTanahBarat" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Batas barat tanah" id="batasTanahBarat">
                                </div>
                            </div>
                        </div>
                        <!-- Batas-batas Tanah -->

                        <hr class="my-5">

                        <!-- Lampiran permohonan -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">4.Lampiran Permohonan</h5>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fcSertifikatTanah" id="fcSertifikatTanah">
                                <label class="custom-control-label" for="fcSertifikatTanah">Fotokopi Sertifikat Tanah</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcSertifikatTanah">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fcKTP" id="fcKTP">
                                <label class="custom-control-label" for="fcKTP">Fotokopi KTP Pemohon</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcKTP">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="teknisBangunan" id="teknisBangunan">
                                <label class="custom-control-label" for="teknisBangunan">Gambar rencana teknik bangunan gedung dan / atau prasarana bangunan gedung</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="fileteknisBangunan">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="petaLokasi" id="petaLokasi">
                                <label class="custom-control-label" for="petaLokasi">Peta lokasi, SITEPLAN, denah bangunan, denah sanitasi-drainase, Tampak 4 sisi</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filepetaLokasi">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="detailStruktur" id="detailStruktur">
                                <label class="custom-control-label" for="detailStruktur">Detail struktur pondasi, kolom dan balok dan atap untuk bangunan bertingkat</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filedetailStruktur">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="sistemUtilitas" id="sistemUtilitas">
                                <label class="custom-control-label" for="sistemUtilitas">Sistem utilitas (ME), sistem penanggulangan kebakaran dan gambar kerja untuk baangunan kepentingan umum</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filesistemUtilitas">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="pbgLama" id="pbgLama">
                                <label class="custom-control-label" for="pbgLama">PBG lama untuk Bangunan Renovasi / Pemecahan PBG</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filepbgLama">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="pkkpr" id="pkkpr">
                                <label class="custom-control-label" for="pkkpr">PKKPR (Persetujuan Kesesuaian Kegiatan Pemanfaatan Ruang)</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filepkkpr">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="dokLinkungan" id="dokLinkungan">
                                <label class="custom-control-label" for="dokLinkungan">Dokumen Lingkungan dari Dinas Lingkungan Hidup Kota Magelang untuk bangunan berdampak pada lingkungan</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filedokLinkungan">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="rekomendasiLaluLintas" id="rekomendasiLaluLintas">
                                <label class="custom-control-label" for="rekomendasiLaluLintas">Rekomendasi Lalu Lintas dari Dinas Perhubungan Kota Magelang untuk bangunan berdampak lalu-lintas</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filerekomendasiLaluLintas">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="konstruksiBetonTulang" id="konstruksiBetonTulang">
                                <label class="custom-control-label" for="konstruksiBetonTulang">Perhitungan Konstruksi Beton Bertulang ditandatangani oleh penangguna jawab konstruksi untuk bangunan bertingkat</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filekonstruksiBetonTulang">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="konstruksiBajaRingan" id="konstruksiBajaRingan">
                                <label class="custom-control-label" for="konstruksiBajaRingan">Perhitungan Konstruksi baja/Baja Ringan ditandatangani oleh penanggung jawab konstruksi</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filekonstruksiBajaRingan">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="suratPenyelidikanTanah" id="suratPenyelidikanTanah">
                                <label class="custom-control-label" for="suratPenyelidikanTanah">Surat hasil Penyelidikan Tanah/sondir ditandatangani oleh penanggung jawab untuk bangunan 4 lantai atau lebih</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filesuratPenyelidikanTanah">
                            </div>
                        </div>
                        <!-- Lampiran permohonan -->

                        <div class="text-center mt-5">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>

                    <form id="formCetak" target="_blank" class="d-none" action="<?= base_url('form-surat-pbg-cetak') ?>" method="POST">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    body{
        background-color:#dce0e8;
    }
</style>

<?php include 'script.php'; ?>