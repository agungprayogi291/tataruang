<script type="text/javascript">
    jQuery(document).ready(function() {
        // console.log('ok')

        $("input[type=radio][name='bertindakUntuk']").change(function() {
            if (this.value == '1') {
                $('#bertindakAtasNama').addClass('d-none')
                $('#bertindakAtasNama').val('')

                if ($('#namaPemohon').val() == '') {
                    // alert("Mohon isi nama pemohon")
                    Swal.fire(
                        "Warning!",
                        "Mohon Isi Nama Pemohon",
                        "warning"
                    )
                    this.checked = false;
                } else {
                    $('#bertindakAtasNama').val($('#namaPemohon').val())
                }
            } else {
                $('#bertindakAtasNama').removeClass('d-none')
                $('#bertindakAtasNama').val('')
            }
        })

        $('input[type=checkbox]').change(function() {
            $(this).next().next().toggleClass('d-none')
        })

        $('#selectRencana').change(function() {
            $('#rencanaLain').val('')
            if (this.value == 'lainnya') {
                $('#rencanaLain').removeClass('d-none')
            } else {
                $('#rencanaLain').addClass('d-none')
            }
        })

        $('#statusTanah').change(function() {
            $('#statusLain').val('')
            if (this.value == 'lainnya') {
                $('#statusLain').removeClass('d-none')
            } else {
                $('#statusLain').addClass('d-none')
            }
        })

        $('#formIRK').submit(function(e) {
            e.preventDefault();
            let data = $('#formIRK').serializeArray();

        $.ajax({
            url: '<?= base_url('FormSuratIRK/simpan') ?>',
            type: 'POST',
            dataType: 'json',
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(response) {
                let html ='';
                if(response.success == 1){
                    Swal.fire(
                        'Good Job!',
                        `${response.message}`,
                        'success'
                    ).then(e=>{
                        // console.log("E");
                        for (const prop in response.data) {
                            html += `<input type="text" name="${prop}" value="${response.data[prop]}">`
                        }

                        $("#formCetak").html(html)
                        $("#formCetak").trigger('submit');
                        $("#formIRK")[0].reset();
                        window.location.reload()
                    })
                }else{
                    Swal.fire(
                    'warning!',
                    `${response.message}`,
                    'warning'
                    )
                }
            }
        })
        })

    })
    function save(){
       
    }
</script>