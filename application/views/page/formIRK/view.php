<section class="content">
    <div class="container">
        <div class="col-md-10 col-xs-12 mx-auto">
            <div class="shadow card my-3">
                <div class="card-header">PERMOHONAN INFORMASI RENCANA KOTA</div>
                <div class="card-body">

                <div class="card">
                    <div class="card-header">
                        <h5>Registrasi</h5>
                    </div>
                    <div class="card-body">
                        <form action="" id="form-verif">
                            <div class="form-group">
                                <label for="">no phone</label>
                                <input type="number" class="form-control" placeholder="08XXXXXXXXXXX..." name="no_telp_verif" id="no_telp_verif">
                                <div class="alert collapse my-2" id="msg-verif"></div>
                            </div>
                            <button class="btn btn-warning" type="submit" id="btn-verif">submit</button>
                        </form>
                        <p class="my-3"><span class="text-info"><i class="bi bi-info-square"></i> informasi :</span>
                        Gunakan No <span class="text-danger">telephone</span>
                        anda , yang digunakan untuk registrasi  pembuatan surat permohonan </p>
                    </div>
                    
                </div>
                <hr>

                    <form id="formIRK">
                        
                        <!-- Identitas Pemohon -->
                        <input type="hidden" name="verifikasi_telp" id="verifikasi_telp">
                        <div class="mt-3">
                            <div class="form-group row mb-5">

                                <div class="col-xs-12 col-md-4">
                                    <label for="namaPemohon" class=" col-form-label">Nama Pemohon</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="namaPemohon" class="form-control" style="line-height: 1.5; padding: 6px 12px;" id="namaPemohon" placeholder="Masukan nama pemohon">
                                    
                                </div>
                            </div>
                            <div class="form-group row mb-5">

                                <div class="col-xs-12 col-md-4">
                                    <label for="" class=" col-form-label">Titik</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    
                                    <input type="text" name="lang" class="col-sm-3 mr-3 form-control" style="line-height: 1.5; padding: 6px 12px;" id="lang" placeholder="lang" value="<?= ($lang != null   ? "$lang"  : ""); ?>">
                                    <input type="text" name="lat" class="col-sm-3 form-control" style="line-height: 1.5; padding: 6px 12px;" id="lat" placeholder="lat" value="<?= ($lat != null  ? "$lat" : ""); ?>">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="bertindakAtasNama" class=" col-form-label">Bertindak untuk dan atas nama</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" value="1" name="bertindakUntuk" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline1">Diri sendiri</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" value="2" name="bertindakUntuk" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline2">Orang lain</label>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-12 mt-1">
                                            <input type="text" class="form-control d-none" id="bertindakAtasNama" name="bertindakAtasNama" placeholder="Isi nama yang bertindak" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="alamatPemohon" class=" col-form-label">Alamat</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="jalanPemohon" placeholder="Jalan" class="form-control" style="line-height: 1.5; padding: 6px 12px;" id="alamatPemohon">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rtPemohon" placeholder="RT" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rwPemohon" placeholder="RW" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kelurahanPemohon" placeholder="Desa/Kelurahan" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kecamatanPemohon" placeholder="Kecamatan" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="kabupatenPemohon" placeholder="Kabupaten/Kota" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="noTelpPemohon" class=" col-form-label">Telepon</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="noTelpPemohon" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Masukan no telepon" id="noTelpPemohon">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="emailPemohon" class=" col-form-label">Email</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="email" name="emailPemohon" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Masukan email" id="emailPemohon">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="nikPemohon" class=" col-form-label">NIK</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="nikPemohon" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Masukan NIK" id="nikPemohon">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="pekerjaanPemohon" class=" col-form-label">Pekerjaan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="pekerjaanPemohon" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Masukan pekerjaan" id="pekerjaanPemohon">
                                </div>
                            </div>
                        </div>
                        <!-- Identitas Pemohon -->

                        <hr class="my-5">

                        <!-- Penguasaan Tanah -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">Penguasaan Tanah</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="statusTanah" class=" col-form-label">Status Tanah</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <select class="custom-select" id="statusTanah" name="statusTanah">
                                        <option selected value="">Open this select menu</option>
                                        <option value="HM">HM</option>
                                        <option value="HGB">HGB</option>
                                        <option value="HP">HP</option>
                                        <option value="HPL">HPL</option>
                                        <option value="lainnya">Lainnya</option>
                                    </select>
                                    <div class="row mt-2">
                                        <div class="col-md-12 mt-1">
                                            <input type="text" name="statusLain" placeholder="Masukan status" class="form-control d-none" style="line-height: 1.5; padding: 6px 12px;" id="statusLain">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="luasTanah" class=" col-form-label">Luas</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="luasTanah" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Masukan luas tanah" id="luasTanah">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="pemilikTanah" class=" col-form-label">Atas Nama</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="pemilikTanah" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Masukan pemilik tanah" id="pemilikTanah">
                                </div>
                            </div>
                        </div>
                        <!-- Penguasaan Tanah -->

                        <hr class="my-5">

                        <!-- Lokasi Lahan -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">Lokasi Lahan</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="lokasiLahan" class=" col-form-label">Jalan / Kampung</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="lokasiLahan" placeholder="Jalan" class="form-control" style="line-height: 1.5; padding: 6px 12px;" id="lokasiLahan">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rtLahan" placeholder="RT" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rwLahan" placeholder="RW" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label class=" col-form-label">Kelurahan</label>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <input type="text" name="kelurahanLahan" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="selectKecamatan" class=" col-form-label">Kecamatan</label>
                                </div>
                                <div class="col-md-8 col-xs-12 ">
                                    <select class="custom-select" id="selectKecamatan" name="kecamatanLahan">
                                        <option selected value="">Open this select menu</option>
                                        <option value="Magelang Utara">Magelang Utara</option>
                                        <option value="Magelang Tengah">Magelang Tengah</option>
                                        <option value="Magelang Selatan">Magelang Selatan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label class=" col-form-label">Kota</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" class="form-control" readonly style="line-height: 1.5; padding: 6px 12px;" value="Magelang">
                                </div>
                            </div>
                        </div>
                        <!-- Lokasi Lahan -->

                        <hr class="my-5">

                        <!-- Batas-batas Tanah -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">Batas-batas tanah</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="batasTanahUtara" class=" col-form-label">Utara</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="batasTanahUtara" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Batas utara tanah" id="batasTanahUtara">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="batasTanahSelatan" class=" col-form-label">Selatan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="batasTanahSelatan" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Batas selatan tanah" id="batasTanahSelatan">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="batasTanahTimur" class=" col-form-label">Timur</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="batasTanahTimur" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Batas timur tanah" id="batasTanahTimur">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="batasTanahBarat" class=" col-form-label">Barat</label>
                                </div>

                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="batasTanahBarat" class="form-control" style="line-height: 1.5; padding: 6px 12px;" placeholder="Batas barat tanah" id="batasTanahBarat">
                                </div>
                            </div>
                        </div>
                        <!-- Batas-batas Tanah -->

                        <hr class="my-5">

                        <!-- Rencana Penggunaan & Pemanfaatan Tanah -->
                        <div class="mt-3">
                            <h5 class="card-title mb-4">Rencana Penggunaan & Pemanfaatan Tanah</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="selectRencana" class=" col-form-label">Rencana</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <select class="form-control form-control-lg" id="selectRencana" name="rencanaPembangunan">
                                        <option selected value="">Open this select menu</option>
                                        <option value="perumahan">Perumahan</option>
                                        <option value="perdagangan dan jasa">Perdagangan dan Jasa</option>
                                        <option value="perkantoran">Perkantoran</option>
                                        <option value="sektor informal">Sektor Informal</option>
                                        <option value="kesehatan">Kesehatan</option>
                                        <option value="pendidikan">Pendidikan</option>
                                        <option value="peribadatan">Peribadatan</option>
                                        <option value="transportasi">Transportasi</option>
                                        <option value="olahraga">Olahraga</option>
                                        <option value="lainnya">Lainnya</option>
                                    </select>
                                    <div class="row mt-2">
                                        <div class="col-md-12 mt-1">
                                            <input type="text" name="rencanaLain" placeholder="Masukan rencana" class="form-control d-none" style="line-height: 1.5; padding: 6px 12px;" id="rencanaLain">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Rencana Penggunaan & Pemanfaatan Tanah -->

                        <hr class="my-5">

                        <!-- Jenis Kegiatan -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">Jenis Kegiatan</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label class=" col-form-label">Kegiatan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radioJenisKegiatan1" name="jenisKegiatan" class="custom-control-input" value="berusaha">
                                        <label class="custom-control-label" for="radioJenisKegiatan1">Berusaha</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radioJenisKegiatan2" name="jenisKegiatan" class="custom-control-input" value="non berusaha">
                                        <label class="custom-control-label" for="radioJenisKegiatan2">Non Berusaha</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="rincianKegiatan" class=" col-form-label">Rincian Kegiatan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <input type="text" name="rincianKegiatan" class="form-control" style="line-height: 1.5; padding: 6px 12px;" id="rincianKegiatan">
                                </div>
                            </div>
                        </div>
                        <!-- Jenis Kegiatan -->

                        <hr class="my-5">

                        <!-- Teknik Bangunan -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">Teknik Bangunan</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="luasLantaiDasar" class=" col-form-label">Luas Lantai Dasar Bangunan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="input-group">
                                        <div class="col-xs-10 col-md-8">
                                            <input type="text" name="luasLantaiDasar" class="form-control" id="luasLantaiDasar" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                        <div class="col-xs-2 col-md-4 input-group-append">
                                            <span class="input-group-text" id="basic-addon2">m<sup>2</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="jumlahLantaiBangunan" class=" col-form-label">Jumlah Lantai Bangunan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="input-group">
                                        <div class="col-xs-10 col-md-8">
                                            <input type="text" name="jumlahLantaiBangunan" class="form-control" id="jumlahLantaiBangunan" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                        <div class="col-xs-2 col-md-4 input-group-append">
                                            <span class="input-group-text" id="basic-addon2">lantai</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="totalLuasLantai" class=" col-form-label">Total Luas Lantai Bangunan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="input-group">
                                        <div class="col-xs-10 col-md-8">
                                            <input type="text" name="totalLuasLantai" class="form-control" id="totalLuasLantai" style="line-height: 1.5; padding: 6px 12px;">
                                        </div>
                                        <div class="col-xs-2 col-md-4 input-group-append">
                                            <span class="input-group-text" id="basic-addon2">m<sup>2</sup></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Teknik Bangunan -->

                        <hr class="my-5">

                        <!-- Lampiran permohonan -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">Lampiran Permohonan</h5>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" data-file="filefcKTP" value="1" name="fcKTP" id="fcKTP">
                                <label class="custom-control-label" for="fcKTP">Fotokopi KTP Pemohon</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none"  name="filefcKTP">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" data-file="filefcNPWP" value="1" name="fcNPWP" id="fcNPWP">
                                <label class="custom-control-label" for="fcNPWP">Fotokopi NPWP Pemohon</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcNPWP">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" data-file="filefcSertifikat" value="1" name="fcSertifikat" id="fcSertifikat">
                                <label class="custom-control-label" for="fcSertifikat">Fotokopi Sertifikat Tanah</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcSertifikat">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" data-file="filesuratIzinPenggunaanTanah" name="suratIzinPenggunaaanTanah" id="suratIzinPenggunaaanTanah">
                                <label class="custom-control-label" for="suratIzinPenggunaaanTanah">Surat Izin Penggunaan Tanah bermaterai beserta Fotokopi KTP apabila pemohon bukan pemilik tanah</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filesuratIzinPenggunaaanTanah">
                            </div>
                            <!-- <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="suratKuasa" id="suratKuasa">
                                <label class="custom-control-label" for="suratKuasa">Surat Kuasa bermaterai beserta Fotokopi KTP apabila yang mengurus bukan pemohon</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filesuratKuasa">
                            </div> -->
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="petaGoogle" data-file="filepetaGoogle"id="petaGoogle">
                                <label class="custom-control-label" for="petaGoogle">Peta Google Map lokasi yang dimohon tampilan Satelit (Koordinasi Lokasi)</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filepetaGoogle">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fcNIB" data-file="filefcNIB" id="fcNIB">
                                <label class="custom-control-label" for="fcNIB">Fotokopi NIB (Nomor Induk Berusaha) dan Izin Kegiatan Berusaha yang diajukan</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcNIB">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="pernyataanMandiri" data-file="filepernyataanMandiri" id="pernyataanMandiri">
                                <label class="custom-control-label" for="pernyataanMandiri">Self Declaration / Pernyataan Mandiri dari OSS (Pernyataan Sesuai Tata Ruang)</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filepernyataanMandiri">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fcAktePendirian" id="fcAktePendirian" data-file="filefcAktePendirian">
                                <label class="custom-control-label" for="fcAktePendirian">Fotokopi Akte pendirian perusahaan bagi pemohon yang berbentuk Badan Usaha</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcAktePendirian">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fotoExisting" id="fotoExisting" data-file="filefotoExisting">
                                <label class="custom-control-label" for="fotoExisting">Foto eksisting lokasi terbaru (Tampak 4 sisi)</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefotoExisting">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="rencanaTeknikBangunan" id="rencanaTeknikBangunan" data-file="filerencanaTeknikBangunan">
                                <label class="custom-control-label" for="rencanaTeknikBangunan">Rencana teknik bangunan dan/atau rencana untuk kawasan (Siteplan, Denah, Tampak 4 sisi, Potongan)</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filerencanaTeknikBangunan">
                            </div>
                        </div>
                        <!-- Lampiran permohonan -->

                        <div class="collapse shadow" id="collapseFormKuasa">
                            <hr>
                            <div class="card">
                                <div class="card-header">SURAT KUASA</div>
                                <div class="card-body">

                                    <hr class="my-5">

                                    <!-- Pihak pertama -->
                                    <div class="mt-3">
                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="namaP1" class="col-form-label">Nama</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <input type="text" name="namaP1" class="form-control" id="namaP1" placeholder="Nama pihak pertama">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="noktpP1" class="col-form-label">No. KTP</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <input type="number" name="noktpP1" class="form-control" id="noktpP1" placeholder="No. KTP">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="pekerjaanP1" class="col-form-label">Pekerjaan</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <input type="text" name="pekerjaanP1" class="form-control" id="pekerjaanP1" placeholder="Pekerjaan">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="noTelpP1" class="col-form-label">No. Telp / Hp</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <input type="text" name="noTelpP1" class="form-control" id="noTelpP1" placeholder="081234567890">
                                            </div>
                                        </div>

                                        <div class=" form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="alamatP1" class="col-form-label">Alamat</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="jalanP1" placeholder="Jalan" class="form-control" id="alamatP1">
                                                    </div>
                                                    <div class="col-md-3 mt-1">
                                                        <input type="number" name="rtP1" placeholder="RT" class="form-control">
                                                    </div>
                                                    <div class="col-md-3 mt-1">
                                                        <input type="number" name="rwP1" placeholder="RW" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="kelurahanP1" placeholder="Desa/Kelurahan" class="form-control">
                                                    </div>
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="kecamatanP1" placeholder="Kecamatan" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-sm-12">
                                                        <input type="text" name="kabupatenP1" placeholder="Kabupaten/Kota" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Pihak pertama -->
                                    <div class="card-footer">
                                        <p>Selanjutnya disebut PIHAK PERTAMA</p>
                                    </div>

                                    <hr class="my-5">

                                    <!-- Pihak kedua -->
                                    <div class="mt-3">
                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="namaP2" class="col-form-label">Nama</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <input type="text" name="namaP2" class="form-control" id="namaP2" placeholder="Nama pihak kedua">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="noktpP2" class="col-form-label">No. KTP</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <input type="number" name="noktpP2" class="form-control" id="noktpP2" placeholder="No. KTP">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="pekerjaanP2" class="col-form-label">Pekerjaan</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <input type="text" name="pekerjaanP2" class="form-control" id="pekerjaanP2" placeholder="Pekerjaan">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="noTelpP2" class="col-form-label">No. Telp / Hp</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <input type="text" name="noTelpP2" class="form-control" id="noTelpP2" placeholder="No. Telp">
                                            </div>
                                        </div>

                                        <div class=" form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="alamatP2" class="col-form-label">Alamat</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="jalanP2" placeholder="Jalan" class="form-control" id="alamatP2">
                                                    </div>
                                                    <div class="col-md-3 mt-1">
                                                        <input type="number" name="rtP2" placeholder="RT" class="form-control">
                                                    </div>
                                                    <div class="col-md-3 mt-1">
                                                        <input type="number" name="rwP2" placeholder="RW" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="kelurahanP2" placeholder="Desa/Kelurahan" class="form-control">
                                                    </div>
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="kecamatanP2" placeholder="Kecamatan" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-sm-12">
                                                        <input type="text" name="kabupatenP2" placeholder="Kabupaten/Kota" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Pihak kedua -->
                                    <div class="card-footer">
                                        <p>Selanjutnya disebut PIHAK KEDUA</p>
                                    </div>

                                    <hr class="my-5">

                                    <div class="card-header">
                                        <p>PIHAK KESATU memberikan kuasa kepada PIHAK KEDUA guna mengurus IRK, KKPR dan IMB/PBG di DPUPR Kota Magelang untuk Bangunan : </p>
                                    </div>
                                    <div class="mt-3">
                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="fungsiBangunan" class="col-form-label">Fungsi Bangunan</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <input type="text" name="fungsiBangunanKuasa" class="form-control" id="fungsiBangunan" placeholder="Fungsi bangunan">
                                            </div>
                                        </div>

                                        <div class=" form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="jalanLahanKuasa" class="col-form-label">Letak Bangunan</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="jalanLahanKuasa" placeholder="Jalan" class="form-control" id="jalanLahanKuasa">
                                                    </div>
                                                    <div class="col-md-3 mt-1">
                                                        <input type="number" name="rtLahanKuasa" placeholder="RT" class="form-control">
                                                    </div>
                                                    <div class="col-md-3 mt-1">
                                                        <input type="number" name="rwLahanKuasa" placeholder="RW" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="kelurahanLahanKuasa" placeholder="Desa/Kelurahan" class="form-control">
                                                    </div>
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="kecamatanLahanKuasa" placeholder="Kecamatan" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-sm-12">
                                                        <input type="text" name="kabupatenLahanKuasa" placeholder="Kabupaten/Kota" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="statusBangunan" class="col-form-label">Status Tanah</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="hakTanahKuasa" class="form-control" id="statusBangunan" placeholder="Hak tanah">
                                                    </div>
                                                    <div class="col-md-6 mt-1">
                                                        <input type="text" name="noTanahKuasa" class="form-control" placeholder="No. Tanah">
                                                    </div>
                                                </div>
                                                <div class="row mt-2">
                                                    <div class="col-sm-12">
                                                        <input type="text" name="pemilikTanahKuasa" class="form-control" placeholder="Pemilik tanah">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-5">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="luasTanahKuasa" class="col-form-label">Luas Tanah</label>
                                            </div>

                                            <div class="col-sm-12 col-md-8">
                                                <input type="number" name="luasTanahKuasa" class="form-control" id="luasTanahKuasa" placeholder="Luas tanah">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-center mt-5">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>

                    <form id="formCetak" target="_blank" class="d-none" action="<?= base_url('FormSuratIRK/cetak') ?>" method="POST">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    body {
        background-color: #dce0e8;
    }
 

</style>
<?php include 'script.php'; ?>