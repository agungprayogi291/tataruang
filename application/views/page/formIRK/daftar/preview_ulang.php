<html>

<body>
    
    <h3><u>SURAT KUASA</u></h3>

    <p>Yang bertanda tangan dibawah ini :</p>

    <table style="width: 100%">
        <tr>
            <td width="35%">Nama</td>
            <td width="20px"> : </td>
            <td><?= $cetak->namap1 ?></td>
        </tr>
        <tr>
            <td>No. KTP</td>
            <td> : </td>
            <td><?= $cetak->noktpp1 ?></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td> : </td>
            <td><?= $cetak->pekerjaanp1 ?></td>
        </tr>
        <tr>
            <td>No. Telp / Hp</td>
            <td> : </td>
            <td><?= $cetak->notelpp1 ?></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td> : </td>
            <td>Jalan <?= $cetak->jalanp1 ?></td>
            <td>RT <?= $cetak->rtp1 ?> / RW <?= $cetak->rwp1 ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td>Desa / Kelurahan <?= $cetak->kelurahanp1 ?></td>
            <td>Kecamatan <?= $cetak->kecamatanp1 ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">Kabupaten / Kota <?= $cetak->kabupatenp1 ?></td>
        </tr>
    </table>

    <p>Selanjutnya disebut <b>PIHAK PERTAMA</b></p>

    <p>Dengan ini memberikan izin kepada :</p>

    <table style="width: 100%">
        <tr>
            <td width="35%">Nama</td>
            <td width="20px"> : </td>
            <td><?= $cetak->namap2 ?></td>
        </tr>
        <tr>
            <td>No. KTP</td>
            <td> : </td>
            <td><?= $cetak->noktpp2 ?></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td> : </td>
            <td><?= $cetak->pekerjaanp2 ?></td>
        </tr>
        <tr>
            <td>No. Telp / Hp</td>
            <td> : </td>
            <td><?= $cetak->notelpp2 ?></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td> : </td>
            <td>Jalan <?= $cetak->jalanp2 ?></td>
            <td>RT <?= $cetak->rtp2 ?> / RW <?= $cetak->rwp2 ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td>Desa / Kelurahan <?= $cetak->kelurahanp2 ?></td>
            <td>Kecamatan <?= $cetak->kecamatanp2 ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">Kabupaten / Kota <?= $cetak->kabupatenp2 ?></td>
        </tr>
    </table>

    <p>Selanjutnya disebut <b>PIHAK KEDUA</b></p>

    <p>PIHAK KESATU memberikan kuasa akepada PIHAK KEDUA guna mengurus IRK, KKPR dan IMB/PBG di DPUPR Kota Magelang untuk Baangunan Fungsi <?= $cetak->fungsibangunan ?> diatas lahan sebagai berikut :</p>

    <table style="width: 100%">
        <tr>
            <td width="35%">Letak Bangunan</td>
            <td width="20px"> : </td>
            <td colspan="2">Jalan <?= $cetak->jalanlahan ?></td>
        </tr>
        <tr>
            <td></td>
            <td> : </td>
            <td>RT <?= $cetak->rtlahan ?> / RW <?= $cetak->rwlahan ?></td>
            <td>Desa/Kel <?= $cetak->kelurahanlahan ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">Kec <?= $cetak->kecamatanlahan ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">Kota Magelang</td>
        </tr>
        <tr>
            <td>Status Tanah</td>
            <td> : </td>
            <td>Hak <?= $cetak->haktanah ?></td>
            <td>No. <?= $cetak->notanah ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="2">atas nama <?= $cetak->pemiliktanah ?></td>
        </tr>
        <tr>
            <td>Luas Tanah</td>
            <td> : </td>
            <td colspan="2"><?= $cetak->luastanah ?> m<sup>2</sup></td>
        </tr>
    </table>

    <p>Demikian Surat Izin ini dibuat untuk dijadikan periksa bagi yang berkepentingan serta dapat dipergunakan semestinya</p>

    <div class="ttd" style="display: inline;">

        <table style="width: 100%;">
            <tr>
                <td width="70%">
                    <table>
                        <tr>
                            <td style="color: white;" colspan="2">Magelang,</td>
                        </tr>
                        <tr>
                            <td class="center"><b>PIHAK KEDUA</b></td>
                        </tr>
                        <tr>
                            <td class="center">Yang Menerima Kuasa</td>
                        </tr>
                        <tr>
                            <td style="height: 50px;"></td>
                        </tr>
                        <tr>
                            <td class="center"><?= $cetak->namap2 ?></td>
                        </tr>
                    </table>
                </td>
                <td width="30%">
                    <table class="center">
                        <tr>
                            <td>Magelang,.......................20....</td>
                        </tr>
                        <tr>
                            <td class="center" colspan="2"><b>PIHAK PERTAMA</b></td>
                        </tr>
                        <tr>
                            <td>Yang Memberi Kuasa</td>
                        </tr>
                        <tr>
                            <td style="height: 50px;"></td>
                        </tr>
                        <tr>
                            <td><?= $cetak->namap1 ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </div>
</body>

</html>