
<div class="container my-5">
    
    <div class="col-md-12">
    <h3 class="text-center">Daftar Permohonan <br>
        INFORMASI RENCANA KOTA</h3>
        <div class="row mx-5 my-5">
            <button class="btn btn-light" id="btn-filter" ><i class="bi bi-filter-left"></i> Filter </button>
        </div>
        <div class="card my-4 mb-3 collapse" style="padding:5px; box-shadow:-2px 5px #f0f0f0;" id="container-filter">
            <form action="" id="form-filter">
                <div class="row ">
                    <div class="col-sm-6 ">
                        <div class="form-check">
                            <label for="">Bertindak : </label>
                            <br>
                            <input class="form-check-input" type="radio" name="jenis" id="kkrp-cek" value="" checked>
                            <label class="form-check-label" for="kkrp-cek">
                                <span class="mx-4">Tidak Diwakilkan / Diwakilkan</span>
                            </label><br>
                            <input class="form-check-input" type="radio" name="jenis" id="sendiri" value="1" >
                            <label class="form-check-label" for="sendiri">
                                <span class="mx-4">Oleh Diri Sendiri</span>
                            </label><br>
                            <input class="form-check-input" type="radio" name="jenis"id="diwakilkan" value="2">
                            <label class="form-check-label" for="diwakilkan">
                                <span class="mx-4">Diwakilkan</span>
                            </label>
                            
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="">Status :</label>
                            <br>
                            <select name="status" id="" class="form-control form-control-lg">
                                <option value="">-- Status --</option>
                                <option value="">none</option>
                                <option value="0">Daftar</option>
                                <option value="1">Proses</option>
                                <option value="2">selesai</option>
                            </select>
                        </div>
                        <button class="btn btn-primary" type="submit" id="filter-save">save</button>
                    </div>
                </form> 
            </div>
        </div>
       
        <table class="table table-bordered" id="tabel_surat" style="width:100%">
            <thead>
                <tr>
                    <th width="5%">No</th>
                    <th width="5%">Nama Pemohon</th>
                    <th width="5%">No.Ktp Pemohon</th>
                    <th width="5%">No.telephone</th>
                    <th width="10%">Alamat</th>
                    <th width="40%">Lampiran</th>
                    <th width="5%">Status</th>
                    <th width="5%">Keterangan</th>
                    <!-- <th>Margin</th>
                    <th>Stock</th> -->
                    <th width="5%">Aksi</th>
                </tr>
            </thead>
            <tbody id="show_surat">
           
            </tbody>
        </table>
    </div>
    
    </div>


<form id="formCetak" target="_blank" class="d-none" action="<?= base_url() ?>permohonan-irk-cetak-ulang" method="POST">
    <input type="hidden" id="kode_form" name="kode_form">
</form>
<form id="formCetakKuasa" target="_blank" class="d-none" action="<?= base_url() ;?>permohonan-irk-cetak-ulang-kuasa" method="POST">
    <input type="hidden" id="kode_form_kuasa" name="kode_form_kuasa">
</form>

<?php include_once 'script.php' ;?>
<?php include_once 'modal.php' ;?>