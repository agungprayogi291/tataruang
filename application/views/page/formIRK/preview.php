<html>
<head>
    <style></style>
</head>
<body>
    <htmlpagefooter name="myFooter1" style="display:none;">
        <table>
            <tr>
                <td width="33%" style="font-style:italic;font-size:10px;">
                    {DATE j-m-y}
                </td>
                <td width="33%" style="text-align:right; font-size:10px; padding-right:0;">
                {PAGENO}/{nbpg}

                </td>
                
            </tr>
        </table>        
    </htmlpagefooter>
    <div class="kepada">
        <p>Kepada</p>
        <p>Yth. Walikota Magelang</p>
        <p>Cq. Kepala Dinas Pekerjaan Umum dan Penataan Ruang</p>
        <p style="margin-left: 30px;">Jl. Jend. Sudirman No. 54, Kel. Magersari, Kec. Magelang Selatan, Kota Magelang 56126</p>
        <p style="margin-left: 30px;">Telp. (0293) 362542</p>
        <p style="margin-left: 30px;">di <u><b>MAGELANG</b></u></p>
    </div>

    <p>Yang bertanda tangan di bawah ini :</p>
    <table>
        <tr>
            <td>1.</td>
            <td>Nama Pemohon</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['namapemohon'] ?></td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Bertindak untuk dan atas nama</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['bertindakatasnama'] ?></td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Alamat</td>
            <td> : </td>
            <td>Jalan <?= $cetak['jalanpemohon'] ?></td>
            <td>RT <?= $cetak['rtpemohon'] ?> / RW <?= $cetak['rwpemohon'] ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Desa / Kelurahan <?= $cetak['kelurahanpemohon'] ?></td>
            <td>Kecamatan <?= $cetak['kecamatanpemohon'] ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="2">Kabupaten <?= $cetak['kabupatenpemohon'] ?></td>
        </tr>
        <tr>
            <td>4.</td>
            <td>No. Telp & Email</td>
            <td> : </td>
            <td><?= $cetak['notelppemohon'] ?></td>
            <td>& <?= $cetak['emailpemohon'] ?></td>
        </tr>
        <tr>
            <td>5.</td>
            <td>NIK</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['nikpemohon'] ?></td>
        </tr>
        <tr>
            <td>6.</td>
            <td>Pekerjaan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['pekerjaanpemohon'] ?></td>
        </tr>
    </table>

    <p>Dengan ini mengajukan permohonan informasi Rencana Kota sesuai Rencana Tata Ruang Wilayah (RTRW) dan/atau Rencana Detail Tata Ruang (RDTR) Kota Magelang yang berlaku pada lokasi tanah sesuai sertifikat, dengan data-data sebagai berikut :</p>

    <table>
        <!-- PENGUASAAN TANAH -->
        <tr>
            <td>1.</td>
            <td colspan="4"><b>Penguasaan Tanah</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Status Tanah</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['statustanah'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>No Tanah</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['luastanah'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Atas nama</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['pemiliktanah'] ?></td>
        </tr>

        <!-- LOKASI LAHAN -->
        <tr>
            <td>2.</td>
            <td colspan="4"><b>Lokasi Lahan</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Jalan / Kampung</td>
            <td> : </td>
            <td>Jalan <?= $cetak['lokasilahan'] ?></td>
            <td>RT <?= $cetak['rtlahan'] ?> / RW <?= $cetak['rwlahan'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Kelurahan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['kelurahanlahan'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Kecamatan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['kecamatanlahan'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Kota</td>
            <td> : </td>
            <td colspan="2">Magelang</td>
        </tr>

        <!-- BATAS BATAS TANAH -->
        <tr>
            <td>3.</td>
            <td colspan="4"><b>Batas-batas tanah</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Utara</td>
            <td>:</td>
            <td><?= $cetak['batastanahutara'] ?></td>
            <td>Timur : <?= $cetak['batastanahtimur'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Selatan</td>
            <td>:</td>
            <td><?= $cetak['batastanahselatan'] ?></td>
            <td>Barat : <?= $cetak['batastanahbarat'] ?></td>
        </tr>

        <!-- RENCANA PENGGUNAAN -->
        <tr>
            <td>4.</td>
            <td><b>Rencana Penggunaan</b></td>
            <td> : </td>
            <td><?= $cetak['rencanapembangunan'] ?></td>
        </tr>

        <!-- JENIS KEGIATAN -->
        <tr>
            <td>5.</td>
            <td><b>Jenis Kegiatan</b></td>
            <td> : </td>
            <td><?= $cetak['jeniskegiatan'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td>Rincian kegiatan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['rinciankegiatan'] ?></td>
        </tr>

        <!-- TEKNIK BANGUNAN -->
        <tr>
            <td>6.</td>
            <td colspan="4"><b>Teknik Bangunan</b></td>
        </tr>
        <tr>
            <td></td>
            <td>Luas Lantai Dasar Bangunan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['luaslantaidasar'] ?> m<sup>2</sup></td>
        </tr>
        <tr>
            <td></td>
            <td>Jumlah Lantai Bangunan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['jumlahlantaibangunan'] ?> lantai</td>
        </tr>
        <tr>
            <td></td>
            <td>Total Luas Lantai</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['totalluaslantai'] ?> m<sup>2</sup></td>
        </tr>


        <!-- LAMPIRAN -->
          <!-- LAMPIRAN -->
          <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td style="height:30px">7.</td>
            <td colspan="5"><b>Lampiran permohonan</b></td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcktp']) ? "√" : "") ?>
            </td>
            <td colspan="3" style="padding-left: 10px; height: 30px">Fotokopi KTP Pemohon</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcnpwp']) ? "√" : "") ?>
            </td>
            <td colspan="3" style="padding-left: 10px; height: 30px">Fotokopi NPWP Pemohon</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcsertifikat']) ? "√" : "") ?>
            </td>
            <td colspan="3" style="padding-left: 10px; height: 30px">Fotokopi Sertifikat Tanah</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['suratizinpenggunaantanah']) ? "√" : "") ?>
            </td>
            <td colspan="3" style="padding-left: 10px;">Surat Izin Penggunaan Tanah bermaterai beserta Fotokopi KTP apabila pemohon bukan pemilik tanah</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['petagoogle']) ? "√" : "") ?>
            </td>
            <td colspan="3" style="padding-left: 10px; height: 30px">Peta Google Map lokasi yang dimohon tampilan Satelit (Koordinasi Lokasi)</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcnib']) ? "√" : "") ?>
            </td>
            <td colspan="3" style="padding-left: 10px; height: 30px">Fotokopi NIB (Nomor Induk Berusaha) dan Izin Kegiatan Berusaha yang diajukan</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['pernyataanmandiri']) ? "√" : "") ?>
            </td>
            <td colspan="3" style="padding-left: 10px; height: 30px">Self Declaration / Pernyataan Mandiri dari OSS (Pernyataan Sesuai Tata Ruang)</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcaktependirian']) ? "√" : "") ?>
            </td>
            <td colspan="3" style="padding-left: 10px; height: 30px">Fotokopi Akte pendirian perusahaan bagi pemohon yang berbentuk Badan Usaha</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fotoexisting']) ? "√" : "") ?>
            </td>
            <td colspan="3" style="padding-left: 10px; height: 30px">Foto eksisting lokasi terbaru (Tampak 4 sisi)</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['rencanateknikbangunan']) ? "√" : "") ?>
            </td>
            <td colspan="3" style="padding-left: 10px;">Rencana teknik bangunan dan/atau rencana untuk kawasan (Siteplan, Denah, Tampak 4 sisi, Potongan)</td>
        </tr>
    </table>

    <br><br>
    <table style="width: 100%; margin-top:100%;" >
        <tr>
            <td width="60%"></td>
            <td>
                <table class="center">
                    <tr>
                        <td>Magelang <?php echo date("d-m-Y") ;?></td>
                    </tr>
                    <tr>
                        <td style="text-align:center;"> Pemohon</td>
                    </tr>
                    <tr>
                        <td style="height: 50px;"></td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px dotted black; text-align:center;"><?= $cetak['namapemohon'] ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


</body>

</html>