  
  
  
  <?php  if (!isset($_SESSION["logged"])){header('Location:login'); exit;  }?>
  <div class="clearfix"></div>


  <div class="container">
    <div class="pengaduan">
      <div id="main_j">
        <div class="pengaduan-top-grid">
          <h3>Daftar Masukan Publik Terkait Regulasi Tata Ruang Kota Magelang</h3>
        </div>
      </div>
      <p>
        <div class="col-md-6 left " style="margin-bottom:20px;">
          <select class="form-control" id="a_kriteria" name="a_kriteria" onchange="ReloadDataMasukan();">
            <option value="0" selected="">Semua</option>
            <option value="1">Rancangan Peraturan Daerah Kota Magelang</option>
            <option value="2">Undang-Undang No. 26 Tahun 2007</option>
            <option value="3">Peraturan Menteri Pekerjaan Umum No. 20 Tahun 2011</option>
            <option value="4">Peraturan Daerah Kota Magelang No. 4 Tahun 2012</option>
            <option value="5">Peraturan Pemerintah No. 15 Tahun 2010</option>
            <option value="6">Peraturan Menteri Agraria dan Tata Ruang / Kepala Badan Pertanahan Nasional No. 6 Tahun
              2017</option>
            <option value="7">Peraturan Pemerintah No. 68 Tahun 2010</option>
            <option value="8">Peraturan Menteri Agraria dan Tata Ruang / Kepala Badan Pertanahan Nasional No. 8 Tahun
              2017</option>

          </select>
        </div>

        <input type="hidden" id="tanggal_X" name="tanggal_X" value="<?php echo (date("Y-m-d")); ?>" </div> </div> <div
          class="col-md-12 left">
        <table class='table table-condensed table-striped table-bordered'>
          <thead>
            <tr>
              <td><strong>No</strong></td>
              <td><strong>Tgl/Jam </strong></td>
              <td><strong>Nama </strong></td>
              <td><strong>Telp </strong></td>
              <td><strong>Email</strong></td>
              <td style="width:50%"><strong>Isi Masukan</strong></td>
              <td><strong>Aksi</strong></td>

            </tr>
          </thead>
          <tbody id="IsiTabel">

          </tbody>
        </table>
    </div>


  </div>
  </div>

  <hr>

  <?php include_once 'daftar_masukan_tambah.php';  ?>
  <script type="text/javascript" src="<?= base_url() ;?>ajax/daftar_masukan_js.js"></script>