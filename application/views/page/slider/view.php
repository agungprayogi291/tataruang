<section class="section">
    <div class="container">

        <div class="col-md-6" style="margin-top: 60px;">
            <div id="msg" class="alert alert-success d-none" role="alert">
                Slider berhasil disimpan.
            </div>

            <form id="formSlider" method="POST">
                <div class="form-group row mb-5">
                    <input type="hidden" id="id" name="id">
                    <label for="inputFileName" class="col-sm-4 col-form-label" style="margin-top: 40px;">Nama File</label>
                    <div class="col-sm-8">
                        <input type="text" name="inputFileName" readonly class="form-control" id="inputFileName">
                        <!-- <input type="hidden" class="form-control" id="inputFileName"> -->
                    </div>
                </div>
                <div class="form-group row mb-5">
                    <label for="inputUrutan" class="col-sm-4 col-form-label" style="margin-top: 40px;">Urutan</label>
                    <div class="col-sm-8">
                        <input type="number" name="inputUrutan" class="form-control" id="inputUrutan">
                    </div>
                </div>
                <div class="form-group row mb-5">
                    <label for="inputStatus" class="col-sm-4 col-form-label" style="margin-top: 40px;">Status</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="inputStatus" style="height: 35px;">
                            <option value="">-- Pilih Status ---</option>
                            <option value="1">Aktif</option>
                            <option value="0">Non Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row mb-5">
                    <label for="inputFile" class="col-sm-4 col-form-label" style="margin-top: 40px;">File</label>
                    <div class="col-sm-8">
                        <input type="file" name="inputFile" class="form-control" id="inputFile" style="height: 40px;">
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-8">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" id='btn_reset' class="btn btn-warning collapse">Reset</button> 
                    </div>
                </div>
            </form>
            <img id="foto_z" src="<?php echo base_url();?>assets/files/slider/1bc8ad366e5b4a9f76e0a9358fdd0e94.png" alt="" width="550" height="300">  
        <br>
            
        <div class="col-md-12 text-center">
            proporsi dimensi ukuran slider    1024 x 450 pixel
            
        </div>
        </div>
       
           
        <div class="col-md-6" style="margin-top: 60px;">
            <table id="t-slider" class='table table-condensed table-striped table-bordered'>
                <thead>
                    <tr>
                        <td style="width:30px;">No</td>
                        <td>Nama Slider</td>
                        <td>Urutan</td>
                        <td>Status</td>
                        <td style="width:100px;">Aksi</td>
                    </tr>
                </thead>
                <tbody id="Tisi"> </tbody>
            </table>
        </div>

    </div>
</section>

<?php include('script.php'); ?>