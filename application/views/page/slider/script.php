<script type="text/javascript">
    const msg = $('#msg');

    $(document).ready(function() {
        Tampildata();
        $("#btn_reset").click(function(event) {  
            Clearform();
        });  
        $('#formSlider').submit(function(e) {
            e.preventDefault();

            $.ajax({
                url: '<?= base_url('Slider/simpan') ?>',
                type: 'POST',
                dataType: 'json',
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                success: function(response) {
                    //console.log(response)
                    msg.removeClass('alert-success alert-danger')

                    if (response.success == 1) {
                        Swal.fire({
                            title: 'Information!',
                            text:`${response.message}`,
                            imageUrl: `<?= base_url() ;?>assets/files/slider/${response.file_name}`,
                            imageWidth: 400,
                            imageHeight: 200,
                            imageAlt: 'Custom image',
                        }).then(e=>{
                            Tampildata() 
                            Clearform()
                            window.location.reload()
                        })
                      
                    } else {
                        Swal.fire({
                            title: 'Information!',
                            text:`${response.message}`,
                            imageUrl: `<?= base_url() ;?>assets/files/slider/${response.file_name}`,
                            imageWidth: 400,
                            imageHeight: 200,
                            imageAlt: 'Custom image',
                        }).then(e=>{
                            Tampildata() 
                        })
                      
                    }
                }
            })
        });
    });

    function Tampildata() {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Slider/tampil',
            dataType: 'json',
            success: function(data) {
                RefreshTable($('#t-slider'), $('#Tisi'), data.tabel,data.baris);
                $("#foto_z").attr('src','<?= base_url() ;?>assets/files/slider/' + data.file_name['file_name']);
            
                // $('#Tisi').html(data.tabel);
            }
        });
    }
    function ExecuteHapus(Kode) {

    $.ajax({
        type: "POST",
        url: '<?php echo base_url(); ?>Slider/hapus',
        data: {
            id: Kode
        },
        dataType: 'json',
        success: function(data) {
            Tampildata();
            window.location.reload()
            // var icone = URLE + 'asset/image/slider/' + data[0].nama;
            // $("#foto_z").attr('src', icone);
        }
        });
    }

    function BukaHapus(kode) {
        Swal.fire({
        title: 'Yakin Data Di Hapus?',
        
        showCancelButton: true,
        confirmButtonText: 'Oke',
        }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                ExecuteHapus(kode)
            } 
        })
       
    }

    function BukaEdit(Kode) {
        $.ajax({
            type: "POST",
            url: '<?php echo base_url(); ?>Slider/detail',
            data: {
                kode: Kode
            },
            dataType: 'json',
            success: function(data) {
                $('#id').val(data[0].id);
                $("[name='inputFileName']").val(data[0].file_name);
                $("[name='inputUrutan']").val(data[0].urutan);
                //$("[name='inputFile']").val(data[0].file_name);
                $("[name='inputStatus']").val(data[0].status).trigger('change');
                $('#btn_reset').removeClass('collapse');
                if (data[0].file_name != '') {
                    var icone = '<?= base_url() ;?>assets/files/slider/' + data[0].file_name;
                    $("#foto_z").attr('src', icone);
                }
            }

        });


    }
    
    function Clearform(){
        $("#formSlider")[0].reset();
        $("[name='inputStatus']").val("")
    }
</script>