 

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!--<div class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='app_modal_label'>
    <div id='app_modal_size' class='modal-dialog' role='document'>-->
        <div class='modal-content'>
            <div class='modal-header'>
                <h4 class='modal-title' id='app_modal_label'>Detail Inforamsi</h4>
            </div>
            <div id='app_modal_body' class='modal-body'> 
                <table class='table table-condensed table-striped table-bordered'>
                    <thead></thead>
                    <tbody id="isi_d"> 
                    </tbody>
                </table> 
                <hr>
                
                <div id="list_foto"> 
                        <table class='table table-condensed ' id="tb_foto" border="0">
                            <thead></thead>
                            <tbody id="isi_foto"> 
                            </tbody>
                        </table> 
                </div>    
                <div id="zoom_foto" class="collapse">                    
                    <img id="foto_z" src="<?php echo base_url();?>/asset/image/logo.png" alt="Forest" width="100%" height="400"> 
                    
                </div>    
                
                
            </div>
            <div id='app_modal_footer' class='modal-footer'>
                <button data-dismiss="modal" type='button' id='cancel_order' class='btn btn-default btn-sm'><span
                        class='glyphicon glyphicon-off'></span>&nbsp;Tutup / Keluar</button>
               <button type='button' id='btn_tutupfoto' class='btn btn-primary btn-sm collapse'><span class='glyphicon glyphicon-file'></span>Tutup Foto</button>
            </div>
        </div>
    </div>
</div>
 


<div class="modal fade" id="zoomFoto" role="dialog">
    <div class="modal-dialog"> 
            <div class='modal-content '>
                    <div class='modal-header'>
                            <h4 class='modal-title' id='app_modal_label'>Upload Gambar Jalan </h4>
                    </div>
                    <div id='app_modal_body' class='modal-body'>
                         
                                                     
                        <table>
                            <tr>
                                <td> 
                                    <input type="file" name="file_gambar" id="file_gambar">  
                                </td>
                                <td>
                                    <button  type='button' id='bt_upload' class='btn btn-success btn-sm'><span class='glyphicon glyphicon-upload'></span>&nbsp;Upload</button> 
                                </td>
                            </tr>
                        </table> 
                        
                        
                    </div> 
                
                    <div id='app_modal_footer' class='modal-footer'>
                       <button data-dismiss="modal" type='button' id='cancel_order' class='btn btn-default btn-sm'><span class='glyphicon glyphicon-off'></span>&nbsp;Tutup / Keluar</button> 
                    </div>
            </div>
    </div>
</div>
 