 

 <div class="modal fade" id="ModalProsesIMB" role="dialog">
    <div class="modal-dialog modal-small"> 
            <div class='modal-content'> 
                
                <div class='modal-header'>
                            <h4 class='modal-title' id='app_modal_label'>Pengajuan IMB</h4>
                </div>
                <form action="#" name ="frm_input_imb" id="frm_input_imb" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="form-horizontal form-row-seperated">    
                    <div id='app_modal_body' class='modal-body'>
                            <table class='table table-condensed table-striped table-bordered'>
                            <thead></thead>
                            <tbody> 
                         
                            <tr><td>Koordinat</td><td>  
                                    <input type="hidden" id="lng" name ="lng">
                                    <input type="hidden" id="lat" name ="lat">
                                    <input type="text"  id="a_koordinat" name="a_koordinat"  class="form-control" readonly="true">  
                            </td></tr>
                            <tr><td>Kecamatan</td><td><input type="text"  id="a_kecamatan" name="a_kecamatan" class="form-control"  readonly="true"></td></tr>
                            <tr><td>Kelurahan</td><td><input type="text"  id="a_kelurahan" name="a_kelurahan" class="form-control"  readonly="true"></td></tr>
                            <tr><td>Peruntukan</td><td><input type="text"  id="a_peruntukan" name="a_peruntukan" class="form-control"></td></tr>
                            <tr><td>Fungsi Bangunan</td><td><input type="text"  id="a_fungsi" name="a_fungsi" class="form-control"></td></tr>
                                                       
                            
                            <tr><td>Nama Pemohon</td><td><input type="text"  id="a_nama" name="a_nama"  class="form-control"></td></tr>
                            <tr><td>Alamat</td><td><input type="text"  id="a_alamat" name="a_alamat" class="form-control"></td></tr>
                            <tr><td>No Telp</td><td><input type="text"  id="a_telp" name="a_telp" class="form-control"></td></tr>                            
                            <tr><td>Hak Milik</td><td><input type="text"  id="a_hak_milik" name="a_hak_milik" class="form-control"></td></tr>                             
                            <tr><td>Lokasi</td><td><input type="text"  id="a_lokasi" name="a_lokasi" class="form-control"></td></tr>
                            <tr><td>Luas</td><td><input type="text"  id="a_luas" name="a_luas" class="form-control"></td></tr> 
                            
                            </tbody>
                            </table>
                    </div>
                </form> 
                    
                    <div id='app_modal_footer' class='modal-footer'>
                            <div class="col-md-12 left"> 
                            <div class="col-md-6 left"> 
                                <button type="button" id="btn_proses_pengajuan" name ="btn_proses_pengajuan"  class="btn btn-warning">Proses</button>
                                     
                             </div>
                             <div class="col-md-6">                    
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>                  
                             </div>
                             </div>
                        
                             
                    </div>
            </div>
    </div>
</div> 


<div class="modal fade" id="ModalInfoIMB" role="dialog">
    <div class="modal-dialog modal-small"> 
            <div class='modal-content'>
                    <div class='modal-header'>
                            <h4 class='modal-title' id='app_modal_label'>Informasi IMB</h4>
                    </div>
                    <div id='app_modal_body' class='modal-body'>
                        <input type="hidden" id="kd_imb" name="kd_imb">
                            <table class='table table-condensed table-striped table-bordered'>
                                    <thead></thead>
                                    <tbody> 
                                            <input type="hidden" id="x" name ="lng">
                                            <input type="hidden" id="y" name ="lat">
                                            
                                            <tr><td>Koordinat</td><td id='op_koordinat'></td></tr>
                                            <tr><td>Nama Pemohon</td><td id='op_nama'></td></tr>
                                            <tr><td>Kriteria</td><td id='op_kriteria'></td></tr>
                                            <tr><td>Kecamatan</td><td id='op_kecamatan'></td></tr>
                                            <tr><td>Kelurahan</td><td id='op_kelurahan'></td></tr>
                                            <tr><td>Hak Milik</td><td id='op_hak_milik'></td></tr>
                                            <tr><td>Peruntukan</td><td id='op_peruntukan'></td></tr>
                                            <tr><td>Fungsi Bangunan</td><td id='op_fungsi'></td></tr>
                                            <tr><td>Lokasi</td><td id='op_lokasi'></td></tr>
                                            <tr><td>Luas</td><td id='op_luas'></td></tr>
                                            <tr><td>No SK IMB</td><td id='op_sk'></td></tr>
                                            <tr><td>id</td><td id='op_id'></td></tr>
                                             
                                          
                                            
                                    </tbody>
                            </table>
                    </div>
                    <div id='app_modal_footer' class='modal-footer'>
                            <div class="col-md-12 left"> 
                            <div class="col-md-6 left"> 
                                <div id="tombolproses" display="none"> 
                                <button type="button" onclick="LanjutProses();"class="btn btn-warning">Lanjut Proses Pengajuan</button>
                                </div>   
                             </div>
                                
                             <div class="col-md-6">                    
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>                  
                             </div>
                             </div>
                        
<!--                            <button data-dismiss="modal" type='button' id='cancel_order' class='btn btn-default btn-sm'><span class='glyphicon glyphicon-off'></span>&nbsp;Tutup / Keluar</button> -->
                    </div>
            </div>
    </div>
</div>

 