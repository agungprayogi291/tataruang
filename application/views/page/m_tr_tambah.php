<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-small">
<!--<div class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='app_modal_label'>
    <div id='app_modal_size' class='modal-dialog' role='document'>-->
            <div class='modal-content'>
                    <div class='modal-header'>
                            <h4 class='modal-title' id='app_modal_label'>Informasi Tata Ruang Kota Magelang</h4>
                    </div>
                    <div id='app_modal_body' class='modal-body'>
                         
                            <table class='table table-condensed table-striped table-bordered'>
                                    <thead></thead>
                                    <tbody>
                                            <tr><td>Koordinat</td><td id='op_str_coordinate'></td></tr>
                                            <tr><td>Kecamatan</td><td id='op_str_kecamatan'></td></tr>
                                            <tr><td>Kelurahan</td><td id='op_str_kelurahan'></td></tr>
                                            <tr><td>Kode Blok/Zonasi Rencana</td><td id='op_str_kdblokzonasi'></td></tr>
                                            <tr><td>Pola Ruang</td><td id='op_str_polaruang'></td></tr>
                                            <tr><td>Kawasan</td><td id='op_str_kawasan'></td></tr>
                                            <tr><td>Keterangan</td><td id='op_str_keteranganpr'></td></tr>
                                            <tr><td>Koefisien Dasar Bangunan</td><td id='op_str_kdb'></td></tr>
                                            <tr><td>Ketinggian Maksimum</td><td id='op_str_kmb'></td></tr>
                                    </tbody>
                            </table>
                    </div>
                    <div id='app_modal_footer' class='modal-footer'>
                            <button data-dismiss="modal" type='button' id='cancel_order' class='btn btn-default btn-sm'><span class='glyphicon glyphicon-off'></span>&nbsp;Tutup / Keluar</button>
<!--                            <button type='button' id='process_order' class='btn btn-primary btn-sm'><span class='glyphicon glyphicon-file'></span>&nbsp;Proses ke Permohonan</button>-->
                    </div>
            </div>
    </div>
</div>
 

 