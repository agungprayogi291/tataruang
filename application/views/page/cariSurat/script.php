<script>
    $(document).ready(function(e){
        $("#cekPermohonan").submit(function(e){
            e.preventDefault()
            cekPermohonan()
        })
    })
    function cekPermohonan(){
        $.ajax({
            type:"POST",
            url :"<?= base_url() ;?>penelusuran/cari",
            dataType:"JSON",
            data : $("#cekPermohonan").serialize(),
            success: function(response){
                $("#content").html(response.message)
            }
        })
    }
    function preview(kode){
        jenis = $("#preview").attr('data-jenis');
        $("#jenis").val(jenis)
        $("#kode_form").val(kode)
        $("#formCetak").trigger("submit");
    }

</script>