<div class="container">
    <div class="row my-4">
        <div class="col-md-6">
            <h3>Penelusuran Status Permohonan Surat</h3>
            <form id="cekPermohonan" class="from">
                <div class="form-check">
                    <label for="">Jenis : </label>
                    <input class="form-check-input" type="radio" name="jenis"id="irk-cek" value="1" checked>
                    <label class="form-check-label" for="irk-cek">
                        <span class="mx-4">Permohonan IRK</span>
                    </label>
                    <input class="form-check-input" type="radio" name="jenis" id="kkrp-cek" value="2" >
                    <label class="form-check-label" for="kkrp-cek">
                        <span class="mx-4">Permohonan KKRP</span>
                    </label>
                    <input class="form-check-input" type="radio" name="jenis"id="rekomtek-cek" value="3" >
                    <label class="form-check-label" for="rekomtek-cek">
                        <span class="mx-4">Permohonan IRK</span>
                    </label>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Nomor Telepon:</label>
                        <input type="number" class="form-control" id="phone" required name="phone">
                    </div>
                </div>
                    
                <!-- For success/fail messages -->
                <button type="submit" class="btn btn-primary"><i class='fa fa-paper-plane'></i>&nbsp;Cek Status</button>
                <p> <div id="success"></div>
            </form>
        </div>
        <div class="col-md-6">
            <div class="content" id="content">

            </div>
        </div>
    </div>
    
    <form id="formCetak" target="_blank" class="d-none" action="<?= base_url() ?>cari-surat/cetak" method="POST">
        <input type="hidden" id="kode_form" name="kode_form">
        <input type="hidden" id="jenis" name="jenis">
    </form>
</div>
<?php include 'style.php';?>
<?php include_once 'script.php' ;?>
