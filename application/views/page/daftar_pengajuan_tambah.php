 

 <div class="modal fade" id="ModalProsesSKRK" role="dialog">
    <div class="modal-dialog modal-small"> 
            <div class='modal-content'> 
                
                <div class='modal-header'>
                            <h4 class='modal-title' id='app_modal_label'>STATUS PENGAJUAN SKRK</h4>
                </div>
                <form action="#" name ="frm_pengajuan" id="frm_pengajuan" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="form-horizontal form-row-seperated">    
                    <div id='app_modal_body' class='modal-body'>
                            <table class='table table-condensed table-striped table-bordered'>
                            <thead></thead>
                            <tbody> 
                            
                            <input type="hidden" id="kode" name="kode">
                            <input type="hidden" id="aksi" name="aksi" value="updatestatus">
                            
                            <tr><td style="width:30%;">Tgl Pengajuan</td><td>   
                                    <input type="text"  id="a_tgl" name="a_tgl"  class="form-control" readonly="true">  
                            </td></tr> 
                            <tr><td>Nama Pemohon</td><td><input type="text"  id="a_nama" name="a_nama" readonly class="form-control"></td></tr>
                            <tr><td>Alamat</td><td><input type="text"  id="a_alamat" name="a_alamat" readonly class="form-control"></td></tr>
                            <tr><td>No Telp</td><td><input type="text"  id="a_telp" name="a_telp" readonly class="form-control"></td></tr>                            
                            <tr><td>Peruntukan</td><td><input type="text"  id="a_peruntukan" name="a_peruntukan"  readonly class="form-control"></td></tr>                              
                           
                            <tr><td>Status Tanah</td><td><input type="text"  id="a_status_t" name="a_status_t" readonly class="form-control"></td></tr> 
                            <tr><td>Status</td><td> 
                                     <select class="form-control" id="a_status" name="a_status">
                                        <option value="0" selected="">Masuk</option>
                                        <option value="1">Proses</option>
                                        <option value="2">Selesai</option> 
                                        <option value="3">Diambil</option> 
                                        <option value="4">Tolak</option>  
                                    </select> 
                            </td></tr> 
                            <tr><td>Tgl Perubahan Status</td><td><input type="text"  id="a_tgl_status" name="a_tgl_status" class="form-control"></td></tr> 
                            <tr><td>Keterangan</td><td>
                                    <textarea  rows="3" id="a_ket" name="a_ket"class="form-control"> </textarea> 
                                     
                                     
                                </td></tr> 
                            </tbody>
                            </table>
                    </div>
                </form> 
                    
                    <div id='app_modal_footer' class='modal-footer'>
                            <div class="col-md-12 left"> 
                            <div class="col-md-6 left"> 
                                <button type="button" id="btn_proses_pengajuan" name ="btn_proses_pengajuan"  class="btn btn-warning">Proses</button>
                                     
                             </div>
                             <div class="col-md-6">                    
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>                  
                             </div>
                             </div>
                        
                             
                    </div>
            </div>
    </div>
</div> 

 