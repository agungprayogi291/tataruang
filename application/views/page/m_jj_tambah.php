<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
                <div class='modal-content '>
                        <div class='modal-header'>
                                <h4 class='modal-title' id='app_modal_label'>Informasi Jalan </h4>
                        </div>
                        <div id='app_modal_body' class='modal-body'>

                                <table class='table table-condensed table-striped table-bordered'>
                                        <thead></thead>
                                        <tbody>
                                                <tr>
                                                        <td>ID / Klasifikasi</td>
                                                        <td id='a_id'></td>
                                                </tr>
                                                <tr>
                                                        <td>Nama Ruas Jalan</td>
                                                        <td id='a_ruas'></td>
                                                </tr>
                                                <tr>
                                                        <td>Titik Pengenal Pangkal/Ujung</td>
                                                        <td id='a_pengenal'></td>
                                                </tr>
                                                <tr>
                                                        <td>Panjang / Lebar</td>
                                                        <td id='a_panjang'></td>
                                                </tr>
                                                <tr>
                                                        <td>Lebar Bahu Jalan</td>
                                                        <td id='a_bahu'></td>
                                                </tr>
                                                <tr>
                                                        <td>Lebar Trotoar</td>
                                                        <td id='a_trotoar'></td>
                                                </tr>
                                                <tr>
                                                        <td>Lebar Drainase</td>
                                                        <td id='a_drainase'></td>
                                                </tr>
                                                <tr>
                                                        <td>Median</td>
                                                        <td id='a_median'></td>
                                                </tr>
                                                <tr>
                                                        <td>Jenis Kendaraan</td>
                                                        <td id='a_jenis_kend'></td>
                                                </tr>
                                                <tr>
                                                        <td>Tingkat Keramaian</td>
                                                        <td id='a_keramaian'></td>
                                                </tr>
                                                <tr>
                                                        <td>Perkerasan</td>
                                                        <td id='a_perkerasan'></td>
                                                </tr>
                                                <tr>
                                                        <td>Kondisi</td>
                                                        <td id='a_kondis'></td>
                                                </tr>
                                                <tr>
                                                        <td colspan="3" id='a_gambar' align="center"> </td>
                                                </tr>
                                        </tbody>
                                </table>
                        </div>

                        <!--                <div id="upload">
                    Gambar
                    <input type="file" name="file_gambar" id="file_gambar">
                </div>-->



                        <div id='app_modal_footer' class='modal-footer'>
                                <table border="0" style="width:100%">
                                        <tr>
                                                <td align="left">
                                                        <?php  if (isset($_SESSION["SudahLogin"])){  ?>
                                                        <button type='button' onclick="BukaUploadFoto();"
                                                                id='btn_upload' class='btn btn-danger btn-sm'><span
                                                                        class='glyphicon glyphicon-upload'></span>&nbsp;Upload
                                                                Gambar</button>
                                                        <?php } ?>
                                                </td>
                                                <td align="right">
                                                        <button data-dismiss="modal" type='button' id='cancel_order'
                                                                class='btn btn-default btn-sm'><span
                                                                        class='glyphicon glyphicon-off'></span>&nbsp;Tutup
                                                                / Keluar</button>
                                                </td>
                                        </tr>
                                </table>
                        </div>
                </div>
        </div>
</div>