<section class="content">
    <div class="container">
        <div class="col-md-10 col-sm-12 mx-auto">
            <div class="shadow card my-3">
                <div class="card-header">FORMULIR PERMOHONAN<BR> PERSETUJUAN KESESUAIAN KEGIATAN PEMANFAATAN RUANG</div>
                <div class="card-body">
                    <form id="formIRK">
                        <!-- Identitas Pemohon -->
                        <div class="mt-3">
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="namaPemohon" class="col-form-label">Nama Pemohon</label>
                                </div>
                              
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="namaPemohon" class="form-control"  id="namaPemohon" placeholder="Masukan nama pemohon">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="bertindakAtasNama" class="col-form-label">Bertindak untuk dan atas nama</label>
                                </div>
                            
                                <div class="col-sm-12 col-md-8">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline1" value="1" name="bertindakUntuk" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline1">Diri sendiri</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="customRadioInline2" value="2" name="bertindakUntuk" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline2">Orang lain</label>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-12 mt-1">
                                            <input type="text" class="form-control d-none" id="bertindakAtasNama" name="bertindakAtasNama" placeholder="Isi nama yang bertindak" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="alamatPemohon" class="col-form-label">Alamat</label>
                                </div>
                              
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="jalanPemohon" placeholder="Jalan" class="form-control"  id="alamatPemohon">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rtPemohon" placeholder="RT" class="form-control" >
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rwPemohon" placeholder="RW" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kelurahanPemohon" placeholder="Desa/Kelurahan" class="form-control" >
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="kecamatanPemohon" placeholder="Kecamatan" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12">
                                            <input type="text" name="kabupatenPemohon" placeholder="Kabupaten/Kota" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="noTelpPemohon" class=" col-form-label">Telepon</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="noTelpPemohon" class="form-control"  placeholder="Masukan no telepon" id="noTelpPemohon">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="emailPemohon" class="col-form-label">Email</label>
                                </div>
                          
                                <div class="col-sm-12 col-md-8">
                                    <input type="email" name="emailPemohon" class="form-control"  placeholder="Masukan email" id="emailPemohon">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="nikPemohon" class="col-form-label">NIK</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="nikPemohon" class="form-control"  placeholder="Masukan NIK" id="nikPemohon">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="pekerjaanPemohon" class="col-form-label">Pekerjaan</label>
                                </div>
                              
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="pekerjaanPemohon" class="form-control"  placeholder="Masukan NIK" id="pekerjaanPemohon">
                                </div>
                            </div>
                        </div>
                        <!-- Identitas Pemohon -->

                        <hr class="my-5">
                        <div class="card-header">
                            <p>Dengan ini mengajukan permohonan Kesesuaian Kegiatan Pemanfaatan Ruang sesuai Rencana Tata Ruang Wilayah (RTRW) dan/atau Rencana Detail Tata Ruang (RDTR) Kota Magelang yang berlaku pada lokasi tanah sesuai sertifikat, dengan data-data  sebagai berikut :</p>
                        </div>
                        <!-- Penguasaan Tanah -->
                        <div class="mt-3">
                            <h5 class="card-title mb-5">1.Penguasaan Tanah</h5>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="statusTanah" class="col-form-label">Status Tanah</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="statusTanah" class="form-control"  placeholder="Masukan status tanah" id="statusTanah">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="luasTanah" class="col-form-label">Luas</label>
                                </div>
                           
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="luasTanah" class="form-control"  placeholder="Masukan luas tanah" id="luasTanah">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="pemilikTanah" class="col-form-label">Atas Nama</label>
                                </div>
                             
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="pemilikTanah" class="form-control"  placeholder="Masukan pemilik tanah" id="pemilikTanah">
                                </div>
                            </div>
                        </div>
                        <!-- Penguasaan Tanah -->

                        <hr class="my-5">

                        <!-- Lokasi Lahan -->
                        <div class="mt-3">
                            <h5 class="card-title mb-5">2.Lokasi Lahan</h5>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="lokasiLahan" class=" col-form-label">Jalan / Kampung</label>
                                </div>
                           
                                <div class="col-sm-12 col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 mt-1">
                                            <input type="text" name="lokasiLahan" placeholder="Jalan" class="form-control"  id="lokasiLahan">
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rtLahan" placeholder="RT" class="form-control" >
                                        </div>
                                        <div class="col-md-3 mt-1">
                                            <input type="text" name="rwLahan" placeholder="RW" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                
                                    <div class="col-xs-12 col-md-4">
                                        <label class="col-form-label">Kelurahan</label>
                                    </div>
                                    <div class="col-xs-12 col-md-8">
                                        <input type="text" name="kelurahanLahan" placeholder="Kelurahan" class="form-control" style="line-height: 1.5; padding: 6px 12px;">
                                    </div>
                                
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="selectKecamatan" class="col-form-label">Kecamatan</label>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <select class="form-control form-control-lg" id="selectKecamatan" name="kecamatanLahan">
                                        <option selected>Open this select menu</option>
                                        <option value="Magelang Utara">Magelang Utara</option>
                                        <option value="Magelang Tengah">Magelang Tengah</option>
                                        <option value="Magelang Selatan">Magelang Selatan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label class="col-form-label">Kota</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" class="form-control" readonly  value="Magelang">
                                </div>
                            </div>
                        </div>
                        <!-- Lokasi Lahan -->

                        <hr class="my-5">

                        <!-- Batas-batas Tanah -->
                        <div class="mt-3">
                            <h5 class="card-title mb-5">3.Batas-batas tanah</h5>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="batasTanahUtara" class="col-form-label">Utara</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="batasTanahUtara" class="form-control"  placeholder="Batas utara tanah" id="batasTanahUtara">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="batasTanahSelatan" class="col-form-label">Selatan</label>
                                </div>
                             
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="batasTanahSelatan" class="form-control"  placeholder="Batas selatan tanah" id="batasTanahSelatan">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="batasTanahTimur" class="col-form-label">Timur</label>
                                </div>
                             
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="batasTanahTimur" class="form-control"  placeholder="Batas timur tanah" id="batasTanahTimur">
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label for="batasTanahBarat" class="col-form-label">Barat</label>
                                </div>
                               
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="batasTanahBarat" class="form-control"  placeholder="Batas barat tanah" id="batasTanahBarat">
                                </div>
                            </div>
                        </div>
                        <!-- Batas-batas Tanah -->

                        <hr class="my-5">

                        <!-- Rencana Penggunaan & Pemanfaatan Tanah -->
                        <div class="mt-3">
                            <h5 class="card-title mb-5">4.Rencana Penggunaan & Pemanfaatan Tanah</h5>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                    <label for="selectRencana" class="col-form-label">Rencana</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <select class="custom-select" id="selectRencana" name="rencanaPembangunan">
                                        <option selected>Open this select menu</option>
                                        <option value="perumahan">Perumahan</option>
                                        <option value="perdagangan dan jasa">Perdagangan dan Jasa</option>
                                        <option value="perkantoran">Perkantoran</option>
                                        <option value="sektor informal">Sektor Informal</option>
                                        <option value="kesehatan">Kesehatan</option>
                                        <option value="pendidikan">Pendidikan</option>
                                        <option value="peribadatan">Peribadatan</option>
                                        <option value="transportasi">Transportasi</option>
                                        <option value="olahraga">Olahraga</option>
                                        <option value="lainnya">Lainnya</option>
                                    </select>
                                    <div class="row mt-2">
                                        <div class="col-md-12 mt-1">
                                            <input type="text" name="rencanaLain" placeholder="Masukan rencana" class="form-control d-none"  id="rencanaLain">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Rencana Penggunaan & Pemanfaatan Tanah -->

                        <hr class="my-5">

                        <!-- Jenis Kegiatan -->
                        <div class="mt-3">
                            <h5 class="card-title mb-5">5.Jenis Kegiatan</h5>
                            <div class="form-group row mb-5">
                                <div class="col-sm-12 col-md-4">
                                <label class=" col-form-label">Kegiatan</label>
                                </div>
                                
                                <div class="col-sm-12 col-md-8">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radioJenisKegiatan1" name="jenisKegiatan" class="custom-control-input" value="berusaha">
                                        <label class="custom-control-label" for="radioJenisKegiatan1">Berusaha</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radioJenisKegiatan2" name="jenisKegiatan" class="custom-control-input" value="non berusaha">
                                        <label class="custom-control-label" for="radioJenisKegiatan2">Non Berusaha</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-md-4 col-sm-12">
                                <label for="rincianKegiatan" class="col-form-label">Rincian Kegiatan</label>
                                </div>
                             
                                <div class="col-sm-12 col-md-8">
                                    <input type="text" name="rincianKegiatan" class="form-control"  id="rincianKegiatan">
                                </div>
                            </div>
                        </div>
                        <!-- Jenis Kegiatan -->

                        <hr class="my-5">

                        <!-- Teknik Bangunan -->
                        <div class="mt-3">
                            <h5 class="card-title mb-5">6.Teknik Bangunan</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                <label for="luasLantaiDasar" class=" col-form-label">Luas Lantai Dasar Bangunan</label>
                                </div>
                         
                                <div class="col-xs-12 col-md-8">
                                    <div class="input-group">
                                        <div class="col-md-10 col-xs-8">
                                            <input type="text" name="luasLantaiDasar" class="form-control" id="luasLantaiDasar" >
                                        </div>
                                       <div class="col-md-2 col-xs-4">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">m<sup>2</sup></span>
                                            </div>
                                       </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="jumlahLantaiBangunan" class="col-form-label">Jumlah Lantai Bangunan</label>
                                </div>
                              
                                <div class="col-xs-12 col-md-8">
                                    <div class="input-group">
                                        <div class="col-md-10 col-xs-8">
                                            <input type="text" name="jumlahLantaiBangunan" class="form-control" id="jumlahLantaiBangunan" >
                                        </div>
                                       <dov class="col-md-2 col-xs-4">
                                        <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">lantai</span>
                                            </div>
                                       </dov>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="totalLuasLantai" class="col-form-label">Total Luas Lantai Bangunan</label>
                                </div>
                                
                                <div class="col-xs-12 col-md-8">
                                    <div class="input-group">
                                        <div class="col-xs-8 col-md-10">
                                        <input type="text" name="totalLuasLantai" class="form-control" id="totalLuasLantai" >
                                        </div>
                                        <div class="col-xs-4 col-md-2">
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">m<sup>2</sup></span>
                                        </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Teknik Bangunan -->
                            <!-- Rencana penggunaan air baku / air bersih -->
                            <div class="mt-3">
                            <h5 class="card-title mb-3">7.Rencana Penggunaan Air Bersih</h5>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="penggunaAir" class="col-form-label">Rencana Pengguna</label>
                                </div>
                                
                                <div class="col-xs-12 col-md-8">
                                    <div class="input-group">
                                        <div class="col-xs-8 col-md-10">
                                            <input type="text" name="penggunaAir" class="form-control" id="penggunaAir" >
                                        </div>
                                        <div class="col-xs-4 col-md-2">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">Orang/Hari</span>
                                            </div>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="jumlahKebutuhanAir" class="col-form-label">Jumlah Kebutuhan Air</label>
                                </div>

                                <div class="col-xs-12 col-md-8">
                                    <div class="input-group">
                                        <div class="col-xs-8 col-md-10">
                                            <input type="text" name="jumlahKebutuhanAir" class="form-control" id="jumlahKebutuhanAir" >
                                        </div>
                                        <div class="col-xs-4 col-md-2">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">Liter/orang/hari</span>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-5">
                                <div class="col-xs-12 col-md-4">
                                    <label for="sumberAir" class="col-form-label">Sumber Air Baku</label>
                                </div>

                                <div class="col-xs-12 col-md-8">
                                    <div class="input-group">
                                        <input type="text" name="sumberAir" class="form-control" id="sumberAir" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Rencana penggunaan air baku / air bersih -->
                        <hr class="my-5">

                        <!-- Lampiran permohonan -->
                        <div class="mt-3">
                            <h5 class="card-title mb-3">8.Lampiran Permohonan</h5>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fcKTP" id="fcKTP">
                                <label class="custom-control-label" for="fcKTP">Fotokopi KTP Pemohon</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcKTP">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fcNPWP" id="fcNPWP">
                                <label class="custom-control-label" for="fcNPWP">Fotokopi NPWP Pemohon</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcNPWP">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fcSertifikat" id="fcSertifikat">
                                <label class="custom-control-label" for="fcSertifikat">Fotokopi Sertifikat Tanah</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcSertifikat">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="suratIzinPenggunaanTanah" id="suratIzinPenggunaanTanah">
                                <label class="custom-control-label" for="suratIzinPenggunaanTanah">Surat Izin Penggunaan Tanah bermaterai beserta Fotokopi KTP apabila pemohon bukan pemilik tanah</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filesuratIzinPenggunaanTanah">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="suratKuasa" id="suratKuasa">
                                <label class="custom-control-label" for="suratKuasa">Surat Kuasa bermaterai beserta Fotokopi KTP apabila yang mengurus bukan pemohon</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filesuratKuasa">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="petaGoogle" id="petaGoogle">
                                <label class="custom-control-label" for="petaGoogle">Peta Google Map lokasi yang dimohon tampilan Satelit (Koordinasi Lokasi)</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filepetaGoogle">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fcPendaftaranPTP" id="fcPendaftaranPTP">
                                <label class="custom-control-label" for="fcPendaftaranPTP">Fotokopi Bukti Pendaftaran PTP (Pertimbangan Teknis Pertahanan) dari Kantor Pertahanan</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcPendaftaranPTP">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fcNIB" id="fcNIB">
                                <label class="custom-control-label" for="fcNIB">Fotokopi NIB (Nomor Induk Berusaha) dan Izin Kegiatan Berusaha yang diajukan</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcNIB">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fcAktePendirian" id="fcAktePendirian">
                                <label class="custom-control-label" for="fcAktePendirian">Fotokopi Akte pendirian perusahaan bagi pemohon yang berbentuk Badan Usaha</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefcAktePendirian">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="fotoExisting" id="fotoExisting">
                                <label class="custom-control-label" for="fotoExisting">Foto eksisting lokasi terbaru (Tampak 4 sisi)</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filefotoExisting">
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="rencanaTeknikBangunan" id="rencanaTeknikBangunan">
                                <label class="custom-control-label" for="rencanaTeknikBangunan">Rencana teknik bangunan dan/atau rencana untuk kawasan (Siteplan, Denah, Tampak 4 sisi, Potongan)</label>
                                <input type="file" class="form-control mt-1 mb-3 d-none" name="filerencanaTeknikBangunan">
                            </div>
                        </div>
                        <!-- Lampiran permohonan -->

                        <div class="text-center mt-5">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                    <form id="formCetak" target="_blank" class="d-none" action="<?= base_url('FormSuratPKKPR/cetak') ?>" method="POST">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    body{
        background-color:#dce0e8;
    }
</style>
<?php include 'script.php'; ?>