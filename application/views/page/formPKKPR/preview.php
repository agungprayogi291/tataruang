<html>
<head>
    <style></style>
</head>
<body>
<htmlpagefooter name="myFooter1" style="display:none;">
        <table>
            <tr>
                <td width="33%" style="font-style:italic;font-size:10px;">
                    {DATE j-m-y}
                </td>
                <td width="33%" style="text-align:right; font-size:10px; padding-right:0;">
                {PAGENO}/{nbpg}

                </td>
                
            </tr>
        </table>        
    </htmlpagefooter>
    <div class="kepada">
        <p>Kepada</p>
        <p>Yth. Walikota Magelang</p>
        <p>Cq. Kepala Dinas Pekerjaan Umum dan Penataan Ruang</p>
        <p style="margin-left: 30px;">Jl. Jend. Sudirman No. 54, Kel. Magersari, Kec. Magelang Selatan, Kota Magelang 56126</p>
        <p style="margin-left: 30px;">Telp. (0293) 362542</p>
        <p style="margin-left: 30px;">di <u><b>MAGELANG</b></u></p>
    </div>

    <p>Yang bertanda tangan di bawah ini :</p>
    <table style="width: 100%">
        <tr>
            <td width="30px">1.</td>
            <td width="35%">Nama Pemohon</td>
            <td width="20px"> : </td>
            <td colspan="2"><?= $cetak['namapemohon'] ?></td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Bertindak untuk dan atas nama</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['bertindakatasnama'] ?></td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Alamat</td>
            <td> : </td>
            <td>Jalan <?= $cetak['jalanpemohon'] ?></td>
            <td>RT <?= $cetak['rtpemohon'] ?> / RW <?= $cetak['rwpemohon'] ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Desa / Kelurahan <?= $cetak['kelurahanpemohon'] ?></td>
            <td>Kecamatan <?= $cetak['kecamatanpemohon'] ?></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td colspan="2">Kabupaten <?= $cetak['kabupatenpemohon'] ?></td>
        </tr>
        <tr>
            <td>4.</td>
            <td>No. Telp & Email</td>
            <td> : </td>
            <td><?= $cetak['notelppemohon'] ?></td>
            <td>& <?= $cetak['emailpemohon'] ?></td>
        </tr>
        <tr>
            <td>5.</td>
            <td>NIK</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['nikpemohon'] ?></td>
        </tr>
        <tr>
            <td>6.</td>
            <td>Pekerjaan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['pekerjaanpemohon'] ?></td>
        </tr>
    </table>

    <p>Dengan ini mengajukan permohonan Kesesuaian Kegiatan Pemanfaatan Ruang sesuai Rencana Tata Ruang Wilayah (RTRW) dan/atau Rencana Detail Tata Ruang (RDTR) Kota Magelang yang berlaku pada lokasi tanah sesuai sertifikat, dengan data-datasebagai berikut :</p>


    <table style="width: 100%">
        <tr>
            <td width="30px"></td>
            <td width="30px"></td>
            <td width="30%"></td>
            <td width="20px"></td>
            <td width="30%"></td>
            <td></td>
        </tr>
        <!-- PENGUASAAN TANAH -->
        <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td width="30px">1.</td>
            <td colspan="5"><b>Penguasaan Tanah</b></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Status Tanah</td>
            <td width="20px"> : </td>
            <td colspan="2"><?= $cetak['statustanah'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">No Tanah</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['luastanah'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Atas nama</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['pemiliktanah'] ?></td>
        </tr>

        <!-- LOKASI LAHAN -->
        <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td>2.</td>
            <td colspan="5"><b>Lokasi Lahan</b></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Jalan / Kampung</td>
            <td> : </td>
            <td>Jalan <?= $cetak['lokasilahan'] ?></td>
            <td>RT <?= $cetak['rtlahan'] ?> / RW <?= $cetak['rwlahan'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Kelurahan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['kelurahanlahan'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Kecamatan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['kecamatanlahan'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Kota</td>
            <td> : </td>
            <td colspan="2">Magelang</td>
        </tr>

        <!-- BATAS BATAS TANAH -->
        <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td>3.</td>
            <td colspan="5"><b>Batas-batas tanah</b></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Utara</td>
            <td>:</td>
            <td><?= $cetak['batastanahutara'] ?></td>
            <td>Timur : <?= $cetak['batastanahtimur'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Selatan</td>
            <td>:</td>
            <td><?= $cetak['batastanahselatan'] ?></td>
            <td>Barat : <?= $cetak['batastanahbarat'] ?></td>
        </tr>

        <!-- RENCANA PENGGUNAAN -->
        <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td>4.</td>
            <td colspan="2"><b>Rencana Penggunaan</b></td>
            <td> : </td>
            <td><?= $cetak['rencanapembangunan'] ?></td>
        </tr>

        <!-- JENIS KEGIATAN -->
        <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td>5.</td>
            <td colspan="2"><b>Jenis Kegiatan</b></td>
            <td> : </td>
            <td><?= $cetak['jeniskegiatan'] ?></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Rincian kegiatan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['rinciankegiatan'] ?></td>
        </tr>

        <!-- TEKNIK BANGUNAN -->
        <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td>6.</td>
            <td colspan="5"><b>Teknik Bangunan</b></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Luas Lantai Dasar Bangunan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['luaslantaidasar'] ?> m<sup>2</sup></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Jumlah Lantai Bangunan</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['jumlahlantaibangunan'] ?> lantai</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Total Luas Lantai</td>
            <td> : </td>
            <td colspan="2"><?= $cetak['totalluaslantai'] ?> m<sup>2</sup></td>
        </tr>

        <!-- TEKNIK BANGUNAN -->
        <tr>
            <td style="color: white; font-size: 3px;">afasdfa</td>
        </tr>
        <tr>
            <td>7.</td>
            <td colspan="5"><b>Rencana penggunaan air baku / air bersih</b></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Rencana Pengguna</td>
            <td> : </td>
            <td><?= $cetak['penggunaair'] ?> Orang/Hari</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Jumlah Kebutuhan Air</td>
            <td> : </td>
            <td><?= $cetak['jumlahkebutuhanair'] ?> Liter/orang/hari</td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2">Sumber Air Baku</td>
            <td> : </td>
            <td><?= $cetak['sumberair'] ?></td>
        </tr>

        <!-- LAMPIRAN -->
        <tr>
            <td colspan="6" style="color: white; font-size: 30px;">afasdfa</td>
        </tr>
        <tr>
            <td style="height:30px">8.</td>
            <td colspan="5"><b>Lampiran permohonan</b></td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcktp']) ? "√" : "") ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Fotokopi KTP Pemohon</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcnpwp']) ? "√" : "") ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Fotokopi NPWP Pemohon</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcsertifikat']) ? "√" : "") ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Fotokopi Sertifikat Tanah</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['suratizinpenggunaantanah']) ? "√" : "") ?>
            </td>
            <td colspan="4" style="padding-left: 10px;">Surat Izin Penggunaan Tanah bermaterai beserta Fotokopi KTP apabila pemohon bukan pemilik tanah</td>
        </tr>
      
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['petagoogle']) ? "√" : "") ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Peta Google Map lokasi yang dimohon tampilan Satelit (Koordinasi Lokasi)</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcpendaftaranptp']) ? "√" : "") ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Fotokopi Bukti Pendaftaran PTP (Pertimbangan Teknis Pertahanan) dari Kantor Pertahanan</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcnib']) ? "√" : "") ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Fotokopi NIB (Nomor Induk Berusaha) dan Izin Kegiatan Berusaha yang diajukan</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fcaktependirian']) ? "√" : "") ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Fotokopi Akte pendirian perusahaan bagi pemohon yang berbentuk Badan Usaha</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['fotoexisting']) ? "√" : "") ?>
            </td>
            <td colspan="4" style="padding-left: 10px; height: 30px">Foto eksisting lokasi terbaru (Tampak 4 sisi)</td>
        </tr>
        <tr>
            <td></td>
            <td class="cekbox">
                <?= (isset($cetak['rencanateknikbangunan']) ? "√" : "") ?>
            </td>
            <td colspan="4" style="padding-left: 10px;">Rencana teknik bangunan dan/atau rencana untuk kawasan (Siteplan, Denah, Tampak 4 sisi, Potongan)</td>
        </tr>
    </table>

    <br>
    <br>

    <table style="width: 100%;">
        <tr>
            <td width="60%"></td>
            <td>
                <table class="center">
                    <tr>
                        <td>Magelang,.......................20....</td>
                    </tr>
                    <tr>
                        <td>Pemohon</td>
                    </tr>
                    <tr>
                        <td style="height: 50px;"></td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px dotted black;">nama Pemohon</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</body>

</html>