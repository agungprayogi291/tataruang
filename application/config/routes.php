<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 

$route['default_controller'] = 'main';
$route['tataruang'] = 'index.php?page=tr';

$route['login'] = "Account/page_login";
$route['do_login'] = "Account";
$route['logout'] = "Account/logout";
//$route['default_controller'] = 'login';

$route['tataruang'] = "TataRuang";
$route['imb'] = "Imb";
$route['imb-statistic'] ="Imb/statistic";
$route['skrk-ajuan'] = "SkrkAjuan";
$route['daftar-pengajuan-skrk'] = "SkrkAjuan/daftarPengajuanSkrk";
$route['daftar-masukan-jadi-gampang'] = "JadiGampang/daftarMasukanJadiGampang";
$route['daftar-laporan-jadi-gampang'] = "JadiGampang/daftarLaporanJadiGampang";

$route['tambah-pengguna'] = "Account/addUser";
$route['setting-account'] = "Account/edit";

$route['s-p'] ="Account/session_password";



$route['form-surat'] = "FormSurat";
$route['form-surat-cetak'] = "FormSurat/cetak";
//$route['daftar-surat-pengajuan-tanah'] = "FormSurat/viewListSuratTanah";
// $route['list-penggunaan-tanah'] = "FormSurat/listSuratTanah";

$route['form-surat-irk'] = "FormSuratIRK";
$route['form-surat-irk-cetak'] = "FormSuratIRK/cetak";
$route['daftar-surat-irk'] = "FormSuratIRK/viewListSuratIRK";
$route['list-surat-irk'] = "FormSuratIRK/listSuratIRK";
$route['permohonan-irk-cetak-ulang'] = 'FormSuratIRK/cetakUlang';
$route['permohonan-irk-cetak-ulang-kuasa'] = 'FormSuratIRK/cetakUlangKuasa';
$route['permohonan-irk-update-status'] = 'FormSuratIRK/updateStatus';
$route['permohonan-irk-get'] = 'FormSuratIRK/get';
$route['permohonan-irk-get-surat'] = "FormSuratIRK/getPermohonan";
$route['verification-irk'] = "FormSuratIRK/verification"; 

$route['form-surat-pbg'] = "FormSuratPBG";
$route['form-surat-pbg-cetak'] = "FormSuratPBG/cetak";
$route['daftar-permohonan-pbg'] = "FormSuratPBG/viewListSuratPBG";
$route['list-permohonan-pbg'] = "FormSuratPBG/listSuratPBG";
$route['permohonan-pbg-cetak-ulang'] = "FormSuratPBG/cetakUlang";
$route['permohonan-pbg-update-status'] = 'FormSuratPBG/updateStatus';
$route['permohonan-pbg-get'] = 'FormSuratPBG/get';
$route['permohonan-pbg-get-surat'] = "FormSuratPBG/getPermohonan";
$route['verification-pbg'] = "FormSuratPBG/verification"; 
$route['form-surat-kuasa'] = "FormSuratKuasa";
$route['form-surat-kuasa-cetak'] = "FormSuratKuasa/cetak";



$route['form-surat-pkkpr'] = "FormSuratPKKPR";
$route['form-surat-pkkpr-cetak'] = "FormSuratPKKPR/cetak";
$route['daftar-permohonan-pkkpr'] = "FormSuratPKKPR/viewListPermohonanPKKPR";
$route['list-permohonan-pkkpr'] = "FormSuratPKKPR/listPermohonan";
$route['permohonan-pkkpr-cetak-ulang'] = 'FormSuratPKKPR/cetakUlang';
$route['permohonan-pkkpr-cetak-ulang-kuasa'] = 'FormSuratPKKPR/cetakUlangKuasa';
$route['permohonan-pkkpr-update-status'] = 'FormSuratPKKPR/updateStatus';
$route['permohonan-pkkpr-get'] = 'FormSuratPKKPR/get';
$route['permohonan-pkkpr-get-surat'] = "FormSuratPKKPR/getPermohonan";
$route['verification-pkkpr'] = "FormSuratPKKPR/verification"; 


$route['cari-surat'] = "CariSurat";
$route['penelusuran/cari'] = "CariSurat/get";
$route['cari-surat/cetak'] ="CariSurat/cetak";
// $route['user.html'] = 'user/simpan';
// $route['cbg.html'] = 'master/cabang/tampil';
// $route['kabupaten.html'] = 'master/mainmaster/loadkabbyprov';
// $route['kecamatan.html'] = 'master/mainmaster/loadkecbykab';
// $route['kelurahan.html'] = 'master/mainmaster/loadkelbykec';
 
//$route['cbg.html'] = 'master/cabang/tampil';
//http://localhost/project/2020/jtlexpres/Login

//$route['cbg/(:any)'] = 'master/cabang/tampil/$1';
//$route['cbg/(:any)/(:any)'] = 'master/cabang/tampil/$1/$2';


$route['download'] = "Download/download";
$route['404_override'] = 'Develop';
$route['translate_uri_dashes'] = FALSE;




$route['setting/simbologi'] = "Simbologi";
$route['slider'] = "Slider";