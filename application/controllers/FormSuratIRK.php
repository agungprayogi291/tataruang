<?php
class FormSuratIRK extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('SimpanFormSurat_m');
        $this->load->model("SuratPermohonan_m");
        $this->load->model("Verification_m");
    }

    public function index()
    {
        $this->data['page'] = '/page/formIRK/view.php';
        // $this->data['page'] = '/page/formSurat/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->data['lang'] =null;
        $this->data['lat'] =null;
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }

    public function upload($file)
    {
        $config['upload_path'] = "./assets/files";
        $config['allowed_types'] = 'pdf|docx|xlsx|jpeg|JPEG|JPG|png|PNG|doc';
        $config['encrypt_name'] = TRUE;
        //$config['file_name'] = time() . $_FILES[$file]['name'];
        // dd($file);
        $this->load->library('upload', $config);
        $hasil = [];
        if ($this->upload->do_upload($file)) {
            $data = ['upload_data' => $this->upload->data()];
            $hasil = $data['upload_data']['file_name'];
        } else {
            $error = array('error' => $this->upload->display_errors());
            $hasil = $error;
        }

        return $hasil;
    }
    function validation_irk($validation)
    {
     
        $validation->set_rules("namaPemohon", "Nama Pemohon ", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("bertindakUntuk", "Bertindak untuk", "required", array("required" => "Mohon dipilih atas nama"));
        $validation->set_rules("jenisKegiatan", "Jenis Kegiatan", "required", array("required" => "%s Harus Di Isi"));
    
        $validation->set_rules("jalanPemohon", "Alamat Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rtPemohon", "RT Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rwPemohon", "RW Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kelurahanPemohon", "Desa Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kecamatanPemohon", "Kecamatan Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kabupatenPemohon", "Kabupaten/Kota Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("noTelpPemohon", "No Telp Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("emailPemohon", "Email Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("nikPemohon", "NIK Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("pekerjaanPemohon", "Pekerjaan Pemohon", "required", array("required" => "%s Harus Di Isi"));
    
        $validation->set_rules("statusTanah", "Status Tanah", "required", array("required" => "%s Harus Di Pilih"));
        $validation->set_rules("luasTanah", "Luas Tanah", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("pemilikTanah", "Pemilik Tanah", "required", array("required" => "%s Harus Di Isi"));
    
        $validation->set_rules("lokasiLahan", "Lokasi Lahan", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rtLahan", "RT Lahan", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rwLahan", "RW Lahan", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kelurahanLahan", "Kelurahan Lahan", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kecamatanLahan", "Kecamatan Lahan", "required", array("required" => "%s Harus Di Isi"));
    
        $validation->set_rules("batasTanahUtara", "Batas Tanah Utara", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("batasTanahSelatan", "Batas Tanah Selatan", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("batasTanahTimur", "Batas Tanah Timur", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("batasTanahBarat", "Batas Tanah Barat", "required", array("required" => "%s Harus Di Isi"));
    
        $validation->set_rules("rencanaPembangunan", "Rencana Pembangunan", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("jenisKegiatan", "Jenis Kegiatan", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rincianKegiatan", "Rincian Kegiatan", "required", array("required" => "%s Harus Di Isi"));
    
        $validation->set_rules("luasLantaiDasar", "Luas Lantai Dasar", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("jumlahLantaiBangunan", "Jumlah Lantai Bangunan", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("totalLuasLantai", "Total Luas Lantai", "required", array("required" => "%s Harus Di Isi"));
    
    }
    function validation_kuasa($validation)
    {
        $validation->set_rules("namaP1", "Nama Pihak Pertama", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("noktpP1", "No KTP Pihak Pertama", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("pekerjaanP1", "Pekerjaan Pihak Pertama", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("noTelpP1", "No Telp Pihak Pertama", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("jalanP1", "Alamat Pihak Pertama", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rtP1", "RT Pihak Pertama", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rwP1", "RW Pihak Pertama", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kelurahanP1", "Kelurahan Pihak Pertama", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kecamatanP1", "Kecamatan Pihak Pertama", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kabupatenP1", "Kabupaten Pihak Pertama", "required", array("required" => "%s Harus Di Isi"));

        $validation->set_rules("namaP2", "Nama Pihak Kedua", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("noktpP2", "No KTP Pihak Kedua", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("pekerjaanP2", "Pekerjaan Pihak Kedua", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("noTelpP2", "No Telp Pihak Kedua", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("jalanP2", "Alamat Pihak Kedua", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rtP2", "RT Pihak Kedua", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rwP2", "RW Pihak Kedua", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kelurahanP2", "Kelurahan Pihak Kedua", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kecamatanP2", "Kecamatan Pihak Kedua", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kabupatenP2", "Kabupaten Pihak Kedua", "required", array("required" => "%s Harus Di Isi"));

        $validation->set_rules("fungsiBangunanKuasa", "Fungsi Bangunan", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("jalanLahanKuasa", "Alamat Lahan Kuasa", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rtLahanKuasa", "RT Lahan Kuasa", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rwLahanKuasa", "RW Lahan Kuasa", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kelurahanLahanKuasa", "Kelurahan Lahan Kuasa", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kecamatanLahanKuasa", "Kecamatan Lahan Kuasa", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kabupatenLahanKuasa", "Kabupaten Lahan Kuasa", "required", array("required" => "%s Harus Di Isi"));

        $validation->set_rules("hakTanahKuasa", "Hak Tanah Kuasa", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("noTanahKuasa", "No Tanah Kuasa", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("pemilikTanahKuasa", "Pemilik Tanah Kuasa", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("luasTanahKuasa", "Luas Tanah Kuasa", "required", array("required" => "%s Harus Di Isi"));
    }

    public function simpan()
    {
        $valid= $this->form_validation;
        $this->validation_irk($valid);

        // Surat Kuasa
        if (isset($_POST['bertindakUntuk']) && $_POST['bertindakUntuk'] == 2) {
            $this->validation_kuasa($valid);
        }

        $result = [];
        if ($valid->run() == FALSE) {
            $result = [
                "message" => validation_errors(),
                "success" => 0
            ];
        } else {
            if ($_POST) {
                $post = $this->input->post();

                $data = [
                    'namapemohon' => anti_injection($post['namaPemohon']),
                    'bertindakatasnama' => anti_injection($post['bertindakAtasNama']),
                    'bertindakuntuk' => anti_injection($post['bertindakUntuk']),
                    'jalanpemohon' => anti_injection($post['jalanPemohon']),
                    'rtpemohon' => anti_injection($post['rtPemohon']),
                    'rwpemohon' => anti_injection($post['rwPemohon']),
                    'kelurahanpemohon' => anti_injection($post['kelurahanPemohon']),
                    'kecamatanpemohon' => anti_injection($post['kecamatanPemohon']),
                    'kabupatenpemohon' => anti_injection($post['kabupatenPemohon']),
                    'notelppemohon' => anti_injection($post['noTelpPemohon']),
                    'emailpemohon' => anti_injection($post['emailPemohon']),
                    'nikpemohon' => anti_injection($post['nikPemohon']),
                    'pekerjaanpemohon' => anti_injection($post['pekerjaanPemohon']),
                    'statustanah' => ($post['statusTanah'] == 'lainnya' ? anti_injection($post['statusLain']) : anti_injection($post['statusTanah'])),
                    'luastanah' => anti_injection($post['luasTanah']),
                    'pemiliktanah' => anti_injection($post['pemilikTanah']),
                    'lokasilahan' => anti_injection($post['lokasiLahan']),
                    'rtlahan' => anti_injection($post['rtLahan']),
                    'rwlahan' => anti_injection($post['rwLahan']),
                    'kelurahanlahan' => anti_injection($post['kelurahanLahan']),
                    'kecamatanlahan' => anti_injection($post['kecamatanLahan']),
                    'batastanahutara' => anti_injection($post['batasTanahUtara']),
                    'batastanahselatan' => anti_injection($post['batasTanahSelatan']),
                    'batastanahtimur' => anti_injection($post['batasTanahTimur']),
                    'batastanahbarat' => anti_injection($post['batasTanahBarat']),
                    'rencanapembangunan' => ($post['rencanaPembangunan'] == 'lainnya' ? anti_injection($post['rencanaLain']) : anti_injection($post['rencanaPembangunan'])),
                    'jeniskegiatan' => anti_injection($post['jenisKegiatan']),
                    'rinciankegiatan' => anti_injection($post['rincianKegiatan']),
                    'luaslantaidasar' => anti_injection($post['luasLantaiDasar']),
                    'jumlahlantaibangunan' => anti_injection($post['jumlahLantaiBangunan']),
                    'totalluaslantai' => anti_injection($post['totalLuasLantai']),
                    'verifikasi_telephone' => anti_injection($post['verifikasi_telp']),
                    'lang' => anti_injection($post['lang']),
                    'lat' => anti_injection($post['lat'])
                ];

                $data_kuasa = [];

                if (isset($_POST['fcKTP'])) {

                    $filename = $this->upload('filefcKTP');
                    $data['fcktp'] = anti_injection($post['fcKTP']);
                    $data['file_fcktp'] = $filename;
                }

                if (isset($_POST['fcNPWP'])) {
                    $filename = $this->upload('filefcNPWP');
                    $data['fcnpwp'] = anti_injection($post['fcNPWP']);
                    $data['file_fcnpwp'] = $filename;
                }

                if (isset($_POST['fcSertifikat'])) {
                    $filename = $this->upload('filefcSertifikat');
                    $data['fcsertifikat'] = anti_injection($post['fcSertifikat']);
                    $data['file_fcsertifikat'] = $filename;
                }

                if (isset($_POST['suratIzinPenggunaaanTanah'])) {
                    $filename = $this->upload('filesuratIzinPenggunaaanTanah');
                    $data['suratizinpenggunaantanah'] = anti_injection($post['suratIzinPenggunaaanTanah']);
                    $data['file_suratizinpenggunaantanah'] = $filename;
                }

                if (isset($_POST['suratKuasa'])) {
                    $filename = $this->upload('filesuratKuasa');
                    $data['suratkuasa'] = anti_injection($post['suratKuasa']);
                    $data['file_suratkuasa'] = $filename;
                }

                if (isset($_POST['petaGoogle'])) {
                    $filename = $this->upload('filepetaGoogle');
                    $data['petagoogle'] = anti_injection($post['petaGoogle']);
                    $data['file_petagoogle'] = $filename;
                }

                if (isset($_POST['fcNIB'])) {
                    $filename = $this->upload('filefcNIB');
                    $data['fcnib'] = anti_injection($post['fcNIB']);
                    $data['file_fcnib'] = $filename;
                }

                if (isset($_POST['pernyataanMandiri'])) {
                    $filename = $this->upload('filepernyataanMandiri');
                    $data['pernyataanmandiri'] = anti_injection($post['pernyataanMandiri']);
                    $data['file_pernyataanmandiri'] = $filename;
                }

                if (isset($_POST['fcAktePendirian'])) {
                    $filename = $this->upload('filefcAktePendirian');
                    $data['fcaktependirian'] = anti_injection($post['fcAktePendirian']);
                    $data['file_fcaktependirian'] = $filename;
                }

                if (isset($_POST['fotoExisting'])) {
                    $filename = $this->upload('filefotoExisting');
                    $data['fotoexisting'] = anti_injection($post['fotoExisting']);
                    $data['file_fotoexisting'] = $filename;
                }

                if (isset($_POST['rencanaTeknikBangunan'])) {
                    $filename = $this->upload('filerencanaTeknikBangunan');
                    $data['rencanateknikbangunan'] = anti_injection($post['rencanaTeknikBangunan']);
                    $data['file_rencanateknikbangunan'] = $filename;
                }

                if ($post['bertindakUntuk'] == 2) {
                    $data_kuasa = [
                        'namap1' => anti_injection($post['namaP1']),
                        'noktpp1' => anti_injection($post['noktpP1']),
                        'pekerjaanp1' => anti_injection($post['pekerjaanP1']),
                        'notelpp1' => anti_injection($post['noTelpP1']),
                        'jalanp1' => anti_injection($post['jalanP1']),
                        'rtp1' => anti_injection($post['rtP1']),
                        'rwp1' => anti_injection($post['rwP1']),
                        'kelurahanp1' => anti_injection($post['kelurahanP1']),
                        'kecamatanp1' => anti_injection($post['kecamatanP1']),
                        'kabupatenp1' => anti_injection($post['kabupatenP1']),
                        'namap2' => anti_injection($post['namaP2']),
                        'noktpp2' => anti_injection($post['noktpP2']),
                        'pekerjaanp2' => anti_injection($post['pekerjaanP2']),
                        'notelpp2' => anti_injection($post['noTelpP2']),
                        'jalanp2' => anti_injection($post['jalanP2']),
                        'rtp2' => anti_injection($post['rtP2']),
                        'rwp2' => anti_injection($post['rwP2']),
                        'kelurahanp2' => anti_injection($post['kelurahanP2']),
                        'kecamatanp2' => anti_injection($post['kecamatanP2']),
                        'kabupatenp2' => anti_injection($post['kabupatenP2']),
                        'fungsibangunan' => anti_injection($post['fungsiBangunanKuasa']),
                        'jalanlahan' => anti_injection($post['jalanLahanKuasa']),
                        'rtlahan' => anti_injection($post['rtLahanKuasa']),
                        'rwlahan' => anti_injection($post['rwLahanKuasa']),
                        'kelurahanlahan' => anti_injection($post['kelurahanLahanKuasa']),
                        'kecamatanlahan' => anti_injection($post['kecamatanLahanKuasa']),
                        'kabupatenlahan' => anti_injection($post['kabupatenLahanKuasa']),
                        'haktanah' => anti_injection($post['hakTanahKuasa']),
                        'notanah' => anti_injection($post['noTanahKuasa']),
                        'pemiliktanah' => anti_injection($post['pemilikTanahKuasa']),
                        'luastanah' => anti_injection($post['luasTanahKuasa'])
                    ];

                    if ($this->SimpanFormSurat_m->simpan('kuasa', $data_kuasa) == true) {
                        $idkuasa = LoadNamaItemTabel('kuasa', 'id', "id=(SELECT max(id) FROM kuasa)");

                        $data['id_kuasa'] = $idkuasa;
                        if ($this->SimpanFormSurat_m->simpan('irk', $data) == true) {
                            // Simpan id form ke status permohonan
                            if ($this->simpanStatus('irk', 1) == true) {
                                $result = [
                                    'success' => 1, 'message' => "Form berhasil disimpan",
                                    'data' => $data
                                ];
                            }
                        }
                    }
                } else {
                    if ($this->SimpanFormSurat_m->simpan('irk', $data) == true) {
                        // Simpan id form ke status permohonan
                        if ($this->simpanStatus('irk', 1) == true) {
                            $result = [
                                'success' => 1, 'message' => "Form berhasil disimpan",
                                'data' => $data
                            ];
                        }
                    }
                }
            }
        }
        echo json_encode($result);
    }

    public function simpan_v2()
    {
        $valid = $this->form_validation;
        $valid->set_rules("namaPemohon", "Nama Pemohon ", "required", array("required" => "%s Harus Di Isi"));
        $valid->set_rules("jenisKegiatan", "Jenis Kegiatan", "required", array("required" => "%s Harus Di Isi"));
        $result = [];
        if ($valid->run() == FALSE) {
            $result = [
                "message" => validation_errors(),
                "success" => 0
            ];
        } else {
            if ($_POST) {
                $post = $this->input->post();

                $data = [
                    'namapemohon' => anti_injection($post['namaPemohon']),
                    'bertindakatasnama' => anti_injection($post['bertindakAtasNama']),
                    'jalanpemohon' => anti_injection($post['jalanPemohon']),
                    'rtpemohon' => anti_injection($post['rtPemohon']),
                    'rwpemohon' => anti_injection($post['rwPemohon']),
                    'kelurahanpemohon' => anti_injection($post['kelurahanPemohon']),
                    'kecamatanpemohon' => anti_injection($post['kecamatanPemohon']),
                    'kabupatenpemohon' => anti_injection($post['kabupatenPemohon']),
                    'notelppemohon' => anti_injection($post['noTelpPemohon']),
                    'emailpemohon' => anti_injection($post['emailPemohon']),
                    'nikpemohon' => anti_injection($post['nikPemohon']),
                    'pekerjaanpemohon' => anti_injection($post['pekerjaanPemohon']),
                    'statustanah' => ($post['statusTanah'] == 'lainnya' ? anti_injection($post['statusLain']) : anti_injection($post['statusTanah'])),
                    'luastanah' => anti_injection($post['luasTanah']),
                    'pemiliktanah' => anti_injection($post['pemilikTanah']),
                    'lokasilahan' => anti_injection($post['lokasiLahan']),
                    'rtlahan' => anti_injection($post['rtLahan']),
                    'rwlahan' => anti_injection($post['rwLahan']),
                    'kelurahanlahan' => anti_injection($post['kelurahanLahan']),
                    'kecamatanlahan' => anti_injection($post['kecamatanLahan']),
                    'batastanahutara' => anti_injection($post['batasTanahUtara']),
                    'batastanahselatan' => anti_injection($post['batasTanahSelatan']),
                    'batastanahtimur' => anti_injection($post['batasTanahTimur']),
                    'batastanahbarat' => anti_injection($post['batasTanahBarat']),
                    'rencanapembangunan' => ($post['rencanaPembangunan'] == 'lainnya' ? anti_injection($post['rencanaLain']) : anti_injection($post['rencanaPembangunan'])),
                    'jeniskegiatan' => anti_injection($post['jenisKegiatan']),
                    'rinciankegiatan' => anti_injection($post['rincianKegiatan']),
                    'luaslantaidasar' => anti_injection($post['luasLantaiDasar']),
                    'jumlahlantaibangunan' => anti_injection($post['jumlahLantaiBangunan']),
                    'totalluaslantai' => anti_injection($post['totalLuasLantai']),
                ];

                if (isset($_POST['fcKTP'])) {
                    $filename = $this->upload('filefcKTP');
                    $data['fcktp'] = anti_injection($post['fcKTP']);
                    $data['file_fcktp'] = $filename;
                }

                if (isset($_POST['fcNPWP'])) {
                    $filename = $this->upload('filefcNPWP');
                    $data['fcnpwp'] = anti_injection($post['fcNPWP']);
                    $data['file_fcnpwp'] = $filename;
                }

                if (isset($_POST['fcSertifikat'])) {
                    $filename = $this->upload('filefcSertifikat');
                    $data['fcsertifikat'] = anti_injection($post['fcSertifikat']);
                    $data['file_fcsertifikat'] = $filename;
                }

                if (isset($_POST['suratIzinPenggunaaanTanah'])) {
                    $filename = $this->upload('filesuratIzinPenggunaaanTanah');
                    $data['suratizinpenggunaantanah'] = anti_injection($post['suratIzinPenggunaaanTanah']);
                    $data['file_suratizinpenggunaantanah'] = $filename;
                }

                if (isset($_POST['suratKuasa'])) {
                    $filename = $this->upload('filesuratKuasa');
                    $data['suratkuasa'] = anti_injection($post['suratKuasa']);
                    $data['file_suratkuasa'] = $filename;
                }

                if (isset($_POST['petaGoogle'])) {
                    $filename = $this->upload('filepetaGoogle');
                    $data['petagoogle'] = anti_injection($post['petaGoogle']);
                    $data['file_petagoogle'] = $filename;
                }

                if (isset($_POST['fcNIB'])) {
                    $filename = $this->upload('filefcNIB');
                    $data['fcnib'] = anti_injection($post['fcNIB']);
                    $data['file_fcnib'] = $filename;
                }

                if (isset($_POST['pernyataanMandiri'])) {
                    $filename = $this->upload('filepernyataanMandiri');
                    $data['pernyataanmandiri'] = anti_injection($post['pernyataanMandiri']);
                    $data['file_pernyataanmandiri'] = $filename;
                }

                if (isset($_POST['fcAktePendirian'])) {
                    $filename = $this->upload('filefcAktePendirian');
                    $data['fcaktependirian'] = anti_injection($post['fcAktePendirian']);
                    $data['file_fcaktependirian'] = $filename;
                }

                if (isset($_POST['fotoExisting'])) {
                    $filename = $this->upload('filefotoExisting');
                    $data['fotoexisting'] = anti_injection($post['fotoExisting']);
                    $data['file_fotoexisting'] = $filename;
                }

                if (isset($_POST['rencanaTeknikBangunan'])) {
                    $filename = $this->upload('filerencanaTeknikBangunan');
                    $data['rencanateknikbangunan'] = anti_injection($post['rencanaTeknikBangunan']);
                    $data['file_rencanateknikbangunan'] = $filename;
                }

                // echo '<pre>';
                // print_r($data);
                // die();

                if ($this->SimpanFormSurat_m->simpan('irk', $data) == true) {
                    $result = [
                        'success' => 1, 'message' => "Form berhasil disimpan",
                        'data' => $data
                    ];
                }
            }
        }
        echo json_encode($result);
    }

    public function simpanStatus($table, $jenis)
    {
        $result = false;

        // Ambil id terakhir
        $idform = LoadNamaItemTabel($table, 'id', "id=(SELECT max(id) FROM $table)");
        $data_status = [
            'jenis' => $jenis,
            'tanggal' => Date('Y-m-d'),
            'keterangan' => '',
            'id_form' => $idform,
            'status' => 0
        ];

        // Simpan id form ke status permohonan
        if ($this->SimpanFormSurat_m->simpan('status_permohonan', $data_status) == true) {
            $result = true;
        }
        $this->SimpanFormSurat_m->simpan('status_log', $data_status);
        return $result;
    }

    public function cetak()
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');

        // echo '<pre>';
        // print_r($_POST);
        // die();

        $html = '';
        $data['cetak'] = $_POST;
        $data['html'] = $html;
        $filename              = 'Permohonan Informasi Rencana Kota';
        $author                = 'Form Surat';
        $header                = headerL(["app_title" => "
        Formulir<br> 
        PERMOHONAN	<br>
        INFORMASI RENCANA KOTA<br>
        ( IRK ) "]);
        $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
        $html                  = $this->load->view('page/formIRK/preview', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formIRK/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            // $mpdf->debug = true;
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->SetHTMLHeader($header);
            $mpdf->setFooter('{PAGENO}');

            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }
    }

    public function cetakUlang()
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');

        $id = anti_injection($this->input->post('kode_form'));
        $pengajuan  = LoadDataTabel("irk", "id=" . $id);
        $data['cetak'] = $pengajuan[0];
        $filename              = 'Permohonan Informasi Rencana Kota';
        $author                = 'Form Surat';
        $header                = headerL(["app_title" => "
        Formulir<br> 
        PERMOHONAN	<br>
        INFORMASI RENCANA KOTA<br>
        ( IRK ) "]);
        $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
        $html                  = $this->load->view('page/formIRK/daftar/preview', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formIRK/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            // $mpdf->debug = true;
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->SetHTMLHeader($header);
            $mpdf->setFooter('{PAGENO}');

            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }
    }

    public function cetakUlangKuasa()
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');
        $id = anti_injection($this->input->post('kode_form_kuasa'));
        $permohonan = $this->db->select("*")->from("kuasa")->where("id", $id)->get()->row();
        $data['cetak'] = $permohonan;
        $filename              = 'Surat Kuasa';
        $html                  = $this->load->view('page/formIRK/daftar/preview_ulang', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formKuasa/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        } catch (\Mpdf\MpdfException $e) {
            echo $e->getMessage();
        }
    }

    // public function simpan()
    // {
    //     $valid = $this->form_validation;
    //     $valid->set_rules("namaPemohon","Nama Pemohon","required" ,array("required" => '%s Harus Diisi'));
    //     $valid->set_rules("bertindakAtasNama","Bertindak Untuk dan atas nama" ,"required" , array("required"=>'%s Harus Diisi'));
    //     $valid->set_rules("jalanPemohon","Jalan Pemohon" ,"required",array("required" => '%s Harus Diisi'));
    //     $val>set_rules("nikPemohon","Nik Pemohon" ,"required" ,array("required" => '%s Harus Diisi'));
    //     $valid->set_rules("statusTanah","Status Tanah","required" ,array("required" => '%s Harus Diisi'));
    //     $valid->setid->set_rules("noTelpPemohon","Nomor Telepone Pemohon" ,"required",array("required" => '%s Harus Diisi'));
    //     $valid->set_rules("jenisKegiatan","Jenis Kegiatan" ,"required", array("requried" => "%s Harus Diisi"));
    //     $valid-_rules("luasTanah","Luas Tanah" ,"required",array("required" => '%s Harus Diisi'));

    //     $result = ['success' => 0, 'message' => ""];
    //     if($valid->run() == false){
    //         $result = ['success' => 0, 'message' => validation_errors()];
    //     }else{
    //     if ($_POST) {
    //         $post = $this->input->post();

    //         $data = [
    //             'namapemohon' => anti_injection($post['namaPemohon']),
    //             'bertindakatasnama' => anti_injection($post['bertindakAtasNama']),
    //             'jalanpemohon' => anti_injection($post['jalanPemohon']),
    //             'rtpemohon' => anti_injection($post['rtPemohon']),
    //             'rwpemohon' => anti_injection($post['rwPemohon']),
    //             'kelurahanpemohon' => anti_injection($post['kelurahanPemohon']),
    //             'kecamatanpemohon' => anti_injection($post['kecamatanPemohon']),
    //             'kabupatenpemohon' => anti_injection($post['kabupatenPemohon']),
    //             'notelppemohon' => anti_injection($post['noTelpPemohon']),
    //             'emailpemohon' => anti_injection($post['emailPemohon']),
    //             'nikpemohon' => anti_injection($post['nikPemohon']),
    //             'pekerjaanpemohon' => anti_injection($post['pekerjaanPemohon']),
    //             'statustanah' => ($post['statusTanah'] == 'lainnya' ? anti_injection($post['statusLain']) : anti_injection($post['statusTanah'])),
    //             'luastanah' => anti_injection($post['luasTanah']),
    //             'pemiliktanah' => anti_injection($post['pemilikTanah']),
    //             'lokasilahan' => anti_injection($post['lokasiLahan']),
    //             'rtlahan' => anti_injection($post['rtLahan']),
    //             'rwlahan' => anti_injection($post['rwLahan']),
    //             'kelurahanlahan' => anti_injection($post['kelurahanLahan']),
    //             'kecamatanlahan' => anti_injection($post['kecamatanLahan']),
    //             'batastanahutara' => anti_injection($post['batasTanahUtara']),
    //             'batastanahselatan' => anti_injection($post['batasTanahSelatan']),
    //             'batastanahtimur' => anti_injection($post['batasTanahTimur']),
    //             'batastanahbarat' => anti_injection($post['batasTanahBarat']),
    //             'rencanapembangunan' => ($post['rencanaPembangunan'] == 'lainnya' ? anti_injection($post['rencanaLain']) : anti_injection($post['rencanaPembangunan'])),
    //             'jeniskegiatan' => (anti_injection($post['jenisKegiatan']) == 1 ? "Berusaha" : "Non Berusaha") ,
    //             'rinciankegiatan' => anti_injection($post['rincianKegiatan']),
    //             'luaslantaidasar' => anti_injection($post['luasLantaiDasar']),
    //             'jumlahlantaibangunan' => anti_injection($post['jumlahLantaiBangunan']),
    //             'totalluaslantai' => anti_injection($post['totalLuasLantai']),
    //         ];

    //         if (isset($_POST['fcKTP'])) {
    //             $filename = $this->upload('filefcKTP');
    //             $data['fcktp'] = anti_injection($post['fcKTP']);
    //             $data['file_fcktp'] = $filename;
    //         }

    //         if (isset($_POST['fcNPWP'])) {
    //             $filename = $this->upload('filefcNPWP');
    //             $data['fcnpwp'] = anti_injection($post['fcNPWP']);
    //             $data['file_fcnpwp'] = $filename;
    //         }

    //         if (isset($_POST['fcSertifikat'])) {
    //             $filename = $this->upload('filefcSertifikat');
    //             $data['fcsertifikat'] = anti_injection($post['fcSertifikat']);
    //             $data['file_fcsertifikat'] = $filename;
    //         }

    //         if (isset($_POST['suratIzinPenggunaaanTanah'])) {
    //             $filename = $this->upload('filesuratIzinPenggunaaanTanah');
    //             $data['suratizinpenggunaantanah'] = anti_injection($post['suratIzinPenggunaaanTanah']);
    //             $data['file_suratizinpenggunaaantanah'] = $filename;
    //         }

    //         if (isset($_POST['suratKuasa'])) {
    //             $filename = $this->upload('filesuratKuasa');
    //             $data['suratkuasa'] = anti_injection($post['suratKuasa']);
    //             $data['file_suratkuasa'] = $filename;
    //         }

    //         if (isset($_POST['petaGoogle'])) {
    //             $filename = $this->upload('filepetaGoogle');
    //             $data['petagoogle'] = anti_injection($post['petaGoogle']);
    //             $data['file_petagoogle'] = $filename;
    //         }

    //         if (isset($_POST['fcNIB'])) {
    //             $filename = $this->upload('filefcNIB');
    //             $data['fcnib'] = anti_injection($post['fcNIB']);
    //             $data['file_fcnib'] = $filename;
    //         }

    //         if (isset($_POST['pernyataanMandiri'])) {
    //             $filename = $this->upload('filepernyataanMandiri');
    //             $data['pernyataanmandiri'] = anti_injection($post['pernyataanMandiri']);
    //             $data['file_pernyataanmandiri'] = $filename;
    //         }

    //         if (isset($_POST['fcAktePendirian'])) {
    //             $filename = $this->upload('filefcAktePendirian');
    //             $data['fcaktependirian'] = anti_injection($post['fcAktePendirian']);
    //             $data['file_fcaktependirian'] = $filename;
    //         }

    //         if (isset($_POST['fotoExisting'])) {
    //             $filename = $this->upload('fotoExisting');
    //             $data['fotoexisting'] = anti_injection($post['fotoExisting']);
    //             $data['file_fotoexisting'] = $filename;
    //         }

    //         if (isset($_POST['rencanaTeknikBangunan'])) {
    //             $filename = $this->upload('filerencanaTeknikBangunan');
    //             $data['rencanateknikbangunan'] = anti_injection($post['rencanaTeknikBangunan']);
    //             $data['file_rencanateknikbangunan'] = $filename;
    //         }

    //         // echo '<pre>';
    //         // print_r($data);
    //         // die();


    //         if ($this->SimpanFormSurat_m->simpan('irk', $data) == true) {
    //             $result = [
    //                 'success' => 1, 'message' => "Form berhasil disimpan",
    //                 'data' => $data
    //             ];
    //             // echo json_encode($result);
    //         }
    //     }
    //     }
    //     echo json_encode($result);
    // }

    // public function cetak()
    // {
    //     ini_set('memory_limit', '-1');
    //     ini_set('pcre.backtrack_limit', '50000000');

    //     // echo '<pre>';
    //     // print_r($_POST);
    //     // die();

    //     $html = '';
    //     $data['cetak'] = $_POST;
    //     $data['html'] = $html;
    //     $filename              = 'Permohonan Informasi Rencana Kota';
    //     $author                = 'Form Surat';
    //     $header                = headerL([]);
    //     $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
    //     $html                  = $this->load->view('page/formIRK/preview', $data, true);
    //     $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formIRK/style.css');

    //     try {
    //         $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
    //         // $mpdf->debug = true;
    //         $mpdf->SetAuthor('Admin');
    //         $mpdf->SetTitle('Formulir Permohonan');
    //         $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
    //         $mpdf->SetHTMLHeader($header);
    //         $mpdf->setFooter('{PAGENO}');

    //         $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
    //         $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
    //         $mpdf->Output($filename . '.pdf', 'I');
    //     } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
    //         // Process the exception, log, print etc.
    //         echo $e->getMessage();
    //     }
    // }

    public function simpan_v1()
    {
        if ($_POST) {
            $post = $this->input->post();

            $data = [
                'namapemohon' => anti_injection($post['namaPemohon']),
                'bertindakatasnama' => anti_injection($post['bertindakAtasNama']),
                'jalanpemohon' => anti_injection($post['jalanPemohon']),
                'rtpemohon' => anti_injection($post['rtPemohon']),
                'rwpemohon' => anti_injection($post['rwPemohon']),
                'kelurahanpemohon' => anti_injection($post['kelurahanPemohon']),
                'kecamatanpemohon' => anti_injection($post['kecamatanPemohon']),
                'kabupatenpemohon' => anti_injection($post['kabupatenPemohon']),
                'notelppemohon' => anti_injection($post['noTelpPemohon']),
                'emailpemohon' => anti_injection($post['emailPemohon']),
                'nikpemohon' => anti_injection($post['nikPemohon']),
                'statustanah' => anti_injection($post['statusTanah']),
                'luastanah' => anti_injection($post['luasTanah']),
                'pemiliktanah' => anti_injection($post['pemilikTanah']),
                'lokasilahan' => anti_injection($post['lokasiLahan']),
                'rtlahan' => anti_injection($post['rtLahan']),
                'rwlahan' => anti_injection($post['rwLahan']),
                'kelurahanlahan' => anti_injection($post['kelurahanLahan']),
                'batastanahutara' => anti_injection($post['batasTanahUtara']),
                'batastanahselatan' => anti_injection($post['batasTanahSelatan']),
                'batastanahtimur' => anti_injection($post['batasTanahTimur']),
                'batastanahbarat' => anti_injection($post['batasTanahBarat']),
                'rencanapembangunan' => anti_injection($post['rencanaPembangunan']),
                'jeniskegiatan' => (anti_injection($post['jenisKegiatan']) == "1") ? "Berusaha" : "Non Berusaha",
                'rinciankegiatan' => anti_injection($post['rincianKegiatan']),
                'luaslantaidasar' => anti_injection($post['luasLantaiDasar']),
                'jumlahlantaibangunan' => anti_injection($post['jumlahLantaiBangunan']),
                'totalluaslantai' => anti_injection($post['totalLuasLantai']),
            ];

            if (isset($_POST['fcKTP'])) {
                $filename = $this->upload('filefcKTP');
                $data['fcktp'] = anti_injection($post['fcKTP']);
                $data['file_fcktp'] = $filename;
            }

            if (isset($_POST['fcNPWP'])) {
                $filename = $this->upload('filefcNPWP');
                $data['fcnpwp'] = anti_injection($post['fcNPWP']);
                $data['file_fcnpwp'] = $filename;
            }

            if (isset($_POST['fcSertifikat'])) {
                $filename = $this->upload('filefcSertifikat');
                $data['fcsertifikat'] = anti_injection($post['fcSertifikat']);
                $data['file_fcsertifikat'] = $filename;
            }

            if (isset($_POST['suratIzinPenggunaaanTanah'])) {
                $filename = $this->upload('filesuratIzinPenggunaaanTanah');
                $data['suratizinpenggunaantanah'] = anti_injection($post['suratIzinPenggunaaanTanah']);
                $data['file_suratizinpenggunaaantanah'] = $filename;
            }

            if (isset($_POST['suratKuasa'])) {
                $filename = $this->upload('filesuratKuasa');
                $data['suratkuasa'] = anti_injection($post['suratKuasa']);
                $data['file_suratkuasa'] = $filename;
            }

            if (isset($_POST['petaGoogle'])) {
                $filename = $this->upload('filepetaGoogle');
                $data['petagoogle'] = anti_injection($post['petaGoogle']);
                $data['file_petagoogle'] = $filename;
            }

            if (isset($_POST['fcNIB'])) {
                $filename = $this->upload('filefcNIB');
                $data['fcnib'] = anti_injection($post['fcNIB']);
                $data['file_fcnib'] = $filename;
            }

            if (isset($_POST['pernyataanMandiri'])) {
                $filename = $this->upload('filepernyataanMandiri');
                $data['pernyataanmandiri'] = anti_injection($post['pernyataanMandiri']);
                $data['file_pernyataanmandiri'] = $filename;
            }

            if (isset($_POST['fcAktePendirian'])) {
                $filename = $this->upload('filefcAktePendirian');
                $data['fcaktependirian'] = anti_injection($post['fcAktePendirian']);
                $data['file_fcaktependirian'] = $filename;
            }

            if (isset($_POST['fotoExisting'])) {
                $filename = $this->upload('fotoExisting');
                $data['fotoexisting'] = anti_injection($post['fotoExisting']);
                $data['file_fotoexisting'] = $filename;
            }

            if (isset($_POST['rencanaTeknikBangunan'])) {
                $filename = $this->upload('filerencanaTeknikBangunan');
                $data['rencanateknikbangunan'] = anti_injection($post['rencanaTeknikBangunan']);
                $data['file_rencanateknikbangunan'] = $filename;
            }
            // echo "<pre>";
            // print_r($data);
            // die();


            if ($this->SimpanFormSurat_m->simpan('irk', $data) == true) {
                $result = ['success' => 1, 'message' => "Form berhasil disimpan"];
                echo json_encode($result);
            }
        }
    }



    public function viewListSuratIRK()
    {
        $this->data['page'] = '/page/formIRK/daftar/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }

    public function listSuratIRK()
    {
        $jenis = anti_injection($this->input->post('jenis'));
        $status = anti_injection($this->input->post('status'));
        $data = $this->SuratPermohonan_m->get("irk",$jenis,$status);
        $i = 1;
        $html = "";
        foreach ($data as $row) {
            $html  .= "<tr>";
            $html .= "<td style=' vertical-align: top;'><p>$i</p></td>";
            $html .= "<td style=' vertical-align: top;'>$row->namapemohon</td>";
            $html .= "<td style=' vertical-align: top;'>$row->nikpemohon</td>";
            $html .= "<td style='vertical-align:top;'>$row->verifikasi_telephone</td>";
            $html .= "<td style=' vertical-align: top;'>" . $row->jalanpemohon . "<br>kel." . $row->kelurahanpemohon . "<br>kec." . $row->kecamatanpemohon . "<br>kab." . $row->kabupatenpemohon . "</td>";
            $html .= "<td style=' vertical-align: top;' width='40%'>"
                .
                ($row->fcktp > 0 ?  "√ Fotokopi KTP Pemohon <a href='" . base_url() . "Download/download/" . $row->file_fcktp . "' target='_blank'>download</a><br>" :  "<span class='text-danger'>X Fotokopi KTP Pemohon</span><br>") .
                ($row->fcnpwp > 0 ?  "√ Fotokopi NPWP Pemohon <a href='" . base_url() . "Download/download/" . $row->file_fcnpwp . "' target='_blank'>download</a><br>" :  "<span class='text-danger'>X Fotokopi NPWP Pemohon</span><br>") .
                ($row->fcsertifikat > 0 ?  "√ Fotokopi Sertifikat Tanah <a href='" . base_url() . "Download/download/" . $row->file_fcsertifikat . "' target='_blank'>download</a><br>" :  "<span class='text-danger'>X Fotokopi Sertifikat Tanah</span><br>") .
                ($row->suratizinpenggunaantanah > 0 ?  "√ Surat Izin Penggunaan Tanah bermaterai beserta Fotokopi KTP apabila pemohon bukan pemilik tanah <a href='" . base_url() . "Download/download/" . $row->file_suratizinpenggunaantanah . "' target='_blank'>download</a><br>" :  "<span class='text-danger'>X Surat Izin Penggunaan Tanah bermaterai beserta Fotokopi KTP apabila pemohon bukan pemilik tanah</span><br>")
                .
                ($row->bertindakuntuk == 2 ?
                    ($row->id_kuasa > 0 ? "√ surat kuasa<br>" :  "<span class='text-danger'>X surat kuasa</span><br>") : ""
                )

                . ($row->petagoogle > 0 ?  "√ Peta Google Map lokasi yang dimohon tampilan Satelit (Koordinasi Lokasi) <a href='" . base_url() . "Download/download/" . $row->file_petagoogle . "' target='_blank'>download</a><br>" :  "<span class='text-danger'>x Peta Google Map lokasi yang dimohon tampilan Satelit (Koordinasi Lokasi)</span><br>")
                . ($row->fcnib > 0 ? "√ Fotokopi NIB (Nomor Induk Berusaha) dan Izin Kegiatan Berusaha yang diajukan <a href='" . base_url() . "Download/download/" . $row->file_fcnib . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X Fotokopi NIB (Nomor Induk Berusaha) dan Izin Kegiatan Berusaha yang diajukan</span><br>")
                . ($row->pernyataanmandiri > 0 ? "√ Self Declaration / Pernyataan Mandiri dari OSS (Pernyataan Sesuai Tata Ruang) <a href='" . base_url() . "Download/download/" . $row->file_pernyataanmandiri . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X Self Declaration / Pernyataan Mandiri dari OSS (Pernyataan Sesuai Tata Ruang)</span><br>")
                . ($row->fcaktependirian > 0 ? "√ Fotokopi Akte pendirian perusahaan bagi pemohon yang berbentuk Badan Usaha <a href='" . base_url() . "Download/download/" . $row->file_fcaktependirian . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X Fotokopi Akte pendirian perusahaan bagi pemohon yang berbentuk Badan Usaha</span><br>")
                . ($row->fotoexisting > 0  ? "√ Foto eksisting lokasi terbaru (Tampak 4 sisi) <a href='" . base_url() . "Download/download/" . $row->file_fotoexisting . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X Foto eksisting lokasi terbaru (Tampak 4 sisi)</span><br>")
                . ($row->rencanateknikbangunan > 0 ? "√  Rencana teknik bangunan dan/atau rencana untuk kawasan (Siteplan, Denah, Tampak 4 sisi, Potongan) <a href='" . base_url() . "Download/download/" . $row->file_fcktp . "' target='_blank'>download</a><br>" : " <span class='text-danger'>X Rencana teknik bangunan dan/atau rencana untuk kawasan (Siteplan, Denah, Tampak 4 sisi, Potongan)</span>")

                . "</td>";
            if ($row->status == 0) {
                $html .= "<td style=' vertical-align: top;'>daftar</td>";
            } else if ($row->status == 1) {
                $html .= "<td style=' vertical-align: top;'>proses</td>";
            } else if ($row->status == 2) {
                $html .= "<td style=' vertical-align: top;'>selesai</td>";
            } else {
                $html .= "<td style=' vertical-align: top;'></td>";
            }
            $html .= "<td style=' vertical-align: top;'>Bertindak untuk " . ($row->bertindakuntuk == 1 ? "sendiri" : "orang lain") . "</td>";
            $html .= "<td style=' vertical-align: top;'>";
            $html .= " 
                <button class='btn btn-info my-2 ' style='padding-right:50px;' onClick='openDetail($row->id)'><i class='bi bi-card-heading'></i> Detail</button>
                <button class='btn btn-primary my-2' onClick='cetakUlang($row->id)'><i class='bi bi-file-earmark-pdf'></i> Cetak Ulang</button>
                <button class='btn btn-danger'style='padding-right:15px;' onClick='openStatus($row->id)'> <i class='bi bi-calendar4-event mx-1'></i>Cek Status</button>
                ";
            if ($row->id_kuasa != null) {
                $html .= "<buttn class='btn btn-secondary my-2' onclick='cetakUlangKuasa($row->id_kuasa)'><i class='bi bi-file-earmark-pdf mx-1'></i>Cetak Kuasa</button>";
            }
            $html .= "</td>";
            $html .= "</tr>";
            $i++;
        }
        $response  = array(
            "tabel" => $html,
            "row" => $i
        );
        echo json_encode($response);
    }

    public function updateStatus()
    {
        $id_form = anti_injection($this->input->post("id_permohonan"));
        $tanggal = anti_injection($this->input->post("tanggal_status"));
        $status = anti_injection($this->input->post("level_status"));
        $keterangan = anti_injection($this->input->post('keterangan_status'));
        $jenis = 1;

        $data = [
            "jenis" => $jenis,
            "tanggal"  => $tanggal,
            "keterangan" => $keterangan,
            "id_form" => $id_form,
            "status" => $status
        ];

        $response = [
            "message" => "gagal",
            "success" => false
        ];
        //dd($this->SuratPermohonan_m->status($id_form,$data));
        if ($this->SuratPermohonan_m->status($id_form, $data)) {

            $response = [
                "message" => "berhasil",
                "success" => true
            ];
        }

        echo json_encode($response);
    }

    public function get()
    {
        $id_form = anti_injection($this->input->post('kode'));
        //$result = LoadDataTabel("status_permohonan","id_form =". $id_form);
        $result = $this->SuratPermohonan_m->getStatus("status_permohonan", $id_form, 1);
        if ($result == null) {
            $response = [
                "kode" => $id_form,
                "data" => null
            ];
        } else {
            $response = [
                "data" => $result
            ];
        }
        echo json_encode($response);
    }

    public function getPermohonan()
    {
        $id = anti_injection($this->input->post("kode"));
        $row = $this->db->select("*")->from("irk")->where("id", $id)->get()->row();
        $html = "";
        $htmlImg = "";
        $indicator = "";
        $lampiran = "";
        $ullist = "";
        // foreach($result as $row){
        $html .= "<tr><td>Nama Pemohon</td><td>$row->namapemohon</td></tr>";
        $html .= "<tr><td>Lokasi Pemohon</td><td>" . $row->jalanpemohon . " Rt/Rw " . $row->rtpemohon . "/" . $row->rwpemohon . " kel. " . $row->kelurahanpemohon . " kec." . $row->kecamatanpemohon . " kab." . $row->kabupatenpemohon . "</td></tr>";
        $html .= "<tr><td>No telp</td><td>$row->notelppemohon</td></tr>";
        $html .= "<tr><td>Email Pemohon</td><td>$row->emailpemohon</td></tr>";
        $html .= "<tr><td>Nik Pemohon</td><td>$row->nikpemohon</td></tr>";
        $html .= "<tr><td>Status Tanah</td><td>$row->statustanah</td></tr>";
        $html .= "<tr><td>lokasi lahan</td><td>" . $row->lokasilahan . " Rt/Rw" . $row->rtlahan . "/" . $row->rwlahan . " kel." . $row->kelurahanlahan . "</td></tr>";
        $html .= "<tr><td>Batas Tanah utara </td><td> " . $row->batastanahutara . "</td></tr>";
        $html .= "<tr><td>Batas Tanah selatan </td><td> " . $row->batastanahselatan . "</td></tr>";
        $html .= "<tr><td>Batas Tanah timur </td><td> " . $row->batastanahtimur . "</td></tr>";
        $html .= "<tr><td>Batas Tanah barat </td><td> " . $row->batastanahbarat . "</td></tr>";


        if ($row->fcktp > 0) {
            $htmlImg .= "<div class='carousel-item active'>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" . base_url() . "assets/files/" . $row->file_fcktp . "' alt='First slide' />";
            $htmlImg .= " <figcaption>Fc ktp</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= "<li
                data-target='#carouselExampleIndicators'
                data-slide-to='0'
                class='active'
                ></li>";
            $lampiran .= "<li>Fc KTP √</li>";
        }

        if ($row->fcnpwp > 0) {
            $htmlImg .= "<div class='carousel-item '>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" . base_url() . "assets/files/" . $row->file_fcnpwp . "' alt='First slide' />";
            $htmlImg .= " <figcaption>fc npwp</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= "<li
                data-target='#carouselExampleIndicators'
                data-slide-to='1'
                ></li>";

            $lampiran .= "<li>Fc NPWP √</li>";
        }

        if ($row->fcsertifikat > 0) {
            $htmlImg .= "<div class='carousel-item '>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" .   base_url() . "assets/files/" . $row->file_fcsertifikat . "' alt='First slide' />";
            $htmlImg .= " <figcaption>fc sertifikat</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= " <li
                data-target='#carouselExampleIndicators'
                data-slide-to='2'
                ></li>";

            $lampiran .= "<li>Fc Sertifikat √</li>";
        }

        if ($row->suratizinpenggunaantanah > 0) {
            $htmlImg .= "<div class='carousel-item'>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" . base_url() . "assets/files/" . $row->file_suratizinpenggunaantanah . "' alt='First slide' />";
            $htmlImg .= " <figcaption>Surat Izin Penggunaan Tanah</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= "<li
                data-target='#carouselExampleIndicators'
                data-slide-to='3'
                ></li>";

            $lampiran .= "<li>Surat Izin Penggunaan Tanah √</li>";
        }

        if ($row->suratkuasa > 0) {
            $htmlImg .= "<div class='carousel-item '>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" . base_url() . "assets/files/" . $row->file_suratkuasa . "' alt='First slide' />";
            $htmlImg .= " <figcaption>surat kuasa</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= "<li
                data-target='#carouselExampleIndicators'
                data-slide-to='4'
                ></li>";

            $lampiran .= "<li>Surat Kuasa √</li>";
        }

        if ($row->petagoogle > 0) {
            $htmlImg .= "<div class='carousel-item '>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" .   base_url() . "assets/files/" . $row->file_petagoogle . "' alt='First slide' />";
            $htmlImg .= " <figcaption>Peta google</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= " <li
                data-target='#carouselExampleIndicators'
                data-slide-to='5'
                ></li>";

            $lampiran .= "<li>Peta Google √</li>";
        }


        if ($row->fcnib > 0) {
            $htmlImg .= "<div class='carousel-item '>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" . base_url() . "assets/files/" . $row->file_fcnib . "' alt='First slide' />";
            $htmlImg .= " <figcaption>fcnib</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= "<li
                data-target='#carouselExampleIndicators'
                data-slide-to='6'
                ></li>";

            $lampiran .= "<li>FcNIB √</li>";
        }

        if ($row->pernyataanmandiri > 0) {
            $htmlImg .= "<div class='carousel-item '>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" .   base_url() . "assets/files/" . $row->file_pernyataanmandiri . "' alt='First slide' />";
            $htmlImg .= " <figcaption>peryataan mandiri</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= " <li
                data-target='#carouselExampleIndicators'
                data-slide-to='7'
                ></li>";

            $lampiran .= "<li>Peryatan Mandiri √</li>";
        }


        if ($row->fcaktependirian > 0) {
            $htmlImg .= "<div class='carousel-item '>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" .   base_url() . "assets/files/" . $row->file_fcaktependirian . "' alt='First slide' />";
            $htmlImg .= " <figcaption>fc akte pendirian</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= " <li
                data-target='#carouselExampleIndicators'
                data-slide-to='8'
                ></li>";

            $lampiran .= "<li>Fc Akte Pendirian √</li>";
        }


        if ($row->rencanateknikbangunan > 0) {
            $htmlImg .= "<div class='carousel-item '>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" .   base_url() . "assets/files/" . $row->file_rencanateknikbangunan . "' alt='First slide' />";
            $htmlImg .= " <figcaption>Rencana Teknik Bangunan</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= " <li
                data-target='#carouselExampleIndicators'
                data-slide-to='9'
                ></li>";

            $lampiran .= "<li>Rencana Teknik Bangunan √</li>";
        }







        $ullist .= "<ul>";
        $ullist .= $lampiran;
        $ullist .= "</ul>";
        //  $lampiran .="<ul>";

        // $lampiran .="<li>Sertifikat Tanah √</li>";
        // $lampiran .="<li>NPWP √</li>";
        // $lampiran .="</ul>";
        // }
        $response = [
            "detail" => $html,
            "img" => $htmlImg,
            "indicator" => $indicator,
            "lampiran" => $ullist
        ];
        echo json_encode($response);
    }



    public function verification()
    {
        $notelp = anti_injection($this->input->post('no_telp_verif'));
        $cekNomor = validation_phone($notelp);
        ///dd($cekNomor);
        if ($cekNomor['verif']) {
            $response = $this->Verification_m->cek("irk", $notelp, 1);
        } else {
            $response = [
                "message" => $cekNomor['message'],
                "access" => false
            ];
        }
        echo json_encode($response);
    }
}


// FORM VALIDATION

