<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengajuan extends CI_Controller {
    function __construct(){
        parent::__construct();
    }

    public function index(){
        $jenis = anti_injection($this->input->post('jenis'));
        $data =[
            'lang' =>  anti_injection($this->input->post('ajukan-lang')),
            'lat' =>  anti_injection($this->input->post('ajukan-lat'))
        ];
        switch ($jenis) {
            case '1':
                $this->formIrk($data);
                break;
            case '2';
                $this->formKKPR($data);
                break;
            case '3':
                $this->formRekomtek($data);
                break;
            default:
                redirect(base_url());
                break;
        }

    }

    private function formIrk($lokasi){
        $this->data['page'] = '/page/formIRK/view.php';
        // $this->data['page'] = '/page/formSurat/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->data['lang'] = $lokasi['lang'];
        $this->data['lat'] = $lokasi['lat'];
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }

    private function formKKPR($lokasi){
        $this->data['page'] = '/page/formPKKPR/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->data['lang'] = $lokasi['lang'];
        $this->data['lat'] = $lokasi['lat'];
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }


    private function formRekomtek($lokasi){
        $this->data['page'] = '/page/formPBG/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->data['lang'] = $lokasi['lang'];
        $this->data['lat'] = $lokasi['lat'];
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }
    
}