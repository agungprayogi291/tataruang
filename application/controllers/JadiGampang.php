<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JadiGampang extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('User_m');
    }

    public function daftarMasukanJadiGampang(){
        $data =[
            "home" => false,
            "baseMap" => false,
            "page" => "/page/daftar_masukan.php"
        ];
        $this->load->view("layout/wrapper",$data);
    }
    public function daftarLaporanJadiGampang(){
        $data = [
            "home"=> false,
            "baseMap" => false,
            "page" => "/page/daftar_laporan.php"
        ];
        $this->load->view("layout/wrapper",$data);
    }
}