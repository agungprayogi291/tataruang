<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('User_m');
       
    }

    public function index(){
        $username = anti_injection($this->input->post('u'));
        $password = anti_injection($this->input->post('p'));
        if($username =='' && $password == ''){
            $this->halaman_login();      
        }else{
            $this->do_login($username,$password);
        }
    }

    private function do_login($u,$p){
        $data = $this->User_m->login_user($u,$p);
        $hasil = $data['hasil'];
        if ($data['hasil']=='1'){ 
            $User = $data['isi']; 
            $IP     ='';
            //            $IP     =IpAddressClient();
            //$ses1   =load_setting_app();              
            //$menu = createmenu();
            if(password_verify($p,$User->passw_user)){
                $sessione =array(
                    'user_id' => $User->kode,                              
                    'user_nama' =>$User->nama_user,
                    'user_level'  =>$User->levele,
                    'user_namalengkap'  =>$User->nama_lengkap,                                                           
                    'logged' => TRUE,                                         
                    'ipAddr'  => $IP,
                    'password' => $p
                );               
               
                
                //           $this->session->sess_destroy();           
                //    $sessione= array_merge($ses2); 
               $this->session->set_userdata($sessione);   
               
               redirect(base_url(),'/'); 
            }else{
                $pesan ="password salah";
                $this->session_loged_false($u,$p,$pesan); 
            }
            
        }else{             
            $pesan ="Username tidak ditemukan";
             $this->session_loged_false($u,$p,$pesan);
        }

    }
    private function halaman_login(){
        $data = [
            "home" => false,
            "baseMap" => false,
            "page" =>"/page/login.php"
        ];
        $this->load->view("layout/wrapper",$data);
    }
    private function session_loged_false($u,$p,$pesan){
        $this->session->set_flashdata('logine',$pesan);
        $this->halaman_login($u,$p);
    }
    public function logout(){
        session_start();
        session_destroy();
        redirect(base_url(),'');
    }

    public function page_login(){
        $data = [
            "home" => false,
            "baseMap" => false,
            "page" =>"/page/login.php"
        ];
        $this->load->view("layout/wrapper",$data);
    }


    public function addUser(){
        if($this->session->userdata('user_level') < 1){
            redirect(base_url());
            exit;
        }
        $data = [
            "home" => false,
            "baseMap" => false,
            "page" => "/page/user/user_daftar.php",
            "edit" => false
        ];
       
        $this->load->view('layout/wrapper',$data);
    }

    public function edit(){
        
        $data = [
            "home" => false,
            "baseMap" => false,
            "page" => "/page/user/update.php",
            "edit" => true,
            "pass" => $this->session->userdata("password"),
            "self" => true
        ];

        $option =" <option >-- Pilih Level --</option>
        <option value='0'>Pengguna</option>" ;
        if($this->session->userdata("user_level") > 0){
            $option .="<option value='1'>Admin</option>";
        }
        $data["elm"] = $option;
        
        $this->load->view('layout/wrapper',$data);
    }

    public function session_password(){
        $_SESSION['password'] =anti_injection($this->input->post('sp')); 
    }


}