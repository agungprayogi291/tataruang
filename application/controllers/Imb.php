<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imb extends CI_Controller {
    function __construct(){
        parent::__construct();
    }
    public function index(){
        $data =  [
            "home" => false,
            "baseMap" => true,
            "page" => "/page/m_imb.php"
        ];
        $this->load->view("layout/wrapper",$data);
    }

    public function statistic(){
        $data =  [
            "home" => false,
            "baseMap" => false,
            "page" => "/page/m_statistik.php"
        ];
        $this->load->view("layout/wrapper",$data);
    }
}