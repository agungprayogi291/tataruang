<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CariSurat extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('SuratPermohonan_m');
       
    }

    public function index(){
        $this->data['page'] = '/page/cariSurat/view.php';
        // $this->data['page'] = '/page/formSurat/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }


    public function get(){
        $notelp = anti_injection($this->input->post('phone'));
        $jenis = anti_injection($this->input->post('jenis'));
        $cekNomor =validation_phone($notelp);
        ///dd($cekNomor);
        $response=[];
        $html ="";
        if($cekNomor['verif']){
            $result = $this->SuratPermohonan_m->cariPermohonan($notelp,$jenis);
            $title ='';
            switch ($jenis) {
                case '1':
                    $title = 'IRK';
                    break;
                case '2':
                    $title = 'PKKPR';
                    break;
                case '3':
                    $title = 'Rekomtek';
                    break;
                default:
                    $title = '';
                    break;
            }
            if(count($result) > 0){
                $id_form;
                $html .="<div class='card mb-4'>";
                $html .="<div class='card-header'>Status Permohonan</div>";
                $html .="<div class='card-body'>";
                foreach($result as $row){
                    $status = '';
                   
                    switch ($row->status) {
                        case '2':
                            $status = 'selesai';
                            break;
                        case '1':
                            $status = 'proses';
                            break;
                        default:
                            $status = 'daftar';
                            break;
                    }
                    $html .="<ul>";
                    $html .= "<li >$row->tanggal ------> $status</li>";
                    $html .= "</ul>";
                    $id_form = $row->id_form;

                }
                $html .="</div>";
                $html .= "</div>";
                $html .= "<button class='btn btn-primary' onclick='preview(".$id_form.")' id='preview' data-jenis='$jenis'>preview</button>";
            }else{
                $html .="<h3 class='text-secondary'>Surat Permohonan $title Tidak Ditemukan</h3>";
            }
            $response =[
                "success" => true,
                "message" => $html
            ];
            
            //dd($response);
        }else{
            // $response = [
            //     "message" => $cekNomor['message'],
            //     "access" => false
            // ];
           // $response = $this->SuratPermohonan_m->cariPermohonan("irk",$notelp);
            //dd($response);
        }
        echo json_encode($response);
    }

    public function cetak()
    {
      $id_form = anti_injection($this->input->post('kode_form'));
      $jenis = anti_injection($this->input->post('jenis'));
  //    dd($jenis);

        switch ($jenis) {
            case '1':
                $this->irk($id_form);
                break;
            case '2':
                $this->pkkpr($id_form);
                break;
            case '3':
                $this->pbg($id_form);
                break;
            default:
                '';
            break;
        }
    }

    public function irk($id)
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');

        $pengajuan  = LoadDataTabel("irk","id=".$id);
        $data['cetak'] = $pengajuan[0];
        $filename              = 'Permohonan Informasi Rencana Kota';
        $author                = 'Form Surat';
        $header                = headerL(["app_title" => "
        Formulir<br> 
        PERMOHONAN	<br>
        INFORMASI RENCANA KOTA<br>
        ( IRK ) "]);
        $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
        $html                  = $this->load->view('page/formIRK/daftar/preview', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formIRK/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            // $mpdf->debug = true;
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->SetHTMLHeader($header);
            $mpdf->setFooter('{PAGENO}');

            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }
    }

    public function pbg($id)
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');
        $pengajuan  = LoadDataTabel("pbg","id=".$id);
       
        $data['cetak'] = $pengajuan[0];
        $filename              = 'Permohonan Informasi Rencana Kota';
        $author                = 'Form Surat';
        $header                = headerL(["app_title" => "
        Formulir<br>
        PERMOHONAN <br>
        PEMERIKSAAN PERSYARATAN TEKNIS<br> 
        PERSETUJUAN BANGUNAN GEDUNG (PBG) "]);
        $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
        $html                  = $this->load->view('page/formPBG/daftar/preview', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formIRK/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            // $mpdf->debug = true;
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->SetHTMLHeader($header);
            $mpdf->setFooter('{PAGENO}');

            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }
    }
    public function pkkpr($id)
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');

      
        $pengajuan  = $this->db->select("*")->from("pkkpr")->where("id",$id)->get()->row();
 
        $data['cetak'] = $pengajuan;
        $filename              = 'Permohonan Informasi Rencana Kota';
        $author                = 'Form Surat';
        $header                = headerL(["app_title" => "
        Formulir <br>
        PERMOHONAN PERSETUJUAN <br>	
        KESESUAIAN KEGIATAN PEMANFAATAN RUANG<br> 
        ( PKKPR ) "]);
        $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
        $html                  = $this->load->view('page/formPKKPR/daftar/preview', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formIRK/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            // $mpdf->debug = true;
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->SetHTMLHeader($header);
            $mpdf->setFooter('{PAGENO}');

            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }
    }

}


