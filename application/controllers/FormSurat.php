<?php
class FormSurat extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('SimpanFormSurat_m');
        $this->load->model('SuratPermohonan_m');
    }
    public function index()
    {
        $this->data['page'] = '/page/formSurat/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }

    public function simpan()
    {
        $valid = $this->form_validation;
        $valid->set_rules("namaPP1","nama pihak pertama 1","required" ,array("required" => '%s Harus DI isi'));
        $valid->set_rules("namaPK","nama pihak kedua","required",array("required" => '%s Harus Di isi'));
        $result = [];
        if($valid->run() == FALSE){
            $result = [
                'message' => validation_errors(),
                'success' => 0
            ];
        }else{
            if ($_POST) {
                $post = $this->input->post();

                $data = [
                    'namapp1' => anti_injection($post['namaPP1']),
                    'noktppp1' => anti_injection($post['noktpPP1']),
                    'pekerjaanpp1' => anti_injection($post['pekerjaanPP1']),
                    'notelppp1' => anti_injection($post['noTelpPP1']),
                    'jalanpp1' => anti_injection($post['jalanPP1']),
                    'rtpp1' => anti_injection($post['rtPP1']),
                    'rwpp1' => anti_injection($post['rwPP1']),
                    'kelurahanpp1' => anti_injection($post['kelurahanPP1']),
                    'kecamatanpp1' => anti_injection($post['kecamatanPP1']),
                    'kabupatenpp1' => anti_injection($post['kabupatenPP1']),
                    'namapp2' => anti_injection($post['namaPP2']),
                    'noktppp2' => anti_injection($post['noktpPP2']),
                    'pekerjaanpp2' => anti_injection($post['pekerjaanPP2']),
                    'notelppp2' => anti_injection($post['noTelpPP2']),
                    'jalanpp2' => anti_injection($post['jalanPP2']),
                    'rtpp2' => anti_injection($post['rtPP2']),
                    'rwpp2' => anti_injection($post['rwPP2']),
                    'kelurahanpp2' => anti_injection($post['kelurahanPP2']),
                    'kecamatanpp2' => anti_injection($post['kecamatanPP2']),
                    'kabupatenpp2' => anti_injection($post['kabupatenPP2']),
                    'namapk' => anti_injection($post['namaPK']),
                    'noktppk' => anti_injection($post['noktpPK']),
                    'pekerjaanpk' => anti_injection($post['pekerjaanPK']),
                    'notelppk' => anti_injection($post['noTelpPK']),
                    'jalanpk' => anti_injection($post['jalanPK']),
                    'rtpk' => anti_injection($post['rtPK']),
                    'rwpk' => anti_injection($post['rwPK']),
                    'kelurahanpk' => anti_injection($post['kelurahanPK']),
                    'kecamatanpk' => anti_injection($post['kecamatanPK']),
                    'kabupatenpk' => anti_injection($post['kabupatenPK']),
                    'jalanlahan' => anti_injection($post['jalanLahan']),
                    'rtlahan' => anti_injection($post['rtLahan']),
                    'rwlahan' => anti_injection($post['rwLahan']),
                    'kelurahanlahan' => anti_injection($post['kelurahanLahan']),
                    'kecamatanlahan' => anti_injection($post['kecamatanLahan']),
                    'kabupatenlahan' => anti_injection($post['kabupatenLahan']),
                    'haklahan' => anti_injection($post['hakLahan']),
                    'nolahan' => anti_injection($post['noLahan']),
                    'luaslahan' => anti_injection($post['luasLahan']),
                ];

                if ($this->SimpanFormSurat_m->simpan('izinpenggunaantanah', $data) == true) {
                    // $id = LoadNamaItemTabel('izinpenggunaantanah', 'id',);
                    $result = [
                        'success' => 1, 'message' => "Form berhasil disimpan",
                        'data' => $data,
                        'message' => 'Form berhasil disimpan'
                    ];
                    
                }
            }
            
        }
        echo json_encode($result);
    }

    public function cetak()
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');

        // echo '<pre>';
        // print_r($_POST);
        // die();
        $data['cetak'] = $this->input->post();

        $html = '';

        $data['html'] = $html;
        $filename              = 'Surat Izin Penggunaan Tanah';
        // $author                = 'Form Surat';
        // $header                = headerL([]);
        // $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
        $html                  = $this->load->view('page/formSurat/preview', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formSurat/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            // $mpdf->debug = true;
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            // $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
            // $mpdf->SetHTMLHeader($header);
            // $mpdf->setFooter('{PAGENO}');

            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }
    }

    public function simpan_v1()
    {
        if ($_POST) {
            $post = $this->input->post();

            $data = [
                'namapp1' => anti_injection($post['namaPP1']),
                'noktppp1' => anti_injection($post['noktpPP1']),
                'pekerjaanpp1' => anti_injection($post['pekerjaanPP1']),
                'notelppp1' => anti_injection($post['noTelpPP1']),
                'jalanpp1' => anti_injection($post['jalanPP1']),
                'rtpp1' => anti_injection($post['rtPP1']),
                'rwpp1' => anti_injection($post['rwPP1']),
                'kelurahanpp1' => anti_injection($post['kelurahanPP1']),
                'kecamatanpp1' => anti_injection($post['kecamatanPP1']),
                'kabupatenpp1' => anti_injection($post['kabupatenPP1']),
                'namapp2' => anti_injection($post['namaPP2']),
                'noktppp2' => anti_injection($post['noktpPP2']),
                'pekerjaanpp2' => anti_injection($post['pekerjaanPP2']),
                'notelppp2' => anti_injection($post['noTelpPP2']),
                'jalanpp2' => anti_injection($post['jalanPP2']),
                'rtpp2' => anti_injection($post['rtPP2']),
                'rwpp2' => anti_injection($post['rwPP2']),
                'kelurahanpp2' => anti_injection($post['kelurahanPP2']),
                'kecamatanpp2' => anti_injection($post['kecamatanPP2']),
                'kabupatenpp2' => anti_injection($post['kabupatenPP2']),
                'namapk' => anti_injection($post['namaPK']),
                'noktppk' => anti_injection($post['noktpPK']),
                'pekerjaanpk' => anti_injection($post['pekerjaanPK']),
                'notelppk' => anti_injection($post['noTelpPK']),
                'jalanpk' => anti_injection($post['jalanPK']),
                'rtpk' => anti_injection($post['rtPK']),
                'rwpk' => anti_injection($post['rwPK']),
                'kelurahanpk' => anti_injection($post['kelurahanPK']),
                'kecamatanpk' => anti_injection($post['kecamatanPK']),
                'kabupatenpk' => anti_injection($post['kabupatenPK']),
                'jalanlahan' => anti_injection($post['jalanLahan']),
                'rtlahan' => anti_injection($post['rtLahan']),
                'rwlahan' => anti_injection($post['rwLahan']),
                'kelurahanlahan' => anti_injection($post['kelurahanLahan']),
                'kecamatanlahan' => anti_injection($post['kecamatanLahan']),
                'kabupatenlahan' => anti_injection($post['kabupatenLahan']),
                'haklahan' => anti_injection($post['hakLahan']),
                'nolahan' => anti_injection($post['noLahan']),
                'luaslahan' => anti_injection($post['luasLahan']),
            ];

            if ($this->SimpanFormSurat_m->simpan('izinpenggunaantanah', $data) == true) {
                $result = ['success' => 1, 'message' => "Form berhasil disimpan" ];
                // redirect(base_url()."FormSurat/cetak");
                echo json_encode($result);
            }
        }
    }

    public function cetak_v1()
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');
        // dd($this->input->post('wadah'));
        // die;
        $data = $this->SimpanFormSurat_m->getDataById('izinpenggunaantanah', 1);
        // echo '<pre>';
        // print_r($data);
        // die();

        $html = '';

        $data['html'] = $html;
        $filename              = 'Surat Izin Penggunaan Tanah';
        // $author                = 'Form Surat';
        // $header                = headerL([]);
        // $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
        $html                  = $this->load->view('page/formSurat/preview', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formSurat/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            // $mpdf->debug = true;
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            // $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
            // $mpdf->SetHTMLHeader($header);
            // $mpdf->setFooter('{PAGENO}');

            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }

        // redirect(base_url(),"FormSurat/cetak");
    }



    public function viewListSuratTanah(){
        $this->data['page'] = '/page/formSurat/daftar/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }

    public function listSuratTanah(){
        $data = $this->SuratPermohonan_m->get("izinpenggunaantanah");
        $i = 1;
        $html = "";
        foreach($data as $row){
            $html .= "<tr>";
            $html .= "<td>$i</td>";
            $html .= "<td>$row->namapp1</td>";
            $html .= "<td>$row->noktppp1</td>";
            $html .= "<td>$row->notelppp1</td>";
            $html .= "
            <td>
                <button class='btn btn-primary my-2' onClick='cetakUlang($row->id)'> Cetak Ulang</button>
                <button class='btn btn-danger' onClick='cetakUlang($row->id)'> Cek Status</button>
            </td>";
            $html .="</tr>";
            $i++;
        }
        $response  = array(
            "tabel" => $html,
            "row" => $i
        );
        echo json_encode($response);
    }
}
