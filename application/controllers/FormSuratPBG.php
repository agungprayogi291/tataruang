<?php
class FormSuratPBG extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('SimpanFormSurat_m');
        $this->load->model('SuratPermohonan_m');
        $this->load->model("Verification_m");
    }

    public function index()
    {
        $this->data['page'] = '/page/formPBG/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->data['lang'] = null;
        $this->data['lat'] = null;
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }
    public function upload($file)
    {
        $config['upload_path'] = "./assets/files";
        $config['allowed_types'] = 'pdf|docx|xlsx|jpeg|JPEG|jpg|jpg|png|PNG|doc|docx';
        $config['encrypt_name'] = TRUE;
        //$config['file_name'] = time() . $_FILES[$file]['name'];

        $this->load->library('upload', $config);

        $hasil = [];

        if ($this->upload->do_upload($file)) {
            $data = ['upload_data' => $this->upload->data()];
            $hasil = $data['upload_data']['file_name'];
        } else {
            $error = array('error' => $this->upload->display_errors());
            $hasil = $error;
        }

        return $hasil;
    }

    public function simpan_v2()
    {
        $valid = $this->form_validation;
        $valid->set_rules("namaPemohon", "Nama Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $valid->set_rules("noTelpPemohon", "No Telepon Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $valid->set_rules("noktpPemohon", "No Ktp Pemohon", "required", array("required" => "%s Harus Di Isi"));

        $result = [];
        if ($valid->run() == FALSE) {
            $result = [
                "message" => validation_errors(),
                "success" => 0
            ];
        } else {
            $post = $this->input->post();

            $data = [
                'namapemohon' => anti_injection($post['namaPemohon']),
                'jalanpemohon' => anti_injection($post['jalanPemohon']),
                'rtpemohon' => anti_injection($post['rtPemohon']),
                'rwpemohon' => anti_injection($post['rwPemohon']),
                'kelurahanpemohon' => anti_injection($post['kelurahanPemohon']),
                'kecamatanpemohon' => anti_injection($post['kecamatanPemohon']),
                'kabupatenpemohon' => anti_injection($post['kabupatenPemohon']),
                'notelppemohon' => anti_injection($post['noTelpPemohon']),
                'noktppemohon' => anti_injection($post['noktpPemohon']),
                'pekerjaanpemohon' => anti_injection($post['pekerjaanPemohon']),
                'fungsigedung' => anti_injection($post['fungsiGedung']),
                'pondasigedung' => anti_injection($post['pondasiGedung']),
                'dindinggedung' => anti_injection($post['dindingGedung']),
                'lantaigedung' => anti_injection($post['lantaiGedung']),
                'kusengedung' => anti_injection($post['kusenGedung']),
                'rangkaatapgedung' => anti_injection($post['rangkaAtapGedung']),
                'atapgedung' => anti_injection($post['atapGedung']),
                'namalokasi' => anti_injection($post['namaLokasi']),
                'rtlokasi' => anti_injection($post['rtLokasi']),
                'rwlokasi' => anti_injection($post['rwLokasi']),
                'kelurahanlokasi' => anti_injection($post['kelurahanLokasi']),
                'kecamatanlokasi' => anti_injection($post['kecamatanLokasi']),
                'luastanah' => anti_injection($post['luasTanah']),
                'haktanah' => ($post['statusTanah'] == 'lainnya' ? anti_injection($post['statusLain']) : anti_injection($post['statusTanah'])),
                'notanah' => anti_injection($post['noTanah']),
                'pemiliktanah' => anti_injection($post['pemilikTanah']),
                'batastanahutara' => anti_injection($post['batasTanahUtara']),
                'batastanahselatan' => anti_injection($post['batasTanahSelatan']),
                'batastanahtimur' => anti_injection($post['batasTanahTimur']),
                'batastanahbarat' => anti_injection($post['batasTanahBarat']),
                'statustanah' => ($post['statusTanah'] == null || $post['statusTanah'] == "Lainnya" ? anti_injection($post['statusLain']) : anti_injection($post['statusTanah'])),
                'lang' => anti_injection($post['lang']),
                'lat' => anti_injection($post['lat'])
            ];

            // echo '<pre>';
            // print_r($data);
            // die();

            if (isset($_POST['fcSertifikatTanah'])) {
                $filename = $this->upload('filefcSertifikatTanah');
                $data['fcsertifikattanah'] = anti_injection($post['fcSertifikatTanah']);
                $data['file_fcsertifikattanah'] = $filename;
            }

            if (isset($_POST['fcKTP'])) {
                $filename = $this->upload('filefcKTP');
                $data['fcktp'] = anti_injection($post['fcKTP']);
                $data['file_fcktp'] = $filename;
            }

            if (isset($_POST['teknisBangunan'])) {
                $filename = $this->upload('fileteknisBangunan');
                $data['teknisbangunan'] = anti_injection($post['teknisBangunan']);
                $data['file_teknisbangunan'] = $filename;
            }

            if (isset($_POST['detailStruktur'])) {
                $filename = $this->upload('filedetailStruktur');
                $data['detailstruktur'] = anti_injection($post['detailStruktur']);
                $data['file_detailstruktur'] = $filename;
            }

            if (isset($_POST['petaLokasi'])) {
                $filename = $this->upload('filepetaLokasi');
                $data['petalokasi'] = anti_injection($post['petaLokasi']);
                $data['file_petalokasi'] = $filename;
            }

            if (isset($_POST['sistemUtilitas'])) {
                $filename = $this->upload('filesistemUtilitas');
                $data['sistemutilitas'] = anti_injection($post['sistemUtilitas']);
                $data['file_sistemutilitas'] = $filename;
            }

            if (isset($_POST['pbgLama'])) {
                $filename = $this->upload('filepbgLama');
                $data['pbglama'] = anti_injection($post['pbgLama']);
                $data['file_pbglama'] = $filename;
            }

            if (isset($_POST['pkkpr'])) {
                $filename = $this->upload('filepkkpr');
                $data['pkkpr'] = anti_injection($post['pkkpr']);
                $data['file_pkkpr'] = $filename;
            }

            if (isset($_POST['dokLinkungan'])) {
                $filename = $this->upload('filedokLinkungan');
                $data['doklinkungan'] = anti_injection($post['dokLinkungan']);
                $data['file_doklinkungan'] = $filename;
            }

            if (isset($_POST['rekomendasiLaluLintas'])) {
                $filename = $this->upload('filerekomendasiLaluLintas');
                $data['rekomendasilalulintas'] = anti_injection($post['rekomendasiLaluLintas']);
                $data['file_rekomendasilalulintas'] = $filename;
            }

            if (isset($_POST['konstruksiBetonTulang'])) {
                $filename = $this->upload('filekonstruksiBetonTulang');
                $data['konstruksibetontulang'] = anti_injection($post['konstruksiBetonTulang']);
                $data['file_konstruksibetontulang'] = $filename;
            }

            if (isset($_POST['konstruksiBajaRingan'])) {
                $filename = $this->upload('filekonstruksiBajaRingan');
                $data['konstruksibajaringan'] = anti_injection($post['konstruksiBajaRingan']);
                $data['file_konstruksibajaringan'] = $filename;
            }

            if (isset($_POST['suratPenyelidikanTanah'])) {
                $filename = $this->upload('filesuratPenyelidikanTanah');
                $data['suratpenyelidikantanah'] = anti_injection($post['suratPenyelidikanTanah']);
                $data['file_suratpenyelidikantanah'] = $filename;
            }

            // echo '<pre>';
            // print_r($data);
            // die();

            if ($this->SimpanFormSurat_m->simpan('pbg', $data) == true) {
                $result = [
                    'success' => 1, 'message' => "Form berhasil disimpan",
                    'data' => $data
                ];
            }
        }
        echo json_encode($result);
    }

    public function simpan()
    {
        $valid = $this->form_validation;
        $this->validation_pbg($valid);

        $result = [];
        if ($valid->run() == FALSE) {
            $result = [
                "message" => validation_errors(),
                "success" => 0
            ];
        } else {
            $post = $this->input->post();

            $data = [
                'namapemohon' => anti_injection($post['namaPemohon']),
                'jalanpemohon' => anti_injection($post['jalanPemohon']),
                'rtpemohon' => anti_injection($post['rtPemohon']),
                'rwpemohon' => anti_injection($post['rwPemohon']),
                'kelurahanpemohon' => anti_injection($post['kelurahanPemohon']),
                'kecamatanpemohon' => anti_injection($post['kecamatanPemohon']),
                'kabupatenpemohon' => anti_injection($post['kabupatenPemohon']),
                'notelppemohon' => anti_injection($post['noTelpPemohon']),
                'noktppemohon' => anti_injection($post['noktpPemohon']),
                'pekerjaanpemohon' => anti_injection($post['pekerjaanPemohon']),
                'fungsigedung' => anti_injection($post['fungsiGedung']),
                'pondasigedung' => anti_injection($post['pondasiGedung']),
                'dindinggedung' => anti_injection($post['dindingGedung']),
                'lantaigedung' => anti_injection($post['lantaiGedung']),
                'kusengedung' => anti_injection($post['kusenGedung']),
                'rangkaatapgedung' => anti_injection($post['rangkaAtapGedung']),
                'atapgedung' => anti_injection($post['atapGedung']),
                'namalokasi' => anti_injection($post['namaLokasi']),
                'rtlokasi' => anti_injection($post['rtLokasi']),
                'rwlokasi' => anti_injection($post['rwLokasi']),
                'kelurahanlokasi' => anti_injection($post['kelurahanLokasi']),
                'kecamatanlokasi' => anti_injection($post['kecamatanLokasi']),
                'luastanah' => anti_injection($post['luasTanah']),
                'haktanah' => ($post['statusTanah'] == 'lainnya' ? anti_injection($post['statusLain']) : anti_injection($post['statusTanah'])),
                'notanah' => anti_injection($post['noTanah']),
                'pemiliktanah' => anti_injection($post['pemilikTanah']),
                'batastanahutara' => anti_injection($post['batasTanahUtara']),
                'batastanahselatan' => anti_injection($post['batasTanahSelatan']),
                'batastanahtimur' => anti_injection($post['batasTanahTimur']),
                'batastanahbarat' => anti_injection($post['batasTanahBarat']),
                'statustanah' => ($post['statusTanah'] == null || $post['statusTanah'] == "Lainnya" ? anti_injection($post['statusLain']) : anti_injection($post['statusTanah'])),
                'verifikasi_telephone' => anti_injection($post['verifikasi_telp']),
                'lang' => anti_injection($post['lang']),
                'lat' => anti_injection($post['lat'])
            ];

            if (isset($_POST['fcSertifikatTanah'])) {
                $filename = $this->upload('filefcSertifikatTanah');
                $data['fcsertifikattanah'] = anti_injection($post['fcSertifikatTanah']);
                $data['file_fcsertifikattanah'] = $filename;
            }

            if (isset($_POST['fcKTP'])) {
                $filename = $this->upload('filefcKTP');
                $data['fcktp'] = anti_injection($post['fcKTP']);
                $data['file_fcktp'] = $filename;
            }

            if (isset($_POST['teknisBangunan'])) {
                $filename = $this->upload('fileteknisBangunan');
                $data['teknisbangunan'] = anti_injection($post['teknisBangunan']);
                $data['file_teknisbangunan'] = $filename;
            }

            if (isset($_POST['detailStruktur'])) {
                $filename = $this->upload('filedetailStruktur');
                $data['detailstruktur'] = anti_injection($post['detailStruktur']);
                $data['file_detailstruktur'] = $filename;
            }

            if (isset($_POST['petaLokasi'])) {
                $filename = $this->upload('filepetaLokasi');
                $data['petalokasi'] = anti_injection($post['petaLokasi']);
                $data['file_petalokasi'] = $filename;
            }

            if (isset($_POST['sistemUtilitas'])) {
                $filename = $this->upload('filesistemUtilitas');
                $data['sistemutilitas'] = anti_injection($post['sistemUtilitas']);
                $data['file_sistemutilitas'] = $filename;
            }

            if (isset($_POST['pbgLama'])) {
                $filename = $this->upload('filepbgLama');
                $data['pbglama'] = anti_injection($post['pbgLama']);
                $data['file_pbglama'] = $filename;
            }

            if (isset($_POST['pkkpr'])) {
                $filename = $this->upload('filepkkpr');
                $data['pkkpr'] = anti_injection($post['pkkpr']);
                $data['file_pkkpr'] = $filename;
            }

            if (isset($_POST['dokLinkungan'])) {
                $filename = $this->upload('filedokLinkungan');
                $data['doklinkungan'] = anti_injection($post['dokLinkungan']);
                $data['file_doklinkungan'] = $filename;
            }

            if (isset($_POST['rekomendasiLaluLintas'])) {
                $filename = $this->upload('filerekomendasiLaluLintas');
                $data['rekomendasilalulintas'] = anti_injection($post['rekomendasiLaluLintas']);
                $data['file_rekomendasilalulintas'] = $filename;
            }

            if (isset($_POST['konstruksiBetonTulang'])) {
                $filename = $this->upload('filekonstruksiBetonTulang');
                $data['konstruksibetontulang'] = anti_injection($post['konstruksiBetonTulang']);
                $data['file_konstruksibetontulang'] = $filename;
            }

            if (isset($_POST['konstruksiBajaRingan'])) {
                $filename = $this->upload('filekonstruksiBajaRingan');
                $data['konstruksibajaringan'] = anti_injection($post['konstruksiBajaRingan']);
                $data['file_konstruksibajaringan'] = $filename;
            }

            if (isset($_POST['suratPenyelidikanTanah'])) {
                $filename = $this->upload('filesuratPenyelidikanTanah');
                $data['suratpenyelidikantanah'] = anti_injection($post['suratPenyelidikanTanah']);
                $data['file_suratpenyelidikantanah'] = $filename;
            }

            if ($this->SimpanFormSurat_m->simpan('pbg', $data) == true) {
                // Simpan id form ke status permohonan
                if ($this->simpanStatus('pbg', 3) == true) {
                    $result = [
                        'success' => 1, 'message' => "Form berhasil disimpan",
                        'data' => $data
                    ];
                }
            }
        }
        echo json_encode($result);
    }

    public function simpanStatus($table, $jenis)
    {
        $result = false;

        // Ambil id terakhir
        $idform = LoadNamaItemTabel($table, 'id', "id=(SELECT max(id) FROM $table)");
        $data_status = [
            'jenis' => $jenis,
            'tanggal' => Date('Y-m-d'),
            'keterangan' => '',
            'id_form' => $idform,
            'status' => 0
        ];

        // Simpan id form ke status permohonan
        if ($this->SimpanFormSurat_m->simpan('status_permohonan', $data_status) == true) {
            $result = true;
        }
        $this->SimpanFormSurat_m->simpan('status_log', $data_status);
        return $result;
    }

    public function cetak()
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');

        // echo '<pre>';
        // print_r($_POST);
        // die();

        $data['cetak'] = $this->input->post();

        $html = '';

        $data['html'] = $html;
        $filename              = 'Surat Persetujuan Bangunan Gedung';
        $author                = 'Form Surat';
        $header                = headerL(["app_title" =>
        "Formulir<br>
        PERMOHONAN <br>
        PEMERIKSAAN PERSYARATAN TEKNIS<br> 
        PERSETUJUAN BANGUNAN GEDUNG (PBG)"]);
        $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
        $html                  = $this->load->view('page/formPBG/preview', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formPBG/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            // $mpdf->debug = true;
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->SetHTMLHeader($header);
            $mpdf->setFooter('{PAGENO}');

            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }
    }










    public function viewListSuratPBG()
    {
        $this->data['page'] = '/page/formPBG/daftar/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }

    public function listSuratPBG()
    {
        $status = anti_injection($this->input->post('status'));
        $data = $this->SuratPermohonan_m->get("pbg", "", $status);
        $i = 1;
        $html = "";
        foreach ($data as $row) {
            $html  .= "<tr>";
            $html .= "<td style='vertical-align:top;'>$i</td>";
            $html .= "<td style='vertical-align:top;'>$row->namapemohon</td>";
            $html .= "<td style='vertical-align:top;'>$row->notelppemohon</td>";
            $html .= "<td style='vertical-align:top;'>Rt.$row->rtpemohon</td>";
            $html .=  "<td style='vertical-align:top;'>$row->verifikasi_telephone</td>";
            $html .= "<td style='vertical-align:top;'>"
                .
                ($row->fcktp > 0 ?  "√ fotocopy Ktp Pemohon <a href='" . base_url() . "Download/download/" . $row->file_fcktp . "' target='_blank'>download</a><br>" :  "<span class='text-danger'>X fotocopy Ktp Pemohon</span><br>") .
                ($row->fcsertifikattanah > 0 ?  "√ Fotokopi Sertifikat Tanah <a href='" . base_url() . "Download/download/" . $row->file_fcsertifikattanah . "' target='_blank'>download</a><br>" :  "<span class='text-danger' >X Fotokopi Sertifikat Tanah</span><br>") .
                // ($row->teknisbagunan > 0 ?  "√ teknis bagunan<br>" :  "").
                ($row->petalokasi > 0 ?  "√ Peta lokasi, SITEPLAN, denah bangunan, denah sanitasi-drainase, Tampak 4 sisi <a href='" . base_url() . "Download/download/" . $row->file_petalokasi . "' target='_blank'>download</a><br>" :  "<span class='text-danger'>X Peta lokasi, SITEPLAN, denah bangunan, denah sanitasi-drainase, Tampak 4 sisi</span><br>") .
                ($row->sistemutilitas > 0 ?  "√ Sistem utilitas (ME), sistem penanggulangan kebakaran dan gambar kerja untuk baangunan kepentingan umum <a href='" . base_url() . "Download/download/" . $row->file_sistemutilitas . "' target='_blank'>download</a><br>" :  "<span class='text-danger'>X Sistem utilitas (ME), sistem penanggulangan kebakaran dan gambar kerja untuk baangunan kepentingan umum</span><br>")
                . ($row->teknisbangunan > 0 ? "√  Gambar rencana teknik bangunan gedung dan / atau prasarana bangunan gedung <a href='" . base_url() . "Download/download/" . $row->file_teknisbangunan . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X Gambar rencana teknik bangunan gedung dan / atau prasarana bangunan gedung</span><br>")
                . ($row->pbglama > 0 ? "√ PBG lama untuk Bangunan Renovasi / Pemecahan PBG  <a href='" . base_url() . "Download/download/" . $row->file_pbglama . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X PBG lama untuk Bangunan Renovasi / Pemecahan PBG</span><br>")
                . ($row->pkkpr > 0 ? "√ PKKPR (Persetujuan Kesesuaian Kegiatan Pemanfaatan Ruang) <a href='" . base_url() . "Download/download/" . $row->file_pkkpr . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X PKKPR (Persetujuan Kesesuaian Kegiatan Pemanfaatan Ruang)</span><br>")
                . ($row->doklinkungan > 0 ? "√ Dokumen Lingkungan dari Dinas Lingkungan Hidup Kota Magelang untuk bangunan berdampak pada lingkungan <a href='" . base_url() . "Download/download/" . $row->file_doklinkungan . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X Dokumen Lingkungan dari Dinas Lingkungan Hidup Kota Magelang untuk bangunan berdampak pada lingkungan</span><br>")
                . ($row->rekomendasilalulintas > 0 ? "√ Rekomendasi Lalu Lintas dari Dinas Perhubungan Kota Magelang untuk bangunan berdampak lalu-lintas <a href='" . base_url() . "Download/download/" . $row->file_rekomendasilalulintas . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X Rekomendasi Lalu Lintas dari Dinas Perhubungan Kota Magelang untuk bangunan berdampak lalu-lintas</span><br>")
                . ($row->konstruksibetontulang > 0 ? "√ Perhitungan Konstruksi Beton Bertulang ditandatangani oleh penangguna jawab konstruksi untuk bangunan berting <a href='" . base_url() . "Download/download/" . $row->file_konstruksibetontulang . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X Perhitungan Konstruksi Beton Bertulang ditandatangani oleh penangguna jawab konstruksi untuk bangunan berting</span><br>")
                . ($row->konstruksibajaringan > 0  ? "√ Perhitungan Konstruksi baja/Baja Ringan ditandatangani oleh penanggung jawab konstruksi  <a href='" . base_url() . "Download/download/" . $row->file_konstruksibajaringan . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X Perhitungan Konstruksi baja/Baja Ringan ditandatangani oleh penanggung jawab konstruksi</span><br>")
                . ($row->suratpenyelidikantanah > 0 ? "√ Surat hasil Penyelidikan Tanah/sondir ditandatangani oleh penanggung jawab untuk bangunan 4 lantai atau <a href='" . base_url() . "Download/download/" . $row->file_suratpenyelidikantanah . "' target='_blank'>download</a><br>" : "<span class='text-danger'>X Surat hasil Penyelidikan Tanah/sondir ditandatangani oleh penanggung jawab untuk bangunan 4 lantai atau </span><br>")
                . "</td>";
            if ($row->status == 0) {
                $html .= "<td style='vertical-align:top;'>daftar</td>";
            } else if ($row->status == 1) {
                $html .= "<td style='vertical-align:top;'>proses</td>";
            } else if ($row->status == 2) {
                $html .= "<td style='vertical-align:top;'>selesai</td>";
            } else {
                $html .= "<td></td>";
            }
            $html .= "<td style='vertical-align:top;'>";
            $html .= " 
                <button class='btn btn-info my-2'style='padding-right:50px;' onClick='openDetail($row->id)'><i class='bi bi-card-heading'></i> Detail</button>
                <button class='btn btn-primary my-2' onClick='cetakUlang($row->id)'><i class='bi bi-file-earmark-pdf'></i> Cetak Ulang</button>
                <button class='btn btn-danger'style='padding-right:15px;' onClick='openStatus($row->id)'><i class='bi bi-calendar4-event mx-1'></i> Cek Status</button>
                ";
            // if($row->id_kuasa != null){
            //     $html .= "<buttn class='btn btn-secondary my-2' onclick='cetakUlangKuasa($row->id_kuasa)'><i class='bi bi-file-earmark-pdf'></i>Cetak Kuasa</button>";
            // }
            $html .= "</td>";
            $html .= "</tr>";
            $i++;
        }
        $response  = array(
            "tabel" => $html,
            "row" => $i
        );
        echo json_encode($response);
    }
    public function cetakUlang()
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');

        $id = anti_injection($this->input->post('kode_form'));
        $pengajuan  = LoadDataTabel("pbg", "id=" . $id);
        $data['cetak'] = $pengajuan[0];
        $filename              = 'Permohonan Informasi Rencana Kota';
        $author                = 'Form Surat';
        $header                = headerL(["app_title" => "
        Formulir<br>
        PERMOHONAN <br>
        PEMERIKSAAN PERSYARATAN TEKNIS<br> 
        PERSETUJUAN BANGUNAN GEDUNG (PBG) "]);
        $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
        $html                  = $this->load->view('page/formPBG/daftar/preview', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formIRK/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            // $mpdf->debug = true;
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->SetHTMLHeader($header);
            $mpdf->setFooter('{PAGENO}');

            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }
    }

    public function updateStatus()
    {
        $id_form = anti_injection($this->input->post("id_permohonan"));
        $tanggal = anti_injection($this->input->post("tanggal_status"));
        $status = anti_injection($this->input->post("level_status"));
        $keterangan = anti_injection($this->input->post('keterangan_status'));
        $jenis = 3;

        $data = [
            "jenis" => $jenis,
            "tanggal"  => $tanggal,
            "keterangan" => $keterangan,
            "id_form" => $id_form,
            "status" => $status
        ];

        $response = [
            "message" => "gagal",
            "success" => false
        ];
        //dd($this->SuratPermohonan_m->status($id_form,$data));
        if ($this->SuratPermohonan_m->status($id_form, $data)) {

            $response = [
                "message" => "berhasil",
                "success" => true
            ];
        }

        echo json_encode($response);
    }

    public function get()
    {
        $id_form = anti_injection($this->input->post('kode'));
        //$result = LoadDataTabel("status_permohonan","id_form =". $id_form);
        $result = $this->SuratPermohonan_m->getStatus("status_permohonan", $id_form, 3);
        if ($result == null) {
            $response = [
                "kode" => $id_form,
                "data" => null
            ];
        } else {
            $response = [
                "data" => $result
            ];
        }
        echo json_encode($response);
    }

    public function getPermohonan()
    {
        $id = anti_injection($this->input->post("kode"));
        $row = $this->db->select("*")->from("pbg")->where("id", $id)->get()->row();
        $html = "";
        $htmlImg = "";
        $indicator = "";
        $lampiran = "";
        $ullist = "";
        // foreach($result as $row){
        $html .= "<tr><td>Nama Pemohon</td><td>$row->namapemohon</td></tr>";
        $html .= "<tr><td>Lokasi Pemohon</td><td>" . $row->jalanpemohon . " Rt/Rw " . $row->rtpemohon . "/" . $row->rwpemohon . " kel. " . $row->kelurahanpemohon . " kec." . $row->kecamatanpemohon . " kab." . $row->kabupatenpemohon . "</td></tr>";
        $html .= "<tr><td>No telp</td><td>$row->notelppemohon</td></tr>";
        //$html .= "<tr><td>Email Pemohon</td><td>$row->emailpemohon</td></tr>";
        //$html .= "<tr><td>Nik Pemohon</td><td>$row->nikpemohon</td></tr>";
        //$html .= "<tr><td>Status Tanah</td><td>$row->statustanah</td></tr>";
        $html .= "<tr><td>lokasi lahan</td><td> Rt/Rw" . $row->rtlokasi . "/" . $row->rwlokasi . " kel." . $row->kelurahanlokasi . "</td></tr>";
        $html .= "<tr><td>Batas Tanah utara </td><td> " . $row->batastanahutara . "</td></tr>";
        $html .= "<tr><td>Batas Tanah selatan </td><td> " . $row->batastanahselatan . "</td></tr>";
        $html .= "<tr><td>Batas Tanah timur </td><td> " . $row->batastanahtimur . "</td></tr>";
        $html .= "<tr><td>Batas Tanah barat </td><td> " . $row->batastanahbarat . "</td></tr>";


        if ($row->fcktp > 0) {
            $htmlImg .= "<div class='carousel-item active'>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" . base_url() . "assets/files/" . $row->file_fcktp . "' alt='First slide' />";
            $htmlImg .= " <figcaption>Fc ktp</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= "<li
                data-target='#carouselExampleIndicators'
                data-slide-to='0'
                class='active'
                ></li>";
            $lampiran .= "<li>Fc KTP √</li>";
        }

        // if($row->fcnpwp > 0){
        //     $htmlImg .= "<div class='carousel-item '>";
        //     $htmlImg .= "<figure>";
        //     $htmlImg .= "<img class='img-size' src='". base_url() ."assets/files/".$row->file_fcnpwp."' alt='First slide' />";
        //     $htmlImg .= " <figcaption>fc npwp</figcaption>";
        //     $htmlImg .= "</figure>";
        //     $htmlImg .= " </div>";

        //     $indicator .= "<li
        //     data-target='#carouselExampleIndicators'
        //     data-slide-to='1'
        //     ></li>";

        //     $lampiran .="<li>Fc NPWP √</li>";
        // }

        if ($row->fcsertifikattanah > 0) {
            $htmlImg .= "<div class='carousel-item '>";
            $htmlImg .= "<figure>";
            $htmlImg .= "<img class='img-size' src='" .   base_url() . "assets/files/" . $row->file_fcsertifikattanah . "' alt='First slide' />";
            $htmlImg .= " <figcaption>fc sertifikat tanah</figcaption>";
            $htmlImg .= "</figure>";
            $htmlImg .= " </div>";

            $indicator .= " <li
                data-target='#carouselExampleIndicators'
                data-slide-to='2'
                ></li>";

            $lampiran .= "<li>Fc Sertifikat tanah√</li>";
        }

        // if($row->suratizinpenggunaantanah > 0){
        //     $htmlImg .= "<div class='carousel-item active'>";
        //     $htmlImg .= "<figure>";
        //     $htmlImg .= "<img class='img-size' src='".base_url() ."assets/files/".$row->file_suratizinpenggunaantanah."' alt='First slide' />";
        //     $htmlImg .= " <figcaption>Surat Izin Penggunaan Tanah</figcaption>";
        //     $htmlImg .= "</figure>";
        //     $htmlImg .= " </div>";

        //     $indicator .= "<li
        //     data-target='#carouselExampleIndicators'
        //     data-slide-to='3'
        //     class='active'
        //     ></li>";

        //     $lampiran .="<li>Surat Izin Penggunaan Tanah √</li>";
        // }

        // if($row->suratkuasa > 0){
        //     $htmlImg .= "<div class='carousel-item '>";
        //     $htmlImg .= "<figure>";
        //     $htmlImg .= "<img class='img-size' src='". base_url() ."assets/files/".$row->file_suratkuasa."' alt='First slide' />";
        //     $htmlImg .= " <figcaption>surat kuasa</figcaption>";
        //     $htmlImg .= "</figure>";
        //     $htmlImg .= " </div>";

        //     $indicator .= "<li
        //     data-target='#carouselExampleIndicators'
        //     data-slide-to='4'
        //     ></li>";

        //     $lampiran .="<li>Surat Kuasa √</li>";
        // }

        // if($row->petagoogle > 0){
        //     $htmlImg .= "<div class='carousel-item '>";
        //     $htmlImg .= "<figure>";
        //     $htmlImg .= "<img class='img-size' src='".   base_url() ."assets/files/".$row->file_petagoogle."' alt='First slide' />";
        //     $htmlImg .= " <figcaption>Peta google</figcaption>";
        //     $htmlImg .= "</figure>";
        //     $htmlImg .= " </div>";

        //     $indicator .= " <li
        //     data-target='#carouselExampleIndicators'
        //     data-slide-to='5'
        //     ></li>";

        //     $lampiran .="<li>Peta Google √</li>";
        // }


        // if($row->fcnib > 0){
        //     $htmlImg .= "<div class='carousel-item '>";
        //     $htmlImg .= "<figure>";
        //     $htmlImg .= "<img class='img-size' src='". base_url() ."assets/files/".$row->file_fcnib."' alt='First slide' />";
        //     $htmlImg .= " <figcaption>fcnib</figcaption>";
        //     $htmlImg .= "</figure>";
        //     $htmlImg .= " </div>";

        //     $indicator .= "<li
        //     data-target='#carouselExampleIndicators'
        //     data-slide-to='6'
        //     ></li>";

        //     $lampiran .="<li>FcNIB √</li>";
        // }

        // if($row->pernyataanmandiri > 0){
        //     $htmlImg .= "<div class='carousel-item '>";
        //     $htmlImg .= "<figure>";
        //     $htmlImg .= "<img class='img-size' src='".   base_url() ."assets/files/".$row->peryataanmandiri."' alt='First slide' />";
        //     $htmlImg .= " <figcaption>peryataan mandiri</figcaption>";
        //     $htmlImg .= "</figure>";
        //     $htmlImg .= " </div>";

        //     $indicator .= " <li
        //     data-target='#carouselExampleIndicators'
        //     data-slide-to='7'
        //     ></li>";

        //     $lampiran .="<li>Peryatan Mandiri √</li>";
        // }


        // if($row->fcaktependirian > 0){
        //     $htmlImg .= "<div class='carousel-item '>";
        //     $htmlImg .= "<figure>";
        //     $htmlImg .= "<img class='img-size' src='".   base_url() ."assets/files/".$row->file_fcaktependirian."' alt='First slide' />";
        //     $htmlImg .= " <figcaption>fc akte pendirian</figcaption>";
        //     $htmlImg .= "</figure>";
        //     $htmlImg .= " </div>";

        //     $indicator .= " <li
        //     data-target='#carouselExampleIndicators'
        //     data-slide-to='8'
        //     ></li>";

        //     $lampiran .="<li>Fc Akte Pendirian √</li>";
        // }


        // if($row->rencanateknikbangunan > 0){
        //     $htmlImg .= "<div class='carousel-item '>";
        //     $htmlImg .= "<figure>";
        //     $htmlImg .= "<img class='img-size' src='".   base_url() ."assets/files/".$row->file_rencanateknikbangunan."' alt='First slide' />";
        //     $htmlImg .= " <figcaption>Rencana Teknik Bangunan</figcaption>";
        //     $htmlImg .= "</figure>";
        //     $htmlImg .= " </div>";

        //     $indicator .= " <li
        //     data-target='#carouselExampleIndicators'
        //     data-slide-to='7'
        //     ></li>";

        //     $lampiran .="<li>Rencana Teknik Bangunan √</li>";
        // }


        $ullist .= "<ul>";
        $ullist .= $lampiran;
        $ullist .= "</ul>";
        // $lampiran .="<ul>";

        // $lampiran .="<li>Sertifikat Tanah √</li>";
        // $lampiran .="<li>NPWP √</li>";
        // $lampiran .="</ul>";
        // }
        $response = [
            "detail" => $html,
            "img" => $htmlImg,
            "indicator" => $indicator,
            "lampiran" => $ullist
        ];
        echo json_encode($response);
    }

    public function verification()
    {
        $notelp = anti_injection($this->input->post('no_telp_verif'));
        $cekNomor = validation_phone($notelp);
        ///dd($cekNomor);
        if ($cekNomor['verif']) {
            $response = $this->Verification_m->cek("pbg", $notelp, 3);
        } else {
            $response = [
                "message" => $cekNomor['message'],
                "access" => false
            ];
        }
        echo json_encode($response);
    }

    function validation_pbg($validation)
    {
        $validation->set_rules("namaPemohon", "Nama Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("jalanPemohon", "Alamat Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rtPemohon", "RT Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rwPemohon", "RW Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kelurahanPemohon", "Desa Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kecamatanPemohon", "Kecamatan Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kabupatenPemohon", "Kabupaten/Kota Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("noTelpPemohon", "No Telp Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("noktpPemohon", "No KTP Pemohon", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("pekerjaanPemohon", "Pekerjaan Pemohon", "required", array("required" => "%s Harus Di Isi"));

        $validation->set_rules("fungsiGedung", "Fungsi Gedung", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("pondasiGedung", "Pondasi Gedung", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("dindingGedung", "Dinding Gedung", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("lantaiGedung", "Lantai Gedung", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kusenGedung", "Kusen Gedung", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rangkaAtapGedung", "Rangka Atap Gedung", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("atapGedung", "Atap Gedung", "required", array("required" => "%s Harus Di Isi"));

        $validation->set_rules("namaLokasi", "Nama Lokasi", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rtLokasi", "RT Lokasi", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("rwLokasi", "RW Lokasi", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kelurahanLokasi", "Kelurahan Lokasi", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("kecamatanLokasi", "Kecamatan Lokasi", "required", array("required" => "%s Harus Di Isi"));

        $validation->set_rules("luasTanah", "Luas Tanah", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("statusTanah", "Status Tanah", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("noTanah", "No Tanah", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("pemilikTanah", "Pemilik Tanah", "required", array("required" => "%s Harus Di Isi"));

        $validation->set_rules("batasTanahUtara", "Batas Tanah Utara", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("batasTanahSelatan", "Batas Tanah Selatan", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("batasTanahTimur", "Batas Tanah Timur", "required", array("required" => "%s Harus Di Isi"));
        $validation->set_rules("batasTanahBarat", "Batas Tanah Barat", "required", array("required" => "%s Harus Di Isi"));
    }
}
