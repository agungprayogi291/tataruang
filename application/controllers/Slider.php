<?php
class Slider extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('SimpanFormSurat_m');
    }

    public function index()
    {
        if($this->session->userdata('logged')==''){ 
            redirect(base_url('login'),'refresh');
            exit;
        } 
        $this->data['page'] = '/page/slider/view.php';
        $this->data['home'] = "";
        $this->data['baseMap'] = "";
       
        $this->load->view('layout/wrapper', $this->data, false);
    }

    public function upload($file)
    {
        $config['upload_path'] = "./assets/files/slider";
        $config['allowed_types'] = 'jpeg|JPEG|JPG|png|PNG|jpg';
        $config['encrypt_name'] = FALSE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $hasil = [];
        if ($this->upload->do_upload($file)) {
            $data = ['upload_data' => $this->upload->data()];
            $hasil = ['success' => 0, 'file_name' => $data['upload_data']['file_name']];
        } else {
            $hasil = ['success' => 1, 'error' => $this->upload->display_errors()];
        }

        return $hasil;
    }

    public function tampil()
    {
        $jenis = LoadDataTabel('slider', 'id > 0 order by urutan');
        $html = '';
        $i = 1;
        if (!empty($jenis)) {
            foreach ($jenis as $key => $val) {
                $html .= '<tr>
                        <td>' . $i . '</td>
                        <td>' . $val->file_name . '</td>                                                  
                        <td>' . $val->urutan . '</td>                                                                              
                        <td>' . ($val->status == 1 ? "Aktif" : "Non Aktif") . '</td>                                                  
                        <td> 
                            <button type="button" style="width:100%; height:25px; padding:0px;" class="btn btn-warning mb-2"  onclick="BukaEdit(' . $val->id . ');">edit</button> 
                            <button type="button" style="width:100%; height:25px; padding:0px;" class="btn btn-danger" onclick="BukaHapus(' . $val->id . ');">delete</button>
                        </td>
                    </tr>';
                $i++;
            }
        }
        $output['tabel'] = $html;
        $output['baris'] = $i;
        $id = $this->data['file_name'] =$this->db->select_min('id')->get("slider")->row();
        $output['file_name']=  $this->db->select("file_name")->from("slider")->where('id',$id->id)->get()->row();
        echo json_encode($output);
    }

    public function simpan()
    {
        $post = $this->input->post();
        $hasil = 0;

        // upload gambar
        if($_FILES['inputFile']["error"] == 0){
            $fileupload = $this->upload('inputFile');
            // dd($fileupload);
            if ($fileupload['success'] == 0) {
                $hasil = 1;
                $data = [
                    'file_name' =>  $fileupload['file_name'],
                    'urutan' => anti_injection($post['inputUrutan']),
                    'status' => anti_injection($post['inputStatus'])
                ];
            } else {
                $hasil = 0;
                $result = [
                    'success' => 0,
                    'message' => "Gagal menyimpan form slider",
                ];
            }
        }else{
            $data = [
                'urutan' => anti_injection($post['inputUrutan']),
                'status' => anti_injection($post['inputStatus'])
            ];
            $hasil = 1; 
        }
        if ($hasil == 0) {
            $result = [
                'file_name' =>  $fileupload['file_name'],
                'success' => 0,
                'message' => "Gagal menyimpan form slider",
            ];
        } else {
            if($post['id'] == null){
                if ($this->SimpanFormSurat_m->simpan('slider', $data) == true) {
                    $result = [
                        'file_name' => $fileupload['file_name'],
                        'success' => 1,
                        'message' => "Slider berhasil disimpan",
                    ];
                }
            }else{
                $update = $this->SimpanFormSurat_m->update('slider', $data,$post['id']);
                if ($update["success"] == true) {
                    $result = [
                        "success" =>true,
                        "message" => "success",
                        "file_name" => $update["file_name"] 
                    ];
                }
               
            }
        }

        echo json_encode($result);
    }
    public function hapus()
    {
        $id = $this->input->post('id');
        $Foto = LoadNamaItemTabel('slider', 'file_name', 'id=' . $id);
        $this->db->where('id', $id);
        $simpan = $this->db->delete('slider');
        $input['pesan'] = 'Data gagal disimpan';
        $input['hasil'] = 0;
        if ($simpan) {
            if ($Foto != '') {
                $target_dir = 'assets/files/slider/' . $Foto;
                if (file_exists($target_dir) != false) {
                    unlink($target_dir);
                }

                $input['pesan'] = 'Data Berhasil dihapus';
                $input['hasil'] = 1;
            }
        }
        echo json_encode($input);
    }
    public function detail()
    {
        $id = $this->input->post('kode');
        $tabel = LoadDataTabel('slider', 'id=' . $id);
        echo json_encode($tabel);
    }    
}
