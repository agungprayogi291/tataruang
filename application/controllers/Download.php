<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends CI_Controller {
    function __construct(){
		parent::__construct();
		$this->load->helper(array('url','download'));				
	}
 
	public function index(){		
		$this->load->view('v_download');
	}
 
	public function download(){	
        $namaFile = anti_injection($this->uri->segment("3"));			
		force_download("./assets/files/$namaFile",NULL);
	}	
}
