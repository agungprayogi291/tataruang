<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TataRuang extends CI_Controller {
    function __construct(){
        parent::__construct();
    }
    
    public function index()
    {
        $data =[
            "home" => false,
            "page" => 'page/tataruang/view',
            "baseMap" => true
        ];
        $kondLogin=' aktif =1 and umum=1';
        if($this->session->userdata('logged')){  $kondLogin=' aktif =1'; } 
        $data['treviw'] =createtreview($kondLogin);
        $this->load->view("layout/wrapper",$data);
    }
          
    function load_peta(){
        $id = FilterAngka($this->input->get_post('key')); 
        $dt= LoadDataKolomTabel('m_kategory','id='.$id);
        $data['tb']= $dt->tabel;
        $data['fi']= $dt->fielde;
        $data['jenis']= $dt->jenis;
        $data['opacity']= $dt->opacity;
        $data['simbologi']= $dt->simbol;
        $data['fikond']= $dt->field_kondisi;
        
        echo json_encode($data);  
    } 
    
    function load_point(){  
            $sql = "SELECT  id,lokasi,lang,lat FROM bencana limit 5";   
            if (isset($_GET['featname'])){
                            $featname = $_GET['featname'];
                            $distance = $_GET['distance'] * 1000; //change km to meters 
                            //join for spatial query - table geom is in EPSG:26916
                            $sql = $sql . " LEFT JOIN $table r ON ST_DWithin(l.geom, r.geom, $distance) WHERE r.featname = '$featname';";
            }    
            $dt= $this->db->query($sql); 
            $row = $dt->result(); 
            $X = array('hasildata'=>$row);               
        //      echo json_encode($X);   
            echo json_encode($row);   
    }  

    function load_petadasar(){  
        //    $dt['key']=$this->input->get_post['key'];
        //    $dt['map']=$this->input->get_post['map']; 
        //    $point    =$this->input->get_post('point');   
        $dt['key']=$_GET['key'];
        $dt['map']=$_GET['map']; 
        $point    =''; 


        $dataX=$this->decodeMapTable($dt);  
        $table      =$dataX['tabel'];
        $fieldstr   =$dataX['fields']; 
        $kondisi    ='';
        if ($point!=''){
        //$fieldstr .= " ST_MakePoint(lat,lang) as gg";
        $fieldstr .= " lat,lang ";
        }else{ 
        $fieldstr .= " ST_AsGeoJSON(l.geom,4326) as gg";   
        // Select ST_SetSrid(ST_MakePoint(lon, lat),4326) from sometable;
        }  
        $sql = "SELECT $fieldstr FROM $table l "; 
        // echo $sql;
        //if (isset($_GET['featname'])){
        //	$featname = $_GET['featname'];
        //	$distance = $_GET['distance'] * 1000; //change km to meters
        //	//join for spatial query - table geom is in EPSG:26916
        //	$sql = $sql . " LEFT JOIN $table r ON ST_DWithin(l.geom, r.geom, $distance) WHERE r.featname = '$featname';";
        //} 
        $rows= $this->db->query($sql)->result();   
        $data=''; 
        foreach ($rows as $i){
        $data.=$i->nama.", ";
        $data.= $i->id.", ";
        $data.=$i->gg.", "; 
        }
        $data.= ";"; 
        echo $data;
    }      

    function decodeMapTable($row){ 
            if ($row['map']=='administrasi'){
                if ($row['key']==1){
                        $d['tabel']='batas_kab'; 
                        $d['fields'] = "nama_kab as nama,id,"; 
                }elseif ($row['key']==2){ 
                        $d['tabel']='batas_kec';
                        $d['fields'] = "nama_kec as nama ,id,"; 
                } else if ($row['key']==3){
                        $d['tabel']='batas_kel'; 
                        $d['fields'] = "nama_kec as nama ,id,"; 
                } 
                }
            //    ------------------------------------------------
                if ($row['map']=='bencana'){
                    $d['tabel'] ='bencana'; 
                    $d['fields'] = "lokasi,id,"; 
                }
                
                
                return $d;
    }         
        
    public function load_bencana()
    {
        $desa = anti_injection($this->input->post('desa'));
        $awal   =anti_injection($this->input->post('awal'));
        $akhir  =anti_injection($this->input->post('akhir'));
        //echo 'awal:'.$awal;
        //echo 'akhir:'.$akhir;
        $this->db->select('*');            
        if ($awal!='' && $akhir!='') {
            $this->db->where('tanggal>=',$awal);   
            $this->db->where('tanggal<=',$akhir);    
        }
        if ($desa!= '') {$this->db->where('id_desa', $desa);}
        $data=$this->db->get('v_bencana')->result();               
        echo json_encode($data);
    }
    public function load_geolistrik()
    {
        //$desa = anti_injection($this->input->post('desa')); 
        $this->db->select('*');              
        //if ($desa!= '') {$this->db->where('id_desa', $desa);}
        $data=$this->db->get('geolistrik')->result();               
        echo json_encode($data);
    }
    
    public function load_bencana_bydate()
    {
        $id= anti_injection($this->input->post('id'));             
        $this->db->select('*');              
        $this->db->where('tanggal',$id);
        $data=$this->db->get('v_bencana')->result();               
        echo json_encode($data);
    }
    
    public function detail()
    {
        $id = $this->input->post('id'); 
        $bencana = LoadDataKolomTabel('v_bencana','id='.$id);
        $tabel='';
        $gambar=''; 
        //                $tabel.="<tr><td>Jenis Kejadian</td><td id=''>".$bencana->id."</td></tr>"; 
        $tabel.="<tr><td>Jenis Kejadian</td><td id=''>".$bencana->nama_jenis."</td></tr>"; 
        $tabel.="<tr><td>Lokasi / Desa</td><td id=''>".$bencana->lokasi." - ". $bencana->nama_desa."</td></tr>";                  
        $tabel.="<tr><td>Tgl Hari Jam</td><td id=''>".$bencana->hari." ".$bencana->tanggal." / ".$bencana->jam."</td></tr>"; 
        $tabel.="<tr><td>Penyebab</td><td id=''>".$bencana->penyebab."</td></tr>"; 
        $tabel.="<tr><td>Kerusakan</td><td id=''>".$bencana->kerusakan."</td></tr>"; 
        $tabel.="<tr><td>Kerugian</td><td id=''>".$bencana->kerugian."</td></tr>"; 
        $tabel.="<tr><td>Korban</td><td id=''>".$bencana->korban."</td></tr>"; 
        $tabel.="<tr><td>Kronologi</td><td id=''>".$bencana->kronologi."</td></tr>"; 
        $tabel.="<tr><td>Kendala/Kebutuhan Mendesak/Potensi Bencana Susulan</td><td id=''>".$bencana->kendala."</td></tr>"; 
        $tabel.="<tr><td>Penanganan</td><td id=''>".$bencana->penanganan."</td></tr>"; 
        $tabel.="<tr><td>Titik Koordinat</td><td id=''>[ ".$bencana->lokasi_x." ,". $bencana->lokasi_y." ]</td></tr>"; 
        $jum=1;
        $foto= LoadDataTabel ('bencana_foto','id_bencana='.$id);  
        if (!empty($foto)>0){
            //            if (count((array)$foto)>0){
                $gambar.="<tr><td colspan=".count((array)$foto)." style='text-align:center;'>KLIK PADA FOTO UNTUK MEMPERBESAR</td></tr>"; 
            foreach ($foto as $gbr){                   
                if ($jum==1) {$gambar.="<tr>"; }
                    $KK='"'.$gbr->foto.'"';
                    $pt="<img src='".base_url().'asset/image/foto_bencana/'.$gbr->foto."' alt='not found' width='90' height='110' onclick='BukaZoom(".$KK.");'>";
                    $gambar.="<td style='text-align:center;'>".$pt."</td>";                                  
                if ($jum==6) {$gambar.="</tr>"; }
                $jum++;
                if ($jum==6) { $jum=1; } 
            }
                if ($jum!=6) { $gambar.="</tr>"; } 
                
        }
        //                                        </div>
        $out['tabel']=$tabel;
        $out['gambar']=$gambar;
        $out['hasil']=1;
        echo json_encode($out);       
    } 
}