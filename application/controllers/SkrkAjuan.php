<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SkrkAjuan extends CI_Controller {
    function __construct(){
        parent::__construct();
    }
    public function index(){
        $data =[
            "page" => "/page/skrk_ajuan.php",
            "baseMap" => true,
            "home" => false
        ];
        $this->load->view("layout/wrapper",$data);
    }

    public function daftarPengajuanSkrk(){
        $data =[
            "page" => "/page/daftar_pengajuan.php",
            "baseMap" => true,
            "home" => false
        ];
        $this->load->view("layout/wrapper",$data);
    }
}
