<?php
class FormSuratKuasa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('SimpanFormSurat_m');
    }

    public function index()
    {
        $this->data['page'] = '/page/formKuasa/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }
    public function simpan()
    {
        $valid = $this->form_validation;
        $valid->set_rules("namaP1", "Nama Pihak Pertama" ,"required" ,array("required" => "%s Harus Di Isi"));
        $valid->set_rules("namaP2","Nama Pihak Kedua" ,"required", array("required" => "%s Harus Di Isi"));
        $result = [];
        if($valid->run() == FALSE){
            $result = [
                "message" => validation_errors(),
                "success" => 0
            ];
        }else{
            if ($_POST) {
                $post = $this->input->post();

                $data = [
                    'namap1' => anti_injection($post['namaP1']),
                    'noktpp1' => anti_injection($post['noktpP1']),
                    'pekerjaanp1' => anti_injection($post['pekerjaanP1']),
                    'notelpp1' => anti_injection($post['noTelpP1']),
                    'jalanp1' => anti_injection($post['jalanP1']),
                    'rtp1' => anti_injection($post['rtP1']),
                    'rwp1' => anti_injection($post['rwP1']),
                    'kelurahanp1' => anti_injection($post['kelurahanP1']),
                    'kecamatanp1' => anti_injection($post['kecamatanP1']),
                    'kabupatenp1' => anti_injection($post['kabupatenP1']),
                    'namap2' => anti_injection($post['namaP2']),
                    'noktpp2' => anti_injection($post['noktpP2']),
                    'pekerjaanp2' => anti_injection($post['pekerjaanP2']),
                    'notelpp2' => anti_injection($post['noTelpP2']),
                    'jalanp2' => anti_injection($post['jalanP2']),
                    'rtp2' => anti_injection($post['rtP2']),
                    'rwp2' => anti_injection($post['rwP2']),
                    'kelurahanp2' => anti_injection($post['kelurahanP2']),
                    'kecamatanp2' => anti_injection($post['kecamatanP2']),
                    'kabupatenp2' => anti_injection($post['kabupatenP2']),
                    'fungsibangunan' => anti_injection($post['fungsiBangunan']),
                    'jalanlahan' => anti_injection($post['jalanLahan']),
                    'rtlahan' => anti_injection($post['rtLahan']),
                    'rwlahan' => anti_injection($post['rwLahan']),
                    'kelurahanlahan' => anti_injection($post['kelurahanLahan']),
                    'kecamatanlahan' => anti_injection($post['kecamatanLahan']),
                    'kabupatenlahan' => anti_injection($post['kabupatenLahan']),
                    'haktanah' => anti_injection($post['hakTanah']),
                    'notanah' => anti_injection($post['noTanah']),
                    'pemiliktanah' => anti_injection($post['pemilikTanah']),
                    'luastanah' => anti_injection($post['luasTanah']),
                ];

                // $this->SimpanFormSurat_m->simpan("kuasa", $data);
                // echo json_encode($post);

                if ($this->SimpanFormSurat_m->simpan('kuasa', $data) == true) {
                    $result = [
                        'success' => 1, 'message' => "Form berhasil disimpan",
                        'data' => $data
                    ];
                   
                }
            }
        }
        echo json_encode($result);
    }

    public function cetak()
    {
        ini_set('memory_limit', '-1');
        ini_set('pcre.backtrack_limit', '50000000');

        // echo '<pre>';
        // print_r($_POST);
        // die();
        $data['cetak'] = $this->input->post();

        $html = '';

        $data['html'] = $html;
        $filename              = 'Surat Kuasa';
        // $author                = 'Form Surat';
        // $header                = headerL(["app_title" => "Formulir<br>
        // PERMOHONAN PERSETUJUAN<br>
        // KESESUAIAN KEGIATAN PEMANFAATAN RUANG (
        // PKKPR )"]);
      //  $stylesheetheader      = file_get_contents(realpath(APPPATH) . '/views/page/headerpdf/style.css');
        $html                  = $this->load->view('page/formKuasa/preview', $data, true);
        $stylesheet            = file_get_contents(realpath(APPPATH) . '/views/page/formKuasa/style.css');

        try {
            $mpdf                  = new \Mpdf\Mpdf(['tempDir' =>  '/tmp/mpdf', 'mode' => 'utf-8',  'format' => 'Folio', 'setAutoBottomMargin' => 'stretch', 'setAutoTopMargin' => 'pad']);
            // $mpdf->debug = true;
            $mpdf->SetAuthor('Admin');
            $mpdf->SetTitle('Formulir Permohonan');
            // $mpdf->WriteHTML($stylesheetheader, \Mpdf\HTMLParserMode::HEADER_CSS);
            // $mpdf->SetHTMLHeader($header);
            // $mpdf->setFooter('{PAGENO}');

            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($filename . '.pdf', 'I');
        } catch (\Mpdf\MpdfException $e) { // Note: safer fully qualified exception name used for catch
            // Process the exception, log, print etc.
            echo $e->getMessage();
        }
    }

   
    public function simpan_v1()
    {
        if ($_POST) {
            $post = $this->input->post();

            $data = [
                'namap1' => anti_injection($post['namaP1']),
                'noktpp1' => anti_injection($post['noktpP1']),
                'pekerjaanp1' => anti_injection($post['pekerjaanP1']),
                'notelpp1' => anti_injection($post['noTelpP1']),
                'jalanp1' => anti_injection($post['jalanP1']),
                'rtp1' => anti_injection($post['rtP1']),
                'rwp1' => anti_injection($post['rwP1']),
                'kelurahanp1' => anti_injection($post['kelurahanP1']),
                'kecamatanp1' => anti_injection($post['kecamatanP1']),
                'kabupatenp1' => anti_injection($post['kabupatenP1']),
                'namap2' => anti_injection($post['namaP2']),
                'noktpp2' => anti_injection($post['noktpP2']),
                'pekerjaanp2' => anti_injection($post['pekerjaanP2']),
                'notelpp2' => anti_injection($post['noTelpP2']),
                'jalanp2' => anti_injection($post['jalanP2']),
                'rtp2' => anti_injection($post['rtP2']),
                'rwp2' => anti_injection($post['rwP2']),
                'kelurahanp2' => anti_injection($post['kelurahanP2']),
                'kecamatanp2' => anti_injection($post['kecamatanP2']),
                'kabupatenp2' => anti_injection($post['kabupatenP2']),
                'fungsibangunan' => anti_injection($post['fungsiBangunan']),
                'jalanlahan' => anti_injection($post['jalanLahan']),
                'rtlahan' => anti_injection($post['rtLahan']),
                'rwlahan' => anti_injection($post['rwLahan']),
                'kelurahanlahan' => anti_injection($post['kelurahanLahan']),
                'kecamatanlahan' => anti_injection($post['kecamatanLahan']),
                'kabupatenlahan' => anti_injection($post['kabupatenLahan']),
                'haktanah' => anti_injection($post['hakTanah']),
                'notanah' => anti_injection($post['noTanah']),
                'pemiliktanah' => anti_injection($post['pemilikTanah']),
            ];

            // $this->SimpanFormSurat_m->simpan("kuasa", $data);
            // echo json_encode($post);

            if ($this->SimpanFormSurat_m->simpan('kuasa', $data) == true) {
                $result = ['success' => 1, 'message' => "Form berhasil disimpan"];
                echo json_encode($result);
            }
            
        }
    }
}
