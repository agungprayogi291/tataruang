<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Simbologi extends CI_Controller {
    function __construct(){
		parent::__construct();				
	}

    public function index(){
        if($this->session->userdata('logged')==''){ 
            redirect(base_url('login'),'refresh');
            exit;
        } 
        $this->data['induk'] = LoadItemCombo('m_kategory','id>0', 'nama','id');   
        $this->data['page'] = '/page/simbologi/view.php';
        $this->data['home'] = false;
        $this->data['baseMap'] = false;
        $this->data['treviw'] =createtreview();
        $this->load->view('layout/wrapper', $this->data, FALSE);
    }

    public function simpan() {   
    
        $valid = $this->form_validation; 
        $valid->set_rules('nama', 'Nama','required', 
                           array(	'required'		=> '%s harus diisi'));               
        $valid->set_rules('urutan', 'urutan ','required', 
                           array(	'required'		=> '%s harus diisi'));        
        $valid->set_rules('aktif', 'Status Aktif','required', 
                           array(	'required'		=> '%s harus diisi')); 
        $induk  =   anti_injection($this->input->post('induk'));                
        if ($induk!=''){
            $valid->set_rules('tabel', 'Tabel','required', 
                        array(	'required'		=> '%s harus diisi'));         
            $valid->set_rules('fieldKondisi', 'Field Select','required', 
                        array(	'required'		=> '%s harus diisi'));
            $valid->set_rules('opacity', 'Opacity','required', 
                        array(	'required'		=> '%s harus diisi'));         
        }
        if($valid->run()==FALSE) { 
                $input['pesan']=  validation_errors();
                $input['hasil']=0;
        }else{ 
            $kode                   =anti_injection($this->input->post('id'));  
            $data['nama']           =anti_injection($this->input->post('nama'));
            $data['id_induk']       =anti_injection($this->input->post('induk'));                
            if ($data['id_induk']==''){$data['id_induk']=0; }
            $data['tabel']          =anti_injection($this->input->post('tabel'));
            $data['fielde']         =anti_injection($this->input->post('fielde'));
            $data['jenis']          =anti_injection($this->input->post('jenis'));
            
            
            $data['field_kondisi']           =anti_injection($this->input->post('fieldKondisi'));
           if ($data['field_kondisi']==''){ $data['field_kondisi']='jenis'; }     
            
            
//                if ($data['jenis']==''){$data['jenis']=$data['nama']; }
//           $data['ket']            =anti_injection($this->input->post('ket'));
            $data['urutan']         = FilterAngka($this->input->post('urutan')); 
            $data['opacity']       = $this->input->post('opacity'); 
            $data['umum']         = $this->input->post('umum'); 
            if($data['opacity']=='') {$data['opacity']="0.7";}
            $data['simbol']        = $this->input->post('warna'); 
            $data['aktif']         = $this->input->post('aktif'); 
            $random         = $this->input->post('random'); 
            if($random){$data['simbol']='';}
            $input['pesan']='Data gagal di simpan';  
            $input['hasil']=0;
            if ($kode!=""){ 
                    $this->db->where('id', $kode);
                if ($this->db->update('m_kategory',$data)){ $hasil=true;} 
            }else{                         
                if ($this->db->insert('m_kategory',$data)){ $hasil=true;}
            } 
            
            if ($hasil==true) {
                $input['pesan']='Data berhasil di simpan';  
                $input['hasil']=1;
                $input['treviw'] =createtreview();
            }
        }          
        echo json_encode($input);   
     }         
     
     public function hapus() {   
       
        $kode  =  anti_injection($this->input->post('kode'));   
              
        $input['pesan']='Data gagal di hapus';  
        $input['hasil']=0;
                
        $this->db->where('id', $kode);
        if ($this->db->delete('m_kategory')){ $hasil=true;}         
        if ($hasil==true) {
            $input['pesan']='Data berhasil di hapus';  
            $input['hasil']=1;
            $input['treviw'] =createtreview();
        }
       echo json_encode($input);       
      
   }

   function detail(){
     
    $id = FilterAngka($this->input->get_post('kode')); 
    $sql = "SELECT * from m_kategory where id=".$id;        
    $dt= $this->db->query($sql); 
    $row = $dt->result();  
      echo json_encode($row);   
} 

}