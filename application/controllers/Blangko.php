

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blangko extends CI_Controller {
    function __construct(){
        parent::__construct();
    }

    public function index(){
        $data = array(
            "page" => "/page/skrk_blangko.php",
            "home" => false,
            "baseMap" => false
        );
        $this->load->view("layout/wrapper",$data);
    }
}
