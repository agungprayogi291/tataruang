<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peta_m extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Listing user
	public function listing($tabel,$kondisi){	
		$this->db->select('*');
		$this->db->from($tabel); 
                if ($kondisi!=''){$this->db->where($kondisi); } 
//		$this->db->order_by('namauser');
		$query = $this->db->get();
		return $query->result();
	}	 
       
//=======================================================================================
    	public function listing_bytanggal(){	
		$this->db->select('tanggal,hari');
		$this->db->from('bencana'); 
//                if ($kondisi!=''){$this->db->where($kondisi); } 
		$this->db->order_by('tanggal','desc');
		$this->db->group_by('tanggal,hari');
		$this->db->limit('15');
		$query = $this->db->get();
		return $query->result();
	}
	public function listingPetaBangunan($itbx,$kec,$kel,$zona)
	{
		$this->db->select("p.*,k.kecamatan as namaKecamatan ,kl.kelurahan as namaKelurahan ,z.nama_zona as zona ");
        $this->db->from('peta_bangunan p');
        $this->db->join('peta_admkecamatan k','k.gid = p.idkec','left');
        $this->db->join('peta_admkelurahan kl','kl.gid = p.idkel','left');
        $this->db->join('zona z','z.id = p.idzona','left');

		if($itbx){
			$this->db->where('p.itbx',$itbx);
		}
		if($kec){
			$this->db->where('p.idkec',$kec);
		}
		if($kel){
			$this->db->where('p.idkel',$kel);
		}if($zona){
			$this->db->where('p.idzona',$zona);
		}
		$this->db->order_by('gid');
        $query = $this->db->get();
		return $query->result();
	}
}
 
 