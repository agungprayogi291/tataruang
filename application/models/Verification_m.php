<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verification_m extends CI_Model {
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
    public function cek($table, $notelp,$jenis){
        $result = $this->db->select_max("id")->from($table)->where("verifikasi_telephone",$notelp)->get()->row();
        //dd($result->id);
        $response =[];
        if($result->id == null || $result->id == ""){
            $response =[
                "message" => "registrasi berhasil",
                "access" => true
            ];
        }else{
            
            $status = $this->db->select("status")->from("status_permohonan")->where("id_form",$result->id)->where('jenis',$jenis)->get()->row();
            
            if($status->status < 2){
                $response = [
                    "message" => "Nomor ini belum bisa melakukan registrasi pembuatan surat permohonan!",
                    "access" => false
                ];
            }else{
                $response = [
                    "message" => "Registrasi berhasil",
                    "access" => true
                ];
            }
        }
        return $response;
    }
}