<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Listing user
	public function listing($idcb='',$kdcb=''){	
		$this->db->select('*');
		$this->db->from('namapengguna'); 
                if ($idcb!=''){$this->db->where('idcabang',$idcb); }
                if ($kdcb!=''){$this->db->where('kdcabang',$kdcb); }
		$this->db->order_by('namauser');
		$query = $this->db->get();
		return $query->result();
	}	 
        // Detail user
	public function detail($kode){
	 
		$this->db->select('*');
		$this->db->from('namapengguna');
		//$this->db->where('kode',$kode); 
		$this->db->where('id',$kode); 
		$query = $this->db->get();
		return $query->row();
	} 
	// Tambah/insert user
	public function tambah($data){
		$hasil=false;
                if ($this->db->insert('namapengguna', $data)){$hasil=true;};
                return $hasil;
	}
	// Edit/update user
	public function edit($data,$id) {
		$hasil=false;
                $this->db->where('id', $id);
                if ($this->db->update('namapengguna', $data)){$hasil=true;}
                return $hasil;
	}
        // Delete/hapus user
	public function delete($kode){
		$hasil=false;
                $this->db->where('id', $kode);
		if($this->db->delete('namapengguna')){$hasil=true;}
                 return $hasil;
	}
//=======================================================================================
        public function login_user($user,$passw){		                 
                $PWD= EncrypsiPassw($passw); 
                $this->db->select('*');
		$this->db->from('data_user');
		$this->db->where('nama_user',$user);  
                //---------------------------------------------------------------- 
		$query = $this->db->get(); 
                
                $data['pesan']="";
                $data['hasil']=0;
                $data['isi']='';   
                if ($query ->num_rows() == 0)  {
                    $data['pesan']="kombinas user dan passw tidak valid"; 
                }else{
                    $data['isi']= $query->row();
                    $data['hasil']= 1;
                } 
		return $data; 
	}        
        
        
        

}
 
 