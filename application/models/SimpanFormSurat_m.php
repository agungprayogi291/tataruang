<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SimpanFormSurat_m extends CI_Model
{

    // Load database
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    public function simpan($table, $data)
    {
        $result = false;

        if ($this->db->insert($table, $data)) {
            $result = true;
        }

        return $result;
    }


    public function getDataById($table, $id)
    {
        $query = $this->db->get_where($table, array('id' => $id))->result();
        return $query;
    }
    public function update($table,$data,$kode){
        $result = [];
        $this->db->where('id', $kode);
        if($this->db->update($table, $data)){
            $get = $this->db->select('file_name')->from("$table")->where('id',$kode)->get()->row();
            $result = [
                'success'=> true,
                'file_name' => $get->file_name
            ];
        }else{
            $result = [
                "message" => "update gagal"
            ];
        }
        return $result;
        
    }
}
