<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SuratPermohonan_m extends CI_Model
{

    // Load database
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get($table,$jenis,$status){
      //$result = LoadDataTabel($table,"");
        $this->db->select("$table.*,status_permohonan.status");
        $this->db->from("$table");
        $this->db->join("status_permohonan", "status_permohonan.id_form = $table.id");
        
        if($jenis != null){
            $this->db->where("$table.bertindakuntuk",$jenis);
        }
        if($status != null){
            $this->db->where("status_permohonan.status",$status);
        } 
        $this->db->order_by("$table.id");
        $query = $this->db->get();
        $result = $query->result();
       // dd($result);
        return $result;
    }

    public function status($id_form , $data){

        $result = LoadDataTabel("status_permohonan","id_form = ".$id_form);
        // dd($result);
        $response = [
            "message" => "gagal",
            "success" => true
        ];
        if($result == null){
            if($this->db->insert("status_permohonan",$data)){
                $response = [
                    "message" => "success",
                    "success" =>  true
                ];
            }else{
                $response = [
                    "message" => "filed",
                    "success" =>  false
                ];
            }           
        }
        else{
            //$this->db->update('mytable', $data, "id = 4");
            if($this->db->update('status_permohonan' , $data , "id_form = ".$id_form )){
                $response = [
                    "message" => "update berhasil",
                    "success" => true
                ];
            }else{
                $response = [
                    "message" => "update gagal",
                    "success" => false
                ];
            }
        }
        
        $this->db->insert("status_log",$data);
        return $response;

    }
    public function getStatus($table, $kode,$jenis){
        return $this->db->select("*")->from($table)->where("id_form",$kode)->where('jenis',$jenis)->get()->row();
    }

    public function cariPermohonan($notelp,$jenis){
        $table = "";
        switch ($jenis) {
            case '1':
                $table ="irk";
                break;
            case '2':
                $table ="pkkpr";
                break;
            case '3':
                $table ="pbg";
                break;
            default:
                die;
                break;
        }
        $id_form = $this->db->select_max("id")->from($table)->where("verifikasi_telephone",$notelp)->get()->row();
        $result = $this->db->select("status_log.*")->from($table)->join("status_log", "status_log.id_form = $table.id")->where("$table.id",$id_form->id)->where('jenis',$jenis)->get()->result();
        return $result;
    }
}