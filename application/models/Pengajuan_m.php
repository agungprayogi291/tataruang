<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan_m extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Listing user
	public function listing($kondisi=''){	
		$this->db->select('*');
		$this->db->from('pengajuan'); 
                if ($kondisi!=''){$this->db->where($kondisi); } 
//		$this->db->order_by('namauser');
		$query = $this->db->get();
		return $query->result();
	}	 
        // Detail user
	public function detail($kode){
	 
		$this->db->select('*');
		$this->db->from('pengajuan');
		$this->db->where('id',$kode); 
		$query = $this->db->get();
		return $query->row();
	} 
	// Tambah/insert user
	public function tambah($data){
		$hasil=false;
                if ($this->db->insert('pengajuan', $data)){$hasil=true;};
                return $hasil;
	}
	// Edit/update user
	public function edit($data) {
		$hasil=false;
                $this->db->where('id', $data['kode']);
                if ($this->db->update('pengajuan', $data)){$hasil=true;}
                return $hasil;
	}
        // Delete/hapus user
	public function delete($kode){
		$hasil=false;
                $this->db->where('id', $kode);
		if($this->db->delete('pengajuan')){$hasil=true;}
                 return $hasil;
	}
//=======================================================================================
    
	// public function listingJoin($table,$fieldKondisi,$paramsKond){
	// 	$this->db->select('')
	// }
}
 
 