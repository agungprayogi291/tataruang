<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_m extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Listing user
	public function listing($tabel,$kondisi){	
		$this->db->select('*');
		$this->db->from($tabel); 
                if ($kondisi!=''){$this->db->where($kondisi); } 
//		$this->db->order_by('namauser');
		$query = $this->db->get();
		return $query->result();
	}	 
        // Detail user
	public function detail($kode){
	 
		$this->db->select('*');
		$this->db->from('namapengguna');
		$this->db->where('kode',$kode); 
		$query = $this->db->get();
		return $query->row();
	} 
	// Tambah/insert user
	public function tambah($data){
		$hasil=false;
                if ($this->db->insert('namapengguna', $data)){$hasil=true;};
                return $hasil;
	}
	// Edit/update user
	public function edit($data) {
		$hasil=false;
                $this->db->where('kode', $data['kode']);
                if ($this->db->update('namapengguna', $data)){$hasil=true;}
                return $hasil;
	}
        // Delete/hapus user
	public function delete($kode){
		$hasil=false;
                $this->db->where('kode', $kode);
		if($this->db->delete('namapengguna')){$hasil=true;}
                 return $hasil;
	}
//=======================================================================================
    

}
 
 