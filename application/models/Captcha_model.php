<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Captcha_model extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database(); 
                $this->load->helper(array('captcha','url','form'));
                
	}

   
     public  function GenereateChapta(){         
        $config_captcha = array(
         'img_path'      => './captcha_img/',
        'img_url'       => base_url('captcha_img/'),
        'img_width'     => '200',
        'img_height'    => 30,
        'border'        => 0, 
        'expiration'    => 7200,
        'font_path'     => FCPATH . 'asset/fonts/times_new_yorker.ttf', #load font jika mau ganti fontnya 
        'pool'          => '23456789ABCDEFGHJKMNPRSTU', #tipe captcha (angka/huruf, atau kombinasi dari keduanya) 
        # atur warna captcha-nya di sini ya.. gunakan kode RGB
        'colors' => array(
                        'background' => array(242, 242, 242),
                        'border' => array(255, 255, 255),
                        'text' => array(0, 0, 0),
                        'grid' => array(255, 40, 40))         
		);  
	return  $config_captcha;
  
}
     public  function BuatChapta(){ 
            $cap = create_captcha($this->GenereateChapta());             
            $this->session->set_userdata('mycaptcha', $cap['word']);  
            return   $cap['image'];  
     }
    
     public function RefreshChapta(){            
        $cap = create_captcha(GenereateChapta());
        $this->session->unset_userdata('mycaptcha');
       $this->session->set_userdata('mycaptcha', $cap['word']);
         return   $cap['image'];
    } 
  
      

}
 