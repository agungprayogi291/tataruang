<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kejadian_m extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Listing user
	public function listing($desa,$jenis,$awal,$akhir){	
		$this->db->select('*');
		$this->db->from('v_bencana'); 
                
                if ($desa!=''){$this->db->where('id_desa='.$desa); } 
                if ($jenis!=''){$this->db->where('id_jenis='.$jenis); } 
                if ($awal!=''){$this->db->where('tanggal>=',$awal); } 
                if ($akhir!=''){$this->db->where('tanggal<=',$akhir);}  
                $this->db->order_by('tanggal','desc');  
		$query = $this->db->get();
		return $query->result();
	}	 
        // Detail user
	public function detail($kode){
	 
		$this->db->select('*');
		$this->db->from('v_bencana');
		$this->db->where('id',$kode); 
		$query = $this->db->get();
		return $query->row();
	} 
	// Tambah/insert user
	public function tambah($data){
		$hasil=false;
                if ($this->db->insert('bencana', $data)){$hasil=true;};
                return $hasil;
	}
	// Edit/update user
	public function edit($data) {
		$hasil=false;
                $this->db->where('id', $data['kode']);
                if ($this->db->update('bencana', $data)){$hasil=true;}
                return $hasil;
	}
        // Delete/hapus user
	public function delete($kode){
		$hasil=false;
                $this->db->where('id', $kode);
		if($this->db->delete('bencana')){$hasil=true;}
                 return $hasil;
	}
//=======================================================================================
    

}
 
 