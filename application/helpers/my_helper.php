<?php
            
        function LoadNamaItemTabel($Tabel,$Field,$Kondisi){
            $ci =& get_instance();
            $sql="select $Field as item from $Tabel where $Kondisi ";             
            $Qry= $ci->db->query($sql);             
            $dt='';
            if($Qry->num_rows()>0){
                 $hasil= $Qry->row(); 
                $dt=$hasil->item;
            }            
            
            return $dt;  
        }
        function LoadDataTabel($Tabel,$Kondisi){
            $ci =& get_instance();
            $Kon= '';
            if ($Kondisi!=''){$Kon=" where ".$Kondisi; }
            $sql="select * from ". $Tabel.$Kon;             
            $Qry= $ci->db->query($sql);             
            $dt='';
            if($Qry->num_rows()>0){
                 $dt= $Qry->result();  
            }             
            return $dt;  
        }         
        function LoadDataKolomTabel($Tabel,$Kondisi){
            $ci =& get_instance();
            $Kon= '';
            if ($Kondisi!=''){$Kon=" where ".$Kondisi; }
            $sql="select * from ". $Tabel.$Kon;             
            $Qry= $ci->db->query($sql);             
            $dt='';
            if($Qry->num_rows()>0){
                 $dt= $Qry->row();  
            }             
            return $dt;  
        }  
        function LoadItemCombo($Tabel,$Kondisi,$field,$fId,$pilih=''){
            $ci =& get_instance();
            $Kon= '';
            if ($Kondisi!=''){$Kon=" where ".$Kondisi; }
            $sql="select ".$field." as nama,".$fId." as id from ". $Tabel.$Kon;             
            $Qry= $ci->db->query($sql);              
            
            $dt ='<option value="" selected="true">'.$pilih.'</option>'; 
            if($Qry->num_rows()>0){ 
                $data= $Qry->result();  
                foreach ($data as $isi){
                    $dt .="<option value='".$isi->id."'>".$isi->nama."</option>"; 
                }
            } 
            return $dt;  
        }  
        function anti_injection($string)
        {
            $data = stripslashes(strip_tags(htmlentities(htmlspecialchars($string, ENT_QUOTES))));
            $data=  str_replace("union", "", $data);
            $data=  str_replace("+", "", $data);
            $data=  str_replace("database", "", $data);
            $data=  str_replace("information_schema", "", $data);
            $data=  str_replace("tabel_name", "", $data);
            $data=  str_replace("columns", "", $data);        
            return str_replace("'", "", $data);
        } 
       
        function validation_phone($notelp){
            $number    = preg_match('/^(^08|62\s?)(\d{3,4}?){2}\d{3,4}$/i', $notelp);
            $response = false;
            if(!$number) {
                $response = [
                    "message" => "harus menggunakan format 628XXX atau 08XXXX dan perhatikan panjang nomor",
                    "verif" => false,
                ];
            }else {
                $response = [
                    "message" => "nomor terverifikasi",
                    "verif" => true
                ];
            }
            return $response;
        }
        if (!function_exists('dd')) {
            function dd($data)
            {
               echo json_encode($data);
               die;
            }
         }
         
         if (!function_exists('last')) {
             function last()
             {
               $ci = &get_instance();
               echo $ci->db->last_query();
               die;
             }
         }
         function EncrypsiPassw($kunci){ 
            return hash('sha512',$kunci . config_item('encryption_key')); 
            }  
         function headerL($data){            
            $ci = &get_instance();
            return $ci->load->view('headerpdf/landscape', $data, true);
        }
        function headerP($data)
        {
            $ci = &get_instance();
            return $ci->load->view('headerpdf/potrait', $data, true);     
        }     

        function createtreview($kond=''){
            $where='';            
            if ($kond!=''){$where=' and '.$kond; } 
            $row= LoadDataTabel('m_kategory','id_induk=0'.$where.'  order by urutan');  
            $telo='';
            if(!empty($row) ) { $telo=build_menu_tree($row,$where);    } 
            return $telo;
        }                    
        function build_menu_tree($menu,$kond='') { 
            $tabel='m_kategory';
            $html=''; 
            if(count($menu) > 0) { 
                foreach ($menu as $item_menu) {
                    $mainLink='';  
                    if( menu_induk($item_menu->id,$tabel,$kond) ) { 
                        $html .="<li id='.$item_menu->id.'>".$item_menu->nama;
                        $html .="<ul>"; 
                                        $mn=getMenuListByParent($item_menu->id,$tabel,$kond);
                                        $html .=  build_menu_tree($mn,$kond);  
                        $html .="</ul>";
                    }else{ 
                            $html .="<li readonly='true' id='.$item_menu->id.'>".$item_menu->nama;

                    } 
                } 
                return $html; 
            }
        } 
        function FilterAngka($angka){
            $hasil ='0';
            if ($angka!=''){ $hasil = str_replace(".","",$angka);  }
            return $hasil; 
        }    
        function menu_induk($id,$tabel,$kond='') {   
            $ada= LoadNamaItemTabel($tabel,'id', "id_induk=".$id.$kond);  
            $hasil = false;
            if ($ada!=''){$hasil = true;} 
            return $hasil;           
        } 
        function getMenuListByParent($parent_id,$tabel,$kond='') {  
            return LoadDataTabel($tabel,'id_induk='.$parent_id.$kond .' order by urutan');             
           }   
           function createmenu(){
           $row= LoadDataTabel('menu','id_induk=0 order by urutan');     
           $telo=build_menu($row);
           return $telo;
           }   
           function build_menu($menu) { 
               $tabel='menu';
               $html=''; 
               if(count((array)$menu) > 0) { 
                   foreach ($menu as $item_menu) {
                      $mainLink='';  
   
                       if( menu_induk($item_menu->id,$tabel) ) { 
                           $html .="<li id='.$item_menu->id.'>".$item_menu->nama;
                           $html .="<ul>"; 
                                           $mn=getMenuListByParent($item_menu->id,$tabel);
                                           $html .=  build_menu_tree($mn);  
                           $html .="</ul>";
                       }else{ 
                            $html .="<li id='.$item_menu->id.'>".$item_menu->nama;
   
                       } 
                   } 
                   return $html; 
               }
           }
               
        
    function getSlider(){
        $result =LoadNamaItemTabel("slider","file_name","status = 0");
        return $result;
    }
?>  