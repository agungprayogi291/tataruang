/*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs 
  */
$(function() {

    $("#cekSKRKForm input").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // something to have when submit produces an error ?
            // Not decided if I need it yet
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
       
            var phone = $("input#phone").val();
            $.ajax({
                url: "./bin/skrk_me.php",
                type: "POST",
                dataType: "json",
                data: { phone: phone},
                cache: false,
                success: function(data) {                    
                    if (data.hasil=='ok'){
                        var tb="<table border='0'>";
                            tb+="<tr>";
                                    tb+="<td>"+ data.isi[0].tgl_masuk+"</td>";
                                    tb+="<td>--></td>";
                                    tb+="<td colspan='2'>Pengajuan Permohonan</td>"; 
                            tb+="</tr>";  
                            // ------------------------------------------------------
                            if(data.isi[0].tgl_proses!==null){
                                tb+="<tr>";
                                    tb+="<td>"+ data.isi[0].tgl_proses+"</td>";
                                     tb+="<td>--></td>";
                                    tb+="<td>Pemrosesan permohonoan</td>"; 
                                     tb+="<td>--></td>";
                                    tb+="<td>"+ data.isi[0].ket_proses +"</td>"; 
                            tb+="</tr>";  
                            } 
                            // ------------------------------------------------------
                            if(data.isi[0].tgl_selesai!==null){
                                tb+="<tr>";
                                    tb+="<td>"+ data.isi[0].tgl_selesai+"</td>";
                                     tb+="<td>--></td>";
                                    tb+="<td>permohonoan selesai di proses</td>"; 
                                     tb+="<td>--></td>";
                                    tb+="<td>"+ data.isi[0].ket_selesai +"</td>"; 
                            tb+="</tr>";  
                            } 
                            // ------------------------------------------------------ 
                            if(data.isi[0].tgl_ambil!==null){
                                tb+="<tr>";
                                    tb+="<td>"+ data.isi[0].tgl_ambil+"</td>";
                                     tb+="<td>--></td>";
                                    tb+="<td>permohonoan telah di ambil</td>"; 
                                     tb+="<td>--></td>";
                                    tb+="<td>"+ data.isi[0].ket_ambil +"</td>"; 
                            tb+="</tr>";  
                            } 
                            // ------------------------------------------------------
                            if(data.isi[0].tgl_tolak!==null){
                                tb+="<tr>";
                                    tb+="<td>"+ data.isi[0].tgl_tolak+"</td>";
                                     tb+="<td>--></td>";
                                    tb+="<td>permohonoan ditolak</td>"; 
                                     tb+="<td>--></td>";
                                    tb+="<td>"+ data.isi[0].ket_tolak +"</td>"; 
                            tb+="</tr>";  
                            } 
                            // ------------------------------------------------------
                            
                            
                            tb+="</table>";
                            
                            $('#success').html(tb);                 
                    }else{
                        $('#success').html("<div class='alert alert-danger'>"+data.pesan);                 
                    }    
                },
                
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#phone').focus(function() {
    $('#success').html('');
});