 <?php include "menu_header.php";  ?>

 
    <!-- Page Content -->
    <div class="container">


        <!-- Contact Form -->
        <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
        <div class="row">
            <div class="col-md-8">
                <h3>Penelusuran Status Permohonan SKRK Kota Magelang</h3>
                <form name="sentMessage" id="cekSKRKForm" novalidate>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Nomor Telepon:</label>
                            <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number.">
                        </div>
                    </div>
                     
                    <!-- For success/fail messages -->
                    <button type="submit" class="btn btn-primary"><i class='fa fa-paper-plane'></i>&nbsp;Cek Status</button>
                    <p> <div id="success"></div>
                </form>
            </div>

        </div>
        <!-- /.row -->

        <hr>

         

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Contact Form JavaScript -->
    <!-- Do not edit these files! In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/skrk_me.js"></script>

   <?php include "menu_footer.php";  ?>