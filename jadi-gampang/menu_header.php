<?php session_start();  ?>
<!DOCTYPE HTML> 
<html>
<head>
    <title>SIG Kota Magelang </title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" /> 
    <link rel="icon" type="image/png" href="../assets/css/logo.png"> 
    
    
    <link rel="stylesheet" href="../assets/css/main.css" /> 
    
  
   
     <link href="../assets/slider/4/js-image-slider.css" rel="stylesheet" type="text/css" />    
    <link href="../assets/slider/generic.css" rel="stylesheet" type="text/css" />   
 
  
 
    <link rel="stylesheet" href="../assets/plugin/leaflet/lib/jquery/jquery-ui.css">       
     <link rel="stylesheet" href="../assets/plugin/leaflet/lib/leaflet/leaflet.css">
   
 
  
   <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css" />  
   <script src="../assets/js/jquery.min.js"></script> 
   <script src="../assets/js/bootstrap.min.js"></script>     
   
   
 
<?php   
include "menu_nav.php";         
function getBaseUrl() 
{
    // output: /myproject/index.php
    $currentPath = $_SERVER['PHP_SELF']; 
    
    // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index ) 
    $pathInfo = pathinfo($currentPath); 
    
    // output: localhost
    $hostName = $_SERVER['HTTP_HOST']; 
    
    // output: http://
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
    
    // return: http://localhost/myproject/
    return $protocol.$hostName.$pathInfo['dirname']."/";
}


?>
<body>
    <div class="clearfix"></div><p>    
 