<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jadi Gampang - Sistem Informasi Geografis Tata Ruang Kota Magelang</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">JADI GAMPANG</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.html">Beranda</a>
                    </li>
                    <li>
                        <a href="regulasi.html">Regulasi Tata Ruang</a>
                    </li>
                    <li>
                        <a href="../index.php?page=jg">Peta &amp; Ajuan SKRK</a>
                    </li>
                    <li>
                        <a href="laporan.html">Laporan Publik</a>
                    </li>
                    <li>
                        <a href="statusskrk.html">Cek Status SKRK</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">


        <!-- Contact Form -->
        <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
        <div class="row">
            <div class="col-md-8">
                <h3>Masukan Publik seputar Regulasi Tata Ruang Kota Magelang</h3>
                <?php
                if ($_GET['cmdx']=="uu262007") { ?>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Undang-Undang No. 68 Tahun 2010</h4>
                        <p>Tentang Bentuk dan Tata Cara Peran Masyarakat dalam Penataan Ruang</p>
                        <span class='pull-right'><a href='pdf/UU_NO_68_TH_2010.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                    </div>
                </div>
                <?php
                } elseif ($_GET['cmdx']=="rpd072012") { ?>
	              <div class="media">
	                  <div class="pull-left">
	                      <span class="fa-stack fa-2x">
	                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
	                            <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
	                      </span> 
	                  </div>
	                  <div class="media-body">
	                      <h4 class="media-heading">Rancangan Peraturan Daerah Kota Magelang</h4>
	                      <p>Tentang Perubahan atas Peraturan Daerah Kota Magelang Nomor 4 Tahun 2012 tentang Rencana Tata Ruang Wilayah Kota Magelang Tahun 2011 - 2031</p>
	                      <span class='pull-right'><a href='pdf/RAPERDA_PERUBAHAN_RTRW_KOTA_MAGELANG.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
	                  </div>
	              </div>
                <?php
                } elseif ($_GET['cmdx']=="permenpu202011") { ?>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Menteri Pekerjaan Umum No. 20 Tahun 2011</h4>
                        <p>Tentang Pedoman Penyusunan Rencana Detail Tata Ruang dan Peraturan Zonasi Kabupaten / Kota</p>
                        <span class='pull-right'><a href='pdf/PERMEN_PU_NO_20_TH_2011.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                    </div>
                </div>
                <?php
								} elseif ($_GET['cmdx']=="perda042012") { ?>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Daerah Kota Magelang No. 4 Tahun 2012</h4>
                        <p>Tentang Rencana Tata Ruang Wilayah Kota Magelang Tahun 2011-2031</p>
                        <span class='pull-right'><a href='pdf/PERDA_NO_04_TH_2012.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                    </div>
                </div>
                <?php
								} elseif ($_GET['cmdx']=="pp152010") { ?>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Pemerintah No. 15 Tahun 2010</h4>
                        <p>Tentang Penyelenggaraan Penataan Ruang</p>
                        <span class='pull-right'><a href='pdf/PP_NO_15_TH_2010.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                    </div>
                </div>
								<?php
								} elseif ($_GET['cmdx']=="permenatr062017") { ?>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Menteri Agraria dan Tata Ruang / Kepala Badan Pertanahan Nasional No. 6 Tahun 2017</h4>
                        <p>Tentang Tata Cara Peninjauan Kembali Rencana Tata Ruang Wilayah</p>
                        <span class='pull-right'><a href='pdf/PERMEN_ATR_NO_6_TH_2017.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                    </div>
                </div>
								<?php
								} elseif ($_GET['cmdx']=="pp682010") { ?>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Pemerintah No. 68 Tahun 2010</h4>
                        <p>Tentang Bentuk dan Tata Cara Peran Masyarakat dalam Penataan Ruang</p>
                        <span class='pull-right'><a href='pdf/PP_NO_68_TH_2010.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                    </div>
                </div>
								<?php
								} elseif ($_GET['cmdx']=="permenatr082017") { ?>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Menteri Agraria dan Tata Ruang / Kepala Badan Pertanahan Nasional No. 8 Tahun 2017</h4>
                        <p>Tentang Pedoman Pemberian Persetujuan Substansi dalam Rangka Penetapan Peraturan Daerah tentang Rencana Tata Ruang Provinsi dan Rencana Tata Ruang Kabupaten / Kota</p>
                        <span class='pull-right'><a href='pdf/PERMEN_ATR_NO_8_TH_2017.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                    </div>
                </div>
								<?php
                } else {
                	
                }
                ?>
                <form name="sentMessage" id="feedbackForm" novalidate>

                <?php
                if ($_GET['cmdx']=="uu262007") { ?>
                <input type='hidden' id='context' name='context' value='uu262007'/>
                <?php
                } elseif ($_GET['cmdx']=="rpd072012") { ?>
                <input type='hidden' id='context' name='context' value='rpd072012'/>
                <?php
                } elseif ($_GET['cmdx']=="permenpu202011") { ?>
                <input type='hidden' id='context' name='context' value='permenpu202011'/>
                <?php
								} elseif ($_GET['cmdx']=="perda042012") { ?>
                <input type='hidden' id='context' name='context' value='perda042012'/>
                <?php
								} elseif ($_GET['cmdx']=="pp152010") { ?>
                <input type='hidden' id='context' name='context' value='pp152010'/>
								<?php
								} elseif ($_GET['cmdx']=="permenatr062017") { ?>
                <input type='hidden' id='context' name='context' value='permenatr062017'/>
								<?php
								} elseif ($_GET['cmdx']=="pp682010") { ?>
                <input type='hidden' id='context' name='context' value='pp682010'/>
								<?php
								} elseif ($_GET['cmdx']=="permenatr082017") { ?>
                <input type='hidden' id='context' name='context' value='permenatr082017'/>
								<?php
                } else {
                	
                }
                ?>

                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Nama:</label>
                            <input type="text" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Nomor Telepon:</label>
                            <input type="tel" class="form-control" id="phone" required data-validation-required-message="Please enter your phone number.">
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Alamat Email:</label>
                            <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address.">
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Masukan:</label>
                            <textarea rows="10" cols="100" class="form-control" id="message" required data-validation-required-message="Please enter your message" maxlength="999" style="resize:none"></textarea>
                        </div>
                    </div>
                    <div id="success"></div>
                    <!-- For success/fail messages -->
                    <button type="submit" class="btn btn-primary"><i class='fa fa-paper-plane'></i>&nbsp;Kirim Laporan</button>
                </form>
            </div>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2017 Bidang Penataan Ruang DPUPR Kota Magelang</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Contact Form JavaScript -->
    <!-- Do not edit these files! In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/feedback_me.js"></script>

</body>

</html>