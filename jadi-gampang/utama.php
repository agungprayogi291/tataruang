  <?php include "menu_header.php";  ?>
<!-- ================================================================================== -->        
    
<div class="container">
 <!-- ================================================================================== -->       
 
 
 
  

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" style='text-align:center;'>Jadi Gampang</h1>
                <h3 style='text-align:center;'>[Jaringan Penyedia Layanan Perizinan dan Penyampaian Gagasan Masyarakat tentang Penataan Ruang]</h3>
                <h4 style='text-align:center;'>Memudahkan masyarakat untuk mengakses informasi, mendapatkan pelayanan perizinan dan melaporkan pelanggaran tata ruang di mana saja (<i>anywhere</i>), kapan saja (<i>anytime</i>) melalui desktop maupun <i>smartphone</i>.</h4>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i> Informasi Tata Ruang</h4>
                    </div>
                    <div class="panel-body">
                        <p>Memuat beragam informasi mengenai panduan sistem informasi rencana tata ruang JADI GAMPANG,rencana tata ruang kota Magelang peraturan perundang-undangan terkait penataan ruang. Selain itu masyarakat juga dapat memberikan masukan mengenai penataan ruang.</p>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-gift"></i>Perizinan Tata Ruang</h4>
                    </div>
                    <div class="panel-body">
                        <p>Fasilitas pendaftaran izin pemanfaatan ruang berupa Surat Keterangan Rencana Kota, selain form pendaftaran juga dimuat arahan pemanfaatan ruang diantaranya KDB (Koefisien Dasar Bangunan, Ketinggian Lantai Maksimum, dan Pola Ruang) </p>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i>Laporan Pelanggaran Tata Ruang </h4>
                    </div>
                    <div class="panel-body">
                        <p>Fasilitas pelaporan pelanggaran tata ruang dari masyarakat dan oleh masyarakat</p>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <hr>

        

    </div>
    <!-- /.container -->    
 <!-- ================================================================================== -->    
 
    
 <!-- ================================================================================== -->    
       
 </div>
<!-- ================================================================================== -->
<?php include "menu_footer.php";  ?>
 


     
        
 