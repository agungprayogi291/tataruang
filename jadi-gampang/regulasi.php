 
  <?php include "menu_header.php";  ?>
 

    <!-- Page Content -->
    <div class="container">
 
        <!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Regulasi Terkait Tata Ruang Kota Magelang</h2>
            </div>
            <div class="col-md-12 col-lg-12">
              <div class="media">
                  <div class="pull-left">
                      <span class="fa-stack fa-2x">
                            <i class="fa fa-circle fa-stack-2x text-primary"></i>
                            <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                      </span> 
                  </div>
                  <div class="media-body">
                      <h4 class="media-heading">Rancangan Peraturan Daerah Kota Magelang</h4>
                      <p>Tentang Perubahan atas Peraturan Daerah Kota Magelang Nomor 4 Tahun 2012 tentang Rencana Tata Ruang Wilayah Kota Magelang Tahun 2011 - 2031</p>
                      <span class='pull-right'><a href='pdf/RAPERDA_PERUBAHAN_RTRW_KOTA_MAGELANG.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                      <span class=''><a href='#' id='rpd072012' class='btn btn-primary btn-sm btn-comment'><i class='fa fa-comments'></i>&nbsp;Masukan Publik</a></span>
                  </div>
              </div>
            </div>
            <div class="col-md-4">
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Undang-Undang No. 26 Tahun 2007</h4>
                        <p>Tentang Penataan Ruang</p>
                        <span class='pull-right'><a href='pdf/UU_NO_26_TH_2007.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                        <span class=''><a href='#' id='uu262007' class='btn btn-primary btn-sm btn-comment'><i class='fa fa-comments'></i>&nbsp;Masukan Publik</a></span>
                    </div>
                </div>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Menteri Pekerjaan Umum No. 20 Tahun 2011</h4>
                        <p>Tentang Pedoman Penyusunan Rencana Detail Tata Ruang dan Peraturan Zonasi Kabupaten / Kota</p>
                        <span class='pull-right'><a href='pdf/PERMEN_PU_NO_20_TH_2011.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                        <span class=''><a href='#' id='permenpu202011' class='btn btn-primary btn-sm btn-comment'><i class='fa fa-comments'></i>&nbsp;Masukan Publik</a></span>
                    </div>
                </div>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Daerah Kota Magelang No. 4 Tahun 2012</h4>
                        <p>Tentang Rencana Tata Ruang Wilayah Kota Magelang Tahun 2011-2031</p>
                        <span class='pull-right'><a href='pdf/PERDA_NO_04_TH_2012.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                        <span class=''><a href='#' id='perda042012' class='btn btn-primary btn-sm btn-comment'><i class='fa fa-comments'></i>&nbsp;Masukan Publik</a></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Pemerintah No. 15 Tahun 2010</h4>
                        <p>Tentang Penyelenggaraan Penataan Ruang</p>
                        <span class='pull-right'><a href='pdf/PP_NO_15_TH_2010.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                        <span class=''><a href='#' id='pp152010' class='btn btn-primary btn-sm btn-comment'><i class='fa fa-comments'></i>&nbsp;Masukan Publik</a></span>
                    </div>
                </div>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Menteri Agraria dan Tata Ruang / Kepala Badan Pertanahan Nasional No. 6 Tahun 2017</h4>
                        <p>Tentang Tata Cara Peninjauan Kembali Rencana Tata Ruang Wilayah</p>
                        <span class='pull-right'><a href='pdf/PERMEN_ATR_NO_6_TH_2017.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                        <span class=''><a href='#' id='permenatr062017' class='btn btn-primary btn-sm btn-comment'><i class='fa fa-comments'></i>&nbsp;Masukan Publik</a></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Pemerintah No. 68 Tahun 2010</h4>
                        <p>Tentang Bentuk dan Tata Cara Peran Masyarakat dalam Penataan Ruang</p>
                        <span class='pull-right'><a href='pdf/PP_NO_68_TH_2010.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                        <span class=''><a href='#' id='pp682010' class='btn btn-primary btn-sm btn-comment'><i class='fa fa-comments'></i>&nbsp;Masukan Publik</a></span>
                    </div>
                </div>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-file-pdf-o fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Peraturan Menteri Agraria dan Tata Ruang / Kepala Badan Pertanahan Nasional No. 8 Tahun 2017</h4>
                        <p>Tentang Pedoman Pemberian Persetujuan Substansi dalam Rangka Penetapan Peraturan Daerah tentang Rencana Tata Ruang Provinsi dan Rencana Tata Ruang Kabupaten / Kota</p>
                        <span class='pull-right'><a href='pdf/PERMEN_ATR_NO_8_TH_2017.pdf' target='_blank'><i class='fa fa-file-pdf-o fa-lg text-danger'></i>&nbsp;Download</a></span>
                        <span class=''><a href='#' id='permenatr082017' class='btn btn-primary btn-sm btn-comment'><i class='fa fa-comments'></i>&nbsp;Masukan Publik</a></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row --> 
        <hr> 
    </div>
    
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
		<script>
			$(function() {
				$('a.btn-comment').on('click', function(evt){
					evt.stopImmediatePropagation();
					var context = $(this).attr('id');
					document.location = './feedback.php?cmdx='+context+'';
					return false;
				});
			});
		</script>
 <?php include "menu_footer.php";  ?>
