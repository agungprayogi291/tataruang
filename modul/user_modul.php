<?php   
ini_set('display_errors', 1); 
session_start(); 
include_once 'koneksi_pg.php';
include_once 'anti_injection.php';
$conn = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");
if (!$conn) {
	echo "Not connected : " . pg_error();
	exit;
} 


//aksi=tampil
$aksi=anti_injection($_POST['aksi']);
if ($aksi=='tampil'){
     $Levele=anti_injection($_POST['level']);
     $KodeU=$_POST['KodeUsr'];
     ListUser($conn,$Levele,$KodeU);    
}
if ($aksi=='hapus'){
    $Kode=anti_injection($_POST['kode']);
    HapusUser($conn,$Kode); 
}

if ($aksi=='edit'){
    $Kode=anti_injection($_POST['kode']);
    LoadDetailUser($conn,$Kode); 
}
if ($aksi=='tambah'){
    
 $Kode=anti_injection($_POST['kode']);
 $Nama=anti_injection($_POST['a_nama']);
 $User=anti_injection($_POST['a_user']);
 $Passwd=anti_injection($_POST['a_passw']);
 $Ulangi=anti_injection($_POST['a_ulang']);
 $Level=anti_injection($_POST['a_level']);     
    TambahUser($conn,$Kode,$Nama,$User,$Passwd,$Ulangi,$Level);
    
}
 
function ListUser($conn,$Levele,$KodeU){ 
    
    $sql = "select * from data_user where jalan =0 order by nama_lengkap ";  
    
     if  ($Levele=='0'){ $sql = "select * from data_user where kode =".$KodeU;} 

     
    if ($response = pg_query($conn, $sql)) { 
            $result = array();
            while($row = pg_fetch_object($response)){
                array_push($result, $row);
            }  
            echo json_encode($result);  
    }
}
function HapusUser($conn,$Kode){ 
    $sql = "delete  from data_user where kode =$Kode";  
    if ($response = pg_query($conn, $sql)) { 
            $data['hasil']='ok';
            echo json_encode($data);  
    }
}

function LoadDetailUser($conn,$Kode){ 
    $sql = "select * from  data_user where kode =$Kode";  
    if ($response = pg_query($conn, $sql)) {  
            $result = array();
            while($row = pg_fetch_object($response)){
                array_push($result, $row);
            } 
            echo json_encode($result);  
    }
}

function TambahUser($conn,$Kode,$NamaL,$UserN,$Passwd,$Ulangi,$Level){
    
    $PasswH=password_hash($Ulangi, PASSWORD_DEFAULT);
    if ($Kode!=''){
                   $sql = "update  data_user set
                            nama_user ='$UserN', 
                            passw_user='$PasswH', 
                            nama_lengkap='$NamaL', 
                            levele=$Level 
                           where kode =$Kode";

    }else{
            $sql="insert into data_user (nama_user,passw_user,nama_lengkap,levele) values ( 
                    '$UserN','$PasswH','$NamaL','$Level')";
    }
    // $_SESSION['password'] = 123;
     $data['hasil']='error';
     $data['pesan']='Gagal ditambahkan';
     
      if($UserN=='' || $Passwd =='' || $NamaL =='' || $Level =''){
          $data['pesan']='Data ada yang belum di isikan';
      } else { 
            if ($Passwd!=$Ulangi){ 
                $data['pesan']='Pengulangan password salah';
            }else{ 
                if ($response = pg_query($conn, $sql)) { 
                    $data['hasil']='ok';   
                    $data['pesan']='Berhasil ditambahkan';
                }  
            }
      }
    echo json_encode($data);
    
}
 


  
?>

 
 
 