<?php
ini_set('display_errors', 1); 
include_once 'koneksi_pg.php';
include_once 'anti_injection.php';
$conn = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");
                if (!$conn) {
                        echo "Not connected : " . pg_error();
                        exit;
                }  
                
                
$Aksi='';
if ($_POST['aksi']){
     if ($_POST['aksi']=="tampil"){
        TampilData($conn);
        exit;
     }
     if ($_POST['aksi']=="hapus"){
         $Kode =anti_injection($_POST['kode']);
        HapusData($conn,$Kode);
        exit;
     }
     if ($_POST['aksi']=="load"){ 
        LoadData($conn);
        exit;
     }
     if ($_POST['aksi']=="simpan"){
         $Kode = anti_injection($_POST['kode']);
         $Tgl =anti_injection($_POST['tgl']);
         $Isi =anti_injection($_POST['isi']);
         
        SimpanData($conn,$Kode,$Tgl,$Isi);
        exit;
     }
} 
                
 
function TampilData($conn){ 
                $sql = "SELECT * from laporan order by kode desc";  
                if (!$response = pg_query($conn, $sql)) {
                        echo "A query error occured.\n";
                        exit;
                } 
                    $result = array();
                    while($row = pg_fetch_object($response)){
                        array_push($result, $row);
                    }  
                    echo json_encode($result);  
}
function LoadData($conn){ 
    $Kode=$_POST['kode'];
                $sql = "SELECT * from laporan where kode =$Kode order by kode desc";  
                if (!$response = pg_query($conn, $sql)) {
                        echo "A query error occured.\n";
                        exit;
                } 
                    $result = array();
                    while($row = pg_fetch_object($response)){
                        array_push($result, $row);
                    }  
                    echo json_encode($result);  
}
function HapusData($conn,$Kode){ 
                $sql = "delete from laporan where kode=$Kode";  
                if (!$response = pg_query($conn, $sql)) {
                        $result['hasil']='error';
                        $result['pesan']='Data gagal dihapus';
                }else{
                    $result['hasil']='ok';
                    $result['pesan']='Data berhasil dihapus';
                }    
                echo json_encode($result);  
}


function SimpanData($conn,$Kode,$Tgl,$Isi){ 
                $sql = "update laporan set 
                        proses=1,
                        tindak_lanjut='$Isi',
                        tgl_tindaklanjut='$Tgl'
                        where kode=$Kode";  
                if (!$response = pg_query($conn, $sql)) {
                        $result['hasil']='Error';
                        $result['pesan']='Data Gagal Simpan';
                }else{
                    $result['hasil']='ok';
                    $result['pesan']='Data berhasil Simpan';
                }    
                echo json_encode($result);  
}


?>

 
 
 