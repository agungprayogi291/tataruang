
<?php
ini_set('display_errors', 1); 

 
 
include_once 'koneksi_pg.php';
// include_once 'anti_injection.php';
$conn = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");
echo json_encode($conn);
if (!$conn) {
	echo "Not connected : " . pg_error();
	exit;
}

//get the table and fields data
$table = $_POST['table'];
$fields =$_POST['fields'];
$kondisi=$_POST['kondisi'];

//turn fields array into formatted string
$fieldstr = "";
foreach ($fields as $i => $field){
	$fieldstr = $fieldstr . "l.$field, ";
}

//get the geometry as geojson in WGS84 
$fieldstr = $fieldstr . "ST_AsGeoJSON(l.geom,4326)"; 
$limit='';
//$limit=" limit 3000 ";
//create basic sql statement
$sql = "SELECT $fieldstr FROM $table l $kondisi $limit ";

//if a query, add those to the sql statement
if (isset($_GET['featname'])){
	$featname = $_GET['featname'];
	$distance = $_GET['distance'] * 1000; //change km to meters

	//join for spatial query - table geom is in EPSG:26916
	$sql = $sql . " LEFT JOIN $table r ON ST_DWithin(l.geom, r.geom, $distance) WHERE r.featname = '$featname';";
}

  //echo $sql;

//send the query
if (!$response = pg_query($conn, $sql)) {
	echo "A query error occured.\n";
	exit;
}

//echo the data back to the DOM
while ($row = pg_fetch_row($response)) {
	foreach ($row as $i => $attr){
		echo $attr.", ";
	}
	
	echo ";";
}


?>
