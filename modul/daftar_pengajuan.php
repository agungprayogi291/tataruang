<?php
ini_set('display_errors', 1); 
include_once 'koneksi_pg.php';
include_once 'anti_injection.php';
$conn = pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");
                if (!$conn) {
                        echo "Not connected : " . pg_error();
                        exit;
                }  
                
                
$Aksi='';
if ($_POST['aksi']){
     if ($_POST['aksi']=="tampil"){
        TampilData($conn);
        exit;
     }
     if ($_POST['aksi']=="hapus"){
         $Kode = $_POST['kode'];
        HapusData($conn,$Kode);
        exit;
     }
     if ($_POST['aksi']=="updatestatus"){ 
        UpdateStatusPengajuan($conn);
        exit;
     }
     
      if ($_POST['aksi']=="submit"){ 
        SubmitDataPengajuanBaru($conn);
        exit;
     }
      if ($_POST['aksi']=="load"){ 
        LoadData($conn);
        exit;
     }
     
     
} 

function UpdateStatusPengajuan($conn){
    $kode=anti_injection($_POST['kode']);
    $status=anti_injection($_POST['a_status']);
    $ket=anti_injection($_POST['a_ket']); 
    $tglstatus=anti_injection($_POST['a_tgl_status']);
    $kondisiUpdate =''; 
    if($status==1){$kondisiUpdate=",tgl_proses='$tglstatus', ket_proses='$ket'"; }
    if($status==2){$kondisiUpdate=",tgl_selesai='$tglstatus', ket_selesai='$ket'"; }
    if($status==3){$kondisiUpdate=",tgl_ambil='$tglstatus', ket_ambil='$ket'"; }
    if($status==4){$kondisiUpdate=",tgl_tolak='$tglstatus', ket_tolak='$ket'"; }
    
    
      $sql="update pengajuan_skrk set status_e=$status $kondisiUpdate where kode =$kode";
     
        if (!$response = pg_query($conn, $sql)) {
                $result['hasil']='Error';
                $result['pesan']='Data Gagal Simpan';
        }else{
            $result['hasil']='ok';
            $result['pesan']='Data berhasil Simpan';
        }    
        echo json_encode($result);  
          
    
}

function TampilData($conn){ 
    $Kriteria =anti_injection($_POST['kriteria']);
    $KondisiKriteria ='';
    if ($Kriteria!='-'){$KondisiKriteria=" where status_e=$Kriteria";}
    $sql = "SELECT * from pengajuan_skrk $KondisiKriteria order by kode desc";  
    //echo ($sql);
        if (!$response = pg_query($conn, $sql)) {
                echo "A query error occured.\n";
                exit;
        } 
        $result = array();
        while($row = pg_fetch_object($response)){
            array_push($result, $row);
        }  
    echo json_encode($result); 
}   

function LoadData($conn){ 
                 $Kode=anti_injection($_POST['kode']);
                $sql = "SELECT * from pengajuan_skrk where kode =$Kode";   
                if (!$response = pg_query($conn, $sql)) {
                        echo "A query error occured.\n";
                        exit;
                } 
                    $result = array();
                    while($row = pg_fetch_object($response)){
                        array_push($result, $row);
                    }  
                    echo json_encode($result);  
}



function SubmitDataPengajuanBaru($conn){
    $kode=anti_injection($_POST['kode']); 
    $a_nama=anti_injection($_POST['a_nama']);
    $b_kecamatan=anti_injection($_POST['b_kecamatan']);  
    $a_pekerjaan=anti_injection($_POST['a_pekerjaan']);
    $b_kelurahan=anti_injection($_POST['b_kelurahan']);
    $a_alamat =anti_injection($_POST['a_alamat']);
    $a_peruntukan =anti_injection($_POST['a_peruntukan']);
    $a_telp=anti_injection($_POST['a_telp']);
    $a_fungsi_b =anti_injection($_POST['a_fungsi_b']);
    $a_x=anti_injection($_POST['a_x']);
    $a_status_t =anti_injection($_POST['a_status_t']); 
    $a_y =anti_injection($_POST['a_y']);
    $a_luas=anti_injection($_POST['a_luas']); 
    $a_atasnama=anti_injection($_POST['a_atasnama']);
    $a_letak =anti_injection($_POST['a_letak']);
    $a_jalan=anti_injection($_POST['a_jalan']);
    $a_kelurahan=anti_injection($_POST['a_kelurahan']);
    $a_kecamatan=anti_injection($_POST['a_kecamatan']);
    $a_kota =anti_injection($_POST['a_kota']);
    $a_utara=anti_injection($_POST['a_utara']);
    $a_timur=anti_injection($_POST['a_timur']);
    $a_selatan=anti_injection($_POST['a_selatan']);
    $a_barat=anti_injection($_POST['a_barat']);
    $a_untuk =anti_injection($_POST['a_untuk']);
    $Tgl=date("Y-m-d");
    
    
    
   $result['hasil']='Error';
   
   
   $Kosong=false;
  if($a_nama==''){$Kosong=true;  $result['pesan']='Nama belum di isikan';}
   // if($b_kecamatan==''){$Kosong=true;  $result['pesan']='Kecamatan belum di isikan';}
    if($a_pekerjaan==''){$Kosong=true;  $result['pesan']='Pekerjaan belum di isikan';}
   // if($b_kelurahan==''){$Kosong=true;  $result['pesan']='Kelurahan belum di isikan';}
    if($a_alamat ==''){$Kosong=true;  $result['pesan']='alamat belum di isikan';}
   // if($a_peruntukan==''){$Kosong=true;  $result['pesan']='Peruntukan belum di isikan';}
    if($a_telp==''){$Kosong=true;  $result['pesan']='Telp belum di isikan';}
    if($a_fungsi_b==''){$Kosong=true;  $result['pesan']='Fungsi bangunaan belum di isikan';}  
    if($a_status_t==''){$Kosong=true;  $result['pesan']='Status Tanah belum di isikan';}    
    if($a_luas==''){$Kosong=true;  $result['pesan']='Luas belum di isikan';} 
    if($a_atasnama==''){$Kosong=true;  $result['pesan']='Atas nama belum di isikan';}
    if($a_letak==''){$Kosong=true;  $result['pesan']='Letak belum di isikan';}
    if($a_jalan==''){$Kosong=true;  $result['pesan']='Jalan belum di isikan';}
    if($a_kelurahan==''){$Kosong=true;  $result['pesan']='Kelurahan belum di isikan';}
    if($a_kecamatan==''){$Kosong=true;  $result['pesan']='Kecamatan belum di isikan';}
    if($a_kota==''){$Kosong=true;  $result['pesan']='Kota belum di isikan';}
    if($a_utara==''){$Kosong=true;  $result['pesan']='Batas Utara belum di isikan';}
    if($a_timur==''){$Kosong=true;  $result['pesan']='Batas Timur belum di isikan';}
    if($a_selatan==''){$Kosong=true;  $result['pesan']='Batas selatan belum di isikan';}
    if($a_barat==''){$Kosong=true;  $result['pesan']='Batas Barat belum di isikan';}
    if($a_untuk==''){$Kosong=true;  $result['pesan']='Akan digunakan untuk belum di isikan';}
    
    
    
    
    if ($Kosong==false){ 
    $sql="INSERT INTO pengajuan_skrk(
            aju_nama, aju_alamat, aju_telp, aju_kecamatan, aju_kelurahan, 
            aju_peruntukan, aju_x, aju_y, status_tanah, status_luas, status_nama, 
            letak, letak_jalan, letak_kelurahan, letak_kecamatan, letak_kota, 
            batas_utara, batas_timur, batas_selatan, batas_barat, penggunaan_untuk, 
            status_e, tgl_masuk,gid_imb)
    VALUES ('$a_nama','$a_alamat','$a_telp','$b_kecamatan','$b_kelurahan', 
            '$a_peruntukan','$a_x','$a_y','$a_status_t','$a_luas','$a_atasnama', 
            '$a_letak','$a_jalan','$a_kelurahan','$a_kecamatan','$a_kota', 
            '$a_utara','$a_timur','$a_selatan','$a_barat','$a_untuk', 
            1,'$Tgl',$kode)";  
    
                if (!$response = pg_query($conn, $sql)) { 
                        $result['pesan']='Data Gagal Simpan';
                }else{
                    $result['hasil']='ok';
                    $result['pesan']='Data berhasil Simpan';
                }    
                
                
    }             
                
                
                echo json_encode($result);  
    
}



 

 