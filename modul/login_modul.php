 <?php if (isset($_GET['log'])) {
   Logout();
   exit;
 }

 ini_set('display_errors', 1);
 include_once 'koneksi_pg.php';
 include('anti_injection.php');
 $conn=pg_connect("host=$host port=$port dbname=$dbname user=$user password=$password");

 if ( !$conn) {
   echo "Not connected : ". pg_error();
   exit;
 }

 $u=anti_injection($_POST['u']);
 $passwInput=anti_injection($_POST['p']);
 $sql="select  * from data_user where jalan=0 and nama_user='$u'";

 if ( !$response=pg_query($conn, $sql)) {
   echo "A query error occured.\n";
   exit;
 }


 $rows=pg_num_rows($response);
 session_start();

 // if (pg_num_rows($response) > 0) { 

 if ($rows >0) {
   $row=pg_fetch_array($response);
   $passwDB=$row['passw_user'];

   //session_destroy(); 
   if (password_verify($passwInput, $passwDB)) {
     $_SESSION["SudahLogin"]=true;
     $_SESSION["user_nama"]=$row['nama_lengkap'];
     $_SESSION["user_level"]=$row['levele'];
     $_SESSION["user_kode"]=$row['kode'];
     $_SESSION["IsiPesan"]='';
     header('Location: ../index.php');
     //echo json_encode($_SESSION['user_nama']);
   }

   else {
     $_SESSION["SudahLogin"]=false;
     $_SESSION["IsiPesan"]='Password tidak terdaftar';
     header('Location: ../index.php?page=lo');
   }


 }

 else {
   $_SESSION["SudahLogin"]=false;
   $_SESSION["IsiPesan"]='User Name / Password tidak terdaftar';
   header('Location: ../index.php?page=lo');
 }



 function Logout() {
   session_start();
   session_destroy();
   header('Location: ../index.php');
 }

 ?>