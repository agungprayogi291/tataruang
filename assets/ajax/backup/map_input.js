//var map,autocomplete = [];
//$(document).ready(initialize); 
var MapTerbuka=0;
var newMarker;
function initialize( ){
        var tinggi=$(window).height()-110;	 
	$("#map").height(tinggi); 
 
        map = L.map('map',{ fullscreenControl: true}).setView([-7.869585,111.462363],17);                 
	map.on('click', onMapClick); 
        map.on('mousemove', function (e) {
             if(!map.hasLayer(newMarker)){
             $('#long').val(e.latlng.lng);
             $('#lat').val(e.latlng.lat);
                }
  
        });
 
	 
googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
}); 

googleSat = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});

googleTerrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
}).addTo(map);
	
googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});  

openStreets = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });   
   
};
function clickbasemaps(){
   var myRadio = $("input[name=basemaps]");
   var nilai = myRadio.filter(":checked").val();  
   map.removeLayer(citramaps);
   map.removeLayer(googleHybrid);    
   map.removeLayer(googleTerrain);
   map.removeLayer(googleStreets);
   map.removeLayer(openStreets);    
   map.removeLayer(googleSat);
 
   
if (nilai==0){citramaps.addTo(map);    }
if (nilai==1){googleHybrid.addTo(map);    }
if (nilai==2){googleSat.addTo(map);    }
if (nilai==3){googleTerrain.addTo(map); }   
if (nilai==4){googleStreets.addTo(map);  } 
if (nilai==5){openStreets.addTo(map); }
} 

function onMapClick(e) {
    console.log("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng);
    var lang   =e.latlng.lng;
    var lat    =e.latlng.lat;  
    $('#long').val(lang);
    $('#lat').val(lat);  
   if(map.hasLayer(newMarker)){
       map.removeLayer(newMarker);
   }else{    
        addMarker(lat,lang);
    }    
 }    
function addMarker(lang,lat){
    var icone = L.icon({
        iconUrl: URLE+'asset/image/icon_map/default.png',
        iconSize:     [50, 55],     
    });      
//    if(map.hasLayer(newMarker)){ map.removeLayer(newMarker);} 
    newMarker = new L.marker([lang,lat],{icon:icone}).addTo(map); 
    map.flyTo([lang,lat],17, {
            animate: true,
            duration: 2, // in seconds
            essential: true  
          }); 
    
}

function ZoomToMap(lang,lat){
    addMarker(lang,lat);
}