 
//const MainLangLat = [-7.413272, 111.017330];
const MainZoom =12;

const arrLayer = []; 
const arrPoint = []; 
var map, autocomplete = [];
var LayerPeta; 
var opct;
var simbologi; 
var LayerPoint;
var LayerGeolistrik;
var newMarker;
var CheckPeta=false;


$(document).ready(initialize);
var fields = [];
//  function initialize() {
//      var tinggi = $(window).height() - 110;
//      $("#map").height(tinggi);       
// //     map = L.map('map').setView([-7.413272, 111.017330], 12); 
//      map = L.map('map').setView(MainLangLat, MainZoom); 
//      map.on('click', onMapClick);
     

//      googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
//          maxZoom: 20,
//          subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
//      });

//      googleSat = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
//          maxZoom: 20,
//          subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
//      }).addTo(map);

//      googleTerrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
//          maxZoom: 20,
//          subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
//      });

//      googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
//          maxZoom: 20,
//          subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
//      });

//      openStreets = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//          attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
//      });

//      citramaps = L.tileLayer.wms('http://localhost:8080/geoserver/data_gis/wms?', {
//          layers: 'data_gis:Citra_Mgl_2016',
//          transparent: true,
//          format: 'image/png',
//          zIndex: 5,
//          opacity: 1
//      });


//  };
function clickbasemaps() {
    var myRadio = $("input[name=basemaps]");
    var nilai = myRadio.filter(":checked").val();
    map.removeLayer(citramaps);
    map.removeLayer(googleHybrid);
    map.removeLayer(googleTerrain);
    map.removeLayer(googleStreets);
    map.removeLayer(openStreets);
    map.removeLayer(googleSat);


    if (nilai == 0) { citramaps.addTo(map); }
    if (nilai == 1) { googleHybrid.addTo(map); }
    if (nilai == 2) { googleSat.addTo(map); }
    if (nilai == 3) { googleTerrain.addTo(map); }
    if (nilai == 4) { googleStreets.addTo(map); }
    if (nilai == 5) { openStreets.addTo(map); }
}
function Overlaypeta(data,klik='') {
    var geojson = {
        "type": "FeatureCollection",
        "features": []
    };
    var dataArray = data.split(", ;");
    dataArray.pop();
    dataArray.forEach(function(d) {
        d = d.split(", ");


        if (d[fields.length] != "0") {
            var feature = {
                "type": "Feature",
                "properties": {}, //properties object container
                "geometry": JSON.parse(d[fields.length]) //parse geometry
            };
            //console.log(feature);
            for (var i = 0; i < fields.length; i++) {
                var Attr =  fields[i].toUpperCase() ; 
                Attr=Attr.replace("_", " ");                  
                feature.properties[Attr] = d[i];
//                 feature.properties[fields[i].toUpperCase()] = d[i];

            };
            geojson.features.push(feature);
        }


    });
    var mapDataLayer = L.geoJson([geojson], {
        //L.geoJson([geojson], {
        style: function(feature) { 
            return styleCOlor(simbologi); 
        },
        onEachFeature: function(feature, layer) {              
            if (klik!='') {
                layer.on('click', function(e) {
                    detailInf(feature.properties.id, klik);
                });
            }else {
                var html = "";
                for (prop in feature.properties) {
                    html += prop + ": " + feature.properties[prop] + "<br>";
                };
//                 alert(html);
                layer.bindPopup(html);
            }
        }
    }).addTo(map);


    return mapDataLayer;


};

function Overlaypoint(resp,geolistrik=''){
    var marker;     
    var LaterTemp;     
    var icn;      
//     var icn='default.png';      
    var icon= URLE+'asset/image/icon_map/';      

    
    
    LaterTemp = new L.LayerGroup().addTo(map);    
    $.each(resp, function(i, item) { 
                icn='default.png';
                    if (geolistrik!=''){icn='geolistrik.png';} 
                if (item.icon){icn=item.icon;} 
                var v_long = parseFloat(item.lokasi_x);
                var icon_custom = L.icon({ 
                    iconUrl: icon+icn,                         
                    iconSize: [50, 50]
                });             
                var v_lat = parseFloat(item.lokasi_y);
                
                    if (geolistrik!=''){
                        var html = "Lokasi      : " + item.lokasi + "<br>";
                            html += "X(meter)   : " + item.meter_x + "<br>";
                            html += "Y(meter)   : " + item.meter_y + "<br>";
                            html += "Ket        : " + item.ket + "<br>";
                        marker=new L.marker ([v_long, v_lat], {
                        icon: icon_custom})  
                                        
//                            .bindPopup(item.lokasi, {
                        .bindPopup(html, {
//                                        maxWidth: 150,
//                                        closeButton: true,
                                    offset: L.point(0, -20)
                                }).openPopup();
                        // .on('click', onClick.bind(null, this));                           
                    }else{ 
                        marker=new L.marker ([v_long, v_lat], {
                        icon: icon_custom})                        
                        .on('click', onClick.bind(null, this));  
                    }
                LaterTemp.addLayer(marker);  
            })    
    return LaterTemp;        
}
function OverlaypetaPoint1(data) {
    var lang;
    var lat;
    
    for (var i = 0; i < data.length; i++) {
        lang = data[i].lang;
        lat = data[i].lat;
    }

    var geojson = {
        "type": "Feature",
        "features": []
    };
    var dataArray = data.split(", ;");
    dataArray.pop();
    dataArray.forEach(function(d) {
        d = d.split(", ");

        if (d[fields.length] != "0") {
            var feature = {
                "type": "Feature",
                "properties": {}, //properties object container
                "geometry": JSON.parse(d[fields.length]) //parse geometry
            };

            for (var i = 0; i < fields.length; i++) { 
                feature.properties[fields[i]] = d[i];
            };
            geojson.features.push(feature);
        }


    });
    var mapDataLayer = L.geoJson([geojson], {
        //L.geoJson([geojson], {
        style: function(feature) {
            //return feature.properties && feature.properties.style;
            //alert(feature.properties.pola_ruang)
            //                        alert(feature.properties.gid);
            return styleCOlor(feature.properties.id);
        },
        onEachFeature: function(feature, layer) {
            
            if (klik != '') {
                layer.on('click', function(e) {
                    detailInf(feature.properties.id, klik);
                }); 
            } else {
                var html = "";
                for (prop in feature.properties) {
                    html += prop + ": " + feature.properties[prop] + "<br>";
                };
                
                layer.bindPopup(html);
            }
        }
    }).addTo(map);
    return mapDataLayer;
    //       var marker = L.marker([37.7858, -122.401], { title: "My marker" }).addTo(map);  

};


function load_map(id, title, select) { 
    var jdul = filterName(title + '_' + id);      
    if (select == false) {
//         console.log(jdul)
//         console.log(arrLayer[jdul])              
        $.when(
            map.removeLayer(arrLayer[jdul])
        ).promise().done(() => {
            delete arrLayer[jdul]
        })
    } else {          
        $.ajax(URLE + "TataRuang/load_peta", {
            dataType: 'json',
            type: "POST",
            data: { key: id },
            success: function(data) {
                var tb = data.tb;
                var fl = data.fi;
                var jenis = data.jenis;   
                opct = data.opacity;
                simbologi = data.simbologi;                  
                open_map(URLE, tb, fl, jenis, jdul);
            }
        });
    }
};

function cleararray(ar) {
    while (fields.length) { fields.pop(); }
    if (ar) {
        var dataArray = ar.split(",");
        for (i = 0; i < dataArray.length; i++) {
            fields.push(dataArray[i]);
        }
    } else {
        fields["Nama", "gid"];
    }

}

function open_map(url, tb, fi, jenis, jdul) {
    cleararray(fi);
    $.ajax(url + "modul/tata_ruang_modul.php", {
        //	$.ajax("<?php  echo base_url();?>Peta/load_petadasar", {	  
        data: {
            tb: tb,
            fi: fi,
            je: jenis
        },
        success: function(data) { 
            LayerPeta = Overlaypeta(data);
            arrLayer[jdul] = LayerPeta; 
        }
    });


};

function getColorR() {
    var randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
    return randomColor;

}

function getColor(str) {  
var d;
var hasil = '';
    if (!str) {
//         d=getColorR(); 
        hasil=getColorR(); 
    }else{
    d = str.toLowerCase();
}


    
    if (d == "1") { hasil = '#800026' }
    if (d == "2") { hasil = '#7151e8' }
    if (d == "3") { hasil = '#E31A1C' }
    if (d == "4") { hasil = '#b7713e' }
    if (d == "5") { hasil = '#FD8D3C' }
    if (d == "6") { hasil = '#70f169' }
    if (d == "7") { hasil = '#b4c339' }
    if (d == "8") { hasil = '#FED976' }
    if (d == "9") { hasil = '#5d6ce0' }
    if (d == "10") { hasil = '#ca33bd' }
    if (d == "12") { hasil = '#2a0c9e' }
    if (d == "13") { hasil = '#fbe809' }
    if (d == "14") { hasil = '#9c9e24' }
    if (d == "15") { hasil = '#3c8cd2' }
    if (d == "16") { hasil = '#8145dc' }
    if (d == "17") { hasil = '#4ae6e0' }
    if (d == "18") { hasil = '#FEB24C' }
    if (d == "19") { hasil = '#FEB24C' }
    if (d == "20") { hasil = '#FC4E2A' }

    //costume
    if (d == "merah") { hasil = '#f50733' }
    if (d == "hijau") { hasil = '#15f363' }
    if (d == "kuning") { hasil = '#fbf306' }
    if (d == "biru") { hasil = '#0c33f7' }
    if (d == "hitam") { hasil = '#141415' } 
    if (d == "ungu") { hasil = '#330066' } 
//     http://informant12.blogspot.com/2011/06/daftar-code-warna-html-buat-java-script.html
    if (hasil==''){ hasil = getColorR(); }  
    return hasil;
}
function styleCOlor(feature) { 
//     console.log('tahu:'+feature);
var warna = feature;
if (feature==''){ warna = getColor(feature) ; } 
var Opc=7;    
if (opct!=''){Opc=opct;}    
return {
        weight: 2,
        opacity: 1, 
        color: warna,
        dashArray: '3',
        fillOpacity: Opc, 
        //fillColor: getColor(feature.properties.density)
//         fillColor: '#0d0c0c'
        fillColor: warna
    };
}
function styleColorLine(feature) {

    return {
        weight: 6,
        opacity: 1,
        color: getColor(feature),
        dashArray: '3',
        fillOpacity: 0.7
            //fillColor: getColor(feature.properties.density)
            //fillColor: getColor(feature)
    };
}
function PointCheck(cek){    
   if (cek==0){
       $('#frprose').addClass('collapse');
       $('#t_lang').val('');
     $('#t_lat').val('');
       if(map.hasLayer(newMarker)){ map.removeLayer(newMarker);}        
       NgeplayOut();
        
   }
    CheckPeta=cek;     
}
function onMapClick(e) {            
     if (CheckPeta==false) { return false;      }
     
 
     var lang =e.latlng.lng
     var lat =e.latlng.lat
     var info = "Lang: " + e.latlng.lat + " \nLat : " + e.latlng.lng;  
     addMarker(lat, lang, info);
     
     $('#t_lang').val(lang);
     $('#t_lat').val(lat);      
     $('#frprose').removeClass('collapse');
     
  //console.log("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng);  
//     popup.setLatLng(e.latlng)
//         .setContent("You clicked the map at " + e.latlng.toString())
//         .openOn(map);
 
 
}
function centerLeafletMapOnMarker(mape, marker) {
  var latLngs = [ marker.getLatLng() ];
  var markerBounds = L.latLngBounds(latLngs);
  mape.fitBounds(markerBounds);
}
function filterName(dt) 
{
    var xx = dt.trim(); 
//     
//     
//     var i;
//     var kar;
var dtz=xx;
for (i = 0; i < xx.length; i++) {
    kar = xx.substring(i,1).trim();            
    if (kar=='.'){dtz = dtz.replace(".", "");}
    if (kar=='-'){dtz = dtz.replace("-", "");}
    if (kar==' '){dtz = dtz.replace(" ", "");}
}
//


dtz = dtz.replace(".", "");
dtz = dtz.replace(".", "");
dtz = dtz.replace(" ", "");
dtz = dtz.replace("-", "");
//     alert('hasil:'+dtz);
return dtz;
}
function Ngeplay(data){   
        var lang =data[0].lokasi_x;
        var lat=data[0].lokasi_y;
        
    map.flyTo([lang,lat],17, {
        animate: true,
        duration: 2, // in seconds
        essential: true  
        }); 
}
function NgeplayOut(){               
     map.flyTo(MainLangLat,MainZoom, {
            animate: true,
            duration: 2, // in seconds
            essential: true  
          }); 
}
function addMarker(lang,lat,info=''){
    var icone = L.icon({
        iconUrl: URLE+'asset/image/icon_map/default.png',
        iconSize:     [50, 55],     
    });      
    if(map.hasLayer(newMarker)){ map.removeLayer(newMarker);} 
         newMarker = new L.marker([lang,lat],{icon:icone}).addTo(map); 
    map.flyTo([lang,lat],17, {
        animate: true,
        duration: 2, // in seconds
        essential: true  
     }); 
    //     if (info!=''){ newMarker.bindPopup(info).openPopup();}
    if (info!=''){ newMarker.bindPopup(info);}
 
}
 
 