var togglePassword = document.querySelector("#toggle_password");
var toggleToken = document.querySelector("#toggle_token");
var username = document.querySelector("#username");
var password = document.querySelector("#password");

togglePassword.addEventListener('click', function (e) {
    var type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type',type);
    this.classList.toggle('mdi-eye-outline');
});

toggleToken.addEventListener('click', function (e) {
    var type = token.getAttribute('type') === 'password' ? 'text' : 'password';
    token.setAttribute('type',type);
    this.classList.toggle('mdi-eye-outline');
});

$("#alert_embed").fadeIn("slow").delay(600).fadeOut("slow");

function BtnLoading(element){
    $(element).attr("Simpan", $(element).html());
    $(element).prop("disabled", true);
    $(element).html("<div class='spinner-border spinner-border-sm'></div>&nbsp;Loading...");
  }
  
  function BtnReset(element){
    setTimeout(function(){
      $(element).prop("disabled", false);
      $(element).html($(element).attr("Simpan"));
    },500);
  }