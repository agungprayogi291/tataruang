function tableServerSide(nodeId,link,postdata = null,columnDefs = null,expand = null){
    return new Promise((resolve, reject) => {
        let table = $(`#${nodeId}`).DataTable({
            "processing" : false,
            "serverSide" : true,
            "bDestroy" : true,
            "searching" : true,
            "autoWidth" : false,
            "order" : [],
            "ajax" : {
                "url" : link,
                "type" : "POST",
                data : postdata
            },
            "columnDefs" : columnDefs,
            "initComplete" : function(){
                var api = this.api();
                if (api.$('tr').length >= 1) {
                    resolve(true)
                }else{
                    reject(false)
                }
            }
        });
        var detailRows = [];
        $(`#${nodeId}`).DataTable().on('draw', function(){
            $.each(detailRows, function(i, id) {
                $('#' + id + ' td.details-control').trigger('click');
            });
        });
        $(`#${nodeId} tbody`).off('click', 'tr td.details-control');
        $(`#${nodeId} tbody`).on('click', 'tr td.details-control', function(){
            var tr = $(this).closest('tr');
            var row = $(`#${nodeId}`).DataTable().row(tr);
            var idx = $.inArray(tr.attr('id'), detailRows);
            if(row.child.isShown()){
                tr.removeClass('shown');
                row.child.hide();
                detailRows.splice(idx, 1);
            }else{
                tr.addClass('shown');
                row.child(expand(row.data())).show();
                if (idx === -1) {
                    detailRows.push(tr.attr('id'));
                }
            }
        });
    });
}

$('.uang').mask('000.000.000.000',{
    reverse : true
});

function price_to_number(v){
    if (!v) {
        return 0;
    }
    v = v.split('.').join('');
    v = v.split(',').join('.');
    v = Number(v.replace(/[^0-9.]/g, "")).toFixed(2);
    v = parseFloat(v);
    return v;
}

function price_to_number_extra(v){
    if (!v) {
        return 0;
    }
    v = v.split('.').join('');
    v = v.split(',').join('.');
    v = Number(v.replace(/[^0-9.]/g, "")).toFixed(4);
    v = parseFloat(v);
    return v;
}

function number_to_price(v) { //gotit
    if (v == 0) {
        return '0,00';
    }
    v = parseFloat(v);
    v = v.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    v = v.split('.').join('*').split(',').join('.').split('*').join(',');
    return v;
}

function format_uang(v) { //gotit
    if (v == 0) {
        return '0';
    }
    v = parseFloat(v);
    v = v.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    v = v.split('.').join('*').split(',').join('.').split('*').join(',');
    return v;
}

function validate(txt){
    txt.value = txt.value.replace(/[^0-9\+,]/g,'');
}

function FormatUang(angka){
    if(angka == ''){
        return '0';
    }else{
        var reverse = angka.toString().split('').reverse().join(''),
        ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join('.').split('').reverse().join('');
        return ribuan;
    }
}

function pembulatan_rupiah(nilai, pembulat){
    var hasil = (Math.ceil(parseInt(nilai)) % parseInt(pembulat) == 0) ? Math.ceil(parseInt(nilai)) :
    Math.round((parseInt(nilai) + parseInt(pembulat) / 2) / parseInt(pembulat)) * parseInt(pembulat);
    return hasil;
}

function BtnLoading(element){
    $(element).attr("Simpan", $(element).html());
    $(element).prop("disabled", true);
    $(element).html("<div class='spinner-border spinner-border-sm'></div>&nbsp;Loading...");
}

function BtnReset(element){
    setTimeout(function(){
        $(element).prop("disabled", false);
        $(element).html($(element).attr("Simpan"));
    },500);
}

$('.select2').select2({
    placeholder:"Pilih Data"
});

$('.tanggal_datepicker').datepicker({
    dateFormat : 'dd-mm-yy'
});

var invalidChars = ["-", "e", "+", "E"];
    $("input[type='number']").on("keydown", function(e){ 
    if(invalidChars.includes(e.key)){
        e.preventDefault();
    }
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function(){
    "use strict";
    function scrollWithBigMedia(media){
        var $elDataScrollHeight = $("[data-scroll-height]");
        if(media.matches){
            $elDataScrollHeight.each(function() {
                var scrollHeight = $(this).attr("data-scroll-height");
                $(this).css({ height: scrollHeight + "px", overflow: "hidden" });
            });

            $(".slim-scroll")
            .slimScroll({
                opacity: 0,
                height: "100%",
                color: "#999",
                size: "5px",
                wheelStep: 10
            })

            .mouseover(function(){
                $(this).next(".slimScrollBar").css("opacity", 0.4);
            });
        }else{
            $elDataScrollHeight.css({ height: "auto", overflow: "auto" });
        }
    }

    var media = window.matchMedia("(min-width: 992px)");
    scrollWithBigMedia(media);
    media.addListener(scrollWithBigMedia);

    $('[data-toggle="tooltip"]').tooltip({
        container : "body",
        template : '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
    });
    $('[data-toggle="popover"]').popover();

    var mapData = {
        US : 1298,
        FR : 540,
        DE : 350,
        BW : 450,
        NA : 250,
        ZW : 300,
        AU : 760,
        GB : 120,
        ZA : 450
    };

    if(document.getElementById("world")){
        $("#world").vectorMap({
            map : "world_mill",
            backgroundColor : "transparent",
            zoomOnScroll : false,
            regionStyle : {
                initial : {
                    fill : "#e4e4e4",
                    "fill-opacity" : 0.9,
                    stroke : "none",
                    "stroke-width" : 0,
                    "stroke-opacity" : 0
                }
            },
            markerStyle : {
                initial : {
                    stroke : "transparent"
                },
                hover : {
                    stroke : "rgba(112, 112, 112, 0.30)"
                }
            },
            markers : [
                {
                    latLng : [54.673629, -62.347026],
                    name : "America",
                    style : {
                        fill : "limegreen"
                    }
                },
                {
                    latLng : [62.466943, 11.797592],
                    name : "Europe",
                    style : {
                    fill : "orange"
                }
                },
                {
                    latLng : [23.956725, -8.768815],
                    name : "Africa",
                    style : {
                        fill  : "red"
                    }
                },
                {
                    latLng : [-21.943369, 123.102198],
                    name : "Australia",
                    style : {
                        fill : "royalblue"
                    }
                }
            ]
        });
    }

    var mapData2 = {
        IN : 19000,
        US : 13000,
        TR : 9500,
        DO : 7500,
        PL : 4600,
        UK : 4000
    };

    if(document.getElementById("analytic-world")){
        $("#analytic-world").vectorMap({
            map : "world_mill",
            backgroundColor : "transparent",
            zoomOnScroll : false,
            regionStyle : {
                initial: {
                    fill: "#e4e4e4",
                    "fill-opacity": 0.9,
                    stroke: "none",
                    "stroke-width": 0,
                    "stroke-opacity": 0
                }
            },
            series : {
                regions : [
                    {
                        values : mapData2,
                        scale : ["#6a9ef9", "#b6d0ff"],
                        normalizeFunction : "polynomial"
                    }
                ]
            }
        });
    }

    if (document.getElementById("demoworld")) {
        $("#demoworld").vectorMap({
            map : "world_mill",
            backgroundColor : "transparent",
            regionStyle : {
                    initial : {
                    fill : "#9c9c9c"
                }
            }
        });
    }

    NProgress.done();
});