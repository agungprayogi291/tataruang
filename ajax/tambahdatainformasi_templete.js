 $(document).ready(function() {       
       //validasi gambar yang di pilih     
        $('#gambar').change(function(){
               var file = this.files[0];
                var imagefile = file.type; 
				 var imageSize = file.size; 
                 if (ukuranfilemelebihibatas(imageSize)){ 
                    document.getElementById('file_input').value=null;
                     return false 
                } 
         
                 //-------------------------------------------- 
                var match= ["image/jpeg","image/png","image/jpg","image/bmp"];
                if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])|| (imagefile==match[3])))
                { 
                       document.getElementById('gambar').value=null;
                        $.messager.alert('Peringatan','Format File tidak dapat di upload','error');
                        return false  
                }
                 
           });  
       //----------------------------------------------------------------------------------------------         
        //validasi file_skema yang di pilih 
        $('#file_skema').change(function(){
               var file = this.files[0];
                var imagefile = file.type; 
		 var imageSize = file.size; 
                 if (ukuranfilemelebihibatas(imageSize)){ 
                    document.getElementById('file_skema').value=null;
                     return false 
                } 
                 //-------------------------------------------- 
              
                var match= ["application/pdf"];
                if(!((imagefile==match[0])))
                { 
                       document.getElementById('file_skema').value=null;
                        $.messager.alert('Peringatan','Format File tidak dapat di upload','error');
                        return false  
                }
                 
           });  
       //----------------------------------------------------------------------------------------------  
        $('#file_input').change(function(){
                for (var i = 0; i < $("#file_input").get(0).files.length; ++i) {
                           // var file1=$("#file_input").get(0).files[i].name;
                                
                           var file = this.files[i];
                            var imagefile = file.type; 
                            var imageSize = file.size; 
                             if (ukuranfilemelebihibatas(imageSize)){ 
                                document.getElementById('file_input').value=null;
                                 return false 
                            } 
                             //-------------------------------------------- 
                            var match= ["image/jpeg","image/png","image/jpg","image/bmp"];
                            if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])|| (imagefile==match[3])))
                            { 
                                   document.getElementById('file_input').value=null;
                                    $.messager.alert('Peringatan','Format File ke '+ (i+1) +'  ('+ imagefile +') tidak dapat di upload','error');
                                    return false  
                            }
                } 
           });  
       //----------------------------------------------------------------------------------------------  
      
       
        $('#bttambahdata').click(function(event) {  
                var judul=document.getElementById('judul').value;    
                var resume=document.getElementById('resume').value;  
               
                if ((judul=="")||(resume=="") ){
                    $.messager.alert('Peringatan','Data Judul / Resume ada yang belum di isikan','error'); 
                    return  false
                }   
            
                if (JenisInputBelumDiPilih()){return  false}
               
                var form = $('#frdatainformasi');  
                $(form).submit();  
                  
        });   
        
        $('#frdatainformasi').submit(function(event) {  
            event.preventDefault();    
            $.ajax({
                    type: "POST",                     
                    url: '../modul/datainformasi_templete.php?aksi=simpan',
                    data: new FormData(this),  
                    contentType: false,
                    cache: false,             
                    processData:false, 
                    dataType:'json',
                     success: function(data) {
                                    if (data.hasil=="ok"){  
                                            $.messager.alert('Informasi','Data berhasil disimpan');  
                                            clearform();
                                             
                                      }else{
                                            $.messager.alert('Peringatan',data.isierror,'error');
                                      }
                               }
                    });  
            });   
      //======================================================================================== 
   
         $('#fkelola').submit(function(event) {  
             
             var tumb=document.getElementById("lamp_thumb").checked; 
              var peta=document.getElementById("lamp_peta").checked; 
             var polatanam=document.getElementById("lamp_polatanam").checked; 
              var skema=document.getElementById("lamp_skema").checked; 
              var gambar=document.getElementById("lamp_gambar").checked; 
              var debit=document.getElementById("lamp_debitintake").checked; 
              
              if ((tumb==0)&&(peta==0)&&(polatanam==0)&&(skema==0)&&(gambar==0)&&(debit==0))
              {
                  $.messager.alert('Peringatan','Data belum dipilih','error');
             return false;}
             
             
             
             
             
            event.preventDefault();    
            $.ajax({
                    type: "POST",                     
                    url: '../modul/datainformasi_templete.php?aksi=simpan_kelola',
                    data: new FormData(this),  
                    contentType: false,
                    cache: false,             
                    processData:false, 
                    dataType:'json',
                     success: function(data) {
                                    if (data.hasil=="ok"){  
                                            $.messager.alert('Informasi','Data lampiran berhasil di hapus');  
                                           tutup_kelola();
                                             
                                      }else{
                                            $.messager.alert('Peringatan',data.isierror,'error');
                                      }
                               }
                    });  
            });   
         //========================================================================================    

          
 });
 
function simpan_kelola_lamp(){
     var form = $('#fkelola');  
     $(form).submit();  
} 
function buka_kelola() {   
        $('#FrKelola').dialog('open');  
        document.getElementById('bttambahdata').style.visibility = 'hidden';
        document.getElementById('btkelola').style.visibility = 'hidden';
              
} 
function tutup_kelola() {   
        $('#FrKelola').dialog('close'); 
        document.getElementById('bttambahdata').style.visibility = 'visible';
        document.getElementById('btkelola').style.visibility = 'visible';
} 
function JenisInputBelumDiPilih(){
      var ele = document.getElementsByName('embung_di'); 
      var i = ele.length;
      var hasil=1;
        for (var j = 0; j < i; j++) {
                if (ele[j].checked) {hasil=0;} 
        }
           if (hasil==1){$.messager.alert('Peringatan','Tipe Input DI / Embung belum di pilih','error');}
       return hasil;                         
                                
  
}


function BukaTempleteInput(){
        var ele = document.getElementsByName('embung_di');
        var i = ele.length;
        for (var j = 0; j < i; j++) {


        if (j==1){
                        if(ele[j].checked){
                                $('#p_embung').panel('open');
                                          $('#p_di').panel('close');
                        }else{
                                           $('#p_embung').panel('close');
                                          $('#p_di').panel('open');
                        }

        }

                                // if (ele[j].checked) { //index has to be j.
                                        // alert('radio '+j+' checked');
                                // }
                                // else {
                                        // alert('radio '+j+' unchecked');
                                // }
                }
}
function bukalistpeta() {   
            $('#inputitem').form('clear');
            $('#FormInput').dialog('open'); 
}
          
function btn_gunakan_peta(){
     
             	var ids = [];
                var Npet = [];
			var rows = $('#dg').datagrid('getSelections');
			for(var i=0; i<rows.length; i++){
				ids.push(rows[i].id); 
                                Npet.push(rows[i].data);
			} 
     //=================================================================================                  
            var kdslideberanda = ids.join(); 
           var itempeta = Npet.join(); 
            
           // var itempeta = Npet.join().replace(/\r?\n/g, '<br>');
            
            
            if (kdslideberanda!=''){ 
                         document.getElementById('link_peta').value=kdslideberanda;
                         var isi =itempeta.replace(/<br>/g ,'\n');  
                         document.getElementById('nama_link_peta').value= isi;  
                          
                         $('#FormInput').dialog('close'); 
            }else{alert('data belum ada yang di pilih');}
    }        
 
  
 
   
 function clearform(){
     var KdKate=document.getElementById('kdkategory').value;
     var kodeuser=document.getElementById('kodeuser').value;
      document.getElementById('judul').value="";
      document.getElementById('resume').value="";    
      $('#frdatainformasi').form('clear');
      //kembalikan nilai setelah di clear
      document.getElementById('kdkategory').value=KdKate;
      document.getElementById('kodeuser').value=kodeuser;
       
 } 
 
 function ukuranfilemelebihibatas(ukuran){ 
        var hasil=0;
                 if ((ukuran>100000000)){
                     $.messager.alert('Peringatan','Ukuran File maxsimal adalah 100Mb','error'); 
                     hasil=1;
                 }
              return hasil; 
 }