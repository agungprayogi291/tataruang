   var fields = [];
   
   var LyrPola; 
   var Lyrkoof; 
   var Lyrketing; 
   var LyrGabungan; 
   
   
   
   
 $(document).ready(function() {  
    LoadMapGabungan(); 
 });
function ClickInfo(){
     var checkBox = document.getElementById("xxx"); 
    //alert(checkBox);
    if (checkBox.checked == true){  
           map.addLayer(LyrGabungan);
           LyrGabungan.bringToFront(); 
    }else{
        map.removeLayer(LyrGabungan);   
    }
}
function ClickKoofbangunan(){  
         var checkBox = document.getElementById("koef");  
          fields = ["ket_kdb"];
       var   Tabel="koefisien_dasar_bangunan"; 
      var Jenis="koof";
     if (checkBox.checked == true){  
	$.ajax({
                type:"POST",
                url :"modul/tata_ruang_modul.php", 
		data: {			
			table: Tabel, 
			fields: fields,
			kondisi: ''
		},
		success: function(data){
			mapData(data,Jenis); 
                        $("#p_kooef").show(); 
                       
		}
	})
    }else{ 
           map.removeLayer(Lyrkoof); 
           $("#p_kooef").hide(); 
    }
};  
function ClickKetinggianBangunan(){ 
      
         var checkBox = document.getElementById("tinggi");  
          fields = ["ket_kmb"];
          var  Tabel="ketinggian_lantai_bangunan";
            var Jenis='tinggi'; 
     if (checkBox.checked == true){ 
	$.ajax({
                type:"POST",
                url :"modul/tata_ruang_modul.php", 
		data: {			
			table: Tabel, 
			fields: fields,
			kondisi: ''
		},
		success: function(data){
			mapData(data,Jenis);
                         $("#p_tinggi").show();  
		}
	})
    }else{ 
           map.removeLayer(Lyrketing); 
           $("#p_tinggi").hide(); 
    }
};  

// if(map.hasLayer(DistrictLayer)){ //the old DistrictLayer
     
     
 
function ClickPolaruang(){  
    var checkBox = document.getElementById("pola");  
     if (checkBox.checked == true){ 
        
             
       fields = [ "kawasan"];
	$.ajax({
                type:"POST",
                url :"modul/tata_ruang_modul.php",
		data: {			
			table: "pola_ruang",
			fields: fields,
			kondisi: ''
		},
		success: function(data){
			mapData(data,'pola');
                        $("#p_pola").show();
                        
		}
	})
    }else{map.removeLayer(LyrPola);  $("#p_pola").hide(); }
    
 
        
}; 



function mapData(data,Jenis ){ 
	 var geojson = { 
		"type": "FeatureCollection",
		"features": []
	}; 
        // alert(data);
	var dataArray = data.split(", ;");
	dataArray.pop();
     
	dataArray.forEach(function(d){
		d = d.split(", "); //split the data up into individual attribute values and the geometry
 
                if (d[fields.length]!="0"){
                    var feature = {
                            "type": "Feature",
                            "properties": {}, //properties object container
                            "geometry": JSON.parse(d[fields.length]) //parse geometry
                    };

                    for (var i=0; i<fields.length; i++){ 
                            feature.properties[fields[i]] = d[i];
                    };

                    //add feature names to autocomplete list
//                    if ($.inArray(feature.properties.featname, autocomplete) == -1){
//                            autocomplete.push(feature.properties.featname);
//                    }; 
                    geojson.features.push(feature);
            }
	}); 
     

	 var mapDataLayer = L.geoJson([geojson], { 
            style: function (feature) {  
                        var warna= styleCOlor(feature.properties.kawasan);
                        if (Jenis=="koof" ){ warna=  styleCOlor(feature.properties.ket_kdb);}
                        if (Jenis=="tinggi" ){ warna=  styleCOlor(feature.properties.ket_kmb);} 
                        return  warna;
		},
 
		onEachFeature: function (feature, layer) {
			var html = "";
			for (prop in feature.properties){
				html += prop+": "+feature.properties[prop]+"<br>";
			};
                        layer.on('click', function(e){  
                           //BukaDataPola(feature.properties.gid,e); 
                        });
	        layer.bindPopup(html);
                }
                
                
//                 onEachFeature: function(feature,layer){
//                    layer.on('click', function(e){  
//                          BukaDataPola(feature.properties.gid); 
//                    });
//                } 
	}).addTo(map);
        if (Jenis='pola'){ LyrPola=mapDataLayer;}
        if (Jenis='koof'){ Lyrkoof=mapDataLayer;  }
        if (Jenis='tinggi'){ Lyrketing=mapDataLayer;  } 
};
function LoadMapGabungan(){
        fields = [ "gid"];
	$.ajax("modul/tata_ruang_modul.php", {
		data: {			
			table: "pola_ruang",
			fields: fields,
			kondisi: ''
		},
		success: function(data){
			mapGabungan(data);
                       
		}
	})
}
function mapGabungan(data){ 
	 var geojson = { 
		"type": "FeatureCollection",
		"features": []
	}; 
        // alert(data);
	var dataArray = data.split(", ;");
	dataArray.pop();
     
	dataArray.forEach(function(d){
		d = d.split(", "); //split the data up into individual attribute values and the geometry
 
                if (d[fields.length]!="0"){
                    var feature = {
                            "type": "Feature",
                            "properties": {}, //properties object container
                            "geometry": JSON.parse(d[fields.length]) //parse geometry
                    };

                    for (var i=0; i<fields.length; i++){ 
                            feature.properties[fields[i]] = d[i];
                    }; 
                    geojson.features.push(feature);
            }
	}); 
     

	  LyrGabungan = L.geoJson([geojson], { 
            style: function () { 
                         var warna ={
                                    weight: 0,
                                    opacity: 0,
                                    //color: 'white',
                                    //color: 'red',
                                    dashArray: 0,
                                    fillOpacity: 0,
                                    // fillColor: getColor(feature.properties.density)
                                    //fillColor: getColor(feature)
                                    //fillColor: 'red'
                                    }; 
                           return  warna;
		},
 
                    onEachFeature: function(feature,layer){
                    layer.on('click', function(e){  
                          BukaDataPola(feature.properties.gid,e); 
                    });
                } 
	//});
	//}).addTo(map);
	});
        
    
};
function BukaDataPola(Kode,e){ 
     var coord = e.latlng;
            var lat = coord.lat;
            var lng = coord.lng;
              TidakLoading=true;
      $.ajax({                    
                    type: "POST", 
                    url: 'modul/rtrw_modul.php',
                    data:  {kode:Kode},                       
                    //contentType: false,
                    //cache: false,             
                    //processData:false,  
                    dataType:'json', 
                     success: function(data) {  
                           $("#op_str_coordinate").html('LangLat('+lng+':'+lat+')'); 
                            $("#op_str_kecamatan").html(data[0].kecamatan);
                            $("#op_str_kelurahan").html(data[0].kelurahan);
                            $("#op_str_kdblokzonasi").html(data[0].kode_blok);
                            $("#op_str_polaruang").html(data[0].pola_ruang);
                            $("#op_str_kawasan").html(data[0].kawasan);
                            $("#op_str_keteranganpr").html(data[0].kecamatan);
                            $("#op_str_kdb").html(data[0].ket_kdb);
                            $("#op_str_kmb").html(data[0].lantai);
                          $("#myModal").modal({backdrop: false});
                    }
                }); 
    
       $("#myModal").modal('show');
        TidakLoading=false;
     //alert('xxxx'+Kode);
}


  
