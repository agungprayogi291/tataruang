/*	PostGIS and Leaflet demo for Cart Lab Education Series, April 17, 2015	**
**	By Carl Sack, CC-BY-3.0													*/

 var LyrKec,LyrKel,LyrKab;
var map,autocomplete = [];
var newMarker;

$(document).ready(initialize);

function initialize( ){
        var tinggi=$(window).height()-110;
	//$("#map").height($(window).height());
	$("#map").height(tinggi); 
        map = L.map('map').setView([-7.4764243720274335,110.21827958618167],17);
       // map = L.map('map').setView([-7.4764243720274335,110.21827958618167],14);
       
      
	 
googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
}); 

googleSat = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
}).addTo(map); 

googleTerrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});
	
googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});  

openStreets = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });   
        
 citramaps = L.tileLayer.wms("http://192.168.1.20:8080/geoserver/citra_satelit/wms", {
        layers : 'citra_satelit:citra_satelit-Citra Magelang 2016',
        format: 'image/png',
        transparent: true,
    });
    //}).addTo(map); 
    
   
};


function clickbasemaps(){
     var myRadio = $("input[name=basemaps]");
     var nilai = myRadio.filter(":checked").val(); 
   
   map.removeLayer(googleHybrid);
   map.removeLayer(googleSat);
   map.removeLayer(googleTerrain);
   map.removeLayer(googleStreets);
   map.removeLayer(openStreets);
   map.removeLayer(citramaps);
 
   
if (nilai==0){citramaps.addTo(map);    }
if (nilai==1){googleHybrid.addTo(map);    }
if (nilai==2){googleSat.addTo(map);    }
if (nilai==3){googleTerrain.addTo(map); }   
if (nilai==4){googleStreets.addTo(map);  } 
if (nilai==5){openStreets.addTo(map); }

}


function clickadministrasi(valu){ 
    
    var Proses=false; 
    var checkBox; 
    if (valu==1){
         checkBox = document.getElementById("adm");
         tabel='batas_wilayah_administratif_kabupaten_kota';
         fields = [ "nama_kab","gid"];
    }
    if (valu==2){
        checkBox = document.getElementById("kec");
         tabel='batas_wilayah_administratif_kecamatan';
         fields = [ "nama_kec","gid"];
    }
    if (valu==3){
        checkBox = document.getElementById("kel");
         tabel='batas_wilayah_administratif_desa_kelurahan';
         fields = [ "nama_kel","gid"];
    }     
    if (checkBox.checked == true){ Proses=true; } 

     
      if (Proses)  {
	$.ajax({
        type:"POST",
        url :"modul/tata_ruang_modul.php", 
		data: {			
			table:  tabel,
			fields: fields,
			kondisi: ''
		},
		success: function(data){
			mapDataAdm(data,valu);
		}
	})
    }else{
        
       
           if (valu==1){map.removeLayer(LyrKec);  }
            if (valu==2){ map.removeLayer(LyrKel); }
            if (valu==3){map.removeLayer(LyrKab);}     
    }
}; 
function mapDataAdm(data,nilai){ 
	 var geojson = { 
		"type": "FeatureCollection",
		"features": []
	}; 
        
	var dataArray = data.split(", ;");
	dataArray.pop();
     
	dataArray.forEach(function(d){
		d = d.split(", "); //split the data up into individual attribute values and the geometry

		//feature object container
                //alert(d[fields.length]);
                if (d[fields.length]!="0"){
                    var feature = {
                            "type": "Feature",
                            "properties": {}, //properties object container
                            "geometry": JSON.parse(d[fields.length]) //parse geometry
                    };

                    for (var i=0; i<fields.length; i++){ 
                            feature.properties[fields[i]] = d[i];
                    };

                     
                    geojson.features.push(feature);
            }
	});
	
        
       
    
     

	var mapDataLayer = L.geoJson([geojson], {
	// L.geoJson([geojson], {
            style: function (feature) {
			//return feature.properties && feature.properties.style;
                        //alert(feature.properties.pola_ruang)
                        return  styleCOlor(feature.properties.gid);
		},
                
		onEachFeature: function (feature, layer) {
			var html = "";
			for (prop in feature.properties){
				html += prop+": "+feature.properties[prop]+"<br>";
			};
	        layer.bindPopup(html);
	    }
	}).addTo(map);
   
         
            if (nilai=='1'){LyrKec=mapDataLayer;  }
            if (nilai=='2'){LyrKel=mapDataLayer;  }
            if (nilai=='3'){LyrKab=mapDataLayer;  }
            
            
        
        
};

 function getColor(str) {
   // alert(d); 
//    console.log(str);    
   if (!str){ return false;  }
   var d =  str.toLowerCase();
                var hasil='#FC4E2A';
                if (d=="kawasan pariwisata" || d=="1"){hasil='#800026'}
                if (d=="kawasan kesehatan"  || d=="2"){hasil='#7151e8'}
                if (d=="kawasan pendidikan"  || d=="3"){hasil='#E31A1C'}
                if (d=="iplt"  || d=="4"){hasil='#b7713e'}
                if (d=="kawasan militer"  || d=="5"){hasil='#FD8D3C'}
                if (d=="kawasan permukiman"  || d=="6"){hasil='#70f169'}
                if (d=="ruang terbuka hijau"  || d=="7"){hasil='#b4c339'}
                if (d=="kawasan pemakaman"  || d=="8"){hasil='#FED976'}
                if (d=="kawasan evakuasi bencana"  || d=="9"){hasil='#5d6ce0'}
                if (d=="jalan"  || d=="10"){hasil='#ca33bd'}
                if (d=="sungai" || d=="12"){hasil='#2a0c9e'}
                if (d=="kawasan pertanian" || d=="13"){hasil='#fbe809'}
                if (d=="kawasan industri (pendukung perdagangan / jasa)"  || d=="14"){hasil='#9c9e24'}
                if (d=="kawasan perdagangan / Jasa"  || d=="15"){hasil='#3c8cd2'}
                if (d=="kawasan Olahraga"  || d=="16"){hasil='#8145dc'}
                if (d=="kawasan perkantoran" || d=="17"){hasil='#4ae6e0'}
                if (d=="kawasan peribadatan"  || d=="18"){hasil='#FEB24C'}
                if (d=="kawasan perlindungan terhadap kawasan Bawahannya"  || d=="19"){hasil='#FEB24C'}
                if (d=="kawasan terminal"  || d=="20"){hasil='#FC4E2A'}
                
                //costume
                if (d=="merah"){hasil='#f50733'}
                if (d=="hijau"){hasil='#15f363'}
                if (d=="kuning"){hasil='#fbf306'}
                if (d=="biru"){hasil='#0c33f7'}
                if (d=="hitam"){hasil='#141415'}
                
                
               //Koefisien Dasar Bangunan 
                if (d=="sungai"){hasil='#000068'}
                if (d=="jalan"){hasil='#000000'}                
                if (d=="kawasan kewenangan pemerintah"){hasil='#ffff00'}
                if (d=="kdb 0%"){hasil='#18f727'}
                if (d=="kdb 40%"){hasil='#00e3ee'}
                if (d=="kdb 60%"){hasil='#ff7600'}
                if (d=="kdb 80%"){hasil='#ff0700'} 
                if (d=="-"||d==" "){hasil='#ec1ee4'}
             
//Ketinggian Maksimum Bangunan            
 
//if (d=="kawasan kewenangan pemerintah"){hasil='#a00e51'} 
if (d=="kawasan maksimal 0 lantai"){hasil='#f1bc1d'}
if (d=="kawasan maksimal 3 lantai"){hasil='#c50500'}
if (d=="kawasan maksimal 4 lantai"){hasil='#ff3e39'}
if (d=="kawasan maksimal 5 lantai"){hasil='#36e033'}
if (d=="kawasan maksimal 8 lantai"){hasil='#017277'}
if (d=="kawasan maksimal 10 lantai"){hasil='#ff9439'}
if (d=="kawasan pemakaman"){hasil='#000000'}
if (d=="-" || d==" "){hasil='#291ec5'}

 
 



                 
return  hasil; 
	}	
function styleCOlor(feature) {
        return {
                weight: 2,
                opacity: 1,
                color: 'white',
                dashArray: '3',
                fillOpacity: 0.7,
                //fillColor: getColor(feature.properties.density)
                fillColor: getColor(feature)
        };
    }
function styleColorLine(feature) {
        return {
                weight: 6,
                opacity: 1,
                color: getColor(feature),
                dashArray: '3',
                fillOpacity: 0.7
                //fillColor: getColor(feature.properties.density)
                //fillColor: getColor(feature)
        };
}