$( document ).ready(function() {
    //console.log( "ready!" );    
    $('#idkat').datalist({ 
            onClickRow: function(index,field){ 
                var dlist = $(this).datalist('getSelections'); 
                var kode = dlist[0].value;  
                buat_tabel(kode);                
	}
    }); 
 
    $("#bttambahdata" ).click(function() {
        document.getElementById("kdmeta").value = ""
        $('#form').form('clear');  
       $('#dlgmeta').dialog('open')

    });
     $("#bthapusdata" ).click(function() {
       var kode = document.getElementById("kdmetaedit").value   
       if (kode==""){
            $.messager.alert('Peringatan','Data belum di pilih','error');   
            return false; 
       }
             
       hapusdata(kode);

    });
    
     $("#bteditdata" ).click(function(event) { 
      var kode = document.getElementById("kdmetaedit").value   
       if (kode==""){
            $.messager.alert('Peringatan','Data belum di pilih','error');   
            return false; 
       }
       loaddataedit(kode);
    });
    
//    $("#btsimpanmeta" ).click(function(event) {        
//         var form = $('#frmmeta');
//         $(form).submit();   
//    });
     
  
            
     $('#frmmeta').submit(function(event) { 
            event.preventDefault();   
            $.ajax({
                    type: "POST", 
                   url: '../modul/metadata.php?aksi=simpan',
                    data: new FormData(this),  
                    contentType: false,
                    cache: false,             
                    processData:false, 
                    dataType:'json',
                     success: function(data) {
                                    if (data.hasil=="ok"){
                                            $('#form').form('clear'); 
                                            $.messager.alert('Informasi','Data berhasil disimpan');  
                                      }else{
                                            $.messager.alert('Peringatan',data.isierror,'error');
                                      }
                               }
                    });  
            });   
            
    
    
    
    
//      var form = $('form');  
//      form.on('submit', function (c) {
//                   c.preventDefault();
//                   var data = new FormData(($('#generate_params')[0])); 
//                   $.each($(':input', form ), function(i, fileds){
//                       data.append($(fileds).attr('name'), $(fileds).val());
//                   });
////                   $.each($('input[type=file]',form )[0].files, function (i, file) {
////                       data.append(file.name, file);
////                   });
//                   //simpandatametadata(data);
//                   
//                   
//                   var form = $('form')[0];
//                   var formData = new FormData(form);
//                    var inputLogo = $("#filepeta")[0];
//                    data.append(inputLogo.name, inputLogo.files[0]);
//
//                   
//                   $.ajax({ 
//        url: '../modul/metadata.php?aksi=simpan',
//        type: 'POST',
//        data: data,
//        cache: false,
//        dataType: 'json',
//        processData: false, // Don't process the files
//        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
//        success: function(data)
//        {
//            if (data.hasil=="ok"){
//                 $('#form').form('clear'); 
//                $.messager.alert('Informasi','Data berhasil disimpan');  
//           }else{
//                $.messager.alert('Peringatan',data.isierror,'error');
//           }
//        }
//    });
//    
//    
//     });
                   
});
 
function simpandatametadata(data){ 
 $.ajax({ 
        url: '../modul/metadata.php?aksi=simpan',
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data)
        {
            if (data.hasil=="ok"){
                 $('#form').form('clear'); 
                $.messager.alert('Informasi','Data berhasil disimpan');  
           }else{
                $.messager.alert('Peringatan',data.isierror,'error');
           }
        }
    });
        
        
      
		   
}
function hapusdata(kode){  
        $.messager.confirm('Confirm','Apakah data akan di Hapus?',function(r){
                 $.ajax({
                    type: "POST", 
                     url: '../modul/metadata.php?aksi=hapus', 
                     data: "kd_meta="+kode,  
                     dataType:'json',  
                     success: function(data) { 
                             if (data.hasil=="ok"){ 
                                  $.messager.alert('Informasi','Data berhasil dihapus');  
                                 location.reload(false);
                                  
                             } else{
                                  $.messager.alert('Peringatan',data.isierror,'error');   
                             }
                     }
                 });
        });         
        
}

function loaddataedit(kode){    
                 $.ajax({
                    type: "POST", 
                     url: '../modul/metadata.php?aksi=buka', 
                     data: "kd_meta="+kode,  
                     dataType:'json',  
                     success: function(data) { 
                           $('#dlgmeta').dialog('open')  
                           
                          parserdataedit(data);  
                     }
                 });
}


function buat_tabel(kode){    
                 $.ajax({
                    type: "POST", 
                     url: '../modul/metadata.php?aksi=buka', 
                     data: "kd_meta="+kode,  
                     dataType:'json',  
                     success: function(data) { 
                    
                  document.getElementById("kdmetaedit").value = data[0].kd_metadata ; 
                  
                  var TabelJadi="<table class='metadata' id='tes'>";               
		  TabelJadi+="<tr><td width='400'><b>Nama Layer</b></td> <td>"+ filterkar(data[0].namalayer) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>1. Informasi Identifikasi</td> <td>"+ filterkar(data[0].Informasi_identifikasi) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>1.1. Informasi Sitasi</td> <td>"+ filterkar(data[0].informasi_sitasi) +"</td></tr>";
		  TabelJadi+="<tr><td>- Informasi Identifikasi</td> <td>"+ filterkar(data[0].informasi__identifikasi_1) +"</td></tr>";
		  TabelJadi+="<tr><td>- Tanggal Penerbitan</td> <td>"+ filterkar(data[0].tgl_penerbitan)  +"</td></tr>";
		  TabelJadi+="<tr><td>- Judul</td> <td>"+ filterkar(data[0].judul) +"</td></tr>";
		  TabelJadi+="<tr><td>- Edisi</td> <td>"+ filterkar(data[0].edisi) +"</td></tr>";
                  var Fpeta=data[0].file_peta;
                  if (empty(Fpeta)){Fpeta="";} else{Fpeta="<img src='../images/metadata/"+ data[0].file_peta+"' width='500'/>"}   
                  TabelJadi+="<tr><td>- Bentuk Tampilan Data Geospasial</td> <td>"+ Fpeta +"</td></tr>";
		  TabelJadi+="<tr><td>- Tempat Penerbitan</td> <td>"+ filterkar(data[0].tempat_penerbitan) +"</td></tr>";
		  TabelJadi+="<tr><td>- Penerbit</td> <td> "+ filterkar(data[0].penerbit) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>1.2. Deskripsi</td> <td>"+ filterkar(data[0].deskripsi) +"</td></tr>";
		  TabelJadi+="<tr><td>- Abstrak</td> <td> <p>    "+ filterkar(data[0].abstrak) +"</td></tr>";
		  TabelJadi+="<tr><td>- Tujuan</td> <td> <p>"+ filterkar(data[0].tujuan) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>1.3. Periode Waktu Pengambilan Data</td> <td>"+ filterkar(data[0].periode_waktu) +"</td></tr>";
		  TabelJadi+="<tr><td>- Infomasi Umum</td> <td>"+ filterkar(data[0].informasi_umum) +"</td></tr>";
		  TabelJadi+="<tr><td>- Acuan Baru</td> <td>"+ filterkar(data[0].acuan_baru) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>1.4. Status</td> <td>"+ filterkar(data[0].statuse) +"</td></tr>";
		  TabelJadi+="<tr><td>- Kemajuan Data</td> <td>"+ filterkar(data[0].kemajuan_data) +"</td></tr>";
		  TabelJadi+="<tr><td>- Frekuensi Perubahan dan Penambahan Data</td> <td> "+ filterkar(data[0].frekuensi_perubahan) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>1.5. Letak Geografis</td> <td>"+ filterkar(data[0].letak_geografis) +"</td></tr>";
		  TabelJadi+="<tr><td>- Koordinast Batas Barat</td> <td> "+ filterkar(data[0].k_barat) +"</td></tr>";
		  TabelJadi+="<tr><td>- Koordinast Batas Timur</td> <td> "+ filterkar(data[0].k_timur) +"</td></tr>";
		  TabelJadi+="<tr><td>- Koordinast Batas Utara</td> <td> "+ filterkar(data[0].k_utara) +"</td></tr>";
		  TabelJadi+="<tr><td>- Koordinast Batas Selatan</td> <td> "+ filterkar(data[0].k_selatan) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>1.6. Kata Kunci</td> <td>"+ filterkar(data[0].kata_kunci) +"</td></tr>";
		  TabelJadi+="<tr><td>- Thesaurus</td> <td> "+ filterkar(data[0].thesaurus) +"</td></tr>";
		  TabelJadi+="<tr><td>- Kata Kunci Tematis</td> <td>"+ filterkar(data[0].kunci_tematis) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>1.7. Batas Akses Data</td> <td>"+ filterkar(data[0].batas_akses) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>1.8. Batas Penggunaan Data</td> <td>"+ filterkar(data[0].batas_pengguna) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>1.9. Gambaran Data</td> <td>"+ filterkar(data[0].gambaran) +"</td></tr>";
		  TabelJadi+="<tr><td>- Nama File</td> <td> "+ filterkar(data[0].nama_file) +"</td></tr>";
		  TabelJadi+="<tr><td>- Deskripsi File</td> <td> "+ filterkar(data[0].deskripsi_file) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>2. Informasi Distribusi</td> <td>"+ filterkar(data[0].informasi_distribusi) +"</td></tr>";
		  TabelJadi+="<tr><td bgcolor='#CCC'>2.1. Distribusi</td> <td>"+ filterkar(data[0].distribusi) +"</td></tr>";
		  TabelJadi+="<tr><td>- Kontak Organisasi</td> <td>"+ filterkar(data[0].kontak_organisasi) +"</td></tr>";
		  TabelJadi+="<tr><td>- Kontak Personal</td> <td> "+ filterkar(data[0].kontak_person) +"</td></tr>";
		  TabelJadi+="<tr><td>- Kontak Jabatan</td> <td> "+ filterkar(data[0].kontak_jabatan) +"</td></tr>";
		  TabelJadi+="<tr><td>- Alamat</td> <td> "+ filterkar(data[0].alamat) +"</td></tr>";
		  TabelJadi+="<tr><td>- Kota</td> <td> "+ filterkar(data[0].kota) +"</td></tr>";
		  TabelJadi+="<tr><td>- Provinsi</td> <td> "+ filterkar(data[0].provinsi) +"</td></tr>";
		  TabelJadi+="<tr><td>- Kode Pos</td> <td> "+ filterkar(data[0].kode_pos) +"</td></tr>";
		  TabelJadi+="<tr><td>- Negara</td> <td> "+ filterkar(data[0].negara) +"</td></tr>";
		  TabelJadi+="<tr><td>- Telepon</td> <td> "+ filterkar(data[0].telp) +"</td></tr>";
		  TabelJadi+="<tr><td>- Faks</td> <td> "+ filterkar(data[0].faks) +"</td></tr>";
		   TabelJadi+="<tr><td>- Email</td> <td> "+ filterkar(data[0].email) +"</td></tr>";
		  TabelJadi+="<tr><td>- Waktu Pelayanan</td> <td> "+ filterkar(data[0].waktu_pelayanan) +"</td></tr>";
		   TabelJadi+="<tr><td>- Instruksi Kontak</td> <td> "+ filterkar(data[0].instruksi_kontak) +"</td></tr>";
		   TabelJadi+="<tr><td bgcolor='#CCC'>2.2. Deskripsi Sumber</td> <td>  "+ filterkar(data[0].deskripsi_sumber) +"</td></tr>";
		   TabelJadi+="<tr><td bgcolor='#CCC'>2.3. Pertanggungjawaban Distribusi</td> <td>  "+ filterkar(data[0].pertanggungjawaban) +"</td></tr>";
		   TabelJadi+="<tr><td bgcolor='#CCC'>2.4. Standar Proses Pemesanan</td> <td>"+ filterkar(data[0].standar_proses) +"</td></tr>";
		   TabelJadi+="<tr><td>- Biaya</td> <td> "+ filterkar(data[0].biaya) +"</td></tr>";
		   TabelJadi+="<tr><td>- Tata Cara Pemesanan</td> <td> <p>"+ filterkar(data[0].tata_cara_pemesanan) +"</p>";
                    TabelJadi+="</td></tr>";
//<tr><td>File XML Metadata</td> <td><a href='assets/metadata/metadata_BENDUNGAN.xml' target='_blank'><i class='icon-download'></i>Download</a></td></tr>-->
                   TabelJadi+="</table>";
                    $('#metadataX').html(TabelJadi);  
                    
                    
                     
                }
            });  
}

function empty(e) {
  switch (e) {
    case "":
    case 0:
    case "0":
    case null:
    case false:
    case typeof this == "undefined":
      return true;
    default:
      return false;
  }
}

function filterkar(kar){
    var hasil;
    if (empty(kar)){
        hasil=""; 
    }else{hasil=kar;}
    return hasil;
}


function parserdataedit(data){
        $('#form').form('clear');   
     
       
      
        document.getElementById('kdmeta').value=data[0].kd_metadata; 
         document.getElementById("layer").value  =data[0].namalayer;
         document.getElementById("kategory").value  =data[0].kategory; 
         document.getElementById("identifikasi").value  =data[0].Informasi_identifikasi; 
         document.getElementById("situasi").value  =data[0].informasi_sitasi; 
         document.getElementById("identifikasi1").value  =data[0].informasi__identifikasi_1; 
         document.getElementById("penerbitan").value  =data[0].tgl_penerbitan; 
         document.getElementById("judul").value  =data[0].judul; 
         
        document.getElementById("edisi").value  =data[0].edisi; 
        // document.getElementById("file_peta").value  =data[0].file_peta; 
          document.getElementById("tempat").value  =data[0].tempat_penerbitan; 
           document.getElementById("penerbit").value  =data[0].penerbit; 
            document.getElementById("deskripsi").value  =data[0].deskripsi; 
             document.getElementById("abstarak").value  =data[0].abstrak; 
              document.getElementById("tujuan").value  =data[0].tujuan; 
               document.getElementById("periode").value  =data[0].periode_waktu; 
                document.getElementById("informasi_umum").value  =data[0].informasi_umum; 
                 document.getElementById("acuan").value  =data[0].acuan_baru; 
                 
                 
                 document.getElementById("statuse").value  =data[0].statuse; 
                 document.getElementById("kemajuan").value  =data[0].kemajuan_data; 
                 document.getElementById("frekuensi").value  =data[0].frekuensi_perubahan; 
                 document.getElementById("letak_geografis").value  =data[0].letak_geografis; 
                 document.getElementById("k_barat").value  =data[0].k_barat; 
                 document.getElementById("k_timur").value  =data[0].k_timur; 
                 document.getElementById("k_utara").value  =data[0].k_utara; 
                 document.getElementById("k_selatan").value  =data[0].k_selatan; 
                 document.getElementById("kata_kunci").value  =data[0].kata_kunci; 
                 document.getElementById("thesaurus").value  =data[0].thesaurus; 
                 
                 document.getElementById("tematis").value  =data[0].kunci_tematis; 
                 document.getElementById("batas_data").value  =data[0].batas_akses; 
                 document.getElementById("batas_pengguna").value=data[0].batas_pengguna;  
                 document.getElementById("gambaran_data").value=data[0].gambaran;  
                 
                 document.getElementById("namafile").value  =data[0].nama_file; 
                  
                 document.getElementById("deskripsi_file").value  =data[0].deskripsi_file; 
                 document.getElementById("informasidistribusi").value  =data[0].informasi_distribusi; 
                 
                 //gambaran
                 
                 document.getElementById("distribusi").value  =data[0].distribusi; 
                 document.getElementById("kontak_organisasi").value  =data[0].kontak_organisasi; 
                 document.getElementById("kontak_person").value  =data[0].kontak_person; 
                 document.getElementById("kontak_jabatan").value  =data[0].kontak_jabatan; 
                 document.getElementById("alamat").value  =data[0].alamat; 
                 document.getElementById("kota").value  =data[0].kota; 
                 document.getElementById("propinsi").value  =data[0].provinsi; 
                 document.getElementById("kode_pos").value  =data[0].kode_pos; 
                 document.getElementById("negara").value  =data[0].negara; 
 
                document.getElementById("telp").value  =data[0].telp; 
                document.getElementById("faks").value  =data[0].faks; 
                document.getElementById("email").value  =data[0].email; 
                document.getElementById("waktu_pelayanan").value  =data[0].waktu_pelayanan; 
                document.getElementById("kontak").value  =data[0].instruksi_kontak; 
                
                document.getElementById("deskripsi_sumber").value  =data[0].deskripsi_sumber; 
                document.getElementById("pertanggungjawaban").value  =data[0].pertanggungjawaban; 
                document.getElementById("standart_proses").value  =data[0].standar_proses; 
                document.getElementById("biaya").value  =data[0].biaya; 
                document.getElementById("cara_pemesanan").value  =data[0].tata_cara_pemesanan; 
  
   $('#dlgmeta').dialog('open')  
 
 
}