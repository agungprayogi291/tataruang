 $(document).ready(function() {
       //======================================================================================== 
       //validasi gambar yang di pilih
        var input = $('#gambar');  
        $(input).change(function(){
               var file = this.files[0];
                var imagefile = file.type; 
				 var imageSize = file.size; 
                 if (ukuranfilemelebihibatas(imageSize)){ 
                    document.getElementById('file_input').value=null;
                     return false 
                } 
                 //-------------------------------------------- 
                var match= ["image/jpeg","image/png","image/jpg"];
                if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
                { 
                       document.getElementById('gambar').value=null;
                        $.messager.alert('Peringatan','Format File tidak dapat di upload','error');
                        return false  
                }
                 
           });  
       //---------------------------------------------------------------------------------------------- 
        $('#bttambahdata').click(function(event) {  
              var judul=document.getElementById('judul').value;    
              var resume=document.getElementById('resume').value; 
              var isidata=CKEDITOR.instances.isidata.getData();      
            if ((judul=="")||(resume=="")||(isidata="")){
                $.messager.alert('Peringatan','Data ada yang belum di isikan','error'); 
                return  false
            }   
                var form = $('#frdatainformasi'); 
                CKupdate();
                $(form).submit();  
                //simpandatatambahinformasi(event);   
        });   
        
        $('#frdatainformasi').submit(function(event) {  
            event.preventDefault();   
            //var kode=document.getElementById('kdmetaedit').value;
            $.ajax({
                    type: "POST",                     
                    url: '../modul/datainformasi.php?aksi=simpan',
                    data: new FormData(this),  
                    contentType: false,
                    cache: false,             
                    processData:false, 
                    dataType:'json',
                     success: function(data) {
                                    if (data.hasil=="ok"){  
                                            $.messager.alert('Informasi','Data berhasil disimpan');  
                                            clearform();
                                             
                                      }else{
                                            $.messager.alert('Peringatan',data.isierror,'error');
                                      }
                               }
                    });  
            });   
      //======================================================================================== 
         //==========upload image referensi=========          
        var input = $('#file_input');
        var form = $('#image-upload');
        $(input).change(function(){
               var file = this.files[0];
                var imagefile = file.type; 
                var imageSize = file.size; 
                 //--------------------------------------------
                if (ukuranfilemelebihibatas(imageSize)){ 
                    document.getElementById('file_input').value=null;
                    return false 
                } 
                  //--------------------------------------------
                var match= ["image/jpeg","image/png","image/jpg"];
                if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
                {  
                        document.getElementById('file_input').value=null;
                        $.messager.alert('Peringatan','Format File tidak dapat di upload','error');
                        return false 
                } 
                  
                $(form).submit(); 
                
           }); 
           
           
  
        $('#image-upload').submit(function(event) { 
            event.preventDefault();  
            $.ajax({
			type: 'POST',		 
            url:"../modul/upload.php",
			data: new FormData(this),  
			contentType: false,
			cache: false,             
			processData:false, 
            dataType:'json',
                }).success(function(data){   
                        if (data.status!="1"){ 
                           $.messager.alert('Peringatan','Upload data Gagal','error');
                            }
                        });  
            });         
       //=============================================================================  
         $('#btsimpanslideberanda').click(function() {  
         updateslideberanda();  
        }); 
 });
 
 
 function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
} 
 
 function updateAllMessageForms(){
         for (instance in CKEDITOR.instances) {
             CKEDITOR.instances[instance].updateElement();
         }
    }    
 function clearform(){
      document.getElementById('judul').value="";
      document.getElementById('resume').value="";    
      CKEDITOR.instances['isidata'].setData('');
 } 
 function updateslideberanda(){  
     //------------------------------------------------------
     var kdlevel=document.getElementById('kdlevelE').value;
 
     if (kdlevel=="User"){
         $.messager.alert('Peringatan','Anda tidak memiliki hak untuk merubah Slide beranda','warning'); 
         return false;
     }
     //------------------------------------------------------
     	var ids = [];
			var rows = $('#dgberita').datagrid('getSelections');
			for(var i=0; i<rows.length; i++){
				ids.push(rows[i].kddatainformasi);
			} 
     //=================================================================================                  
    var kdslideberanda = ids.join();
    if (kdslideberanda ==""){
        $.messager.confirm('Peringatan','Slide belum ada yang di pilih','warning'); 
        return false;
    }
    //=================================================================================
 
    var isidata="kddata="+kdslideberanda; 
     $.messager.confirm('Confirm','Apakah data akan di simpan?',function(r){
        if (r){    
            $.ajax({
                     type: "POST", 
                     url: '../modul/datainformasi.php?aksi=simpanslide',
                     data:isidata, 
                     dataType:'json',
                     success: function(data) {
                                   if(data.hasil=="ok"){   
                                            pesanberhasilsimpan();     
                                   } 
                               }
            });               
        }
    }); 
    
    
    
    
     
 } 
 function ukuranfilemelebihibatas(ukuran){ 
        var hasil=0;
                 if ((ukuran>100000000)){
                     $.messager.alert('Peringatan','Ukuran File maxsimal adalah 100Mb','error'); 
                     hasil=1;
                 }
              return hasil; 
 }