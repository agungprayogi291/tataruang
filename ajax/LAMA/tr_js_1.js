   var fields = [];
   
   var LyrPola; 
   var Lyrkoof; 
   var Lyrketing; 
   var LyrGabung; 
   
   
 $(document).ready(function() {  
      
     //LoadLayerPola();
     // LoadLayerKoofbangunan("1");
     // LoadLayerKoofbangunan("2");
     LoadMapGabungan();
 });
 
function LoadLayerPola(){ 
       //fields = [ "pola_ruang","luas_ha"];
        fields = [ "gid"];
	$.ajax("../modul/tata_ruang_modul.php", {
		data: {			
			table: "pola_ruang",
			fields: fields,
			kondisi: ''
		},
		success: function(data){
			mapData(data,'pola');
                        
		}
	})
      
 }
function LoadLayerKoofbangunan(dataN){ 
     var Tabel;
     var Jenis;
     if (dataN=='1'){ 
          fields = [ "pola_ruang","kategori","ket_kmb","gid"];
          Tabel="ketinggian_lantai_bangunan";
          Jenis='tinggi';
     }
     if (dataN=='2'){   
          fields = [ "pola_ruang","kategori","ket_kdb"];
          Tabel="koefisien_dasar_bangunan";
          Jenis="koof";
     }   
	$.ajax("../modul/tata_ruang_modul.php", {
		data: {			
			table: Tabel, 
			fields: fields,
			kondisi: ''
		},
		success: function(data){
			mapData(data,Jenis); 
		}
	}) 
 }
function ClickPolaruang(){ 
    var checkBox = document.getElementById("pola"); 
    if (checkBox.checked == true){  
//          map.addLayer(LyrPola);
            LoadLayerPola();
 	  $("#p_pola").show();
             
    }else{
        map.removeLayer(LyrPola);  
        $("#p_pola").hide(); 
    }
};   
function ClickInfo(){ 
    var checkBox = document.getElementById("xxx"); 
    //alert(checkBox);
    if (checkBox.checked == true){  
          map.addLayer(LyrGabung);
           LyrGabung.bringToFront();
	   
    }else{
        map.removeLayer(LyrGabung);  
        
    }
};   
function ClickKoofbangunan(dataN){ 
      var checkBox;
     if (dataN=='1'){   checkBox = document.getElementById("tinggi");  } 
     if (dataN=='2'){    checkBox = document.getElementById("koef");  }
         
      
     
     if (checkBox.checked == true){   
            if (dataN=='1'){ 
                map.addLayer(Lyrketing);
                $("#p_tinggi").show();
            }
            if (dataN=='2'){ 
                 map.addLayer(Lyrkoof);
                $("#p_kooef").show();
            }  
    }else{ 
            if (dataN=='1'){  
                map.removeLayer(Lyrketing); 
                $("#p_tinggi").hide();
            }
            if (dataN=='2'){ 
                map.removeLayer(Lyrkoof); 
                $("#p_kooef").hide();
            }
    }
};  
function mapData(data,Jenis ){ 
	 var geojson = { 
		"type": "FeatureCollection",
		"features": []
	}; 
        // alert(data);
	var dataArray = data.split(", ;");
	dataArray.pop();
     
	dataArray.forEach(function(d){
		d = d.split(", "); //split the data up into individual attribute values and the geometry
                
                
                var GeoM=d[fields.length].trim();
                         
                
               // if (d[fields.length]!=""){
                if (GeoM!=""){
                    var feature = {
                            "type": "Feature",
                            "properties": {}, //properties object container
                           "geometry": JSON.parse(d[fields.length]) //parse geometry
                    };

                    for (var i=0; i<fields.length; i++){ 
                            feature.properties[fields[i]] = d[i];
                    };

                    //add feature names to autocomplete list
//                    if ($.inArray(feature.properties.featname, autocomplete) == -1){
//                            autocomplete.push(feature.properties.featname);
//                    }; 
                    geojson.features.push(feature);
            }
            //else{ alert(d[fields.length])}
	}); 
     

	 var mapDataLayer = L.geoJson([geojson], { 
            style: function (feature) {  
                        var warna= styleCOlor(feature.properties.pola_ruang);
                        if (Jenis=="koof" ){ warna=  styleCOlor(feature.properties.ket_kdb);}
                        if (Jenis=="tinggi" ){ warna=  styleCOlor(feature.properties.ket_kmb);} 
                        return  warna;
		},
 
		onEachFeature: function (feature, layer) {
			var html = "";
			for (prop in feature.properties){
				html += prop+": "+feature.properties[prop]+"<br>";
			};
	        layer.bindPopup(html);
                }
	});
	//}).addTo(map);
        if (Jenis='pola'){ LyrPola=mapDataLayer;}
        if (Jenis='koof'){ Lyrkoof=mapDataLayer;  }
        if (Jenis='tinggi'){ Lyrketing=mapDataLayer;  } 
};

function LoadMapGabungan(){
        fields = [ "gid"];
	$.ajax("../modul/tata_ruang_modul.php", {
		data: {			
			table: "pola_ruang_gabung",
			fields: fields,
			kondisi: ''
		},
		success: function(data){
			mapGabungan(data);
                       
		}
	})
}
function mapGabungan(data){ 
	 var geojson = { 
		"type": "FeatureCollection",
		"features": []
	}; 
        
	var dataArray = data.split(", ;");
	dataArray.pop(); 
	dataArray.forEach(function(d){
		d = d.split(", "); //split the data up into individual attribute values and the geometry
 
                if (d[fields.length]!="0"){
                    var feature = {
                            "type": "Feature",
                            "properties": {}, //properties object container
                            "geometry": JSON.parse(d[fields.length]) //parse geometry
                    };

                    for (var i=0; i<fields.length; i++){ 
                            feature.properties[fields[i]] = d[i];
                    }; 
                    geojson.features.push(feature);
            }
	}); 
     

	   LyrGabung = L.geoJson([geojson], { 
            style: function () { 
                         var warna ={
                                    weight: 0,
                                    opacity: 0,
                                    //color: 'white',
                                     color: 'red',
                                    dashArray: 0,
                                    fillOpacity: 0,
                                    //fillColor: getColor(feature.properties.density)
                                    //fillColor: getColor(feature)
                                    //fillColor: 'red'
                                    }; 
                           return  warna;
		},
 
                    onEachFeature: function(feature,layer){
                    layer.on('click', function(e){  
                          BukaDataPola(feature.properties.gid); 
                    });
                } 
		}).addTo(map);
        
    
};
function BukaDataPola(Kode){
    
      $.ajax({                    
                    type: "POST", 
                    url: '../modul/rtrw_modul.php',
                    data:  {kode:Kode},                       
                    //contentType: false,
                    //cache: false,             
                    //processData:false,  
                    dataType:'json', 
                     success: function(data) { 
                          $("#op_str_coordinate").html('show'); 
                            $("#op_str_kecamatan").html('camat');
                            $("#op_str_kelurahan").html('lurah');
                            $("#op_str_kdblokzonasi").html('show');
                            $("#op_str_polaruang").html('pola');
                            $("#op_str_kawasan").html('kawasan');
                            $("#op_str_keteranganpr").html('ket');
                            $("#op_str_kdb").html('show');
                            $("#op_str_kmb").html('show');
                          $("#myModal").modal({backdrop: false});
                    }
                }); 
    
       $("#myModal").modal('show');
     //alert('xxxx'+Kode);
}
   