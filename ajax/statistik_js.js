  $(document).ready(function() {
        // LoadChartIMB();
  }); 
 
 function LoadChartIMB(){ 
     var myRadio = $("input[name=chart]");
     var nilaiX = myRadio.filter(":checked").val();  
        
          $.ajax({                    
                    type: "POST", 
                    url: "modul/statistik_modul.php",
                    data:  { nilai:nilaiX    },                       
                    //contentType: false,
                    //cache: false,             
                    //processData:false,  
                    dataType:'json', 
                     success: function(dataX) { 
                         BuatChartBaru(dataX,nilaiX); 
                          //TesChartAjax(dataX);
                    }
                }); 
                     
 
}; 
 
              

 function BuatChartBaru(data,Ke){
     if (Ke==1){BuatChartKota(data);}
     if (Ke==2){BuatChartKecamatan(data);}
     if (Ke==3){BuatChartKelurahan(data);}
 }
 function BuatChartKota(Xdata) { 
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Kriteria');
        data.addColumn('number', 'Jumlah');
        
   
      for (var i = 0; i < Xdata.length; i++){ 
           var kriteria=Xdata[i].criteria;
           var jum=parseFloat(Xdata[i].jum); 
           data.addRow([kriteria, jum ]);     
     } 
           var options = {
              chart: {
                    title: 'Grafik IMB Kota',
                    subtitle: ''
                    },
                    bars: 'vertical',
             vAxis: {format: 'decimal'},
                    height: 400,
                    colors: ['#f9093e', '#0967f9', '#42f909'],
            axes: {
                    x: {
                     0: {side: 'bottom'}
                     }
            }
      };
      //var chart = new google.charts.Bar(document.getElementById('chartbar'));
       var chart = new google.visualization.PieChart(document.getElementById('chartbar'));
      chart.draw(data, options);
      
        //var chart = new google.charts.Bar(document.getElementById('chartbar')); 
        //chart.draw(data, google.charts.Bar.convertOptions(options));
      
       
  
 }   
 function BuatChartKecamatan(Xdata) { 
     
      var data = new google.visualization.DataTable();
        data.addColumn('string', 'Kecamatan');
        data.addColumn('number', 'IMB');
        data.addColumn('number', 'Tidak IMB');
        data.addColumn('number', 'IMB Baru');
        
        for (var i = 0; i < Xdata.length; i++){ 
           var kec=Xdata[i].kecamatan;
           var imb=parseFloat(Xdata[i].imb); 
           var tidakimb=parseFloat(Xdata[i].tidakimb); 
           var imbbaru=parseFloat(Xdata[i].imbbaru); 
           data.addRow([kec, imb,tidakimb,imbbaru]);     
     } 
     
       
        
         var options = {
          title : 'Data Statistik IMB',
          vAxis: {title: 'Jumlah'},
          hAxis: {title: 'Kecamatan'},
           colors: ['#42f909', '#f9093e', '#0967f9'],
          seriesType: 'bars',
          series: {5: {type: 'line'}}
        };
      
      var chart = new google.charts.Bar(document.getElementById('chartbar'));
      chart.draw(data, options);
 }
 function BuatChartKelurahan(Xdata) { 
     
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Kelurahan');
        data.addColumn('number', 'IMB');
        data.addColumn('number', 'Tidak IMB');
        data.addColumn('number', 'IMB Baru');
        
        for (var i = 0; i < Xdata.length; i++){ 
           var kec=Xdata[i].kelurahan;
           var imb=parseFloat(Xdata[i].imb); 
           var tidakimb=parseFloat(Xdata[i].tidakimb); 
           var imbbaru=parseFloat(Xdata[i].imbbaru); 
           data.addRow([kec, imb,tidakimb,imbbaru]);     
     } 
        
 
         var options = {
          title : 'Data Statistik IMB', isStacked:'percent',
          vAxis: {title: 'Jumlah'},
          hAxis: {title: 'Kelurahan'},
           colors: ['#42f909', '#f9093e', '#0967f9'],
          seriesType: 'bars',
          series: {5: {type: 'line'}},
           visibleInLegend: true
        };
      
      var chart = new google.charts.Bar(document.getElementById('chartbar'));
      chart.draw(data, options);
 }   
 function BuatChartKecamatan2(Xdata) { 
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Kecamatan');
        
        data.addColumn('number', 'Jumlah');
         data.addColumn('string', 'Kriteria');
        
   
      for (var i = 0; i < Xdata.length; i++){ 
           var nama=Xdata.hasil[i].nama;
           var kriteria=Xdata.hasil[i].criteria;
           var jum=parseFloat(Xdata.hasil[i].jum); 
           data.addRow([nama,jum,kriteria ]);     
     } 
           var options = {
              chart: {
                    title: 'Grafik IMB Kecamatan',
                    subtitle: ''
                    },
                    //width: 900,
                    //height: 500,
            axes: {
                    x: {
                     0: {side: 'bottom'}
                     }
            }
      };
      var chart = new google.charts.Bar(document.getElementById('chartbar'));
      chart.draw(data, options);
 }

 function BuatChartKelurahan2(Xdata) { 
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Kecamatan');
        data.addColumn('string', 'Kriteria');
        data.addColumn('number', 'Jumlah');
        
   
      for (var i = 0; i < Xdata.length; i++){ 
           var nama=Xdata[i].nama;
           var kriteria=Xdata[i].criteria;
           var jum=parseFloat(Xdata[i].jum); 
           data.addRow([nama,kriteria, jum ]);     
     } 
           var options = {
              chart: {
                    title: 'Grafik IMB Kelurahan',
                    subtitle: ''
                    },
                    //width: 900,
                    //height: 500,
            axes: {
                    x: {
                     0: {side: 'bottom'}
                     }
            }
      };
      var chart = new google.charts.Bar(document.getElementById('chartbar'));
      chart.draw(data, options);
 } 
 
  
function drawChart( data) {
 
     var data = google.visualization.arrayToDataTable([
          ['Kecamatan', 'IMB', 'Tidak IMB', 'Baru', 'Pengajuan'],
          ['kecamatan 1',  165,      938,         522,             998,],
          ['kecamatan 2',  135,      1120,        599,             1268],
          ['kecamatan 3',  157,      1167,        587,             807],
          ['kecamatan  4',  139,      1110,        615,             968],
          ['kecamatan  5',  136,      691,         629,             1026]
        ]);
        
        
//   var data = google.visualization.arrayToDataTable([
//           ['Year', 'Sales', 'Expenses', 'Profit'],
//          ['2014', 1000, 400, 200],
//          ['2015', 1170, 460, 250],
//          ['2016', 660, 1120, 300],
//          ['2017', 1030, 540, 350]
//        ]);
//
 

        var options = {
          title : 'Data Statistik IMB',
          vAxis: {title: 'Jumlah'},
          hAxis: {title: 'Kecamatan'},
          seriesType: 'bars',
          series: {5: {type: 'line'}}
        };
        
        //var chart = new google.charts.Bar(document.getElementById('chartbar'));

        //chart.draw(data, google.charts.Bar.convertOptions(options));
     
   
  
} 
function TesChartAjax(dataX){
     
     var data = google.visualization.arrayToDataTable(dataX);
     
//      data.addColumn('string', 'Kecamatan');
//        data.addColumn('string', 'Kriteria');
//        data.addColumn('number', 'Jumlah');
//        
//        
//         for (var i = 0; i < dataX.length; i++){ 
//           var nama='kecamatan '+i;
//           var kriteria='criteria '+i;
//           //var jum=parseFloat(Xdata[i].jum); 
//            var jum=parseFloat(100); 
//           //var jum= 100 ; 
//          // data.addRow([[nama,kriteria, jum ]]);     
//           data.addRow([[nama, jum ]]);     
//     } 



 var options = {
          title : 'Monthly Coffee Production by Country',
          vAxis: {title: 'Jumlah'},
          hAxis: {title: 'Kecamatan'},
          seriesType: 'bars',
          //series: {5: {type: 'line'}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chartbar'));
        chart.draw(data, options);
 }
   