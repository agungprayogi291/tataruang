$(document).ready(function() {
    
    $('#bttambahuser').click(function() {  
         $('#inputuser').form('clear');
         $('#FormUser').dialog('open');  
    });
    $('#btnclear').click(function() {
       $('#inputuser').form('clear');
    }); 

    $('#bthapususer').click(function() { 
       hapusdata();
    });
    
    $('#btnsimpan').click(function() {
        simpandatauser("1");
    }); 
    
    $('#btnsimpaneditdata').click(function() {
        simpandatauser("2");
    });  
    
    $('#btnsimpanaturuser').click(function() {
        simpandatauser("3"); 
    });  
    
    
    $('#btubahlevel').click(function() {
        loaddatauseruntukatur(); 
    }); 
    
    $('#btnreset').click(function() {
        resetpasswuser();
    }); 
      
    
});
 
 
function resetpasswuser(){
     var kduser = document.getElementById('kduser').value;     
       $.messager.confirm('Confirm','Apakah password user akan di Reset ?',function(r){
        if (r){ 
               $.ajax({
                     type: "POST",  
                     url: 'modul/user.php?aksi=reset',
                     dataType: 'json',
                     data:'isikunci='+kduser, 
                     dataType:'json',
                     success: function(data) {
                                   if(data.hasil=="ok"){ 
                                        $.messager.confirm('Informasi',"Password user berhasil di reset, Password baru adalah=user",'success');   
                                   }else{
                                        $.messager.alert('Informasi',data.isierror,'error'); 
                                   } 
                               }
                     });
           }  
    });
 
}

function hapusdata(){ 
      var row = $('#dguser').datagrid('getSelected');
      if (row){ 
            var kode = row.kode; 
            }  
      if (kode==null){
        $.messager.alert('Peringatan',"Data user belum di pilih",'warning');   
        return false} 
      
    
        
    $.messager.confirm('Confirm','Apakah data akan di Hapus?',function(r){
        if (r){                
                  $.ajax({
                     type: "POST",  
                     url: 'modul/user.php?aksi=hapus',
                     dataType: 'json',
                     data:'isikunci='+kode, 
                     dataType:'json',
                     success: function(data) {
                                   if(data.hasil=="ok"){ 
                                       tampilkandata(); 
                                   }else{
                                        $.messager.alert('Informasi',data.isierror,'error'); 
                                   } 
                               }
                     });   
      
            }
    });     
}

function tampilkandata(){ 
    var kunci='';
      $.ajax({
      type: "POST",
      url:"modul/user.php?aksi=tampil", 
      dataType:'json',       
      data: 'isikunci='+kunci,
      dataType:'json',
    }).done(function(responseJson)       {
          $('#dguser').datagrid('loadData', responseJson ); 
      });  
}
   
function simpandatauser(aksi){ 
  
   
    var namalengkap=$('#namalengkap').textbox('getValue');
    var usern=$('#namauser').textbox('getValue');
    if (aksi=="3") { 
        var passw="";
        var ulangi="";
     }else{
         var passw=$('#passw').textbox('getValue');
         var ulangi=$('#ulangipassw').textbox('getValue');
     }
    //-----------------------------------------------------------' 
    //var skpd=$('#skpd').textbox('getValue');    
    //var bagian=$('#bagian').combobox('getText');
    
    var bagian=$('#bagian').textbox('getValue');    
    var skpd=$('#skpd').combobox('getText');
    //-----------------------------------------------------------'  
    var jabatan=$('#jabatan').textbox('getValue');     
    var kduser = document.getElementById('kduser').value;    
    //-----------------------------------------------------------'
 

    var level="0";
    if (aksi!="2"){ 
         level=$('#level').combobox('getText');
    }
    
    if (level=="Admin"){level="1";} 
    if (level=="User"){level="0";} 
     
    //-----------------------------------------------------------'  
    $formadayangkosong=0;
    //if ((namalengkap=="")||(usern=="")|| (bagian=="")||(jabatan=="")||(skpd=='')){$formadayangkosong=1;}
    if ((namalengkap=="")||(usern=="")||(skpd=='')){$formadayangkosong=1;}
         
    if ($formadayangkosong==0){
            if (aksi=="1"){ 
                    if ((passw=="")||(ulangi=="")|| (level=="")){$formadayangkosong=1;} 
                }
    }     
    
     if ($formadayangkosong==0){
            if (aksi=="2"){ 
                    if ((passw=="")||(ulangi=="")){$formadayangkosong=1;} 
                }
    } 
    
    if ($formadayangkosong==0){
            if (aksi=="3"){ 
                    if ((level=="")){$formadayangkosong=1;} 
                }
    }  
  
    //-----------------------------------------------------------'
    
//     if ((namalengkap=="")||(usern=="")|| (passw=="")||(ulangi=="")|| (bagian=="")||(jabatan=="")||(skpd=='')|| (level=="")){
//        $.messager.alert('Kesalahan',"Kolom dengan warna merah harus diisikan",'error') ; 
//        return false;
//    }    
  if ($formadayangkosong){
         $.messager.alert('Kesalahan',"Kolom dengan warna merah harus diisikan",'error') ; 
        return false;
    }
//-----------------------------------------------------------'
    if ((aksi=="2") || (aksi=="1")){ 
            if (passw!=ulangi){
                $.messager.alert('Kesalahan',"Pengulangan password salah",'error') ; 
                return false;
            }
    }
//-----------------------------------------------------------'
    var isidata="kduser="+kduser+"&namalengkap="+namalengkap+"&usern="+usern+
                 "&passw="+ulangi+"&bagian="+bagian+"&jabatan="+jabatan+"&skpd="+skpd+
                 "&level="+level+"&aksi="+aksi;
    $.ajax({
                url: 'modul/user.php?aksi=tambah',  
                type: "POST",             
                data: isidata,   
                 dataType:'json',
                success: function(data)    
                {
                 if(data.hasil=="ok"){
                           pesanberhasilsimpan();   
                           
                           if (aksi=="1"){
                               $('#inputuser').form('clear');
                               tampilkandata(); 
                               } 
                           if (aksi=="2"){
                               //$('#inputuser').form('clear');
                               //$('#FormUser').dialog('close');
                              } 
                            if (aksi=="3"){
                               $('#inputuser').form('clear');
                               tampilkandata(); 
                               $('#FormUser').dialog('close');
                              }        
                               }else{
                                    $.messager.alert('Kesalahan',data.hasil,'error') ;  
                               }
                }
                });



}      

function loaddatauseruntukatur()
{
    var row = $('#dguser').datagrid('getSelected');
      if (row){ 
            var kode = row.kode; 
            }  
      if (kode==null){
        $.messager.alert('Peringatan',"Data user belum di pilih",'warning');   
        return false
      } 
                     
        $.ajax({
                     type: "POST",  
                     url: 'modul/user.php?aksi=load',
                     dataType: 'json',
                     data:'isikunci='+kode, 
                     dataType:'json',
                     success: function(data) {
                                   if(data.hasil=="ok"){ 
                                       $('#inputuser').form('clear');  
                                        
                                        $('#namalengkap').textbox('setValue',data.namalengkap);
                                        $('#namauser').textbox('setValue',data.namauserlogin); 
                                        $('#bagian').textbox('setValue',data.bidang);
                                        $('#skpd').combobox('setValue',data.dinas); 
                                        $('#jabatan').textbox('setValue',data.jabatan); 
                                        $('#level').textbox('setValue',data.level); 
                                        
                                         
                                        
                                        document.getElementById('kduser').value=data.kode;    
                                        $('#FormUser').dialog('open');  
                                   }else{
                                        $.messager.alert('Informasi',data.isierror,'error'); 
                                   } 
                               }
                });   
      
             
    
}
 
 
//     tutupall();
//     bukamenuhapus();
//     $('#phapus').panel('open');
// }    
//function bukamenuhapus(){
//    var kode=document.getElementById("kduser2").value; 
//            $.ajax({
//                 url: 'modul/user.php?aksi=tampildatauser',  
//                 type: "POST",              
//                 data: "isidata="+kode,   
//                 dataType:'json', 
//                 }).done(function(responseJson) 
//                   {
//                       $('#griduser').datagrid('loadData', responseJson ); 
//                  });  
//
//} 
  
//function tomboleditpassword(){ 
//    var kode=document.getElementById("kduser2").value; 
//    var namalengkap=$('#namalengkap2').textbox('getValue');
//    var usern=$('#usern2').textbox('getValue');
//    var passw=$('#passw2').textbox('getValue');
//    var ulangi=$('#ulangi2').textbox('getValue'); 
//    var bagian=$('#bagian2').combobox('getText');
//    var jabatan=$('#jabatan2').textbox('getValue'); 
//    var level=$('#level2').textbox('getValue'); 
//
//    if ((namalengkap=="")||(usern=="")|| (passw=="")||(ulangi=="")|| (bagian=="")||(jabatan=="")|| (level=="")){
//        $.messager.alert('Kesalahan',"Kolom dengan warna merah harus diisikan",'error') ; 
//        return false;
//    }
//
//
//
//
//   if (passw!=ulangi){
//       $.messager.alert('Kesalahan',"Pengulangan password salah",'error') ; 
//       return false;
//   }
//
//    var isidata="kduser="+kode+"&namalengkap="+namalengkap+"&usern="+usern+"&passw="+ulangi+"&bagian="+bagian+"&jabatan="+jabatan+"&level="+level;
//    $.ajax({
//                url: 'modul/user.php?aksi=editpassw',  
//                type: "POST",              
//                data: isidata,   
//                 dataType:'json',
//                success: function(data)    
//                {
//                 if(data.hasil=="ok"){ 
//                                     pesanberhasilsimpan();  
//                                     //$('#formedituser').form('clear');    
//                               }else{
//                                    $.messager.alert('Kesalahan',data.hasil,'error') ;  
//                               }
//                }
//                });
//
//
//}    
 
function formatlevel(val,row){ 
    var hasil="User";
    if (val=="1"){hasil="Admin";}
      //var  hasil = FormatuangJS2(val);
      return hasil; 
}  
     
