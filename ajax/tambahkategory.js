 var EditData=0;
 $(document).ready(function() {
 $('#tbtambah').click(function() { 
     EditData=0;
     $('#inputitem').form('clear');
     $('#FormInput').dialog('open'); 
 });
 
 $('#tbedit').click(function() {
     EditData=1; 
    loaddatauntukedit();
 });
 $('#tbhapus').click(function() {
    hapusdata();
 });
 
 });

 
 
function simpandata(){ 
      var nama=$('#nama').textbox('getValue');
      var urutan=$('#urutan').textbox('getValue');
      var kode=document.getElementById('kode').value;
      var kduser=document.getElementById('kodeuserL').value;
    //------------------------------------------------------------ 
        if(!isNaN(urutan))
        {
           //do some thing if it's a number
        }else{ 
         $.messager.alert('Error','Urutan harus berupa angka','error'); 
         return  false 
        } 
    //------------------------------------------------------------    
    if (nama==""){
         $.messager.alert('Error','Kolom berwana merah harus di isikan','error'); 
        return  false
    }   
    //------------------------------------------------------------
    if (urutan==""){urutan="0";}
    //------------------------------------------------------------  
      var isidata ='kode='+ kode +
                   '&nama='+ nama +
                   '&kduser='+ kduser +
                   '&urutan='+urutan; 
    //------------------------------------------------------------   
      $.messager.confirm('Confirm','Apakah data akan di simpan?',function(r){
        if (r){    
            $.ajax({
                     type: "POST", 
                     url: 'modul/tambahkategory.php?aksi=simpan',
                     data:isidata, 
                     dataType:'json',
                     success: function(data) {
                                   if(data.hasil=="ok"){ 
                                        tampilkandata();   
                                        if(EditData){ 
                                            $('#FormInput').dialog('close');
                                        }else{
                                         $('#inputitem').form('clear');
                                         pesanberhasilsimpan();    
                                        }
                                   } 
                               }
            });               
        }
    }); 
 }
 
function hapusdata(){ 
      var row = $('#dg').datagrid('getSelected');
      if (row){ 
            var kode = row.kdkategorydata; 
            var kdinstansiinput=row.kdinstansi; 
            }  
      if (kode==null){return false} 
      
      //--------------------------------------------------------------
    var kdinstansilogin=document.getElementById('kodeinstansiL').value;    
    if(kdinstansilogin!=kdinstansiinput){ 
        $.messager.alert('Peringatan','Anda tidak memiliki hak untuk menghapus data ini','warning'); 
        return false;
    }
    //--------------------------------------------------------------
      
       
    $.messager.confirm('Confirm','Apakah data akan di Hapus?',function(r){
        if (r){   
             //url: 'modul/itemuji.php?aksi=hapus&isikunci='+kode,
                  $.ajax({
                     type: "POST",  
                     url: 'modul/tambahkategory.php?aksi=hapus',
                     dataType: 'json',
                     data:'isikunci='+kode, 
                     dataType:'json',
                     success: function(data) {
                                   if(data.hasil=="ok"){ 
                                       tampilkandata(); 
                                   }else{
                                        $.messager.alert('Informasi',data.isierror,'error'); 
                                   } 
                               }
                     });   
      
            }
    });     
}

function tampilkandata(){ 
    var kunci='';
      $.ajax({
      type: "POST",
      url:"modul/tambahkategory.php?aksi=tampil", 
      dataType:'json',       
      data: 'isikunci='+kunci,
      dataType:'json',
    }).done(function(responseJson)       {
          $('#dg').datagrid('loadData', responseJson ); 
      });  
}

function loaddatauntukedit(){
    var row = $('#dg').datagrid('getSelected');
    if (row){ 
            var kode=row.kdkategorydata; 
            var kdinstansiinput=row.kdinstansi; 
            }  
    //--------------------------------------------------------------
    if (kode==null){return false;}  
    //--------------------------------------------------------------
    var kdinstansilogin=document.getElementById('kodeinstansiL').value;    
    if(kdinstansilogin!=kdinstansiinput){ 
        $.messager.alert('Peringatan','Anda tidak memiliki hak untuk merubah data ini','warning'); 
        return false;
    }
    //--------------------------------------------------------------
    
    var isidata = "kode="+kode;
    $.ajax({
                     type: "POST", 
                     url: 'modul/tambahkategory.php?aksi=edit',
                     data:isidata, 
                     dataType:'json',
                     success: function(data) {
                                   if(data.hasil=="ok"){ 
                                        $('#inputitem').form('clear');  
                                        $('#nama').textbox('setValue',data.nama);
                                        $('#urutan').textbox('setValue',data.urutan);
                                        document.getElementById('kode').value=data.kode; 
                                        $('#FormInput').dialog('open');  
                                   }else{
                                       alert('tidak ketemu');
                                   } 
                               }
            });  
            
            
        
}