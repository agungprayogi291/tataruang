 $(document).ready(function() {
        $('#bttambahdata').click(function(event) {  
         simpandatatambahinformasi(event);  
        });
         $('#btsimpanslideberanda').click(function() {  
         updateslideberanda();  
        });
 
         
         
        
        var input = $('#file_input');
        var form = $('#image-upload');
        $(input).change(function(){
               var file = this.files[0];
                var imagefile = file.type; 
                var imageSize = file.size; 
                 //--------------------------------------------
                if (ukuranfilemelebihibatas(imageSize)){ 
                    document.getElementById('file_input').value=null;
                    return false 
                } 
                  //--------------------------------------------
                var match= ["image/jpeg","image/png","image/jpg"];
                if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
                {  
                        document.getElementById('file_input').value=null;
                        $.messager.alert('Peringatan','Format File tidak dapat di upload','error');
                        return false 
                } 
                  
                $(form).submit(); 
                
           });                   
         //==========upload image referensi=========
         $('#image-upload').submit(function(event) { 
            event.preventDefault();  
            $.ajax({
		type: 'POST',		 
                url:"../modul/upload.php",
		data: new FormData(this),  
		contentType: false,
		cache: false,             
		processData:false, 
                dataType:'json',
                }).success(function(data){   
                        if (data.status!="1"){ 
                           $.messager.alert('Peringatan','Upload data Gagal','error');
                            }
                        });  
            });        
         
     //=============================================================================
        var input = $('#gambar');        
       
        $(input).change(function(){
               var file = this.files[0];
                var imagefile = file.type; 
				 var imageSize = file.size; 
                 if (ukuranfilemelebihibatas(imageSize)){ 
                    document.getElementById('file_input').value=null;
                     return false 
                } 
                 //-------------------------------------------- 
                var match= ["image/jpeg","image/png","image/jpg"];
                if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
                { 
                       document.getElementById('gambar').value=null;
                        $.messager.alert('Peringatan','Format File tidak dapat di upload','error');
                        return false  
                }
                 
           });
           
          //==========upload image referensi=========
         $('#formgambar').submit(function(event) { 
            event.preventDefault();   
            $.ajax({
                    type: "POST", 
                    url: '../modul/datainformasi.php?aksi=uploadgambar',
                    data: new FormData(this),  
                    contentType: false,
                    cache: false,             
                    processData:false, 
                    dataType:'json',
                     success: function(data) {
                                   if(data.status!="1"){  
                                          $.messager.alert('Peringatan','File Gambar gagal di upload','error');
                                   } else{
                                        $('#formgambar').trigger('reset');  
                                        
                                   }
                               }
                    });  
            });        
         
 });
    
 function simpandatatambahinformasi(event){ 
              event.preventDefault();  
              var kddata= document.getElementById('iddata').value;
              var kdkategory=document.getElementById('kode').value;              
              var judul=document.getElementById('judul').value;    
              var resume=document.getElementById('resume').value; 
              var isidata=CKEDITOR.instances.isidata.getData();     
              
               isidata = isidata.replace(/&nbsp;/g,''); 
               isidata = isidata.replace(/&amp;/g,''); 
                 isidata = isidata.replace("&amp;&amp;&amp;", "&&&")
                 
                
                
                 
        
        
              var kduser =document.getElementById('kodeuserL').value;  
              
              var gambar1=$('#gambar[type=file]').val().replace(/.*(\/|\\)/, ''); 
             var gambar2=gambar1;
		 
            
            
    
    //------------------------------------------------------------    
    if ((judul=="")||(resume=="")||(data="")){
         $.messager.alert('Peringatan','Data ada yang belum di isikan','error'); 
         return  false
    }        
    //updateAllMessageForms();
    //------------------------------------------------------------  
      var isidata ='judul='+ judul +
                    '&resume='+ resume +
                    '&isidata='+isidata+
                    '&gambar1='+gambar1+
                    '&gambar2='+gambar2+
                    '&kdkategory='+kdkategory+
                    '&kodeuser='+kduser+
                    '&kdkdata='+kddata;                  
    //------------------------------------------------------------   
      $.messager.confirm('Confirm','Apakah data akan di simpan?',function(r){
        if (r){    
           
             $.ajax({
                     type: "POST", 
                     url: '../modul/datainformasi.php?aksi=simpan',
                     data:isidata, 
                     dataType:'json',
                     success: function(data) {
                                   if(data.hasil=="ok"){  
                                       var gambar1=document.getElementById('gambar').value; 
                                       if (gambar1!=""){
                                            document.getElementById('kdgambar').value = data.kodeinput;
                                            var form = $('#formgambar');
                                            $(form).submit();   
                                        }
                                        document.getElementById('iddata').value="";
                                        clearform();
                                        pesanberhasilsimpan();      
                                   } 
                               }
            });   
            
        }    
    }); 
 }
 
 function updateAllMessageForms(){
         for (instance in CKEDITOR.instances) {
             CKEDITOR.instances[instance].updateElement();
         }
    }    
 function clearform(){
      document.getElementById('judul').value="";
      document.getElementById('resume').value="";    
      CKEDITOR.instances['isidata'].setData('');
 } 
 function updateslideberanda(){  
     //------------------------------------------------------
     var kdlevel=document.getElementById('kdlevelE').value;
 
     if (kdlevel=="User"){
         $.messager.alert('Peringatan','Anda tidak memiliki hak untuk merubah Slide beranda','warning'); 
         return false;
     }
     //------------------------------------------------------
     	var ids = [];
			var rows = $('#dgberita').datagrid('getSelections');
			for(var i=0; i<rows.length; i++){
				ids.push(rows[i].kddatainformasi);
			} 
     //=================================================================================                  
    var kdslideberanda = ids.join();
    if (kdslideberanda ==""){
        $.messager.confirm('Peringatan','Slide belum ada yang di pilih','warning'); 
        return false;
    }
    //=================================================================================
 
    var isidata="kddata="+kdslideberanda; 
     $.messager.confirm('Confirm','Apakah data akan di simpan?',function(r){
        if (r){    
            $.ajax({
                     type: "POST", 
                     url: '../modul/datainformasi.php?aksi=simpanslide',
                     data:isidata, 
                     dataType:'json',
                     success: function(data) {
                                   if(data.hasil=="ok"){   
                                            pesanberhasilsimpan();     
                                   } 
                               }
            });               
        }
    }); 
    
    
    
    
     
 }
 
 function ukuranfilemelebihibatas(ukuran){ 
        var hasil=0;
                 if ((ukuran>100000000)){
                     $.messager.alert('Peringatan','Ukuran File maxsimal adalah 100Mb','error'); 
                     hasil=1;
                 }
              return hasil; 
 }